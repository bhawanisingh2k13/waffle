package com.hocrox.belgian.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.hocrox.belgian.config.Constants;
import lombok.*;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.validator.constraints.Email;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.time.Instant;
import java.util.Collection;
import java.util.Objects;

/**
 * A user.
 */
@Entity
@Table(name = "user")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Builder
@Getter
@Setter
//@RequiredArgsConstructor
@ToString(exclude = {"password", "resetKey", "resetDate", "role"})
@NoArgsConstructor
@AllArgsConstructor
public class User extends AbstractAuditingEntity implements Serializable, UserDetails {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Pattern(regexp = Constants.LOGIN_REGEX)
    @Size(min = 6, max = 50)
    @Column(length = 50, unique = true, nullable = false, updatable = false)
    private String login;

    @JsonIgnore
    @NotNull
    @Size(min = 60, max = 60)
    @Column(name = "password_hash", length = 60)
    private String password;

    @Size(max = 50)
    @Column(name = "first_name", length = 50)
    private String firstName;

    @Size(max = 50)
    @Column(name = "last_name", length = 50)
    private String lastName;

    @Email
    @Size(min = 5, max = 100)
    @Column(length = 100, unique = true)
    @Builder.Default
    private String email;

    @NotNull
    @Column(nullable = false)
    @Builder.Default
    private boolean activated = false;

    @Size(min = 2, max = 6)
    @Column(name = "lang_key", length = 6)
    private String langKey;

    @Size(max = 256)
    @Column(name = "image_url", length = 256)
    private String imageUrl;

    @Size(max = 20)
    @Column(name = "activation_key", length = 20)
    @JsonIgnore
    private String activationKey;

    @Size(max = 20)
    @Column(name = "reset_key", length = 20)
    @JsonIgnore
    private String resetKey;

    @Column(name = "reset_date")
    @Builder.Default
    private Instant resetDate = null;

    @Getter
    @Setter
    @Column(name = "enabled", nullable = false)
    @Builder.Default
    private boolean enabled = false;

    @Getter
    @Setter
    @Column(name = "account_non_expired")
    @Builder.Default
    private boolean accountNonExpired = true;

    @Getter
    @Setter
    @Column(name = "account_non_locked")
    @Builder.Default
    private boolean accountNonLocked = true;

    @Getter
    @Setter
    @Column(name = "credential_non_expired")
    @Builder.Default
    private boolean credentialsNonExpired = true;

//    @JsonIgnore
//    @ManyToMany
//    @JoinTable(
//        name = "user_authority",
//        joinColumns = {@JoinColumn(name = "user_id", referencedColumnName = "id")},
//        inverseJoinColumns = {@JoinColumn(name = "authority_name", referencedColumnName = "name")})
//    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
//    @BatchSize(size = 20)
//    private Set<Authority> authorities = new HashSet<>();

    @ManyToOne
    @JoinColumn(name = "role_id")
    private Role role;

    public boolean getActivated() {
        return activated;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return role.getAuthorities();
    }

    @Override
    public String getUsername() {
        return login;
    }

    public String getFullName() {
        if(StringUtils.isBlank(firstName)) return lastName;
        if(StringUtils.isBlank(lastName)) return firstName;
        return firstName + " " + lastName;
    }
}
