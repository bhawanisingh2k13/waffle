package com.hocrox.belgian.domain;

import com.hocrox.belgian.domain.enumeration.NotificationType;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Objects;

/**
 * A WaffleNotification.
 */
@Entity
@Table(name = "waffle_notification")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Getter
@Setter
@ToString
public class WaffleNotification implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "noti_body")
    private String notiBody;

    @Column(name = "title")
    private String title;

    @Enumerated(EnumType.STRING)
    @Column(name = "jhi_type")
    private NotificationType type;

    @Column(name = "image")
    private String image;

    @Column(name = "noti_id")
    private String notiId;

    @Column(name = "ref_id")
    private long refId = 0l;

    @Column(name = "background_noti")
    private boolean background = false;

    @Column(name = "app_version")
    private long apv = -1;

    @Column(name = "app_version_target")
    private long apvt = 0;

    @ManyToOne(optional = false)
    @NotNull
    private WaffleStore waffleStore;


    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        WaffleNotification waffleNotification = (WaffleNotification) o;
        if (waffleNotification.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), waffleNotification.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }
}
