package com.hocrox.belgian.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.Objects;

/**
 * A UserAddress.
 */
@Entity
@Table(name = "user_address")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class UserAddress implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "street")
    private String street;

    @Column(name = "formatted_address")
    private String formattedAddress;

    @Column(name = "flat")
    private String flat;

    @Column(name = "lat")
    private Double lat;

    @Column(name = "lng")
    private Double lng;

    @Column(name = "city")
    private String city;

    @Column(name = "state")
    private String state;

    @Column(name = "country")
    private String country;

    @Column(name = "from_location")
    private Boolean fromLocation = false;

    @ManyToOne
    @NotNull
    @JsonIgnore
    private Profile profile;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getStreet() {
        return street;
    }

    public UserAddress street(String street) {
        this.street = street;
        return this;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getFormattedAddress() {
        return formattedAddress;
    }

    public UserAddress formattedAddress(String formattedAddress) {
        this.formattedAddress = formattedAddress;
        return this;
    }

    public void setFormattedAddress(String formattedAddress) {
        this.formattedAddress = formattedAddress;
    }

    public String getFlat() {
        return flat;
    }

    public UserAddress flat(String flat) {
        this.flat = flat;
        return this;
    }

    public void setFlat(String flat) {
        this.flat = flat;
    }

    public Double getLat() {
        return lat;
    }

    public UserAddress lat(Double lat) {
        this.lat = lat;
        return this;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLng() {
        return lng;
    }

    public UserAddress lng(Double lng) {
        this.lng = lng;
        return this;
    }

    public void setLng(Double lng) {
        this.lng = lng;
    }

    public String getCity() {
        return city;
    }

    public UserAddress city(String city) {
        this.city = city;
        return this;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public UserAddress state(String state) {
        this.state = state;
        return this;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCountry() {
        return country;
    }

    public UserAddress country(String country) {
        this.country = country;
        return this;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public Boolean isFromLocation() {
        return fromLocation;
    }

    public UserAddress fromLocation(Boolean fromLocation) {
        this.fromLocation = fromLocation;
        return this;
    }

    public void setFromLocation(Boolean fromLocation) {
        this.fromLocation = fromLocation;
    }

    public Profile getProfile() {
        return profile;
    }

    public UserAddress profile(Profile profile) {
        this.profile = profile;
        return this;
    }

    public void setProfile(Profile profile) {
        this.profile = profile;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        UserAddress userAddress = (UserAddress) o;
        if (userAddress.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), userAddress.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "UserAddress{" +
            "id=" + getId() +
            ", street='" + getStreet() + "'" +
            ", flat='" + getFlat() + "'" +
            ", lat=" + getLat() +
            ", lng=" + getLng() +
            ", city='" + getCity() + "'" +
            ", state='" + getState() + "'" +
            ", country='" + getCountry() + "'" +
            ", fromLocation='" + isFromLocation() + "'" +
            "}";
    }
}
