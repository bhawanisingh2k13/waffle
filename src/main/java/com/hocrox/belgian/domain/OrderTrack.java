package com.hocrox.belgian.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;

/**
 * A OrderTrack.
 */
@Entity
@Table(name = "order_track")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class OrderTrack implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "lat", nullable = false)
    private Double lat;

    @NotNull
    @Column(name = "lng", nullable = false)
    private Double lng;

    @Column(name = "track_time")
    private Instant trackTime = Instant.now();

    @Column(name = "long_track_time")
    private long longTrackTime = System.currentTimeMillis();

    @Column(name = "delivery_boy")
    private boolean deliveryBoy = true;

    @ManyToOne(optional = false)
    @NotNull
    private WaffleOrder waffleOrder;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getLat() {
        return lat;
    }

    public OrderTrack lat(Double lat) {
        this.lat = lat;
        return this;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLng() {
        return lng;
    }

    public OrderTrack lng(Double lng) {
        this.lng = lng;
        return this;
    }

    public void setLng(Double lng) {
        this.lng = lng;
    }

    public Instant getTrackTime() {
        return trackTime;
    }

    public OrderTrack trackTime(Instant trackTime) {
        this.trackTime = trackTime;
        return this;
    }

//    public void setLongTrackTime(Instant trackTime) {
//        this.trackTime = trackTime;
//    }

    public Long getLongTrackTime() {
        return longTrackTime;
    }

//    public OrderTrack longTrackTime(Instant trackTime) {
//        this.trackTime = trackTime;
//        return this;
//    }

    public void setTrackTime(Instant trackTime) {
        this.trackTime = trackTime;
    }

    public OrderTrack deliveryBoy(boolean deliveryBoy) {
        this.deliveryBoy = deliveryBoy;
        return this;
    }

    public boolean isDeliveryBoy() {
        return deliveryBoy;
    }

    public void setDeliveryBoy(boolean deliveryBoy) {
        this.deliveryBoy = deliveryBoy;
    }

    public WaffleOrder getWaffleOrder() {
        return waffleOrder;
    }

    public OrderTrack waffleOrder(WaffleOrder waffleOrder) {
        this.waffleOrder = waffleOrder;
        return this;
    }

    public void setWaffleOrder(WaffleOrder waffleOrder) {
        this.waffleOrder = waffleOrder;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        OrderTrack orderTrack = (OrderTrack) o;
        if (orderTrack.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), orderTrack.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "OrderTrack{" +
            "id=" + getId() +
            ", lat=" + getLat() +
            ", lng=" + getLng() +
            ", trackTime='" + getTrackTime() + "'" +
            "}";
    }
}
