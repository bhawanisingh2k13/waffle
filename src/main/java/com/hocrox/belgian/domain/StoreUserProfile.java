package com.hocrox.belgian.domain;

import com.hocrox.belgian.domain.enumeration.StoreRoleType;
import lombok.*;
import lombok.experimental.Accessors;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.io.Serializable;
import java.util.Objects;

/**
 * A StoreUserProfile.
 */
@Entity
@Table(name = "store_user_profile")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)

@Builder
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Accessors(fluent = true)
public class StoreUserProfile implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Lob
    @Column(name = "license")
    private String license;

    @NotNull
    @Pattern(regexp = "^[a-zA-Z0-9]*$")
    @Column(name = "phone_number", nullable = false)
    private String phoneNumber;

//    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "role_type", nullable = false, updatable = false)
    private StoreRoleType roleType;

    @ManyToOne
    @JoinColumn(updatable = false)
    private User user;

    @ManyToOne
    @JoinColumn //(updatable = false)
    private WaffleStore waffleStore;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLicense() {
        return license;
    }

    public void setLicense(String license) {
        this.license = license;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public StoreRoleType getRoleType() {
        return roleType;
    }

    public void setRoleType(StoreRoleType roleType) {
        this.roleType = roleType;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public WaffleStore getWaffleStore() {
        return waffleStore;
    }

    public void setWaffleStore(WaffleStore waffleStore) {
        this.waffleStore = waffleStore;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        StoreUserProfile storeUserProfile = (StoreUserProfile) o;
        return storeUserProfile.getId() != null && getId() != null && Objects.equals(getId(), storeUserProfile.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }
}
