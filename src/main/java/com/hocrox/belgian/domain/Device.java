package com.hocrox.belgian.domain;


import com.hocrox.belgian.domain.enumeration.DeviceType;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

/**
 * Created by bhawanisingh on 16/09/15.
 */

@Entity
@Table(name = "device")
@Getter
@Setter
public class Device extends AbstractAuditingEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "device_id", nullable = false)
    private String deviceID;

    @Column(name = "messaging_id", nullable = false)
    private String messagingID;

    @Column(name = "device_name")
    private String deviceName;

    @Enumerated(EnumType.STRING)
    @Column(name = "device_type", nullable = false)
    private DeviceType deviceType;

    @ManyToOne
    @JoinColumn(name = "owner_id")
    private User owner;
}
