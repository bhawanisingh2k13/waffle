package com.hocrox.belgian.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.Objects;

/**
 * A Setting.
 */
@Entity
@Table(name = "setting")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Setting implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "store_available", nullable = false)
    private Boolean storeAvailable;

    @NotNull
    @Column(name = "delayed_delivery", nullable = false)
    private Boolean delayedDelivery;

    @Column(name = "delayed_delivery_reason")
    private String delayedDeliveryReason;

    @Column(name = "delayed_delivery_time")
    private Integer delayedDeliveryTime;

    @NotNull
    @Column(name = "delivery_available", nullable = false)
    private Boolean deliveryAvailable;

    @NotNull
    @Column(name = "cod_available", nullable = false)
    private Boolean codAvailable;


    @NotNull
    @Column(name = "odt_available", nullable = false)
    private Boolean odtAvailable;

    @NotNull
    @Column(name = "delivery_radius", nullable = false)
    private Long deliveryRadius = 0L;

    @NotNull
    @Column(name = "delivery_charges", nullable = false)
    private Float deliveryCharges = 0.0F;

    @NotNull
    @Column(name = "handling_charges", nullable = false)
    private Float handlingCharges = 0.0F;

    @NotNull
    @Column(name = "packaging_charges", nullable = false)
    private Float packagingCharges = 0.0F;

    @NotNull
    @Column(name = "pan_cake_packaging_charges", nullable = false)
    private Float panCakePackagingCharges = 0.0F;


    @OneToOne(optional = false)
    @NotNull
    @JoinColumn(unique = true)
    private WaffleStore waffleStore;

    @NotNull
    @ManyToOne
    @JoinColumn
    private GlobalSetting globalSetting;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean isStoreAvailable() {
        return storeAvailable;
    }

    public Setting storeAvailable(Boolean storeAvailable) {
        this.storeAvailable = storeAvailable;
        return this;
    }

    public void setStoreAvailable(Boolean storeAvailable) {
        this.storeAvailable = storeAvailable;
    }

    public Boolean isDelayedDelivery() {
        return delayedDelivery;
    }

    public Setting delayedDelivery(Boolean delayedDelivery) {
        this.delayedDelivery = delayedDelivery;
        return this;
    }

    public void setDelayedDelivery(Boolean delayedDelivery) {
        this.delayedDelivery = delayedDelivery;
    }

    public String getDelayedDeliveryReason() {
        return delayedDeliveryReason;
    }

    public Setting delayedDeliveryReason(String delayedDeliveryReason) {
        this.delayedDeliveryReason = delayedDeliveryReason;
        return this;
    }

    public void setDelayedDeliveryReason(String delayedDeliveryReason) {
        this.delayedDeliveryReason = delayedDeliveryReason;
    }

    public Integer getDelayedDeliveryTime() {
        return delayedDeliveryTime;
    }

    public Setting delayedDeliveryTime(Integer delayedDeliveryTime) {
        this.delayedDeliveryTime = delayedDeliveryTime;
        return this;
    }

    public void setDelayedDeliveryTime(Integer delayedDeliveryTime) {
        this.delayedDeliveryTime = delayedDeliveryTime;
    }

    public Boolean isDeliveryAvailable() {
        return deliveryAvailable;
    }

    public Setting deliveryAvailable(Boolean deliveryAvailable) {
        this.deliveryAvailable = deliveryAvailable;
        return this;
    }

    public void setDeliveryAvailable(Boolean deliveryAvailable) {
        this.deliveryAvailable = deliveryAvailable;
    }

    public Boolean isCodAvailable() {
        return codAvailable;
    }

    public Setting codAvailable(Boolean codAvailable) {
        this.codAvailable = codAvailable;
        return this;
    }

    public void setCodAvailable(Boolean codAvailable) {
        this.codAvailable = codAvailable;
    }

    public Boolean isOdtAvailable() {
        return odtAvailable;
    }

    public Setting odtAvailable(Boolean odtAvailable) {
        this.odtAvailable = odtAvailable;
        return this;
    }

    public void setOdtAvailable(Boolean odtAvailable) {
        this.odtAvailable = odtAvailable;
    }

    public Long getDeliveryRadius() {
        return deliveryRadius;
    }

    public Setting deliveryRadius(Long deliveryRadius) {
        this.deliveryRadius = deliveryRadius;
        return this;
    }

    public void setDeliveryRadius(Long deliveryRadius) {
        this.deliveryRadius = deliveryRadius;
    }


    public Float getDeliveryCharges() {
        return deliveryCharges;
    }

    public Setting deliveryCharges(Float deliveryCharges) {
        this.deliveryCharges = deliveryCharges;
        return this;
    }

    public void setDeliveryCharges(Float deliveryCharges) {
        this.deliveryCharges = deliveryCharges;
    }

    public Float getHandlingCharges() {
        return handlingCharges;
    }

    public Setting handlingCharges(Float handlingCharges) {
        this.handlingCharges = handlingCharges;
        return this;
    }
    public void setHandlingCharges(Float handlingCharges) {
        this.handlingCharges = handlingCharges;
    }

    public Float getPackagingCharges() {
        return packagingCharges;
    }

    public Setting packagingCharges(Float packagingCharges) {
        this.packagingCharges = packagingCharges;
        return this;
    }
    public void setPackagingCharges(Float packagingCharges) {
        this.packagingCharges = packagingCharges;
    }

    public Float getPanCakePackagingCharges() {
        return panCakePackagingCharges;
    }

    public Setting panCakePackagingCharges(Float panCakePackagingCharges) {
        this.panCakePackagingCharges = panCakePackagingCharges;
        return this;
    }
    public void setPanCakePackagingCharges(Float panCakePackagingCharges) {
        this.panCakePackagingCharges = panCakePackagingCharges;
    }

    public WaffleStore getWaffleStore() {
        return waffleStore;
    }

    public Setting waffleStore(WaffleStore waffleStore) {
        this.waffleStore = waffleStore;
        return this;
    }

    public void setWaffleStore(WaffleStore waffleStore) {
        this.waffleStore = waffleStore;
    }

    public GlobalSetting getGlobalSetting() {
        return globalSetting;
    }

    public Setting globalSetting(GlobalSetting globalSetting) {
        this.globalSetting = globalSetting;
        return this;
    }

    public void setGlobalSetting(GlobalSetting globalSetting) {
        this.globalSetting = globalSetting;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Setting setting = (Setting) o;
        if (setting.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), setting.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Setting{" +
            "id=" + getId() +
            ", storeAvailable='" + isStoreAvailable() + "'" +
            ", delayedDelivery='" + isDelayedDelivery() + "'" +
            ", delayedDeliveryReason='" + getDelayedDeliveryReason() + "'" +
            ", delayedDeliveryTime=" + getDelayedDeliveryTime() +
            ", deliveryAvailable='" + isDeliveryAvailable() + "'" +
            ", odtAvailable='" + isOdtAvailable() + "'" +
            "}";
    }
}
