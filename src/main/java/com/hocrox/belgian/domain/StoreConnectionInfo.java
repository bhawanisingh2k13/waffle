package com.hocrox.belgian.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;

/**
 * A StoreConnectionInfo.
 */
@Entity
@Table(name = "store_connection_info")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class StoreConnectionInfo implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "connection_id", nullable = false)
    private String connectionId;

    @ManyToOne(optional = false)
    @NotNull
    private WaffleStore waffleStore;

    @CreatedDate
    @Column(name = "created_date", nullable = false)
    @JsonIgnore
    private Instant createdDate = Instant.now();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getConnectionId() {
        return connectionId;
    }

    public StoreConnectionInfo connectionId(String connectionId) {
        this.connectionId = connectionId;
        return this;
    }

    public void setConnectionId(String connectionId) {
        this.connectionId = connectionId;
    }

    public WaffleStore getWaffleStore() {
        return waffleStore;
    }

    public StoreConnectionInfo waffleStore(WaffleStore waffleStore) {
        this.waffleStore = waffleStore;
        return this;
    }

    public void setWaffleStore(WaffleStore waffleStore) {
        this.waffleStore = waffleStore;
    }

    public Instant getCreatedDate() {
        return createdDate;
    }

    public StoreConnectionInfo createdDate(Instant createdDate) {
        this.createdDate = createdDate;
        return this;
    }

    public void setCreatedDate(Instant createdDate) {
        this.createdDate = createdDate;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        StoreConnectionInfo storeConnectionInfo = (StoreConnectionInfo) o;
        if (storeConnectionInfo.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), storeConnectionInfo.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "StoreConnectionInfo{" +
            "id=" + getId() +
            ", connectionId='" + getConnectionId() + "'" +
            "}";
    }
}
