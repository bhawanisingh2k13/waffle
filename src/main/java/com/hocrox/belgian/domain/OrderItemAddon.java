package com.hocrox.belgian.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.util.Objects;

/**
 * A OrderItemAddon.
 */
@Entity
@Table(name = "order_item_addon")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class OrderItemAddon implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "price")
    private Double price;

    @Column(name = "offer")
    private Double offer;

    @Column(name = "final_price")
    private Double finalPrice;

    @ManyToOne
    private Addon addon;

    @ManyToOne
    private OrderItem orderItem;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getPrice() {
        return price;
    }

    public OrderItemAddon price(Double price) {
        this.price = price;
        return this;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Double getOffer() {
        return offer;
    }

    public OrderItemAddon offer(Double offer) {
        this.offer = offer;
        return this;
    }

    public void setOffer(Double offer) {
        this.offer = offer;
    }

    public Double getFinalPrice() {
        return finalPrice;
    }

    public OrderItemAddon finalPrice(Double finalPrice) {
        this.finalPrice = finalPrice;
        return this;
    }

    public void setFinalPrice(Double finalPrice) {
        this.finalPrice = finalPrice;
    }

    public Addon getAddon() {
        return addon;
    }

    public OrderItemAddon addon(Addon addon) {
        this.addon = addon;
        return this;
    }

    public void setAddon(Addon addon) {
        this.addon = addon;
    }

    public OrderItem getOrderItem() {
        return orderItem;
    }

    public OrderItemAddon orderItem(OrderItem orderItem) {
        this.orderItem = orderItem;
        return this;
    }

    public void setOrderItem(OrderItem orderItem) {
        this.orderItem = orderItem;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        OrderItemAddon orderItemAddon = (OrderItemAddon) o;
        if (orderItemAddon.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), orderItemAddon.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "OrderItemAddon{" +
            "id=" + getId() +
            ", price=" + getPrice() +
            ", offer=" + getOffer() +
            ", finalPrice=" + getFinalPrice() +
            "}";
    }
}
