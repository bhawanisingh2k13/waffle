package com.hocrox.belgian.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.Objects;

/**
 * A GlobalSetting.
 */
@Entity
@Table(name = "global_setting")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class GlobalSetting implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "min_order_qty", nullable = false)
    private Integer minOrderQty;

    @NotNull
    @Column(name = "max_order_qty", nullable = false)
    private Integer maxOrderQty;

    @NotNull
    @Column(name = "android_app_version", nullable = false)
    private Long androidAppVersion;

    @NotNull
    @Column(name = "pan_cake_price", nullable = false)
    private Float panCakePrice = 0f;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getMinOrderQty() {
        return minOrderQty;
    }

    public GlobalSetting minOrderQty(Integer minOrderQty) {
        this.minOrderQty = minOrderQty;
        return this;
    }

    public void setMinOrderQty(Integer minOrderQty) {
        this.minOrderQty = minOrderQty;
    }

    public Integer getMaxOrderQty() {
        return maxOrderQty;
    }

    public GlobalSetting maxOrderQty(Integer maxOrderQty) {
        this.maxOrderQty = maxOrderQty;
        return this;
    }

    public void setMaxOrderQty(Integer maxOrderQty) {
        this.maxOrderQty = maxOrderQty;
    }

    public Long getAndroidAppVersion() {
        return androidAppVersion;
    }

    public GlobalSetting androidAppVersion(Long androidAppVersion) {
        this.androidAppVersion = androidAppVersion;
        return this;
    }

    public void setAndroidAppVersion(Long androidAppVersion) {
        this.androidAppVersion = androidAppVersion;
    }

    public Float getPanCakePrice() {
        return panCakePrice;
    }

    public GlobalSetting panCakePrice(Float panCakePrice) {
        this.panCakePrice = panCakePrice;
        return this;
    }

    public void setPanCakePrice(Float panCakePrice) {
        this.panCakePrice = panCakePrice;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        GlobalSetting globalSetting = (GlobalSetting) o;
        if (globalSetting.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), globalSetting.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "GlobalSetting{" +
            "id=" + getId() +
            ", minOrderQty=" + getMinOrderQty() +
            ", maxOrderQty=" + getMaxOrderQty() +
            ", androidAppVersion=" + getAndroidAppVersion() +
            "}";
    }
}
