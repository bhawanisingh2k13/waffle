package com.hocrox.belgian.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

/**
 * A Offer.
 */
@Entity
@Table(name = "offer")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Offer implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "to_date", nullable = false)
    private LocalDate toDate;

    @NotNull
    @Column(name = "from_date", nullable = false)
    private LocalDate fromDate;

    @NotNull
    @Column(name = "max_discount_amount")
    private Double maxDiscountAmount;

    @Column(name = "coupon_code")
    private String couponCode;

    @Column(name = "bill")
    private Boolean bill;

    @Column(name = "jhi_global")
    private Boolean global;

    @Lob
    @Column(name = "description")
    private String description;

    @Column(name = "image")
    private String image;

    @NotNull
    @Column(name = "percent_off", nullable = false)
    private Integer percentOff;

    @ManyToOne
    private MenuItem menuItem;

    @ManyToOne
    private WaffleStore waffleStore;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getToDate() {
        return toDate;
    }

    public Offer toDate(LocalDate toDate) {
        this.toDate = toDate;
        return this;
    }

    public void setToDate(LocalDate toDate) {
        this.toDate = toDate;
    }

    public LocalDate getFromDate() {
        return fromDate;
    }

    public Offer fromDate(LocalDate fromDate) {
        this.fromDate = fromDate;
        return this;
    }

    public void setMaxDiscountAmount(Double maxDiscountAmount) {
        this.maxDiscountAmount = maxDiscountAmount;
    }

    public Double getMaxDiscountAmount() {
        return maxDiscountAmount;
    }

    public Offer maxDiscountAmount(Double maxDiscountAmount) {
        this.maxDiscountAmount = maxDiscountAmount;
        return this;
    }

    public void setFromDate(LocalDate fromDate) {
        this.fromDate = fromDate;
    }

    public String getCouponCode() {
        return couponCode;
    }

    public Offer couponCode(String couponCode) {
        this.couponCode = couponCode;
        return this;
    }

    public void setCouponCode(String couponCode) {
        this.couponCode = couponCode;
    }

    public Boolean isBill() {
        return bill;
    }

    public Offer bill(Boolean bill) {
        this.bill = bill;
        return this;
    }

    public void setBill(Boolean bill) {
        this.bill = bill;
    }

    public Boolean isGlobal() {
        return global;
    }

    public Offer global(Boolean global) {
        this.global = global;
        return this;
    }

    public void setGlobal(Boolean global) {
        this.global = global;
    }

    public String getDescription() {
        return description;
    }

    public Offer description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage() {
        return image;
    }

    public Offer image(String image) {
        this.image = image;
        return this;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Integer getPercentOff() {
        return percentOff;
    }

    public Offer percentOff(Integer percentOff) {
        this.percentOff = percentOff;
        return this;
    }

    public void setPercentOff(Integer percentOff) {
        this.percentOff = percentOff;
    }

    public MenuItem getMenuItem() {
        return menuItem;
    }

    public Offer menuItem(MenuItem menuItem) {
        this.menuItem = menuItem;
        return this;
    }

    public void setMenuItem(MenuItem menuItem) {
        this.menuItem = menuItem;
    }

    public WaffleStore getWaffleStore() {
        return waffleStore;
    }

    public Offer waffleStore(WaffleStore waffleStore) {
        this.waffleStore = waffleStore;
        return this;
    }

    public void setWaffleStore(WaffleStore waffleStore) {
        this.waffleStore = waffleStore;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Offer offer = (Offer) o;
        if (offer.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), offer.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Offer{" +
            "id=" + getId() +
            ", toDate='" + getToDate() + "'" +
            ", fromDate='" + getFromDate() + "'" +
            ", couponCode='" + getCouponCode() + "'" +
            ", bill='" + isBill() + "'" +
            ", global='" + isGlobal() + "'" +
            ", description='" + getDescription() + "'" +
            ", image='" + getImage() + "'" +
            ", percentOff=" + getPercentOff() +
            "}";
    }
}
