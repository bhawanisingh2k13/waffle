package com.hocrox.belgian.domain;


import com.hocrox.belgian.domain.enumeration.AccountType;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;

/**
 * Created by bhawani on 23/8/15.
 */

@Entity
@Table(name = "account")
@Getter
@Setter
@ToString
public class Account extends AbstractAuditingEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Enumerated(EnumType.ORDINAL)
	@Column(name = "account_type")
	private AccountType accountType;

	//	@Unique(entity = Account.class)
	@Column(name = "token_id", nullable = false, unique = true)
	private String tokenID;

	@ManyToOne
	@JoinColumn(name = "user_id")
	private User user;

	@Transient
	private String accessToken;

	@Transient
	private String refreshToken;

}
