package com.hocrox.belgian.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.hocrox.belgian.domain.enumeration.OrderStatus;
import com.hocrox.belgian.domain.enumeration.OrderType;
import com.hocrox.belgian.domain.enumeration.PaymentType;
import com.hocrox.belgian.service.util.RandomUtil;
import org.hashids.Hashids;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.Instant;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * A WaffleOrder.
 */
@Entity
@Table(name = "waffle_order")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class WaffleOrder extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    private static final String ORDER_RANDOM = "W4XJCk6JNSZ3NRk4HXwEn77YJ7WEDb";

    private static final String HASH_ALPHABET = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";

    private static final int ORDER_HASH_LENGTH = 10;

    public static final Hashids HASHIDS = new Hashids(WaffleOrder.ORDER_RANDOM, WaffleOrder.ORDER_HASH_LENGTH, WaffleOrder.HASH_ALPHABET);

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "order_id")
    private String orderId;

    @Column(name = "cart_token", unique = true, nullable = false)
    private String cartToken = RandomUtil.generateCartToken();

    @Column(name = "store_order_number")
    private String storeOrderNumber;

    @Column(name = "name")
    private String name;

    @Column(name = "phone_number")
    private String phoneNumber;

    @Column(name = "email")
    private String email;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "order_type", nullable = false)
    private OrderType orderType = OrderType.DROP_OFF;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "payment_type", nullable = false)
    private PaymentType paymentType = PaymentType.COD;

    @Column(name = "coupon_code")
    private String couponCode;

    @Column(name = "expected_delivery_time")
    private Instant expectedDeliveryTime;

    @Column(name = "out_for_delivery_time")
    private Instant outForDeliveryTime;

    @Column(name = "delivery_time")
    private Instant deliveryTime;

    @Column(name = "order_time")
    private Instant orderTime;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "order_status", nullable = false)
    private OrderStatus orderStatus = OrderStatus.CREATING;

    @Column(name = "discount")
    private Double discount = 0D;

    @Column(name = "cgst")
    private Float cgst;

    @Column(name = "sgst")
    private Float sgst;

    @Column(name = "total_price")
    private Double totalPrice = 0D;

    @Column(name = "packaging_charges")
    private Long packagingCharges = 0L;

    @Column(name = "final_price")
    private Double finalPrice;

    @OneToMany(mappedBy = "waffleOrder")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<OrderItem> orderItems = new HashSet<>();

    @ManyToOne
    private UserAddress userAddress;


    @ManyToOne
    private PickupLocation pickupLocation;

    @ManyToOne
    private StoreUserProfile storeUserProfile;

    @ManyToOne
    private WaffleStore waffleStore;

    @ManyToOne
    private Profile profile;

    @Column(name = "payment_id")
    private String paymentId;

    @Column(name = "payment_complete")
    private boolean paymentComplete = false;

    @OneToOne
    @JoinColumn(name = "waffle_order_id")
    private WaffleOrder waffleOrder;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getOrderId() {
        return orderId;
    }

    public WaffleOrder orderId(String orderId) {
        this.orderId = orderId;
        return this;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getName() {
        return name;
    }

    public WaffleOrder name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCartToken() {
        return cartToken;
    }

    public String getStoreOrderNumber() {
        return storeOrderNumber;
    }

    public WaffleOrder storeOrderNumber(String storeOrderNumber) {
        this.storeOrderNumber = storeOrderNumber;
        return this;
    }

    public void setStoreOrderNumber(String storeOrderNumber) {
        this.storeOrderNumber = storeOrderNumber;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public WaffleOrder phoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
        return this;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public WaffleOrder email(String email) {
        this.email = email;
        return this;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public OrderType getOrderType() {
        return orderType;
    }

    public WaffleOrder orderType(OrderType orderType) {
        this.orderType = orderType;
        return this;
    }

    public void setOrderType(OrderType orderType) {
        this.orderType = orderType;
    }

    public PaymentType getPaymentType() {
        return paymentType;
    }

    public WaffleOrder paymentType(PaymentType paymentType) {
        this.paymentType = paymentType;
        return this;
    }

    public void setPaymentType(PaymentType paymentType) {
        this.paymentType = paymentType;
    }

    public String getCouponCode() {
        return couponCode;
    }

    public WaffleOrder couponCode(String couponCode) {
        this.couponCode = couponCode;
        return this;
    }

    public void setCouponCode(String couponCode) {
        this.couponCode = couponCode;
    }

    public Instant getExpectedDeliveryTime() {
        return expectedDeliveryTime;
    }

    public WaffleOrder expectedDeliveryTime(Instant expectedDeliveryTime) {
        this.expectedDeliveryTime = expectedDeliveryTime;
        return this;
    }

    public void setExpectedDeliveryTime(Instant expectedDeliveryTime) {
        this.expectedDeliveryTime = expectedDeliveryTime;
    }

    public Instant getOutForDeliveryTime() {
        return outForDeliveryTime;
    }

    public WaffleOrder outForDeliveryTime(Instant outForDeliveryTime) {
        this.outForDeliveryTime = outForDeliveryTime;
        return this;
    }

    public void setOutForDeliveryTime(Instant outForDeliveryTime) {
        this.outForDeliveryTime = outForDeliveryTime;
    }

    public Instant getDeliveryTime() {
        return deliveryTime;
    }

    public WaffleOrder deliveryTime(Instant deliveryTime) {
        this.deliveryTime = deliveryTime;
        return this;
    }

    public void setDeliveryTime(Instant deliveryTime) {
        this.deliveryTime = deliveryTime;
    }

    public Instant getOrderTime() {
        return orderTime;
    }

    public WaffleOrder orderTime(Instant orderTime) {
        this.orderTime = orderTime;
        return this;
    }

    public void setOrderTime(Instant orderTime) {
        this.orderTime = orderTime;
    }

    public OrderStatus getOrderStatus() {
        return orderStatus;
    }

    public WaffleOrder orderStatus(OrderStatus orderStatus) {
        this.orderStatus = orderStatus;
        return this;
    }

    public void setOrderStatus(OrderStatus orderStatus) {
        this.orderStatus = orderStatus;
    }

    public Double getDiscount() {
        return discount;
    }

    public WaffleOrder discount(Double discount) {
        this.discount = discount;
        return this;
    }

    public void setDiscount(Double discount) {
        this.discount = discount;
    }

    public Float getCgst() {
        return cgst;
    }

    public WaffleOrder cgst(Float cgst) {
        this.cgst = cgst;
        return this;
    }

    public void setCgst(Float cgst) {
        this.cgst = cgst;
    }

    public Float getSgst() {
        return sgst;
    }

    public WaffleOrder sgst(Float sgst) {
        this.sgst = sgst;
        return this;
    }

    public void setSgst(Float sgst) {
        this.sgst = sgst;
    }

    public Double getTotalPrice() {
        return totalPrice;
    }

    public WaffleOrder totalPrice(Double totalPrice) {
        this.totalPrice = totalPrice;
        return this;
    }

    public void setTotalPrice(Double totalPrice) {
        this.totalPrice = totalPrice;
    }

    public Long getPackagingCharges() {
        return packagingCharges;
    }

    public WaffleOrder packagingCharges(Long packagingCharges) {
        this.packagingCharges = packagingCharges;
        return this;
    }

    public void setPackagingCharges(Long packagingCharges) {
        this.packagingCharges = packagingCharges;
    }

    public Double getFinalPrice() {
        return finalPrice;
    }

    public WaffleOrder finalPrice(Double finalPrice) {
        this.finalPrice = finalPrice;
        return this;
    }

    public void setFinalPrice(Double finalPrice) {
        this.finalPrice = finalPrice;
    }

    public Set<OrderItem> getOrderItems() {
        return orderItems;
    }

    public WaffleOrder orderItems(Set<OrderItem> orderItems) {
        this.orderItems = orderItems;
        return this;
    }

    public WaffleOrder addOrderItem(OrderItem orderItem) {
        this.orderItems.add(orderItem);
        orderItem.setWaffleOrder(this);
        return this;
    }

    public WaffleOrder removeOrderItem(OrderItem orderItem) {
        this.orderItems.remove(orderItem);
        orderItem.setWaffleOrder(null);
        return this;
    }

    public void setOrderItems(Set<OrderItem> orderItems) {
        this.orderItems = orderItems;
    }

    public UserAddress getUserAddress() {
        return userAddress;
    }

    public WaffleOrder userAddress(UserAddress userAddress) {
        this.userAddress = userAddress;
        return this;
    }

    public void setUserAddress(UserAddress userAddress) {
        this.userAddress = userAddress;
    }

    public PickupLocation getPickupLocation() {
        return pickupLocation;
    }

    public WaffleOrder userAddress(PickupLocation pickupLocation) {
        this.pickupLocation = pickupLocation;
        return this;
    }

    public void setPickupLocation(PickupLocation pickupLocation) {
        this.pickupLocation = pickupLocation;
    }


    public WaffleStore getWaffleStore() {
        return waffleStore;
    }

    public WaffleOrder waffleStore(WaffleStore waffleStore) {
        this.waffleStore = waffleStore;
        return this;
    }

    public void setWaffleStore(WaffleStore waffleStore) {
        this.waffleStore = waffleStore;
    }

    public StoreUserProfile getStoreUserProfile() {
        return storeUserProfile;
    }

    public WaffleOrder storeUserProfile(StoreUserProfile storeUserProfile) {
        this.storeUserProfile = storeUserProfile;
        return this;
    }

    public void setStoreUserProfile(StoreUserProfile storeUserProfile) {
        this.storeUserProfile = storeUserProfile;
    }

    public Profile getProfile() {
        return profile;
    }

    public WaffleOrder profile(Profile profile) {
        this.profile = profile;
        return this;
    }

    public void setPaymentId(String paymentId) {
        this.paymentId = paymentId;
    }

    public String getPaymentId() {
        return paymentId;
    }

    public WaffleOrder paymentId(String paymentId) {
        this.paymentId = paymentId;
        return this;
    }

    public void setPaymentComplete(boolean paymentComplete) {
        this.paymentComplete = paymentComplete;
    }


    public boolean isPaymentComplete() {
        return paymentComplete;
    }

    public WaffleOrder paymentComplete(boolean paymentComplete) {
        this.paymentComplete = paymentComplete;
        return this;
    }

    public WaffleOrder getWaffleOrder() {
        return waffleOrder;
    }

    public void setWaffleOrder(WaffleOrder waffleOrder) {
        this.waffleOrder = waffleOrder;
    }

    public WaffleOrder waffleOrder(WaffleOrder waffleOrder) {
        this.waffleOrder = waffleOrder;
        return this;
    }

    public void setProfile(Profile profile) {
        this.profile = profile;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        WaffleOrder waffleOrder = (WaffleOrder) o;
        if (waffleOrder.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), waffleOrder.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "WaffleOrder{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", phoneNumber='" + getPhoneNumber() + "'" +
            ", email='" + getEmail() + "'" +
            ", orderType='" + getOrderType() + "'" +
            ", paymentType='" + getPaymentType() + "'" +
            ", couponCode='" + getCouponCode() + "'" +
            ", expectedDeliveryTime='" + getExpectedDeliveryTime() + "'" +
            ", deliveryTime='" + getDeliveryTime() + "'" +
            ", orderTime='" + getOrderTime() + "'" +
            ", orderStatus='" + getOrderStatus() + "'" +
            ", discount=" + getDiscount() +
            ", cgst=" + getCgst() +
            ", sgst=" + getSgst() +
            ", totalPrice=" + getTotalPrice() +
            ", finalPrice=" + getFinalPrice() +
            "}";
    }
}
