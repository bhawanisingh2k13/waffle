package com.hocrox.belgian.domain.enumeration;

import lombok.Getter;
import lombok.ToString;

/**
 * The OrderStatus enumeration.
 */
@Getter
@ToString
public enum OrderStatus {
    CREATING(-1), PENDING_PAYMENT(0), PLACED(1), PREPARING(2), CANCELLED(3), OUT_FOR_DELIVERY(4), DELIVERED(5);

    private long value;

    OrderStatus(int value) {
        this.value = value;
    }
}
