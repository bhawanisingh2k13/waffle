package com.hocrox.belgian.domain.enumeration;

/**
 * Created by bhawani on 22/8/15.
 */
public enum RoleType {
	SYSTEM_GROUP, USER_GROUP
}
