package com.hocrox.belgian.domain.enumeration;

/**
 * Created by bhawani on 23/8/15.
 */
public enum AccountType {
    NORMAL, OTP, ADMIN
}
