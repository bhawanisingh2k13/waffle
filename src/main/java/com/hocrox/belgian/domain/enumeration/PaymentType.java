package com.hocrox.belgian.domain.enumeration;

import lombok.Getter;
import lombok.ToString;

/**
 * The PaymentType enumeration.
 */
@Getter
@ToString
public enum PaymentType {
    COD("Cash on Delivery"),  ONLINE("Online");

    private String name;

    PaymentType(String name) {
        this.name = name;
    }
}
