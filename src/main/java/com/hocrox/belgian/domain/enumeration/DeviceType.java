package com.hocrox.belgian.domain.enumeration;

/**
 * Created by bhawanisingh on 17/09/15.
 */
public enum DeviceType {
    ANDROID, BLACKBERRY, IOS, WINDOWS, UBUNTU, WEB, SAFARI, DESKTOP
}
