package com.hocrox.belgian.domain.enumeration;

import lombok.Getter;

/**
 * The StoreRoleType enumeration.
 */
@Getter
public enum StoreRoleType {
    OWNER(1),  MANAGER(2),  COUNTER(3),  DELIVERY(4);

    private long orderAccess;

    StoreRoleType(long orderAccess) {
        this.orderAccess = orderAccess;
    }
}
