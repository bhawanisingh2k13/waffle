package com.hocrox.belgian.domain.enumeration;

import lombok.Getter;
import lombok.ToString;

/**
 * The OrderType enumeration.
 */
@Getter
@ToString
public enum OrderType {
    ONLINE_DRIVE_THRU("Online Drive Thru"),  DROP_OFF("Drop Off");

    private String name;

    OrderType(String name) {
        this.name = name;
    }
}
