package com.hocrox.belgian.domain.enumeration;

/**
 * The NotiStatus enumeration.
 */
public enum NotiStatus {
    PENDING,  FAILED,  DELIVERED
}
