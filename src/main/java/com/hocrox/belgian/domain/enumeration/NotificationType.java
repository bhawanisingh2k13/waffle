package com.hocrox.belgian.domain.enumeration;

import lombok.Getter;
import lombok.ToString;

/**
 * Created by bhawanisingh on 12/12/16 12:04 PM.
 */
@Getter
@ToString
public enum NotificationType {

    AP_UPD("/", "", "Belgian Waffle"),
    UPD_APP("/", "", "Belgian Waffle"),
    BLANK("/", "", "Belgian Waffle"),
//    For Orders
    O_PLCD("/order", "Order Placed", "order_detail_activity"),
    O_PLCD_DT("/order/ONLINE_DRIVE_THRU", "Order Placed", "order_detail_activity"),
    O_PLCD_HD("/order/DROP_OFF", "Order Placed", "order_detail_activity"),
    O_PREP("/order", "Order Preparing", "order_detail_activity"),
    O_OFD("/order", "Order Out For Delivery", "order_detail_activity"),
    O_DLVD("/order", "Order Delivered", "order_detail_activity"),
    O_CNCL("/order", "Order Cancelled", "order_detail_activity"),
    O_ASGN("/order", "Order Assigned", "order_detail_activity") // To delivery Boy
    ;

    private String url;

    private String title;

    private String androidActivity;

    NotificationType(String url, String title, String androidActivity) {
        this.url = url;
        this.title = title;
        this.androidActivity = androidActivity;
    }
}
