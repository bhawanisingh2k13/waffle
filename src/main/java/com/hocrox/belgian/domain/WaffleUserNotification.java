package com.hocrox.belgian.domain;

import com.hocrox.belgian.domain.enumeration.NotiStatus;
import com.hocrox.belgian.domain.enumeration.NotificationType;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.Instant;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * A WaffleUserNotification.
 */
@Entity
@Table(name = "waffle_user_notification")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Getter
@Setter
@ToString
public class WaffleUserNotification implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "noti_body")
    private String notiBody;

    @Column(name = "title")
    private String title;

    @Enumerated(EnumType.STRING)
    @Column(name = "jhi_type")
    private NotificationType type;

    @Column(name = "image")
    private String image;

    @Column(name = "noti_id")
    private String notiId;

    @Column(name = "ref_id")
    private long refId = 0;

    @Column(name = "background_noti")
    private boolean background = false;

    @Column(name = "to_all")
    private boolean toAll = false;


    @Column(name = "app_version")
    private long apv = -1;

    @Column(name = "app_version_target")
    private long apvt = 0;

    @Column(name = "sent_on")
    private Instant sentOn;

    @NotNull
    @Column(name = "scheduled_time", nullable = false)
    private Instant scheduledTime = Instant.now();

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "noti_status", nullable = false)
    private NotiStatus notiStatus = NotiStatus.PENDING;

    @ManyToMany
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JoinTable(name = "waffle_user_notification_user",
               joinColumns = @JoinColumn(name="waffle_user_notifications_id", referencedColumnName="id"),
               inverseJoinColumns = @JoinColumn(name="users_id", referencedColumnName="id"))
    private Set<User> users = new HashSet<>();

    @ManyToOne
    private WaffleStore waffleStore;


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        WaffleUserNotification waffleUserNotification = (WaffleUserNotification) o;
        if (waffleUserNotification.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), waffleUserNotification.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }
}
