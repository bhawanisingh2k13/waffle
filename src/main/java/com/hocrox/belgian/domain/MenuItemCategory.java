package com.hocrox.belgian.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.util.Objects;

/**
 * A MenuItemCategory.
 */
@Entity
@Table(name = "menu_item_category")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class MenuItemCategory implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;

    @Lob
    @Column(name = "image")
    private String image;

    @Column(name = "enabled")
    private boolean enabled = true;

    @Column(name = "enable_packaging_cost", nullable = false)
    private boolean enablePackagingCost = true;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public MenuItemCategory name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public MenuItemCategory image(String image) {
        this.image = image;
        return this;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public MenuItemCategory enabled(boolean enabled) {
        this.enabled = enabled;
        return this;
    }

    public boolean isEnablePackagingCost() {
        return enablePackagingCost;
    }

    public void setEnablePackagingCost(boolean enablePackagingCost) {
        this.enablePackagingCost = enablePackagingCost;
    }

    public MenuItemCategory enablePackagingCost(boolean enablePackagingCost) {
        this.enablePackagingCost = enablePackagingCost;
        return this;
    }

// jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        MenuItemCategory menuItemCategory = (MenuItemCategory) o;
        if (menuItemCategory.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), menuItemCategory.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "MenuItemCategory{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", image='" + getImage() + "'" +
            "}";
    }
}
