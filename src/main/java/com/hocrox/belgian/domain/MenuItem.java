package com.hocrox.belgian.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * A MenuItem.
 */
@Entity
@Table(name = "menu_item")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class MenuItem implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "name", nullable = false)
    private String name;

    @NotNull
    @Column(name = "price", nullable = false)
    private Float price;

    @NotNull
    @Lob
    @Column(name = "image", nullable = false)
    private String image;

    @NotNull
    @Lob
    @Column(name = "description", nullable = false)
    private String description;

    @Column(name = "enable_packaging_cost", nullable = false)
    private boolean enablePackagingCost = true;

    @Column(name = "sorting_id")
    private long sortingId = 0;

    @Column(name ="be_pan_cake")
    private Boolean panCake = false;

    @ManyToOne(optional = false)
    @NotNull
    private MenuItemCategory category;

    @ManyToMany
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JoinTable(name = "menu_item_addon",
               joinColumns = @JoinColumn(name="menu_items_id", referencedColumnName="id"),
               inverseJoinColumns = @JoinColumn(name="addons_id", referencedColumnName="id"))
    private Set<Addon> addons = new HashSet<>();

    @Column(name = "enabled")
    private boolean enabled = true;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public MenuItem name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Float getPrice() {
        return price;
    }

    public MenuItem price(Float price) {
        this.price = price;
        return this;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public String getImage() {
        return image;
    }

    public MenuItem image(String image) {
        this.image = image;
        return this;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getDescription() {
        return description;
    }

    public MenuItem description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isEnablePackagingCost() {
        return enablePackagingCost;
    }

    public MenuItem enablePackagingCost(boolean enablePackagingCost) {
        this.enablePackagingCost = enablePackagingCost;
        return this;
    }

    public void setEnablePackagingCost(boolean enablePackagingCost) {
        this.enablePackagingCost = enablePackagingCost;
    }

    public long getSortingId() {
        return sortingId;
    }

    public MenuItem sortingId(long sortingId) {
        this.sortingId = sortingId;
        return this;
    }

    public void setSortingId(long sortingId) {
        this.sortingId = sortingId;
    }

    public Boolean getPanCake() {
        return panCake;
    }

    public MenuItem orderItem(Boolean panCake) {
        this.panCake = panCake;
        return this;
    }

    public void setPanCake(Boolean panCake) {
        this.panCake = panCake;
    }

    public MenuItemCategory getCategory() {
        return category;
    }

    public MenuItem category(MenuItemCategory menuItemCategory) {
        this.category = menuItemCategory;
        return this;
    }

    public void setCategory(MenuItemCategory menuItemCategory) {
        this.category = menuItemCategory;
    }

    public Set<Addon> getAddons() {
        return addons;
    }

    public MenuItem addons(Set<Addon> addons) {
        this.addons = addons;
        return this;
    }

    public MenuItem addAddon(Addon addon) {
        this.addons.add(addon);
        return this;
    }

    public MenuItem removeAddon(Addon addon) {
        this.addons.remove(addon);
        return this;
    }

    public void setAddons(Set<Addon> addons) {
        this.addons = addons;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public MenuItem enabled(boolean enabled) {
        this.enabled = enabled;
        return this;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        MenuItem menuItem = (MenuItem) o;
        if (menuItem.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), menuItem.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "MenuItem{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", price=" + getPrice() +
            ", image='" + getImage() + "'" +
            ", description='" + getDescription() + "'" +
            "}";
    }
}
