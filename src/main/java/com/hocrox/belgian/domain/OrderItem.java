package com.hocrox.belgian.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A OrderItem.
 */
@Entity
@Table(name = "order_item")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class OrderItem implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "quantity")
    private Integer quantity;

    @Column(name = "price_per_qty")
    private Double pricePerQty;

    @Column(name = "total_price")
    private Double totalPrice;

    @Column(name = "offer")
    private Double offer;

    @Column(name = "final_price")
    private Double finalPrice;

    @Column(name ="is_pan_cake")
    private Boolean panCake = false;

    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    @NotNull
    private MenuItem menuItem;

    @OneToMany(mappedBy = "orderItem")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<OrderItemAddon> orderItemAddons = new HashSet<>();

    @ManyToOne
    private WaffleOrder waffleOrder;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public OrderItem quantity(Integer quantity) {
        this.quantity = quantity;
        return this;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Double getPricePerQty() {
        return pricePerQty;
    }

    public OrderItem pricePerQty(Double pricePerQty) {
        this.pricePerQty = pricePerQty;
        return this;
    }

    public void setPricePerQty(Double pricePerQty) {
        this.pricePerQty = pricePerQty;
    }

    public Double getTotalPrice() {
        return totalPrice;
    }

    public OrderItem totalPrice(Double totalPrice) {
        this.totalPrice = totalPrice;
        return this;
    }

    public void setTotalPrice(Double totalPrice) {
        this.totalPrice = totalPrice;
    }

    public Double getOffer() {
        return offer;
    }

    public OrderItem offer(Double offer) {
        this.offer = offer;
        return this;
    }

    public void setOffer(Double offer) {
        this.offer = offer;
    }

    public Double getFinalPrice() {
        return finalPrice;
    }

    public OrderItem finalPrice(Double finalPrice) {
        this.finalPrice = finalPrice;
        return this;
    }

    public void setFinalPrice(Double finalPrice) {
        this.finalPrice = finalPrice;
    }

    public Boolean getPanCake() {
        return panCake;
    }

    public OrderItem orderItem(Boolean panCake) {
        this.panCake = panCake;
        return this;
    }

    public void setPanCake(Boolean panCake) {
        this.panCake = panCake;
    }


    public MenuItem getMenuItem() {
        return menuItem;
    }

    public OrderItem menuItem(MenuItem menuItem) {
        this.menuItem = menuItem;
        return this;
    }

    public void setMenuItem(MenuItem menuItem) {
        this.menuItem = menuItem;
    }

    public Set<OrderItemAddon> getOrderItemAddons() {
        return orderItemAddons;
    }

    public OrderItem orderItemAddons(Set<OrderItemAddon> orderItemAddons) {
        this.orderItemAddons = orderItemAddons;
        return this;
    }

    public OrderItem addOrderItemAddon(OrderItemAddon orderItemAddon) {
        this.orderItemAddons.add(orderItemAddon);
        orderItemAddon.setOrderItem(this);
        return this;
    }

    public OrderItem removeOrderItemAddon(OrderItemAddon orderItemAddon) {
        this.orderItemAddons.remove(orderItemAddon);
        orderItemAddon.setOrderItem(null);
        return this;
    }

    public void setOrderItemAddons(Set<OrderItemAddon> orderItemAddons) {
        this.orderItemAddons = orderItemAddons;
    }

    public WaffleOrder getWaffleOrder() {
        return waffleOrder;
    }

    public OrderItem waffleOrder(WaffleOrder waffleOrder) {
        this.waffleOrder = waffleOrder;
        return this;
    }

    public void setWaffleOrder(WaffleOrder waffleOrder) {
        this.waffleOrder = waffleOrder;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        OrderItem orderItem = (OrderItem) o;
        if (orderItem.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), orderItem.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "OrderItem{" +
            "id=" + getId() +
            ", quantity=" + getQuantity() +
            ", pricePerQty=" + getPricePerQty() +
            ", totalPrice=" + getTotalPrice() +
            ", offer=" + getOffer() +
            ", finalPrice=" + getFinalPrice() +
            "}";
    }
}
