package com.hocrox.belgian.repository;

import com.hocrox.belgian.domain.Profile;
import com.hocrox.belgian.domain.User;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import java.util.Optional;


/**
 * Spring Data JPA repository for the Profile entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ProfileRepository extends JpaRepository<Profile, Long>, JpaSpecificationExecutor<Profile> {

    Profile findOneByUser(User user);

    Optional<Profile> findOneByPhoneNumber(String phoneNumber);
}
