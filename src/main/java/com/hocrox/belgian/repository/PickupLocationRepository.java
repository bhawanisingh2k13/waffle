package com.hocrox.belgian.repository;

import com.hocrox.belgian.domain.PickupLocation;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import java.util.List;


/**
 * Spring Data JPA repository for the PickupLocation entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PickupLocationRepository extends JpaRepository<PickupLocation, Long> {

    List<PickupLocation> findAllByEnabled(boolean enable);
}
