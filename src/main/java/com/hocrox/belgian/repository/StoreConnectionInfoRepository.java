package com.hocrox.belgian.repository;

import com.hocrox.belgian.domain.StoreConnectionInfo;
import com.hocrox.belgian.domain.WaffleStore;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import java.util.List;
import java.util.Optional;


/**
 * Spring Data JPA repository for the StoreConnectionInfo entity.
 */
@SuppressWarnings("unused")
@Repository
public interface StoreConnectionInfoRepository extends JpaRepository<StoreConnectionInfo, Long>, JpaSpecificationExecutor<StoreConnectionInfo> {

    List<StoreConnectionInfo> findAllByWaffleStore(WaffleStore waffleStore);

    Optional<StoreConnectionInfo> findOneByConnectionId(String connectionId);
}
