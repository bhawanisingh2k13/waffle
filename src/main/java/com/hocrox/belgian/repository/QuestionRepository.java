package com.hocrox.belgian.repository;

import com.hocrox.belgian.domain.Question;
import com.hocrox.belgian.service.mapper.DeviceMapper;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import java.util.List;


/**
 * Spring Data JPA repository for the Question entity.
 */
@SuppressWarnings("unused")
@Repository
public interface QuestionRepository extends JpaRepository<Question, Long> {

    List<Question> findAllByEnabled(boolean enabled);
}
