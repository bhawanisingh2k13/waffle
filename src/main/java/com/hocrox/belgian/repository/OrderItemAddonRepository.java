package com.hocrox.belgian.repository;

import com.hocrox.belgian.domain.OrderItem;
import com.hocrox.belgian.domain.OrderItemAddon;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import java.util.List;


/**
 * Spring Data JPA repository for the OrderItemAddon entity.
 */
@SuppressWarnings("unused")
@Repository
public interface OrderItemAddonRepository extends JpaRepository<OrderItemAddon, Long> {

    List<OrderItemAddon> findAllByOrderItem(OrderItem orderItem);
}
