package com.hocrox.belgian.repository;

import com.hocrox.belgian.domain.OneTimePassword;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;
import java.util.List;

/**
 * Spring Data JPA repository for the OneTimePassword entity.
 */
@SuppressWarnings("unused")
@Repository
public interface OneTimePasswordRepository extends JpaRepository<OneTimePassword, Long> {

    @Query("select one_time_password from OneTimePassword one_time_password where one_time_password.user.login = ?#{principal.username}")
    List<OneTimePassword> findByUserIsCurrentUser();

    List<OneTimePassword> findAllByPhoneNumber(String phoneNumber);
}
