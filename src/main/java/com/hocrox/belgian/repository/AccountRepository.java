package com.hocrox.belgian.repository;


import com.hocrox.belgian.domain.Account;
import com.hocrox.belgian.domain.enumeration.AccountType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

/**
 * Created by bhawani on 23/8/15.
 */
public interface AccountRepository extends JpaRepository<Account, String> {
	@Query("select account from Account account where account.tokenID = :tokenId and account.accountType = :accountType")
	Optional<Account> getOneUsingAccountToken(@Param(value = "tokenId") String tokenId, @Param(value = "accountType") AccountType accountType);
}
