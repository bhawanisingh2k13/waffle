package com.hocrox.belgian.repository;

import com.hocrox.belgian.domain.OrderItem;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import java.util.List;
import java.util.Optional;


/**
 * Spring Data JPA repository for the OrderItem entity.
 */
@SuppressWarnings("unused")
@Repository
public interface OrderItemRepository extends JpaRepository<OrderItem, Long> {

    Optional<OrderItem> findOneByWaffleOrderCartTokenAndId(String cartToken, Long id);

    List<OrderItem> findAllByWaffleOrderCartTokenAndMenuItemId(String cartToken, Long menuItemId);
}
