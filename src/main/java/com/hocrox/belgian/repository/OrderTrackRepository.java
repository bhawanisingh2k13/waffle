package com.hocrox.belgian.repository;

import com.hocrox.belgian.domain.OrderTrack;
import com.hocrox.belgian.domain.WaffleOrder;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import java.time.Instant;
import java.util.List;
import java.util.Optional;


/**
 * Spring Data JPA repository for the OrderTrack entity.
 */
@SuppressWarnings("unused")
@Repository
public interface OrderTrackRepository extends JpaRepository<OrderTrack, Long> {

    List<OrderTrack> findAllByWaffleOrderIdAndTrackTimeGreaterThanOrderByIdAsc(Long waffleOrderId, Instant lastTime);

    List<OrderTrack> findAllByWaffleOrderIdAndDeliveryBoyAndTrackTimeGreaterThanOrderByIdAsc(Long orderId, boolean deliveryBoy, Instant instant);

    Optional<OrderTrack> findFirstByWaffleOrderAndDeliveryBoy(WaffleOrder waffleOrder, boolean deliveryBoy);

    List<OrderTrack> findAllByWaffleOrderIdAndDeliveryBoyAndLongTrackTimeGreaterThanOrderByIdAsc(Long orderId, boolean isDriver, Long lastTime);

    List<OrderTrack> findTopOneByWaffleOrderIdAndDeliveryBoyOrderByLongTrackTimeDesc(Long orderId, boolean isDriver);
}
