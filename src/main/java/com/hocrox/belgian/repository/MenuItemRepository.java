package com.hocrox.belgian.repository;

import com.hocrox.belgian.domain.MenuItem;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;

/**
 * Spring Data JPA repository for the MenuItem entity.
 */
@SuppressWarnings("unused")
@Repository
public interface MenuItemRepository extends JpaRepository<MenuItem, Long>, JpaSpecificationExecutor<MenuItem> {
    @Query("select distinct menu_item from MenuItem menu_item left join fetch menu_item.addons")
    List<MenuItem> findAllWithEagerRelationships();

    @Query("select menu_item from MenuItem menu_item left join fetch menu_item.addons where menu_item.id =:id")
    MenuItem findOneWithEagerRelationships(@Param("id") Long id);

    List<MenuItem> findAllByCategoryIdOrderByIdDesc(Long catId);

    List<MenuItem> findAllByCategoryIdAndEnabledOrderByIdDesc(Long catId, Boolean enable);

    List<MenuItem> findAllByCategoryIdAndEnabledOrderBySortingIdDesc(Long catId, boolean enable);
}
