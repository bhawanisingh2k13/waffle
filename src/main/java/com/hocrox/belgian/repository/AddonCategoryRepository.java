package com.hocrox.belgian.repository;

import com.hocrox.belgian.domain.AddonCategory;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the AddonCategory entity.
 */
@SuppressWarnings("unused")
@Repository
public interface AddonCategoryRepository extends JpaRepository<AddonCategory, Long>, QueryDslPredicateExecutor<AddonCategory>, JpaSpecificationExecutor<AddonCategory> {

}
