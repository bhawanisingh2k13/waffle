package com.hocrox.belgian.repository;

import com.hocrox.belgian.domain.MenuItemCategory;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the MenuItemCategory entity.
 */
@SuppressWarnings("unused")
@Repository
public interface MenuItemCategoryRepository extends JpaRepository<MenuItemCategory, Long>, QueryDslPredicateExecutor<MenuItemCategory>, JpaSpecificationExecutor<MenuItemCategory> {

}
