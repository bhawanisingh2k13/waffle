package com.hocrox.belgian.repository;

import com.hocrox.belgian.domain.StoreUserProfile;
import com.hocrox.belgian.domain.User;
import com.hocrox.belgian.domain.WaffleStore;
import com.hocrox.belgian.domain.enumeration.StoreRoleType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data JPA repository for the StoreUserProfile entity.
 */
@SuppressWarnings("unused")
@Repository
public interface StoreUserProfileRepository extends JpaRepository<StoreUserProfile, Long>, QueryDslPredicateExecutor<StoreUserProfile>, JpaSpecificationExecutor<StoreUserProfile> {

    @Query("select store_user_profile from StoreUserProfile store_user_profile where store_user_profile.user.login = ?#{principal.username}")
    List<StoreUserProfile> findByUserIsCurrentUser();

    Optional<StoreUserProfile> findOneByUser(User user);

    List<StoreUserProfile> findAllByRoleTypeAndWaffleStore(StoreRoleType storeRoleType, WaffleStore waffleStore);

    List<StoreUserProfile> findAllByRoleTypeAndWaffleStoreId(StoreRoleType storeRoleType, Long waffleStoreId);

}
