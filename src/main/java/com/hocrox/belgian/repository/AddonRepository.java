package com.hocrox.belgian.repository;

import com.hocrox.belgian.domain.Addon;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the Addon entity.
 */
@SuppressWarnings("unused")
@Repository
public interface AddonRepository extends JpaRepository<Addon, Long>, QueryDslPredicateExecutor<Addon>, JpaSpecificationExecutor<Addon> {

}
