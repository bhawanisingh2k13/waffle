package com.hocrox.belgian.repository;

import com.hocrox.belgian.domain.WaffleNotification;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the WaffleNotification entity.
 */
@SuppressWarnings("unused")
@Repository
public interface WaffleNotificationRepository extends JpaRepository<WaffleNotification, Long>, JpaSpecificationExecutor<WaffleNotification> {

}
