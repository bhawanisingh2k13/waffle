package com.hocrox.belgian.repository;

import com.hocrox.belgian.domain.Authority;
import com.hocrox.belgian.domain.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data JPA repository for the Role entity.
 */
@SuppressWarnings("unused")
@Repository
public interface RoleRepository extends JpaRepository<Role, Long>, QueryDslPredicateExecutor<Role> {
    
    @Query("select distinct role from Role role left join fetch role.authorities")
    List<Role> findAllWithEagerRelationships();

    @Query("select role from Role role left join fetch role.authorities where role.id =:id")
    Role findOneWithEagerRelationships(@Param("id") Long id);

    Optional<Role> findOneById(Long id);

    List<Role> findAllByAuthoritiesContaining(List<Authority> authorities);

    @Query("from Role r where r.id = 1")
    Role userRole();
}
