package com.hocrox.belgian.repository;

import com.hocrox.belgian.domain.WaffleUserNotification;
import com.hocrox.belgian.domain.enumeration.NotiStatus;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;

import java.time.Instant;
import java.util.List;

/**
 * Spring Data JPA repository for the WaffleUserNotification entity.
 */
@SuppressWarnings("unused")
@Repository
public interface WaffleUserNotificationRepository extends JpaRepository<WaffleUserNotification, Long>, JpaSpecificationExecutor<WaffleUserNotification> {

    @Query("select distinct waffle_user_notification from WaffleUserNotification waffle_user_notification left join fetch waffle_user_notification.users")
    List<WaffleUserNotification> findAllWithEagerRelationships();

    @Query("select waffle_user_notification from WaffleUserNotification waffle_user_notification left join fetch waffle_user_notification.users where waffle_user_notification.id =:id")
    WaffleUserNotification findOneWithEagerRelationships(@Param("id") Long id);

    List<WaffleUserNotification> findAllByNotiStatusAndScheduledTimeLessThan(NotiStatus notiStatus, Instant instant);

}
