package com.hocrox.belgian.repository;

import com.hocrox.belgian.domain.WaffleStore;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;
import java.util.List;

/**
 * Spring Data JPA repository for the WaffleStore entity.
 */
@SuppressWarnings("unused")
@Repository
public interface WaffleStoreRepository extends JpaRepository<WaffleStore, Long>, QueryDslPredicateExecutor<WaffleStore>, JpaSpecificationExecutor<WaffleStore> {

    @Query("select waffle_store from WaffleStore waffle_store where waffle_store.owner.login = ?#{principal.username}")
    List<WaffleStore> findByOwnerIsCurrentUser();

}
