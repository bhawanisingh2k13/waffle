package com.hocrox.belgian.repository;

import com.hocrox.belgian.domain.Setting;
import com.hocrox.belgian.domain.WaffleStore;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


/**
 * Spring Data JPA repository for the Setting entity.
 */
@SuppressWarnings("unused")
@Repository
public interface SettingRepository extends JpaRepository<Setting, Long> {

    Setting findOneByWaffleStoreId(Long storeId);
    Setting findOneByWaffleStore(WaffleStore waffleStore);
}
