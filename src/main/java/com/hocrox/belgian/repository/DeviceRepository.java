package com.hocrox.belgian.repository;


import com.hocrox.belgian.domain.Device;
import com.hocrox.belgian.domain.User;
import com.hocrox.belgian.domain.enumeration.DeviceType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Created by bhawanisingh on 16/09/15.
 */
@Repository
public interface DeviceRepository extends JpaRepository<Device, Long> {

    Optional<Device> findOneBydeviceID(String id);

    List<Device> findAllByOwner(User owner);

    List<Device> findAllByOwnerAndDeviceType(User user, DeviceType deviceType);

    List<Device> findAllByDeviceType(DeviceType deviceType);

    void deleteByDeviceID(String deviceId);
}
