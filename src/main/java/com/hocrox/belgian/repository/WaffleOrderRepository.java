package com.hocrox.belgian.repository;

import com.hocrox.belgian.domain.Profile;
import com.hocrox.belgian.domain.StoreUserProfile;
import com.hocrox.belgian.domain.WaffleOrder;
import com.hocrox.belgian.domain.WaffleStore;
import com.hocrox.belgian.domain.enumeration.OrderStatus;
import com.hocrox.belgian.service.dto.graph.DateGraph;
import com.hocrox.belgian.service.dto.graph.DeliveryBoyData;
import com.hocrox.belgian.service.dto.graph.InternalDateGraph;
import com.hocrox.belgian.service.mapper.DeviceMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


/**
 * Spring Data JPA repository for the WaffleOrder entity.
 */
@SuppressWarnings("unused")
@Repository
public interface WaffleOrderRepository extends JpaRepository<WaffleOrder, Long>, JpaSpecificationExecutor<WaffleOrder> {

    Page<WaffleOrder> findAllByStoreUserProfileAndOrderStatusNotAndOrderTimeBetweenOrderByIdDesc(StoreUserProfile storeUserProfile, OrderStatus orderStatus, Instant start, Instant end, Pageable pageable);

    Page<WaffleOrder> findAllByStoreUserProfileAndOrderStatusNotInAndOrderTimeBetweenOrderByIdDesc(StoreUserProfile storeUserProfile, ArrayList<OrderStatus> orderStatuses, Instant start, Instant end, Pageable pageable);

    Page<WaffleOrder> findAllByStoreUserProfileAndOrderStatusNotOrderByIdDesc(StoreUserProfile storeUserProfile, OrderStatus orderStatus,  Pageable pageable);

    Page<WaffleOrder> findAllByStoreUserProfileAndOrderStatusNotInOrderByIdDesc(StoreUserProfile storeUserProfile, ArrayList<OrderStatus> orderStatuses,  Pageable pageable);

    Page<WaffleOrder> findAllByProfileAndOrderStatusNotOrderByIdDesc(Profile profile, OrderStatus orderStatus, Pageable pageable);

    Page<WaffleOrder> findAllByProfileAndOrderStatusNotInOrderByIdDesc(Profile profile, ArrayList<OrderStatus> orderStatuses, Pageable pageable);

//    List<WaffleOrder> findAllByStoreUserProfileAndOrderStatusNotInOrderByIdDesc(StoreUserProfile storeUserProfile, ArrayList<OrderStatus> orderStatuses);

    Optional<WaffleOrder> findByPaymentId(String razorPayId);

    Optional<WaffleOrder> findOneById(Long orderId);

    @Query(value = "select new com.hocrox.belgian.service.dto.graph.InternalDateGraph(so.orderTime, sum(so.totalPrice)) from WaffleOrder so where so.orderTime >= :startDate AND so.orderTime <= :endDate AND so.orderStatus != 'PENDING_PAYMENT' AND so.orderStatus != 'CREATING' AND so.orderStatus != 'CANCELLED' group by so.orderTime order by DATE(so.orderTime) ASC ")
    List<InternalDateGraph> graphData(@Param(value = "startDate") Instant startDate, @Param(value = "endDate") Instant endDate);

    @Query(value = "select sum(so.totalPrice) from WaffleOrder so where so.orderTime >= :startDate AND so.orderTime <= :endDate AND so.orderStatus != 'PENDING_PAYMENT' AND so.orderStatus != 'CREATING' AND so.orderStatus != 'CANCELLED'")
    Long totalSale(@Param(value = "startDate") Instant startDate, @Param(value = "endDate") Instant endDate);

    @Query(value = "select count(so) from WaffleOrder so where so.orderTime >= :startDate AND so.orderTime <= :endDate AND so.orderStatus != 'PENDING_PAYMENT' AND so.orderStatus != 'CREATING' AND so.orderStatus != 'CANCELLED'")
    Long totalOrders(@Param(value = "startDate") Instant startDate, @Param(value = "endDate") Instant endDate);

    Optional<WaffleOrder> findOneByCartToken(String cartToken);

    long countByProfileAndOrderStatusNotIn(Profile profile, ArrayList<OrderStatus> orderStatuses);


    @Query(value = "from WaffleOrder so where so.storeUserProfile.id = :storeUserProfileId AND so.waffleStore.id = :waffleStoreId AND so.orderTime >= :startDate AND so.orderTime <= :endDate AND so.orderStatus = 'DELIVERED' AND so.orderType = 'DROP_OFF'")
    List<WaffleOrder> kmsData(@Param(value = "startDate") Instant startDate, @Param(value = "endDate") Instant endDate, @Param(value = "waffleStoreId") Long waffleStoreId, @Param(value = "storeUserProfileId") Long storeUserProfileId);

}
