package com.hocrox.belgian.security;

/**
 * Constants for Spring Security authorities.
 */
public final class AuthoritiesConstants {

    public static final String ADMIN = "ROLE_ADMIN";

    public static final String USER = "user";

    public static final String ANONYMOUS = "anonymous";

    private AuthoritiesConstants() {
    }
}
