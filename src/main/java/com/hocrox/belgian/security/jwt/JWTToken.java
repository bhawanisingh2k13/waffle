package com.hocrox.belgian.security.jwt;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.hocrox.belgian.domain.enumeration.StoreRoleType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by bhawanisingh on 14/12/17 7:05 PM.
 */
@Getter
@Setter
@AllArgsConstructor
public class JWTToken {

    @JsonProperty("id_token")
    private String idToken;

    private boolean haveRestaurant = false;

    private StoreRoleType roleType;

    public JWTToken(String idToken) {
        this.idToken = idToken;
    }
}
