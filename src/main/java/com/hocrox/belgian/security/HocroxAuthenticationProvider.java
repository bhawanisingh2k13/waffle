package com.hocrox.belgian.security;

import com.hocrox.belgian.domain.OneTimePassword;
import com.hocrox.belgian.domain.enumeration.AccountType;
import com.hocrox.belgian.repository.OneTimePasswordRepository;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.cfg.NotYetImplementedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.List;

@Slf4j
public class HocroxAuthenticationProvider implements AuthenticationProvider {

	@Autowired
	private PasswordEncoder passwordEncoder;

	@Autowired
	private DomainUserDetailsService userDetailsService;

	@Autowired
	private OneTimePasswordRepository oneTimePasswordRepository;

	@Override
	public Authentication authenticate(Authentication authentication) throws AuthenticationException {
		HocroxAuthenticationToken token =
			(HocroxAuthenticationToken) authentication;

		String login = (String) token.getPrincipal();
		String tokenPassword = (String) token.getCredentials();
		AccountType loginType = token.getLoginType();
		UserDetails user = null;
		if(loginType == null) {
			loginType = AccountType.NORMAL;
		}
		switch (loginType) {
			case NORMAL:
				//TODO email check
				user = userDetailsService.loadUserByUsername(login);
				if (user == null) {
					throw new UsernameNotFoundException("User does not exists");
				}
				String password = user.getPassword();
				if (!passwordEncoder.matches(tokenPassword, password)) {
					throw new BadCredentialsException("Invalid username/password");
				}
				break;
			case OTP:
				List<OneTimePassword> oneTimePasswordList = oneTimePasswordRepository.findAllByPhoneNumber(login);
				boolean otpMatched = false;
				for(OneTimePassword oneTimePassword : oneTimePasswordList) {
					log.debug("One Time Password : {}", oneTimePassword);
					if (passwordEncoder.matches(tokenPassword, oneTimePassword.getOtp())) {
						otpMatched = true;
						oneTimePasswordRepository.delete(oneTimePassword.getId());
						user = oneTimePassword.getUser();
						break;
					}
				}
				if(!otpMatched) {
					throw new BadCredentialsException("Invalid OTP");
				}
				break;
			default:
				throw  new NotYetImplementedException(loginType + " Yet to be implemented");
		}
		return new HocroxAuthenticationToken(user, user.getPassword(),
			user.getAuthorities());
	}

	@Override
	public boolean supports(Class<?> authentication) {
		return HocroxAuthenticationToken.class.equals(authentication);
	}

}
