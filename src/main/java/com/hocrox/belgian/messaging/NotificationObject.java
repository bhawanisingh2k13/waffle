package com.hocrox.belgian.messaging;

import com.hocrox.belgian.domain.enumeration.NotificationType;
import com.hocrox.belgian.service.dto.WaffleUserNotificationDTO;
import lombok.*;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by hocrox on 09/08/18.
 */
@ToString
@Getter
@Setter
@NoArgsConstructor
public class NotificationObject implements Serializable {
    private static final String DEFAULT_TITLE = "Belgian Waffle";

    @Builder.Default
    private int bck = 0;

    @Builder.Default
    private String body = "";

    @Builder.Default
    private String title = "";

    @Builder.Default
    private String message = "";

    @Builder.Default
    private String notiId = "";

    @Builder.Default
    private long refId = 0L;

    @Builder.Default
    private String imgUrl = "";

    @Builder.Default
    private NotificationType type = NotificationType.BLANK;

    @Builder.Default
    private long apv = -1; // apv is app version

    @Builder.Default
    private long apvt = 0; // -1 Less than apv, 0 exact apv, +1 greater than apv

    public NotificationObject(WaffleUserNotificationDTO waffleUserNotificationDTO) {
        bck = waffleUserNotificationDTO.isBackground() ? 1 : 0;
        message = waffleUserNotificationDTO.getNotiBody();
        title = waffleUserNotificationDTO.getTitle();
        notiId = waffleUserNotificationDTO.getNotiId();
        refId = waffleUserNotificationDTO.getRefId();
        imgUrl = waffleUserNotificationDTO.getImageUrl();
        type = waffleUserNotificationDTO.getType();
        apv = waffleUserNotificationDTO.getApv();
        apvt = waffleUserNotificationDTO.getApvt();
    }

    private String createNotificationMessage() {
        return type.name() + ":" + refId + ":" + message;
    }


    public JSONObject toJSON() {
        JSONObject jsonObject = new JSONObject();
        if(StringUtils.isBlank(message)) bck = 0;
        if(bck !=0) bck = 1;
        if(StringUtils.isBlank(title)) title = DEFAULT_TITLE;
        if(StringUtils.isBlank(notiId)) notiId = System.currentTimeMillis() + "";

        jsonObject.put("bck", bck);
        jsonObject.put("body", createNotificationMessage());
        jsonObject.put("title", title);
        jsonObject.put("message", message);
        jsonObject.put("notiId", notiId);
        jsonObject.put("refId", refId);
        jsonObject.put("imgUrl", imgUrl);
        jsonObject.put("type", type.name());
        jsonObject.put("apv", apv);
        jsonObject.put("apvt", apvt);

        return jsonObject;
    }


}
