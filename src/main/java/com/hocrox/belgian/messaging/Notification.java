package com.hocrox.belgian.messaging;

import com.hocrox.belgian.domain.Device;
import com.hocrox.belgian.domain.User;
import com.hocrox.belgian.domain.enumeration.DeviceType;
import com.hocrox.belgian.domain.enumeration.NotificationType;
import com.hocrox.belgian.messaging.android.FcmNotification;
import com.hocrox.belgian.messaging.apple.AppleNotification;
import com.hocrox.belgian.repository.DeviceRepository;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service
public class Notification {

    @Autowired(required = false)
    private AppleNotification appleNotification;

    @Autowired(required = false)
    private  WebNotification webNotification;

    @Autowired
    private FcmNotification fcmNotification;

    @Autowired
    private DeviceRepository deviceRepository;

    private static final String GCM_NOT_CONFIGURED_ERROR = "Android Messaging Not Configured Properly";
    private static final String APNS_NOT_CONFIGURED_ERROR = "iOS Messaging Not Configured Properly";
    private static final String WEB_NOTIFICATIONS_NOT_CONFIGURED_ERROR = "Web Notification Not Configured Properly";

    public void pushNotification(User user) {
        pushNotification(user, "");
    }

    public void pushNotification(User user, String message) {
        pushNotification(user, message, NotificationType.BLANK.getTitle());
    }

    public void pushNotification(User user, String message, String title) {
        pushNotification(user, message, title, false);
    }

    public void pushNotification(User user, String message, String title, boolean backgroundNotification) {
        pushNotification(user, message, title, backgroundNotification, 0);
    }

    public void pushNotification(User user, String message, String title, boolean backgroundNotification, long referenceId) {
        pushNotification(user, message, title, backgroundNotification, referenceId, System.currentTimeMillis() + "");
    }

    public void pushNotification(User user, String message, String title, boolean backgroundNotification, long referenceId, String notificationId) {
        pushNotification(user, message, title, backgroundNotification, referenceId, notificationId, NotificationType.BLANK);
    }

    public void pushNotification(User user, String message, String title, boolean backgroundNotification, long referenceId, String notificationId, NotificationType notificationType) {
        pushNotification(user, message, title, backgroundNotification, referenceId, notificationId, notificationType, "");
    }

    public void pushNotification(User user, NotificationType notificationType, long referenceId, String message) {
        NotificationObject notificationObject = buildFcmNotificationObject(message, "", false, referenceId, "", notificationType, "");
        pushNotification(user, notificationObject);
    }

    public void pushNotification(User user, String message, String title, boolean backgroundNotification, long referenceId, String notificationId, NotificationType notificationType, String imageUrl) {
        NotificationObject notificationObject = new NotificationObject();
        notificationObject.setMessage(message);
        notificationObject.setTitle(title);
        notificationObject.setBck(backgroundNotification ? 1 : 0);
        notificationObject.setNotiId(notificationId);
        notificationObject.setRefId(referenceId);
        notificationObject.setType(notificationType);
        notificationObject.setImgUrl(imageUrl);
        pushNotification(user, notificationObject);
    }

    public void pushNotification(User user, NotificationObject notificationObject) {
        List<String> androidDeviceIds = getDeviceIdsOfUser(user, DeviceType.ANDROID);
        List<String> iOSDeviceIds = getDeviceIdsOfUser(user, DeviceType.IOS);
        List<String> webDeviceIds = getDeviceIdsOfUser(user, DeviceType.WEB);

        androidPushNotification(notificationObject, androidDeviceIds);
        iOSPushNotification(iOSDeviceIds, notificationObject.getBody());
        try {
            webPushNotification(webDeviceIds, notificationObject.getType(), notificationObject.getRefId(), notificationObject.getMessage());
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void pushNotification(Collection<User> users) {
        pushNotification(users, "");
    }

    public void pushNotification(Collection<User> users, String message) {
        pushNotification(users, message, NotificationType.BLANK.getTitle());
    }

    public void pushNotification(Collection<User> users, String message, String title) {
        pushNotification(users, message, title, false);
    }

    public void pushNotification(Collection<User> users, String message, String title, boolean backgroundNotification) {
        pushNotification(users, message, title, backgroundNotification, 0);
    }

    public void pushNotification(Collection<User> users, String message, String title, boolean backgroundNotification, long referenceId) {
        pushNotification(users, message, title, backgroundNotification, referenceId, System.currentTimeMillis() + "");
    }

    public void pushNotification(Collection<User> users, String message, String title, boolean backgroundNotification, long referenceId, String notificationId) {
        pushNotification(users, message, title, backgroundNotification, referenceId, notificationId, NotificationType.BLANK);
    }

    public void pushNotification(Collection<User> users, String message, String title, boolean backgroundNotification, long referenceId, String notificationId, NotificationType notificationType) {
        pushNotification(users, message, title, backgroundNotification, referenceId, notificationId, notificationType, "");
    }

    public void pushNotification(Collection<User> users, String message, String title, boolean backgroundNotification, long referenceId, String notificationId, NotificationType notificationType, String imageUrl) {
        NotificationObject notificationObject = buildFcmNotificationObject(message, title, backgroundNotification, referenceId, notificationId, notificationType, imageUrl);
        pushNotification(users, notificationObject);
    }

    public void pushNotification(Collection<User> users, NotificationType notificationType, long referenceId, String message) {
        NotificationObject notificationObject = buildFcmNotificationObject(message, "", false, referenceId, "", notificationType, "");
        pushNotification(users, notificationObject);
    }

    public void pushNotification(Collection<User> users, NotificationObject notificationObject) {
        List<String> androidDeviceIds = new ArrayList<>();
        List<String> iOSDeviceIds = new ArrayList<>();
        List<String> webDeviceIds = new ArrayList<>();

        for (User user : users) {
            androidDeviceIds.addAll(getDeviceIdsOfUser(user, DeviceType.ANDROID));
        }
        for (User user : users) {
            iOSDeviceIds.addAll(getDeviceIdsOfUser(user, DeviceType.IOS));
        }
        for (User user : users) {
            webDeviceIds.addAll(getDeviceIdsOfUser(user, DeviceType.WEB));
        }

        androidPushNotification(notificationObject, androidDeviceIds);
        iOSPushNotification(iOSDeviceIds, notificationObject.getBody());
        try {
            webPushNotification(webDeviceIds, notificationObject.getType(), notificationObject.getRefId(), notificationObject.getMessage());
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void broadcastAppUpgrade(DeviceType deviceType, long appVersion, String title, String message) {
        NotificationObject notificationObject = new NotificationObject();
        notificationObject.setApv(appVersion);
        notificationObject.setApvt(-1);
        notificationObject.setTitle(title);
        notificationObject.setMessage(message);
        broadcastMessage(deviceType, notificationObject);
    }

    public void broadcastMessage(DeviceType deviceType) {
        broadcastMessage(deviceType, "");
    }

    public void broadcastMessage(DeviceType deviceType, String message) {
       broadcastMessage(deviceType, message, NotificationType.BLANK.getTitle());
    }

    public void broadcastMessage(DeviceType deviceType, String message, String title) {
        broadcastMessage(deviceType, message, title, false);
    }

    public void broadcastMessage(DeviceType deviceType, String message, String title, boolean backgroundNotification) {
        broadcastMessage(deviceType, message, title, backgroundNotification, 0);
    }

    public void broadcastMessage(DeviceType deviceType, String message, String title, boolean backgroundNotification, long referenceId) {
        broadcastMessage(deviceType, message, title, backgroundNotification, referenceId, System.currentTimeMillis() + "");
    }

    public void broadcastMessage(DeviceType deviceType, String message, String title, boolean backgroundNotification, long referenceId, String notificationId) {
        broadcastMessage(deviceType, message, title, backgroundNotification, referenceId, notificationId, NotificationType.BLANK);
    }

    public void broadcastMessage(DeviceType deviceType, String message, String title, boolean backgroundNotification, long referenceId, String notificationId, NotificationType notificationType) {
        broadcastMessage(deviceType, message, title, backgroundNotification, referenceId, notificationId, notificationType, "");
    }

    public void broadcastMessage(DeviceType deviceType, String message, String title, boolean backgroundNotification, long referenceId, String notificationId, NotificationType notificationType, String imageUrl) {
        NotificationObject notificationObject = buildFcmNotificationObject(message, title, backgroundNotification, referenceId, notificationId, notificationType, imageUrl);
        broadcastMessage(deviceType, notificationObject);
    }

    public void broadcastMessage(DeviceType deviceType, NotificationObject notificationObject) {
        List<String> devices = new ArrayList<>();
        deviceRepository.findAllByDeviceType(deviceType).forEach(device -> devices.add(device.getMessagingID()));
        switch (deviceType) {
            case ANDROID:
                androidPushNotification(notificationObject, devices);
                break;
            case IOS:
                iOSPushNotification(devices, notificationObject.getBody());
                break;
            case WEB:
                try {
                    webPushNotification(devices, NotificationType.BLANK, 0, notificationObject.getMessage());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
        }
    }

    private void androidPushNotification(NotificationObject notificationObject, List<String> deviceIds) {
        if(!deviceIds.isEmpty()) {
            if (fcmNotification == null) throw new NullPointerException(GCM_NOT_CONFIGURED_ERROR);
            try {
                fcmNotification.pushNotification(notificationObject, deviceIds);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void iOSPushNotification(List<String> deviceIds, String message) {

        if(!deviceIds.isEmpty()) {
            if (appleNotification == null) throw new NullPointerException(APNS_NOT_CONFIGURED_ERROR);
            appleNotification.pushNotification(deviceIds, message);
        }
    }

    private void webPushNotification(List<String> deviceIds, NotificationType notificationType, long referenceId, String message) throws JSONException {
        if (!deviceIds.isEmpty()) {
            if (webNotification == null) throw new NullPointerException(WEB_NOTIFICATIONS_NOT_CONFIGURED_ERROR);
            if(notificationType == NotificationType.BLANK) {
                webNotification.pushNotification(deviceIds, new JSONObject()
                        .append("title", message)
                        .append("message", message)
                        .append("url", notificationType.getUrl())
                        .toString());
            } else {
                webNotification.pushNotification(deviceIds, new JSONObject()
                        .append("title", notificationType.getTitle())
                        .append("message", message)
                        .append("url", notificationType.getUrl())
                        .toString());
            }
        }
    }

    private List<String> getDeviceIdsOfUser(User user, DeviceType deviceType) {
        List<Device> devices = deviceRepository.findAllByOwnerAndDeviceType(user, deviceType);
        List<String> androidDeviceIds = devices.stream().map(Device::getMessagingID).collect(Collectors.toList());
        return androidDeviceIds;
    }

    private static NotificationObject buildFcmNotificationObject(String message, String title, boolean backgroundNotification, long referenceId, String notificationId, NotificationType notificationType, String imageUrl) {
        NotificationObject notificationObject = new NotificationObject();
        notificationObject.setMessage(message);
        notificationObject.setTitle(title);
        notificationObject.setBck(backgroundNotification ? 1 : 0);
        notificationObject.setNotiId(notificationId);
        notificationObject.setRefId(referenceId);
        notificationObject.setType(notificationType);
        notificationObject.setImgUrl(imageUrl);
        return notificationObject;
    }

}
