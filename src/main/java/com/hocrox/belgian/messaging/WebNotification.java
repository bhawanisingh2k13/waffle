package com.hocrox.belgian.messaging;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import nl.martijndwars.webpush.PushService;
import nl.martijndwars.webpush.Subscription;
import org.apache.http.HttpResponse;
import org.jose4j.lang.JoseException;
import org.springframework.scheduling.annotation.Async;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.List;
import java.util.concurrent.ExecutionException;

/**
 * Created by bhawanisingh on 16/12/17 11:18 AM.
 */
@Slf4j
@AllArgsConstructor
public class WebNotification {

    private final PushService pushService;

    private final ObjectMapper objectMapper;

    @Async
    public void pushNotification(String token, String payload) {
        try {
            Subscription subscription = objectMapper.readValue(token, Subscription.class);
            nl.martijndwars.webpush.Notification notification = new nl.martijndwars.webpush.Notification(subscription, payload);
            HttpResponse response = pushService.send(notification);
            log.debug("Response : {}", response);
        } catch (IOException | GeneralSecurityException | InterruptedException | JoseException | ExecutionException e) {
            log.debug("exception : {}", e );
        }
    }

    @Async
    public void pushNotification(List<String> tokens, String payload) {
        for(String token : tokens) {
            pushNotification(token, payload);
        }
    }
}
