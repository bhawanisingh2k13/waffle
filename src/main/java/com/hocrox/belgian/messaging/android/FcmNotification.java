package com.hocrox.belgian.messaging.android;

/**
 * Created by bhawanisingh on 09/01/18 2:13 PM.
 */

import com.fasterxml.jackson.databind.ObjectMapper;
import com.hocrox.belgian.messaging.NotificationObject;
import de.bytefish.fcmjava.http.client.IFcmClient;
import de.bytefish.fcmjava.model.options.FcmMessageOptions;
import de.bytefish.fcmjava.requests.notification.NotificationMulticastMessage;
import de.bytefish.fcmjava.requests.notification.NotificationPayload;
import de.bytefish.fcmjava.responses.FcmMessageResponse;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.scheduling.annotation.Async;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.time.Duration;
import java.util.Arrays;
import java.util.List;

/**
 * Created by hocrox on 17/10/17.
 */

@Slf4j
public class FcmNotification {

    private final String url;

    private final String apiKey;

    private final IFcmClient fcmClient;

    private final ObjectMapper objectMapper;

    public FcmNotification(String url, String apiKey, IFcmClient fcmClient, ObjectMapper objectMapper) {
        this.url = url;
        this.apiKey = apiKey;
        this.fcmClient = fcmClient;
        this.objectMapper = objectMapper;
    }

    @Async
    public void pushNotification(List<String> devices, String message) {
        try {
            log.debug("FCM O mai chal reha haan {}", message, Arrays.toString(devices.toArray()));
            // Message Options:
            FcmMessageOptions options = FcmMessageOptions.builder()
                    .setTimeToLive(Duration.ofHours(1))
                    .build();
            NotificationPayload notificationPayload = NotificationPayload.builder()
                    .setTitle(message)
                    .setBody(message)
                    .setClickAction("order_detail_activity")
                    .build();
            // Send a Message:
            NotificationMulticastMessage notificationMulticastMessage = new NotificationMulticastMessage(options, devices, notificationPayload);
            FcmMessageResponse response = this.fcmClient.send(notificationMulticastMessage);
            log.debug("FCM response code, Fail : {} - Success : {}", response.getNumberOfFailure(), response.getNumberOfSuccess());
//            return response.getNumberOfFailure() > 0;

        } catch (Exception e) {
            e.printStackTrace();
//            return false;
        }
    }

    @Async
    public void pushNotification(NotificationObject notificationObject, List<String> deviceIds) {

        JSONObject json = new JSONObject();
        json.put("registration_ids", new JSONArray(deviceIds));

        json.put("data", notificationObject.toJSON());

        log.debug("FCM PUSH DATA : {}", json);

        CloseableHttpClient client = HttpClients.createDefault();

        HttpPost httpPost = new HttpPost(url);

        try {
            StringEntity entity = new StringEntity(json.toString());
            httpPost.setEntity(entity);
            httpPost.setHeader("Accept", "application/json");
            httpPost.setHeader("Content-type", "application/json");
            httpPost.setHeader("Authorization", "key=" + this.apiKey);

            CloseableHttpResponse response = client.execute(httpPost);
            BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
            String line;
            while ((line = rd.readLine()) != null) {
                log.debug("RESPONSE OF FCM : {}", line);
            }
            rd.close();
            response.close();
            client.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
