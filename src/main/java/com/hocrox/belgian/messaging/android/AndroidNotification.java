package com.hocrox.belgian.messaging.android;

import com.hocrox.belgian.messaging.InvalidRequestException;
import lombok.AllArgsConstructor;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.util.List;

/**
 * Created by bhawanisingh on 17/09/15.
 */
@Slf4j
@AllArgsConstructor
public class AndroidNotification {

    @NonNull
    private String GCM_API_KEY;

    @NonNull
    private int RETRIES = -1;

    public boolean pushNotification(String deviceId, String message){
        log.debug("GCM_API_KEY : {}", GCM_API_KEY);
        Sender sender = new Sender(GCM_API_KEY);
        Message msg = new Message.Builder().addData("message",message).build();

        try {
            Result result = sender.send(msg, deviceId, RETRIES);
            if (StringUtils.isBlank(result.getErrorCodeName())) {
                log.debug("GCM Notification is sent successfully : {}", result.toString());
                return true;
            }
            log.error("Error occurred while sending push notification : {}", result.getErrorCodeName());
        } catch (InvalidRequestException e) {
            System.out.println("Invalid Request");
        } catch (IOException e) {
            System.out.println("IO Exception");
        }
        return false;

    }

    public boolean pushNotification(List<String> deviceIds, String message){
        log.debug("GCM_API_KEY : {}", GCM_API_KEY);
        Sender sender = new Sender(GCM_API_KEY);
        Message msg = new Message.Builder().addData("message",message).build();
        try {
            MulticastResult multicastResult = sender.send(msg, deviceIds, RETRIES);

            for (Result result : multicastResult.getResults()) {
                if (StringUtils.isBlank(result.getErrorCodeName()))
                    log.debug("GCM Notification is sent successfully : {}", result.toString());
                else
                    log.error("Error occurred while sending push notification : {}", result.getErrorCodeName());
            }
            return true;
        } catch (InvalidRequestException e) {
            log.error("Invalid Request", e);
        } catch (IOException e) {
            log.error("IO Exception", e);
        }
        return false;
    }
}
