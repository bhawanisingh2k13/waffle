package com.hocrox.belgian.messaging.apple;

import com.notnoop.apns.APNS;
import com.notnoop.apns.ApnsService;
import com.notnoop.apns.PayloadBuilder;
import lombok.AllArgsConstructor;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Async;

import java.util.List;

/**
 * Created by bhawanisingh on 17/09/15.
 */
@Slf4j
@AllArgsConstructor
public class AppleNotification {

    @NonNull
    private ApnsService apnsService;

    @Async
    public void pushNotification(String token, String message) {
        try {
            PayloadBuilder payloadBuilder = APNS.newPayload();
            payloadBuilder = payloadBuilder.badge(1).sound("default").alertBody(message);
            if (payloadBuilder.isTooLong()) {
                payloadBuilder = payloadBuilder.shrinkBody();
            }
            String payload = payloadBuilder.build();
            apnsService.push(token, payload);
//            return true;
        } catch (Exception ex) {
            log.debug("Error While sending push", ex);
//            return false;
        }
    }

    @Async
    public void pushNotification(List<String> token, String message) {
        try {
            PayloadBuilder payloadBuilder = APNS.newPayload();
            String[] messageSplitting = message.split(":", 3);
            String type =  messageSplitting[0];
            String id =  messageSplitting[1];
            String title =  messageSplitting[2];
            payloadBuilder = payloadBuilder.badge(1).sound("default").alertBody(title).customField("completeMessage", message);
            if (payloadBuilder.isTooLong()) {
                payloadBuilder = payloadBuilder.shrinkBody();
            }
            String payload = payloadBuilder.build();
            apnsService.push(token, payload);
//            return true;
        } catch (Exception ex) {
            log.debug("Error While sending push", ex);
//            return false;
        }
    }
}
