package com.hocrox.belgian.service;

import com.hocrox.belgian.domain.StoreUserProfile;
import com.hocrox.belgian.domain.User;
import com.hocrox.belgian.domain.WaffleUserNotification;
import com.hocrox.belgian.domain.enumeration.DeviceType;
import com.hocrox.belgian.domain.enumeration.NotiStatus;
import com.hocrox.belgian.domain.enumeration.NotificationType;
import com.hocrox.belgian.messaging.Notification;
import com.hocrox.belgian.messaging.NotificationObject;
import com.hocrox.belgian.repository.StoreUserProfileRepository;
import com.hocrox.belgian.repository.UserRepository;
import com.hocrox.belgian.repository.WaffleUserNotificationRepository;
import com.hocrox.belgian.service.dto.WaffleUserNotificationDTO;
import com.hocrox.belgian.service.mapper.WaffleUserNotificationMapper;
import com.hocrox.belgian.service.util.RandomUtil;
import com.hocrox.belgian.web.rest.errors.InvalidOperationException;
import com.hocrox.belgian.web.rest.vm.WaffleUserNotificationVM;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.List;
import java.util.Optional;


/**
 * Service Implementation for managing WaffleUserNotification.
 */
@Service
@Transactional
public class WaffleUserNotificationService {

    private final Logger log = LoggerFactory.getLogger(WaffleUserNotificationService.class);

    private final WaffleUserNotificationRepository waffleUserNotificationRepository;

    private final WaffleUserNotificationMapper waffleUserNotificationMapper;

    private final UserService userService;

    private final UserRepository userRepository;

    private final StoreUserProfileRepository storeUserProfileRepository;

    private final Notification notification;

    private final UploadService uploadService;

    public WaffleUserNotificationService(WaffleUserNotificationRepository waffleUserNotificationRepository, WaffleUserNotificationMapper waffleUserNotificationMapper, UserService userService, UserRepository userRepository, StoreUserProfileRepository storeUserProfileRepository, Notification notification, UploadService uploadService) {
        this.waffleUserNotificationRepository = waffleUserNotificationRepository;
        this.waffleUserNotificationMapper = waffleUserNotificationMapper;
        this.userService = userService;
        this.userRepository = userRepository;
        this.storeUserProfileRepository = storeUserProfileRepository;
        this.notification = notification;
        this.uploadService = uploadService;
    }

    /**
     * Save a waffleUserNotification.
     *
     * @param waffleUserNotificationVM the entity to save
     * @return the persisted entity
     */
    public WaffleUserNotificationDTO save(WaffleUserNotificationVM waffleUserNotificationVM) {
        log.debug("Request to save WaffleUserNotification : {}", waffleUserNotificationVM);

        WaffleUserNotification waffleUserNotification = waffleUserNotificationMapper.toEntity(waffleUserNotificationVM);
        User user = userService.currentUser();
        waffleUserNotification = waffleUserNotificationRepository.save(waffleUserNotification);
        return waffleUserNotificationMapper.toDto(waffleUserNotification);
    }

    /**
     * Save a waffleUserNotification.
     *
     * @param waffleUserNotificationVM the entity to save
     * @return the persisted entity
     */
    public WaffleUserNotificationDTO internalSave(WaffleUserNotificationVM waffleUserNotificationVM) {
        log.debug("Request to save WaffleUserNotification : {}", waffleUserNotificationVM);
        WaffleUserNotification waffleUserNotification = waffleUserNotificationMapper.toEntity(waffleUserNotificationVM);
        waffleUserNotification = waffleUserNotificationRepository.save(waffleUserNotification);
        return waffleUserNotificationMapper.toDto(waffleUserNotification);
    }

    /**
     * Get all the waffleUserNotifications.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<WaffleUserNotificationDTO> findAll(Pageable pageable) {
        log.debug("Request to get all WaffleUserNotifications");
        return waffleUserNotificationRepository.findAll(pageable)
            .map(waffleUserNotificationMapper::toDto);
    }

    /**
     * Get one waffleUserNotification by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public WaffleUserNotificationDTO findOne(Long id) {
        log.debug("Request to get WaffleUserNotification : {}", id);
        WaffleUserNotification waffleUserNotification = waffleUserNotificationRepository.findOneWithEagerRelationships(id);
        return waffleUserNotificationMapper.toDto(waffleUserNotification);
    }

    /**
     * Delete the waffleUserNotification by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete WaffleUserNotification : {}", id);
        waffleUserNotificationRepository.delete(id);
    }

    public void fireNotificationSingle(String username, String message, boolean image) {
        Optional<User> userOptional = userRepository.findOneByLogin(username);
        if(userOptional.isPresent()){
            log.debug("USER IS : {}", userOptional.get());
            NotificationObject notificationObject = new NotificationObject();
            notificationObject.setMessage(message);
            notificationObject.setType(NotificationType.BLANK);
            if(image)   notificationObject.setImgUrl(uploadService.getObjectPublicUrl("4490753429142777_778bb21f4fdd588ff6ec4d7a330110e6.png"));
            notification.pushNotification(userOptional.get(), notificationObject);
        }
    }

    public void fireNotification(String message) {
        log.debug("Notification fired : {}", message);
        notification.broadcastMessage(DeviceType.ANDROID, message);
    }

    /**
     *
     * This is scheduled to get fired every minute.
     *
     */
    @Scheduled(cron = "0 * * * * *")
    public void fireScheduledNotifications() {
        List <WaffleUserNotification> waffleUserNotifications = waffleUserNotificationRepository.findAllByNotiStatusAndScheduledTimeLessThan(NotiStatus.PENDING, Instant.now());
        waffleUserNotifications.forEach(waffleUserNotification -> {
            NotificationObject notificationObject = new NotificationObject(waffleUserNotificationMapper.toDto(waffleUserNotification));
            if(waffleUserNotification.isToAll()) notification.broadcastMessage(DeviceType.ANDROID, notificationObject);
            else notification.pushNotification(waffleUserNotification.getUsers(), notificationObject);
            waffleUserNotification.setNotiStatus(NotiStatus.DELIVERED);
            waffleUserNotification.setSentOn(Instant.now());
            waffleUserNotificationRepository.save(waffleUserNotification);
            //TODO MANAGE NOTIFICTIONS SO THAT THEIR RESULTS ARE RECORDED
        });
    }
}
