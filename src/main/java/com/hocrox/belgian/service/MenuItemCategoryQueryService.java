package com.hocrox.belgian.service;


import com.hocrox.belgian.domain.MenuItemCategory;
import com.hocrox.belgian.domain.MenuItemCategory_;
import com.hocrox.belgian.domain.QMenuItemCategory;
import com.hocrox.belgian.repository.MenuItemCategoryRepository;
import com.hocrox.belgian.service.dto.MenuItemCategoryDTO;
import com.hocrox.belgian.service.mapper.MenuItemCategoryMapper;
import com.hocrox.belgian.web.rest.vm.search.MenuItemCategorySearch;
import com.hocrox.belgian.web.rest.vm.search.criteria.MenuItemCategoryCriteria;
import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.OrderSpecifier;
import com.querydsl.core.types.Predicate;
import io.github.jhipster.service.QueryService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.data.querydsl.QPageRequest;
import org.springframework.data.querydsl.QSort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Service for executing complex queries for MenuItemCategory entities in the database.
 * The main input is a {@link MenuItemCategorySearch} which get's converted to {@link Specifications},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link MenuItemCategoryDTO} or a {@link Page} of {@link MenuItemCategoryDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class MenuItemCategoryQueryService extends QueryService<MenuItemCategory> {

    private final Logger log = LoggerFactory.getLogger(MenuItemCategoryQueryService.class);


    private final MenuItemCategoryRepository menuItemCategoryRepository;

    private final MenuItemCategoryMapper menuItemCategoryMapper;

    public MenuItemCategoryQueryService(MenuItemCategoryRepository menuItemCategoryRepository, MenuItemCategoryMapper menuItemCategoryMapper) {
        this.menuItemCategoryRepository = menuItemCategoryRepository;
        this.menuItemCategoryMapper = menuItemCategoryMapper;
    }

    /**
     * Return a {@link List} of {@link MenuItemCategoryDTO} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<MenuItemCategoryDTO> findByCriteria(MenuItemCategoryCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specifications<MenuItemCategory> specification = createSpecification(criteria);
        return menuItemCategoryMapper.toDto(menuItemCategoryRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link MenuItemCategoryDTO} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<MenuItemCategoryDTO> findByCriteria(MenuItemCategoryCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specifications<MenuItemCategory> specification = createSpecification(criteria);
        final Page<MenuItemCategory> result = menuItemCategoryRepository.findAll(specification, page);
        return result.map(menuItemCategoryMapper::toDto);
    }

    /**
     * Function to convert MenuItemCategoryCriteria to a {@link Specifications}
     */
    private Specifications<MenuItemCategory> createSpecification(MenuItemCategoryCriteria criteria) {
        Specifications<MenuItemCategory> specification = Specifications.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), MenuItemCategory_.id));
            }
            if (criteria.getName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getName(), MenuItemCategory_.name));
            }
        }
        return specification;
    }

    /**
     * Return a {@link List} of {@link MenuItemCategoryDTO} which matches the menuItemCategorySearch from the database
     * @param menuItemCategorySearch The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<MenuItemCategoryDTO> findByPredicate(MenuItemCategorySearch menuItemCategorySearch) {
        log.debug("find by menuItemCategorySearch : {}", menuItemCategorySearch);
        final Predicate predicate = createPredicate(menuItemCategorySearch, true);
        return menuItemCategoryMapper.toDto(menuItemCategoryRepository.findAll(predicate, defaultSort()));
    }

    /**
     * Return a {@link Page} of {@link MenuItemCategoryDTO} which matches the menuItemCategorySearch from the database
     * @param menuItemCategorySearch The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<MenuItemCategoryDTO> findByPredicate(MenuItemCategorySearch menuItemCategorySearch, Pageable page) {
        log.debug("find by menuItemCategorySearch : {}, page: {}", menuItemCategorySearch, page);
        final Predicate specification = createPredicate(menuItemCategorySearch, true);
        final Page<MenuItemCategory> result = menuItemCategoryRepository.findAll(specification, defaultPageable(page));
        return result.map(menuItemCategoryMapper::toDto);
    }

    @Transactional(readOnly = true)
    public Page<MenuItemCategoryDTO> findByPredicateComplete(MenuItemCategorySearch menuItemCategorySearch, Pageable page) {
        log.debug("find by menuItemCategorySearch : {}, page: {}", menuItemCategorySearch, page);
        final Predicate specification = createPredicate(menuItemCategorySearch, false);
        final Page<MenuItemCategory> result = menuItemCategoryRepository.findAll(specification, defaultPageable(page));
        return result.map(menuItemCategoryMapper::toDto);
    }

    /**
     * Function to convert MenuItemCategoryCriteria to a {@link Predicate}
     */
    private Predicate createPredicate(MenuItemCategorySearch addonSearch, boolean includeEnable) {
        QMenuItemCategory addon = QMenuItemCategory.menuItemCategory;
        BooleanBuilder booleanBuilder = new BooleanBuilder();
        if(addonSearch.getId() != null) {
            booleanBuilder.and(addon.id.eq(addonSearch.getId()));
        }
        if(addonSearch.getName() != null) {
            booleanBuilder.and(addon.name.contains(addonSearch.getName()));
        }
        if(includeEnable) {
            booleanBuilder.and(addon.enabled.eq(true));
        }
        return booleanBuilder;
    }

    private QSort defaultSort() {
        OrderSpecifier<Long> sortId = QMenuItemCategory.menuItemCategory.id.desc();
        return new QSort(sortId);
    }

    private QPageRequest defaultPageable(Pageable pageable) {
        return new QPageRequest(pageable.getPageNumber(), pageable.getPageSize(), defaultSort());
    }

}
