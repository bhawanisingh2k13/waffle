package com.hocrox.belgian.service;

import com.hocrox.belgian.domain.enumeration.AccountType;
import com.hocrox.belgian.security.HocroxAuthenticationToken;
import com.hocrox.belgian.security.jwt.JWTToken;
import com.hocrox.belgian.security.jwt.TokenProvider;
import com.hocrox.belgian.web.rest.vm.LoginVM;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.Valid;

/**
 * Created by bhawanisingh on 14/12/17 7:37 PM.
 */
@Slf4j
@Service
public class UserJWTService {


    private final TokenProvider tokenProvider;

    private final AuthenticationManager authenticationManager;


    public  UserJWTService(TokenProvider tokenProvider, AuthenticationManager authenticationManager) {
        this.tokenProvider = tokenProvider;
        this.authenticationManager = authenticationManager;
    }

    @Transactional
    public JWTToken getSecurityTokenAfterAuthentication(@Valid LoginVM loginVM) {
        String username = loginVM.getUsername();
        String password = loginVM.getPassword();
        AccountType accountType = loginVM.getAccountType();

        HocroxAuthenticationToken token = new HocroxAuthenticationToken(username, password, accountType);
        Authentication authentication = this.authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        String jwt = tokenProvider.createToken(authentication, loginVM.getRememberMe());
        return new JWTToken(jwt);
    }
}
