package com.hocrox.belgian.service.util;

import com.google.maps.DistanceMatrixApi;
import com.google.maps.DistanceMatrixApiRequest;
import com.google.maps.GeoApiContext;
import com.google.maps.errors.ApiException;
import com.google.maps.model.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.RandomStringUtils;

import java.io.File;
import java.io.IOException;
import java.time.ZoneOffset;

/**
 * Utility class for generating random Strings.
 */
@Slf4j
public final class RandomUtil {

    public static final java.time.ZoneOffset IST =  ZoneOffset.of("+05:30");
    public static final java.time.ZoneOffset UTC =  ZoneOffset.of("+00:00");
    private static final int DEF_COUNT = 6;
    private static final int OTP_DEF_COUNT = 16;
    private static final int NOTI_DEF_COUNT = 16;
    private static final int LOGIN_DEF_COUNT = 10;
    private static final int CART_TOKEN_COUNT = 50;

    private static final GeoApiContext context = new GeoApiContext.Builder().apiKey("GOOGLE_API_KEY").build();

    private RandomUtil() {
    }

    /**
     * Generate a password.
     *
     * @return the generated password
     */
    public static String generatePassword() {
        return RandomStringUtils.randomAlphanumeric(DEF_COUNT);
    }

    /**
     * Generate an activation key.
     *
     * @return the generated activation key
     */
    public static String generateActivationKey() {
        return RandomStringUtils.randomNumeric(DEF_COUNT);
    }

    /**
     * Generate a reset key.
     *
     * @return the generated reset key
     */
    public static String generateResetKey() {
        return RandomStringUtils.randomNumeric(DEF_COUNT);
    }

    public static String generateOTPKey() {
        return RandomStringUtils.randomNumeric(OTP_DEF_COUNT);
    }

    public static String generateNotiKey() {
        return RandomStringUtils.randomAlphanumeric(NOTI_DEF_COUNT);
    }

    public static String generateUsername() {
        return RandomStringUtils.randomNumeric(LOGIN_DEF_COUNT);
    }

    public static String generateFileName(File file) {
        return System.nanoTime() + "_" + DigestUtils.md5Hex(file.getName()) + "." + FilenameUtils.getExtension(file.getAbsolutePath());
    }

    public static String generateCartToken() {
        return RandomStringUtils.randomAlphanumeric(CART_TOKEN_COUNT);
    }
    /**
     * Calculate Distance & time between lat lng
     */

    public static String[] calculateTime(double firstLat, double firstLong, double secondLat, double secondLong) {
        DistanceMatrixApiRequest distanceMatrixApiRequest  = DistanceMatrixApi.newRequest(context)
                .origins(new LatLng(firstLat, firstLong))
                .destinations(new LatLng(secondLat, secondLong))
                .mode(TravelMode.DRIVING);
        final String[] timeItWillTake = new String[2];
        try {
            DistanceMatrix distanceMatrix = distanceMatrixApiRequest.await();
            for(DistanceMatrixRow row : distanceMatrix.rows) {
                DistanceMatrixElement[] elements = row.elements;
                for (DistanceMatrixElement element : elements) {
                    timeItWillTake[0] = element.duration.humanReadable;
                    timeItWillTake[1] = element.distance.humanReadable;
                    break;
                }
                break;
            }
        } catch (ApiException | InterruptedException | IOException | NullPointerException e) {
            e.printStackTrace();
        }
        return timeItWillTake;
    }

    public static long calculateDistance(double firstLat, double firstLong, double secondLat, double secondLong) {
        DistanceMatrixApiRequest distanceMatrixApiRequest  = DistanceMatrixApi.newRequest(context)
            .origins(new LatLng(firstLat, firstLong))
            .destinations(new LatLng(secondLat, secondLong))
            .mode(TravelMode.DRIVING);
        try {
            DistanceMatrix distanceMatrix = distanceMatrixApiRequest.await();
            for(DistanceMatrixRow row : distanceMatrix.rows) {
                DistanceMatrixElement[] elements = row.elements;
                for (DistanceMatrixElement element : elements) {
                    return element.distance.inMeters;
                }
            }
        } catch (ApiException | InterruptedException | IOException | NullPointerException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public static float round(float value) {
        return round(value, 2);
    }

    public static double round(double value) {
        return round(value, 2);
    }

    public static double round(double value, int scale) {
        int pow = 10;
        for (int i = 1; i < scale; i++) {
            pow *= 10;
        }
        double tmp = value * pow;
        double tmpSub = tmp - (int) tmp;

        return ((double) ((int) (
            value >= 0
                ? (tmpSub >= 0.5f ? tmp + 1 : tmp)
                : (tmpSub >= -0.5f ? tmp : tmp - 1)
        ))) / pow;
    }

    public static float round(float value, int scale) {
        int pow = 10;
        for (int i = 1; i < scale; i++) {
            pow *= 10;
        }
        float tmp = value * pow;
        float tmpSub = tmp - (int) tmp;

        return ((float) ((int) (
            value >= 0
                ? (tmpSub >= 0.5f ? tmp + 1 : tmp)
                : (tmpSub >= -0.5f ? tmp : tmp - 1)
        ))) / pow;
    }
}
