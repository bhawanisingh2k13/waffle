package com.hocrox.belgian.service;

import com.hocrox.belgian.domain.StoreUserProfile;
import com.hocrox.belgian.domain.User;
import com.hocrox.belgian.domain.WaffleNotification;
import com.hocrox.belgian.repository.StoreUserProfileRepository;
import com.hocrox.belgian.repository.WaffleNotificationRepository;
import com.hocrox.belgian.service.dto.WaffleNotificationDTO;
import com.hocrox.belgian.service.mapper.WaffleNotificationMapper;
import com.hocrox.belgian.web.rest.errors.InvalidOperationException;
import com.hocrox.belgian.web.rest.vm.WaffleNotificationVM;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;


/**
 * Service Implementation for managing WaffleNotification.
 */
@Service
@Transactional
public class WaffleNotificationService {

    private final Logger log = LoggerFactory.getLogger(WaffleNotificationService.class);

    private final WaffleNotificationRepository waffleNotificationRepository;

    private final WaffleNotificationMapper waffleNotificationMapper;


    private final UserService userService;

    private final StoreUserProfileRepository storeUserProfileRepository;

    public WaffleNotificationService(WaffleNotificationRepository waffleNotificationRepository, WaffleNotificationMapper waffleNotificationMapper, UserService userService, StoreUserProfileRepository storeUserProfileRepository) {
        this.waffleNotificationRepository = waffleNotificationRepository;
        this.waffleNotificationMapper = waffleNotificationMapper;
        this.userService = userService;
        this.storeUserProfileRepository = storeUserProfileRepository;
    }

    /**
     * Save a waffleNotification.
     *
     * @param waffleNotificationVM the entity to save
     * @return the persisted entity
     */
    public WaffleNotificationDTO save(WaffleNotificationVM waffleNotificationVM) {
        log.debug("Request to save WaffleNotification : {}", waffleNotificationVM);
        WaffleNotification waffleNotification = waffleNotificationMapper.toEntity(waffleNotificationVM);
        User user = userService.currentUser();
        Optional<StoreUserProfile> storeUserProfileOptional = storeUserProfileRepository.findOneByUser(user);
        if(!storeUserProfileOptional.isPresent()) {
            throw new InvalidOperationException("You are not allowed for this operation");
        }
        StoreUserProfile storeUserProfile = storeUserProfileOptional.get();
        if(waffleNotification.getWaffleStore() != null && !waffleNotification.getWaffleStore().equals(storeUserProfile.getWaffleStore())) {
            throw new InvalidOperationException("You can't change the notification of other store");
        }
        if(waffleNotification.getWaffleStore() == null) {
            waffleNotification.setWaffleStore(storeUserProfile.getWaffleStore());
        }
        waffleNotification = waffleNotificationRepository.save(waffleNotification);
        return waffleNotificationMapper.toDto(waffleNotification);
    }

    /**
     * Get all the waffleNotifications.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<WaffleNotificationDTO> findAll(Pageable pageable) {
        log.debug("Request to get all WaffleNotifications");
        return waffleNotificationRepository.findAll(pageable)
            .map(waffleNotificationMapper::toDto);
    }

    /**
     * Get one waffleNotification by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public WaffleNotificationDTO findOne(Long id) {
        log.debug("Request to get WaffleNotification : {}", id);
        WaffleNotification waffleNotification = waffleNotificationRepository.findOne(id);
        return waffleNotificationMapper.toDto(waffleNotification);
    }

    /**
     * Delete the waffleNotification by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete WaffleNotification : {}", id);
        waffleNotificationRepository.delete(id);
    }
}
