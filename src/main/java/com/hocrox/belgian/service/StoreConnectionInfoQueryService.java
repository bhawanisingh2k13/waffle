package com.hocrox.belgian.service;


import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.hocrox.belgian.domain.StoreConnectionInfo;
import com.hocrox.belgian.domain.*; // for static metamodels
import com.hocrox.belgian.repository.StoreConnectionInfoRepository;
import com.hocrox.belgian.service.dto.StoreConnectionInfoCriteria;

import com.hocrox.belgian.service.dto.StoreConnectionInfoDTO;
import com.hocrox.belgian.service.mapper.StoreConnectionInfoMapper;

/**
 * Service for executing complex queries for StoreConnectionInfo entities in the database.
 * The main input is a {@link StoreConnectionInfoCriteria} which get's converted to {@link Specifications},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link StoreConnectionInfoDTO} or a {@link Page} of {@link StoreConnectionInfoDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class StoreConnectionInfoQueryService extends QueryService<StoreConnectionInfo> {

    private final Logger log = LoggerFactory.getLogger(StoreConnectionInfoQueryService.class);


    private final StoreConnectionInfoRepository storeConnectionInfoRepository;

    private final StoreConnectionInfoMapper storeConnectionInfoMapper;

    public StoreConnectionInfoQueryService(StoreConnectionInfoRepository storeConnectionInfoRepository, StoreConnectionInfoMapper storeConnectionInfoMapper) {
        this.storeConnectionInfoRepository = storeConnectionInfoRepository;
        this.storeConnectionInfoMapper = storeConnectionInfoMapper;
    }

    /**
     * Return a {@link List} of {@link StoreConnectionInfoDTO} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<StoreConnectionInfoDTO> findByCriteria(StoreConnectionInfoCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specifications<StoreConnectionInfo> specification = createSpecification(criteria);
        return storeConnectionInfoMapper.toDto(storeConnectionInfoRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link StoreConnectionInfoDTO} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<StoreConnectionInfoDTO> findByCriteria(StoreConnectionInfoCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specifications<StoreConnectionInfo> specification = createSpecification(criteria);
        final Page<StoreConnectionInfo> result = storeConnectionInfoRepository.findAll(specification, page);
        return result.map(storeConnectionInfoMapper::toDto);
    }

    /**
     * Function to convert StoreConnectionInfoCriteria to a {@link Specifications}
     */
    private Specifications<StoreConnectionInfo> createSpecification(StoreConnectionInfoCriteria criteria) {
        Specifications<StoreConnectionInfo> specification = Specifications.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), StoreConnectionInfo_.id));
            }
            if (criteria.getConnectionId() != null) {
                specification = specification.and(buildStringSpecification(criteria.getConnectionId(), StoreConnectionInfo_.connectionId));
            }
            if (criteria.getWaffleStoreId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getWaffleStoreId(), StoreConnectionInfo_.waffleStore, WaffleStore_.id));
            }
        }
        return specification;
    }

}
