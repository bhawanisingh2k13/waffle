package com.hocrox.belgian.service;

import com.hocrox.belgian.domain.Profile;
import com.hocrox.belgian.domain.User;
import com.hocrox.belgian.domain.UserAddress;
import com.hocrox.belgian.repository.ProfileRepository;
import com.hocrox.belgian.repository.UserAddressRepository;
import com.hocrox.belgian.service.dto.UserAddressDTO;
import com.hocrox.belgian.service.mapper.UserAddressMapper;
import com.hocrox.belgian.web.rest.vm.UserAddressVM;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * Service Implementation for managing UserAddress.
 */
@Service
@Transactional
public class UserAddressService {

    private final Logger log = LoggerFactory.getLogger(UserAddressService.class);

    private final UserAddressRepository userAddressRepository;

    private final UserService userService;

    private final ProfileRepository profileRepository;

    private final UserAddressMapper userAddressMapper;

    public UserAddressService(UserAddressRepository userAddressRepository, UserService userService, ProfileRepository profileRepository, UserAddressMapper userAddressMapper) {
        this.userAddressRepository = userAddressRepository;
        this.userService = userService;
        this.profileRepository = profileRepository;
        this.userAddressMapper = userAddressMapper;
    }

    /**
     * Save a userAddress.
     *
     * @param userAddressVM the entity to save
     * @return the persisted entity
     */
    public UserAddressDTO save(UserAddressVM userAddressVM) {
        log.debug("Request to save UserAddress : {}", userAddressVM);
        UserAddress userAddress = userAddressMapper.toEntity(userAddressVM);
        User user = userService.currentUser();
        Profile profile = profileRepository.findOneByUser(user);
        userAddress.setProfile(profile);
        userAddress = userAddressRepository.save(userAddress);
        return userAddressMapper.toDto(userAddress);
    }

    /**
     * Get all the userAddresses.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<UserAddressDTO> findAll(Pageable pageable) {
        log.debug("Request to get all UserAddresses");
        return userAddressRepository.findAllByProfileUser(userService.currentUser(), pageable)
            .map(userAddressMapper::toDto);
    }

    /**
     * Get one userAddress by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public UserAddressDTO findOne(Long id) {
        log.debug("Request to get UserAddress : {}", id);
        UserAddress userAddress = userAddressRepository.findOne(id);
        return userAddressMapper.toDto(userAddress);
    }

    /**
     * Delete the userAddress by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete UserAddress : {}", id);
        userAddressRepository.delete(id);
    }
}
