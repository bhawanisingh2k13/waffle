package com.hocrox.belgian.service;


import com.hocrox.belgian.domain.WaffleNotification;
import com.hocrox.belgian.domain.WaffleNotification_;
import com.hocrox.belgian.domain.WaffleStore_;
import com.hocrox.belgian.repository.WaffleNotificationRepository;
import com.hocrox.belgian.service.dto.WaffleNotificationCriteria;
import com.hocrox.belgian.service.dto.WaffleNotificationDTO;
import com.hocrox.belgian.service.mapper.WaffleNotificationMapper;
import io.github.jhipster.service.QueryService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Service for executing complex queries for WaffleNotification entities in the database.
 * The main input is a {@link WaffleNotificationCriteria} which get's converted to {@link Specifications},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link WaffleNotificationDTO} or a {@link Page} of {@link WaffleNotificationDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class WaffleNotificationQueryService extends QueryService<WaffleNotification> {

    private final Logger log = LoggerFactory.getLogger(WaffleNotificationQueryService.class);


    private final WaffleNotificationRepository waffleNotificationRepository;

    private final WaffleNotificationMapper waffleNotificationMapper;

    public WaffleNotificationQueryService(WaffleNotificationRepository waffleNotificationRepository, WaffleNotificationMapper waffleNotificationMapper) {
        this.waffleNotificationRepository = waffleNotificationRepository;
        this.waffleNotificationMapper = waffleNotificationMapper;
    }

    /**
     * Return a {@link List} of {@link WaffleNotificationDTO} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<WaffleNotificationDTO> findByCriteria(WaffleNotificationCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specifications<WaffleNotification> specification = createSpecification(criteria);
        return waffleNotificationMapper.toDto(waffleNotificationRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link WaffleNotificationDTO} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<WaffleNotificationDTO> findByCriteria(WaffleNotificationCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specifications<WaffleNotification> specification = createSpecification(criteria);
        final Page<WaffleNotification> result = waffleNotificationRepository.findAll(specification, page);
        return result.map(waffleNotificationMapper::toDto);
    }

    /**
     * Function to convert WaffleNotificationCriteria to a {@link Specifications}
     */
    private Specifications<WaffleNotification> createSpecification(WaffleNotificationCriteria criteria) {
        Specifications<WaffleNotification> specification = Specifications.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), WaffleNotification_.id));
            }
            if (criteria.getNotiBody() != null) {
                specification = specification.and(buildStringSpecification(criteria.getNotiBody(), WaffleNotification_.notiBody));
            }
            if (criteria.getTitle() != null) {
                specification = specification.and(buildStringSpecification(criteria.getTitle(), WaffleNotification_.title));
            }
            if (criteria.getImage() != null) {
                specification = specification.and(buildStringSpecification(criteria.getImage(), WaffleNotification_.image));
            }
            if (criteria.getWaffleStoreId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getWaffleStoreId(), WaffleNotification_.waffleStore, WaffleStore_.id));
            }
        }
        return specification;
    }

}
