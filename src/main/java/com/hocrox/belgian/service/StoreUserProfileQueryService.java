package com.hocrox.belgian.service;


import com.hocrox.belgian.domain.*;
import com.hocrox.belgian.repository.StoreUserProfileRepository;
import com.hocrox.belgian.service.dto.StoreUserProfileDTO;
import com.hocrox.belgian.service.mapper.StoreUserProfileMapper;
import com.hocrox.belgian.web.rest.vm.search.StoreUserProfileSearch;
import com.hocrox.belgian.web.rest.vm.search.criteria.StoreUserProfileCriteria;
import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.OrderSpecifier;
import com.querydsl.core.types.Predicate;
import io.github.jhipster.service.QueryService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.data.querydsl.QPageRequest;
import org.springframework.data.querydsl.QSort;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service for executing complex queries for StoreUserProfile entities in the database.
 * The main input is a {@link StoreUserProfileSearch} which get's converted to {@link Specifications},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link StoreUserProfileDTO} or a {@link Page} of {@link StoreUserProfileDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class StoreUserProfileQueryService extends QueryService<StoreUserProfile> {

    private final Logger log = LoggerFactory.getLogger(StoreUserProfileQueryService.class);


    private final UserService userService;

    private final StoreUserProfileRepository storeUserProfileRepository;

    private final StoreUserProfileMapper storeUserProfileMapper;

    public StoreUserProfileQueryService(UserService userService, StoreUserProfileRepository storeUserProfileRepository, StoreUserProfileMapper storeUserProfileMapper) {
        this.userService = userService;
        this.storeUserProfileRepository = storeUserProfileRepository;
        this.storeUserProfileMapper = storeUserProfileMapper;
    }

    /**
     * Return a {@link List} of {@link StoreUserProfileDTO} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<StoreUserProfileDTO> findByCriteria(StoreUserProfileCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specifications<StoreUserProfile> specification = createSpecification(criteria);
        return storeUserProfileMapper.toDto(storeUserProfileRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link StoreUserProfileDTO} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<StoreUserProfileDTO> findByCriteria(StoreUserProfileCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specifications<StoreUserProfile> specification = createSpecification(criteria);
        final Page<StoreUserProfile> result = storeUserProfileRepository.findAll(specification, page);
        return result.map(storeUserProfileMapper::toDto);
    }

    /**
     * Function to convert StoreUserProfileCriteria to a {@link Specifications}
     */
    private Specifications<StoreUserProfile> createSpecification(StoreUserProfileCriteria criteria) {

        User user = userService.currentUser();
        Optional<StoreUserProfile> storeUserProfileOptional = storeUserProfileRepository.findOneByUser(user);
        if(!storeUserProfileOptional.isPresent()) {
            throw new AccessDeniedException("You are not the authorized person");
        }

        Specifications<StoreUserProfile> specification = Specifications.where(equalsSpecification(StoreUserProfile_.waffleStore, WaffleStore_.id, storeUserProfileOptional.get().getWaffleStore().getId()));
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), StoreUserProfile_.id));
            }
            if (criteria.getPhoneNumber() != null) {
                specification = specification.and(buildStringSpecification(criteria.getPhoneNumber(), StoreUserProfile_.phoneNumber));
            }
            if (criteria.getRoleType() != null) {
                specification = specification.and(buildSpecification(criteria.getRoleType(), StoreUserProfile_.roleType));
            }
            if (criteria.getUserId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getUserId(), StoreUserProfile_.user, User_.id));
            }
            if (criteria.getWaffleStoreId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getWaffleStoreId(), StoreUserProfile_.waffleStore, WaffleStore_.id));
            }
        }
        return specification;
    }

    /**
     * Return a {@link List} of {@link StoreUserProfileDTO} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<StoreUserProfileDTO> findByPredicate(StoreUserProfileSearch criteria) {
        log.debug("find by criteria : {}", criteria);
        final Predicate predicate = createPredicate(criteria);
        return storeUserProfileMapper.toDto(storeUserProfileRepository.findAll(predicate, defaultSort()));
    }

    /**
     * Return a {@link Page} of {@link StoreUserProfileDTO} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<StoreUserProfileDTO> findByPredicate(StoreUserProfileSearch criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Predicate predicate = createPredicate(criteria);
        final Page<StoreUserProfile> result = storeUserProfileRepository.findAll(predicate, defaultPageable(page));
        return result.map(storeUserProfileMapper::toDto);
    }

    /**
     * Function to convert StoreUserProfileCriteria to a {@link Specifications}
     */
    private Predicate createPredicate(StoreUserProfileSearch storeUserProfileSearch) {

        User user = userService.currentUser();
        Optional<StoreUserProfile> storeUserProfileOptional = storeUserProfileRepository.findOneByUser(user);
        if(!storeUserProfileOptional.isPresent()) {
            throw new AccessDeniedException("You are not the authorized person");
        }

        QStoreUserProfile qStoreUserProfile = QStoreUserProfile.storeUserProfile;

        BooleanBuilder booleanBuilder = new BooleanBuilder();

        booleanBuilder.and( qStoreUserProfile.waffleStore.id.eq(storeUserProfileOptional.get().getWaffleStore().getId()));
        if(storeUserProfileSearch.getId() != null) {
            booleanBuilder.and(qStoreUserProfile.id.eq(storeUserProfileSearch.getId()));
        }
        if(storeUserProfileSearch.getRoleType() != null) {
            booleanBuilder.and(qStoreUserProfile.roleType.eq(storeUserProfileSearch.getRoleType()));
        }
        if(storeUserProfileSearch.getPhoneNumber() != null) {
            booleanBuilder.and(qStoreUserProfile.phoneNumber.contains(storeUserProfileSearch.getPhoneNumber()));
        }
        if(storeUserProfileSearch.getRoleType() != null) {
            booleanBuilder.and(qStoreUserProfile.roleType.eq(storeUserProfileSearch.getRoleType()));
        }
        if(storeUserProfileSearch.getWaffleStoreId() != null) {
            booleanBuilder.and(qStoreUserProfile.waffleStore.id.eq(storeUserProfileSearch.getWaffleStoreId()));
        }
        if(storeUserProfileSearch.getUserId() != null) {
            booleanBuilder.and(qStoreUserProfile.user.id.eq(storeUserProfileSearch.getUserId()));
        }
        return booleanBuilder;
    }

    private QSort defaultSort() {
        OrderSpecifier<Long> sortId = QStoreUserProfile.storeUserProfile.id.desc();
        return new QSort(sortId);
    }

    private QPageRequest defaultPageable(Pageable pageable) {
        return new QPageRequest(pageable.getPageNumber(), pageable.getPageSize(), defaultSort());
    }

}
