package com.hocrox.belgian.service;


import com.hocrox.belgian.domain.*;
import com.hocrox.belgian.domain.enumeration.OrderStatus;
import com.hocrox.belgian.domain.enumeration.StoreRoleType;
import com.hocrox.belgian.repository.StoreUserProfileRepository;
import com.hocrox.belgian.repository.WaffleOrderRepository;
import com.hocrox.belgian.service.dto.WaffleOrderDTO;
import com.hocrox.belgian.service.dto.graph.DateGraph;
import com.hocrox.belgian.service.dto.graph.InternalDateGraph;
import com.hocrox.belgian.service.dto.graph.StoreStatsDTO;
import com.hocrox.belgian.service.mapper.WaffleOrderMapper;
import com.hocrox.belgian.service.util.RandomUtil;
import com.hocrox.belgian.web.rest.vm.search.criteria.WaffleOrderCriteria;
import io.github.jhipster.service.QueryService;
import io.github.jhipster.service.filter.LocalDateFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.metamodel.SingularAttribute;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalAdjusters;
import java.util.*;

/**
 * Service for executing complex queries for WaffleOrder entities in the database.
 * The main input is a {@link WaffleOrderCriteria} which get's converted to {@link Specifications},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link WaffleOrderDTO} or a {@link Page} of {@link WaffleOrderDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class WaffleOrderQueryService extends QueryService<WaffleOrder> {

    private final Logger log = LoggerFactory.getLogger(WaffleOrderQueryService.class);

    private final UserService userService;

    private final StoreUserProfileRepository storeUserProfileRepository;

    private final WaffleOrderRepository waffleOrderRepository;

    private final WaffleOrderMapper waffleOrderMapper;

    public WaffleOrderQueryService(UserService userService, StoreUserProfileRepository storeUserProfileRepository, WaffleOrderRepository waffleOrderRepository, WaffleOrderMapper waffleOrderMapper) {
        this.userService = userService;
        this.storeUserProfileRepository = storeUserProfileRepository;
        this.waffleOrderRepository = waffleOrderRepository;
        this.waffleOrderMapper = waffleOrderMapper;
    }

    /**
     * Return a {@link List} of {@link WaffleOrderDTO} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<WaffleOrderDTO> findByCriteria(WaffleOrderCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specifications<WaffleOrder> specification = createSpecification(criteria);
        return waffleOrderMapper.toDto(waffleOrderRepository.findAll(specification, new Sort(Sort.Direction.DESC, "id")));
    }

    /**
     * Return a {@link Page} of {@link WaffleOrderDTO} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<WaffleOrderDTO> findByCriteria(WaffleOrderCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specifications<WaffleOrder> specification = createSpecification(criteria);
        final Page<WaffleOrder> result = waffleOrderRepository.findAll(specification, new PageRequest(page.getPageNumber(), page.getPageSize(), new Sort(Sort.Direction.DESC, "id")));
        return result.map(waffleOrderMapper::toDto);
    }

    @Transactional(readOnly = true)
    public Page<WaffleOrderDTO> findByCriteriaDriver(LocalDate startDate, LocalDate endDate, Pageable page) {
        final Specifications<WaffleOrder> specification = createDriverSpecification(startDate, endDate);
        final Page<WaffleOrder> result = waffleOrderRepository.findAll(specification, new PageRequest(page.getPageNumber(), page.getPageSize(), new Sort(Sort.Direction.DESC, "id")));
        log.debug("RESULT : {}", result.getTotalElements());
        return result.map(waffleOrderMapper::toDto);
    }

    @Transactional(readOnly = true)
    public StoreStatsDTO getStats(LocalDate localDate) {
        Instant startDate = localDate.atStartOfDay().toInstant(RandomUtil.IST);
        Instant endDate = localDate.atTime(LocalTime.MAX).toInstant(RandomUtil.IST);
        Instant startGraphDate = localDate.with(TemporalAdjusters.firstDayOfMonth()).atStartOfDay().toInstant(RandomUtil.IST);
        Instant endGraphDate = localDate.with(TemporalAdjusters.lastDayOfMonth()).atTime(LocalTime.MAX).toInstant(RandomUtil.IST);
        Long totalDaySale = waffleOrderRepository.totalSale(startDate, endDate);
        Long totalDayOrders = waffleOrderRepository.totalOrders(startDate, endDate);
        Long totalMonthSale = waffleOrderRepository.totalSale(startGraphDate, endGraphDate);
        Long totalMonthOrders = waffleOrderRepository.totalOrders(startGraphDate, endGraphDate);
        List<InternalDateGraph> internalGraphData = waffleOrderRepository.graphData(startGraphDate, endGraphDate);
        List<DateGraph> graphData = new ArrayList<>();
        for(Instant sDate = startGraphDate; sDate.isBefore(endGraphDate) || sDate.equals(endGraphDate);) {
            final Instant eDate = sDate.plus(1, ChronoUnit.DAYS);
            final Instant tsDate = sDate;
            double count = internalGraphData.stream()
                .filter(gd ->  (gd.getValue().isAfter(tsDate) || gd.getValue().equals(tsDate)) && gd.getValue().isBefore(eDate))
                .mapToDouble(InternalDateGraph::getCount).sum();
                graphData.add(new DateGraph(Date.from(sDate), count));
            sDate = sDate.plus(1, ChronoUnit.DAYS);
        }
        Collections.sort(graphData);
        return new StoreStatsDTO(totalDayOrders, totalDaySale, totalMonthOrders, totalMonthSale, graphData);
    }
    /**
     * Function to convert WaffleOrderCriteria to a {@link Specifications}
     */
    private Specifications<WaffleOrder> createSpecification(WaffleOrderCriteria criteria) {
        User user = userService.currentUser();
        Optional<StoreUserProfile> storeUserProfileOptional = storeUserProfileRepository.findOneByUser(user);
        if(!storeUserProfileOptional.isPresent()) {
            throw new AccessDeniedException("You are not the authorized person");
        }
        ArrayList<OrderStatus> orderStatuses = new ArrayList<>();
        orderStatuses.add(OrderStatus.PENDING_PAYMENT);
        orderStatuses.add(OrderStatus.CREATING);
//        Specifications<WaffleOrder> specification = Specifications.where(notEqualsSpecification(WaffleOrder_.orderStatus, OrderStatus.PENDING_PAYMENT));
        Specifications<WaffleOrder> specification = Specifications.where(notInSpecification(WaffleOrder_.orderStatus, orderStatuses));

        specification = specification.and(equalsSpecification(WaffleOrder_.waffleStore, WaffleStore_.id, storeUserProfileOptional.get().getWaffleStore().getId()));

        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), WaffleOrder_.id));
            }
            if (criteria.getName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getName(), WaffleOrder_.name));
            }
            if (criteria.getPhoneNumber() != null) {
                specification = specification.and(buildStringSpecification(criteria.getPhoneNumber(), WaffleOrder_.phoneNumber));
            }
            if (criteria.getEmail() != null) {
                specification = specification.and(buildStringSpecification(criteria.getEmail(), WaffleOrder_.email));
            }
            if (criteria.getOrderType() != null) {
                specification = specification.and(buildSpecification(criteria.getOrderType(), WaffleOrder_.orderType));
            }
            if (criteria.getPayementType() != null) {
                specification = specification.and(buildSpecification(criteria.getPayementType(), WaffleOrder_.paymentType));
            }
            if (criteria.getCouponCode() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCouponCode(), WaffleOrder_.couponCode));
            }
            if (criteria.getExpectedDeliveryTime() != null) {
                specification = specification.and(localDateSpecification(criteria.getExpectedDeliveryTime(), WaffleOrder_.expectedDeliveryTime));
            }
            if (criteria.getDeliveryTime() != null) {
                specification = specification.and(localDateSpecification(criteria.getDeliveryTime(), WaffleOrder_.deliveryTime));
            }
            if (criteria.getOrderTime() != null) {
                specification = specification.and(localDateSpecification(criteria.getOrderTime(), WaffleOrder_.orderTime));
            }
//            if (criteria.getOrderStatus() != null) {
//                specification = specification.and(buildSpecification(criteria.getOrderStatus(), WaffleOrder_.orderStatus));
//            }
            if (criteria.getDiscount() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDiscount(), WaffleOrder_.discount));
            }
            if (criteria.getCgst() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getCgst(), WaffleOrder_.cgst));
            }
            if (criteria.getSgst() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getSgst(), WaffleOrder_.sgst));
            }
            if (criteria.getTotalPrice() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getTotalPrice(), WaffleOrder_.totalPrice));
            }
            if (criteria.getFinalPrice() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getFinalPrice(), WaffleOrder_.finalPrice));
            }
            if (criteria.getOrderItemId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getOrderItemId(), WaffleOrder_.orderItems, OrderItem_.id));
            }
            if (criteria.getUserAddressId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getUserAddressId(), WaffleOrder_.userAddress, UserAddress_.id));
            }
            if (criteria.getStoreUserProfileId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getStoreUserProfileId(), WaffleOrder_.storeUserProfile, StoreUserProfile_.id));
            }
            if (criteria.getProfileId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getProfileId(), WaffleOrder_.profile, Profile_.id));
            }
        }
        return specification;
    }

    private Specifications<WaffleOrder> createDriverSpecification(LocalDate startDate, LocalDate endDate) {
        User driver = userService.currentUser();
        Optional<StoreUserProfile> storeUserProfileOptional = storeUserProfileRepository.findOneByUser(driver);
        if(!storeUserProfileOptional.isPresent() || storeUserProfileOptional.get().getRoleType() != StoreRoleType.DELIVERY) {
            throw new AccessDeniedException("You are not driver");
        }
        StoreUserProfile storeUserProfile = storeUserProfileOptional.get();
        ArrayList<OrderStatus> orderStatuses = new ArrayList<>();
        orderStatuses.add(OrderStatus.CREATING);
        orderStatuses.add(OrderStatus.PENDING_PAYMENT);
        orderStatuses.add(OrderStatus.PLACED);
        orderStatuses.add(OrderStatus.PREPARING);
//        Specifications<WaffleOrder> specification = Specifications.where(notEqualsSpecification(WaffleOrder_.orderStatus, OrderStatus.PENDING_PAYMENT));
        Specifications<WaffleOrder> specification = Specifications.where(notInSpecification(WaffleOrder_.orderStatus, orderStatuses));
        specification = specification.and(equalsSpecification(WaffleOrder_.waffleStore, WaffleStore_.id, storeUserProfile.getWaffleStore().getId()));
        specification = specification.and(equalsSpecification(WaffleOrder_.storeUserProfile, StoreUserProfile_.id, storeUserProfile.getId()));
        LocalDateFilter startDateFilter = new LocalDateFilter();
        if(startDate != null && endDate != null) {
            startDateFilter.setGreaterOrEqualThan(startDate);
            startDateFilter.setLessThan(endDate);
        } else if (startDate != null) {
            startDateFilter.setEquals(startDate);
        }
        specification = specification.and(localDateSpecification(startDateFilter, WaffleOrder_.orderTime));
        return specification;
    }
    protected <X> Specification<WaffleOrder> notEqualsSpecification(SingularAttribute<? super WaffleOrder, X> field, final X value) {
        return (root, query, builder) -> builder.notEqual(root.get(field), value);
    }

    protected <X> Specification<WaffleOrder> notInSpecification(SingularAttribute<? super WaffleOrder, X> field, final List<X> value) {
        return (root, query, builder) -> builder.not(root.get(field).in(value));
    }

    protected <X> Specification<WaffleOrder> localDateSpecification(LocalDateFilter localDateFilter, SingularAttribute<? super WaffleOrder, X> field) {
        if(localDateFilter.getGreaterOrEqualThan() != null && localDateFilter.getLessThan() != null) {
            return (root, query, builder) -> builder.between(root.get(field.getName()), localDateFilter.getGreaterOrEqualThan().atStartOfDay().toInstant(RandomUtil.IST), localDateFilter.getLessThan().atTime(LocalTime.MAX).toInstant(RandomUtil.IST));
        }
        if(localDateFilter.getGreaterThan() != null && localDateFilter.getLessThan() != null) {
            return (root, query, builder) -> builder.between(root.get(field.getName()), localDateFilter.getGreaterThan().atStartOfDay().toInstant(RandomUtil.IST), localDateFilter.getLessThan().atTime(LocalTime.MAX).toInstant(RandomUtil.IST));
        }
        if(localDateFilter.getEquals() != null) {
            return (root, query, builder) -> builder.between(root.get(field.getName()), localDateFilter.getEquals().atStartOfDay().toInstant(RandomUtil.IST), localDateFilter.getEquals().atTime(LocalTime.MAX).toInstant(RandomUtil.IST));
        }
        if(localDateFilter.getGreaterOrEqualThan() != null) {
            return (root, query, builder) -> builder.greaterThanOrEqualTo(root.get(field.getName()), localDateFilter.getGreaterOrEqualThan().atStartOfDay().toInstant(RandomUtil.IST));
        }
        if(localDateFilter.getGreaterThan() != null) {
            return (root, query, builder) -> builder.greaterThan(root.get(field.getName()), localDateFilter.getGreaterThan().atStartOfDay().toInstant(RandomUtil.IST));
        }
        if(localDateFilter.getLessOrEqualThan() != null) {
            return (root, query, builder) -> builder.lessThanOrEqualTo(root.get(field.getName()), localDateFilter.getLessOrEqualThan().atTime(23, 59, 59).toInstant(RandomUtil.IST));
        }
        if(localDateFilter.getLessThan() != null) {
            return (root, query, builder) -> builder.lessThan(root.get(field.getName()), localDateFilter.getLessThan().atTime(23, 59, 59).toInstant(RandomUtil.IST));
        }
        return null;
    }

    private static Date getZeroTimeDate(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        date = calendar.getTime();
        return date;
    }

    @Transactional(readOnly = true)
    public Map<String, String> getDeliveryKms(LocalDate localDate) {
        Instant startDate = localDate.with(TemporalAdjusters.firstDayOfMonth()).atStartOfDay().toInstant(RandomUtil.IST);
        Instant endDate = localDate.with(TemporalAdjusters.lastDayOfMonth()).atTime(LocalTime.MAX).toInstant(RandomUtil.IST);

        Map<String, String> distanceMap = new HashMap<>();
        ArrayList<OrderStatus> orderStatuses = new ArrayList<>();
        orderStatuses.add(OrderStatus.PENDING_PAYMENT);
        orderStatuses.add(OrderStatus.CREATING);
        orderStatuses.add(OrderStatus.CANCELLED);
        List<StoreUserProfile> storeUserProfiles = storeUserProfileRepository.findAllByRoleTypeAndWaffleStoreId(StoreRoleType.DELIVERY, 1L);
        for(StoreUserProfile storeUserProfile : storeUserProfiles) {
            WaffleStore waffleStore = storeUserProfile.getWaffleStore();
            List<WaffleOrder> waffleOrders = waffleOrderRepository.kmsData(startDate, endDate, storeUserProfile.getWaffleStore().getId(), storeUserProfile.getId());
            String kms  = "";
            for(WaffleOrder waffleOrder : waffleOrders) {
                Long dst = RandomUtil.calculateDistance(waffleStore.getLat(), waffleStore.getLng(), waffleOrder.getUserAddress().getLat(), waffleOrder.getUserAddress().getLng());
                if(dst != null) kms +=  " + " + (dst * (2/1000.0));
            }
            distanceMap.put(storeUserProfile.getUser().getFullName(), kms);
        }
        return distanceMap;
    }
}
