package com.hocrox.belgian.service;


import com.hocrox.belgian.domain.AddonCategory;
import com.hocrox.belgian.domain.AddonCategory_;
import com.hocrox.belgian.domain.QAddonCategory;
import com.hocrox.belgian.repository.AddonCategoryRepository;
import com.hocrox.belgian.service.dto.AddonCategoryDTO;
import com.hocrox.belgian.service.mapper.AddonCategoryMapper;
import com.hocrox.belgian.web.rest.vm.search.AddonCategorySearch;
import com.hocrox.belgian.web.rest.vm.search.criteria.AddonCategoryCriteria;
import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.OrderSpecifier;
import com.querydsl.core.types.Predicate;
import io.github.jhipster.service.QueryService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.data.querydsl.QPageRequest;
import org.springframework.data.querydsl.QSort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Service for executing complex queries for AddonCategory entities in the database.
 * The main input is a {@link AddonCategorySearch} which get's converted to {@link Specifications},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link AddonCategoryDTO} or a {@link Page} of {@link AddonCategoryDTO} which fulfills the criteria.
 */
@Slf4j
@Service
@Transactional(readOnly = true)
public class AddonCategoryQueryService extends QueryService<AddonCategory> {

//    private final Logger log = LoggerFactory.getLogger(AddonCategoryQueryService.class);


    private final AddonCategoryRepository addonCategoryRepository;

    private final AddonCategoryMapper addonCategoryMapper;

    public AddonCategoryQueryService(AddonCategoryRepository addonCategoryRepository, AddonCategoryMapper addonCategoryMapper) {
        this.addonCategoryRepository = addonCategoryRepository;
        this.addonCategoryMapper = addonCategoryMapper;
    }

    /**
     * Return a {@link List} of {@link AddonCategoryDTO} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<AddonCategoryDTO> findByCriteria(AddonCategoryCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specifications<AddonCategory> specification = createSpecification(criteria);
        return addonCategoryMapper.toDto(addonCategoryRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link AddonCategoryDTO} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<AddonCategoryDTO> findByCriteria(AddonCategoryCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specifications<AddonCategory> specification = createSpecification(criteria);
        final Page<AddonCategory> result = addonCategoryRepository.findAll(specification, page);
        return result.map(addonCategoryMapper::toDto);
    }

    /**
     * Function to convert AddonCategoryCriteria to a {@link Specifications}
     */
    private Specifications<AddonCategory> createSpecification(AddonCategoryCriteria criteria) {
        Specifications<AddonCategory> specification = Specifications.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), AddonCategory_.id));
            }
            if (criteria.getName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getName(), AddonCategory_.name));
            }
        }
        return specification;
    }

    /**
     * Return a {@link List} of {@link AddonCategoryDTO} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<AddonCategoryDTO> findByPredicate(AddonCategorySearch criteria) {
        log.debug("find by criteria : {}", criteria);
        final Predicate predicate = createPredicate(criteria, true);
        return addonCategoryMapper.toDto(addonCategoryRepository.findAll(predicate, defaultSort()));
    }

    /**
     * Return a {@link Page} of {@link AddonCategoryDTO} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<AddonCategoryDTO> findByPredicate(AddonCategorySearch criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Predicate predicate = createPredicate(criteria, true);
        final Page<AddonCategory> result = addonCategoryRepository.findAll(predicate, defaultPageable(page));
        return result.map(addonCategoryMapper::toDto);
    }

    @Transactional(readOnly = true)
    public Page<AddonCategoryDTO> findByPredicateComplete(AddonCategorySearch criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Predicate predicate = createPredicate(criteria, false);
        final Page<AddonCategory> result = addonCategoryRepository.findAll(predicate, defaultPageable(page));
        return result.map(addonCategoryMapper::toDto);
    }

    /**
     * Function to convert AddonCategorySearch to a {@link Predicate}
     */
    private Predicate createPredicate(AddonCategorySearch addonCategorySearch, boolean includeEnable) {
        QAddonCategory addonCategory = QAddonCategory.addonCategory;
        BooleanBuilder booleanBuilder = new BooleanBuilder();
        if(addonCategorySearch.getId() != null) {
            booleanBuilder.and(addonCategory.id.eq(addonCategorySearch.getId()));
        }
        if(addonCategorySearch.getName() != null) {
            booleanBuilder.and(addonCategory.name.contains(addonCategorySearch.getName()));
        }
        if(includeEnable) {
            booleanBuilder.and(addonCategory.enabled.eq(true));
        }
        return booleanBuilder;
    }

    private QSort defaultSort() {
        OrderSpecifier<Long> sortId = QAddonCategory.addonCategory.id.desc();
        return new QSort(sortId);
    }

    private QPageRequest defaultPageable(Pageable pageable) {
        return new QPageRequest(pageable.getPageNumber(), pageable.getPageSize(), defaultSort());
    }
}
