package com.hocrox.belgian.service;


import com.hocrox.belgian.domain.Addon;
import com.hocrox.belgian.domain.AddonCategory_;
import com.hocrox.belgian.domain.Addon_;
import com.hocrox.belgian.domain.QAddon;
import com.hocrox.belgian.repository.AddonRepository;
import com.hocrox.belgian.service.dto.AddonDTO;
import com.hocrox.belgian.service.mapper.AddonMapper;
import com.hocrox.belgian.web.rest.vm.search.AddonSearch;
import com.hocrox.belgian.web.rest.vm.search.criteria.AddonCriteria;
import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.OrderSpecifier;
import com.querydsl.core.types.Predicate;
import io.github.jhipster.service.QueryService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.data.querydsl.QPageRequest;
import org.springframework.data.querydsl.QSort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Service for executing complex queries for Addon entities in the database.
 * The main input is a {@link AddonSearch} which get's converted to {@link Specifications},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link AddonDTO} or a {@link Page} of {@link AddonDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class AddonQueryService extends QueryService<Addon> {

    private final Logger log = LoggerFactory.getLogger(AddonQueryService.class);


    private final AddonRepository addonRepository;

    private final AddonMapper addonMapper;

    public AddonQueryService(AddonRepository addonRepository, AddonMapper addonMapper) {
        this.addonRepository = addonRepository;
        this.addonMapper = addonMapper;
    }

    /**
     * Return a {@link List} of {@link AddonDTO} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<AddonDTO> findByCriteria(AddonCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specifications<Addon> specification = createSpecification(criteria);
        return addonMapper.toDto(addonRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link AddonDTO} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<AddonDTO> findByCriteria(AddonCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specifications<Addon> specification = createSpecification(criteria);
        final Page<Addon> result = addonRepository.findAll(specification, page);
        return result.map(addonMapper::toDto);
    }

    /**
     * Function to convert AddonCriteria to a {@link Specifications}
     */
    private Specifications<Addon> createSpecification(AddonCriteria criteria) {
        Specifications<Addon> specification = Specifications.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), Addon_.id));
            }
            if (criteria.getName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getName(), Addon_.name));
            }
            if (criteria.getPrice() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getPrice(), Addon_.price));
            }
            if (criteria.getCategoryId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getCategoryId(), Addon_.category, AddonCategory_.id));
            }
        }
        return specification;
    }

    /**
     * Return a {@link List} of {@link AddonDTO} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<AddonDTO> findByPredicate(AddonSearch criteria) {
        log.debug("find by criteria : {}", criteria);
        final Predicate predicate = createPredicate(criteria, true);
        return addonMapper.toDto(addonRepository.findAll(predicate, defaultSort()));
    }

    /**
     * Return a {@link Page} of {@link AddonDTO} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<AddonDTO> findByPredicate(AddonSearch criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Predicate predicate = createPredicate(criteria, true);
        final Page<Addon> result = addonRepository.findAll(predicate, defaultPageable(page));
        return result.map(addonMapper::toDto);
    }

    @Transactional(readOnly = true)
    public Page<AddonDTO> findByPredicateComplete(AddonSearch criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Predicate predicate = createPredicate(criteria, false);
        final Page<Addon> result = addonRepository.findAll(predicate, defaultPageable(page));
        return result.map(addonMapper::toDto);
    }

    /**
     * Function to convert AddonCriteria to a {@link Specifications}
     */
    private Predicate createPredicate(AddonSearch addonSearch, boolean includeEnable) {
        QAddon addon = QAddon.addon;
        BooleanBuilder booleanBuilder = new BooleanBuilder();
        if(addonSearch.getId() != null) {
            booleanBuilder.and(addon.id.eq(addonSearch.getId()));
        }
        if(addonSearch.getName() != null) {
            booleanBuilder.and(addon.name.contains(addonSearch.getName()));
        }
        if(addonSearch.getPrice() != null) {
            booleanBuilder.and(addon.price.goe(addonSearch.getPrice()));
        }
        if(addonSearch.getCategoryId() != null) {
            booleanBuilder.and(addon.category.id.eq(addonSearch.getCategoryId()));
        }
        if(includeEnable) {
            booleanBuilder.and(addon.enabled.eq(true));
        }
        return booleanBuilder;
    }

    private QSort defaultSort() {
        OrderSpecifier<Long> sortId = QAddon.addon.id.desc();
        return new QSort(sortId);
    }

    private QPageRequest defaultPageable(Pageable pageable) {
        return new QPageRequest(pageable.getPageNumber(), pageable.getPageSize(), defaultSort());
    }

}
