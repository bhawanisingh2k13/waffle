package com.hocrox.belgian.service;


import com.hocrox.belgian.domain.Profile_;
import com.hocrox.belgian.domain.User;
import com.hocrox.belgian.domain.UserAddress;
import com.hocrox.belgian.domain.UserAddress_;
import com.hocrox.belgian.repository.UserAddressRepository;
import com.hocrox.belgian.service.dto.UserAddressDTO;
import com.hocrox.belgian.service.mapper.UserAddressMapper;
import com.hocrox.belgian.web.rest.vm.search.criteria.UserAddressCriteria;
import io.github.jhipster.service.QueryService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Service for executing complex queries for UserAddress entities in the database.
 * The main input is a {@link UserAddressCriteria} which get's converted to {@link Specifications},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link UserAddressDTO} or a {@link Page} of {@link UserAddressDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class UserAddressQueryService extends QueryService<UserAddress> {

    private final Logger log = LoggerFactory.getLogger(UserAddressQueryService.class);

    private final UserService userService;

    private final UserAddressRepository userAddressRepository;

    private final UserAddressMapper userAddressMapper;

    public UserAddressQueryService(UserService userService, UserAddressRepository userAddressRepository, UserAddressMapper userAddressMapper) {
        this.userService = userService;
        this.userAddressRepository = userAddressRepository;
        this.userAddressMapper = userAddressMapper;
    }

    /**
     * Return a {@link List} of {@link UserAddressDTO} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<UserAddressDTO> findByCriteria(UserAddressCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specifications<UserAddress> specification = createSpecification(criteria);
        return userAddressMapper.toDto(userAddressRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link UserAddressDTO} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<UserAddressDTO> findByCriteria(UserAddressCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specifications<UserAddress> specification = createSpecification(criteria);
        final Page<UserAddress> result = userAddressRepository.findAll(specification, page);
        return result.map(userAddressMapper::toDto);
    }

    /**
     * Function to convert UserAddressCriteria to a {@link Specifications}
     */
    private Specifications<UserAddress> createSpecification(UserAddressCriteria criteria) {
        User user = userService.currentUser();
        Specifications<UserAddress> specification = Specifications.where(equalsSpecification(UserAddress_.profile, Profile_.user, user));

//        Specifications<UserAddress> specification = Specifications.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), UserAddress_.id));
            }
            if (criteria.getStreet() != null) {
                specification = specification.and(buildStringSpecification(criteria.getStreet(), UserAddress_.street));
            }
            if (criteria.getFlat() != null) {
                specification = specification.and(buildStringSpecification(criteria.getFlat(), UserAddress_.flat));
            }
            if (criteria.getLat() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getLat(), UserAddress_.lat));
            }
            if (criteria.getLng() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getLng(), UserAddress_.lng));
            }
            if (criteria.getCity() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCity(), UserAddress_.city));
            }
            if (criteria.getState() != null) {
                specification = specification.and(buildStringSpecification(criteria.getState(), UserAddress_.state));
            }
            if (criteria.getCountry() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCountry(), UserAddress_.country));
            }
            if (criteria.getFromLocation() != null) {
                specification = specification.and(buildSpecification(criteria.getFromLocation(), UserAddress_.fromLocation));
            }
            if (criteria.getProfileId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getProfileId(), UserAddress_.profile, Profile_.id));
            }
        }
        return specification;
    }

}
