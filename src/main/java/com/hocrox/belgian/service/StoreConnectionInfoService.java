package com.hocrox.belgian.service;

import com.hocrox.belgian.domain.StoreConnectionInfo;
import com.hocrox.belgian.repository.StoreConnectionInfoRepository;
import com.hocrox.belgian.service.dto.StoreConnectionInfoDTO;
import com.hocrox.belgian.service.mapper.StoreConnectionInfoMapper;
import com.hocrox.belgian.web.rest.vm.StoreConnectionInfoVM;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * Service Implementation for managing StoreConnectionInfo.
 */
@Service
@Transactional
public class StoreConnectionInfoService {

    private final Logger log = LoggerFactory.getLogger(StoreConnectionInfoService.class);

    private final StoreConnectionInfoRepository storeConnectionInfoRepository;

    private final StoreConnectionInfoMapper storeConnectionInfoMapper;

    public StoreConnectionInfoService(StoreConnectionInfoRepository storeConnectionInfoRepository, StoreConnectionInfoMapper storeConnectionInfoMapper) {
        this.storeConnectionInfoRepository = storeConnectionInfoRepository;
        this.storeConnectionInfoMapper = storeConnectionInfoMapper;
    }

    /**
     * Save a storeConnectionInfo.
     *
     * @param storeConnectionInfoVM the entity to save
     * @return the persisted entity
     */
    public StoreConnectionInfoDTO save(StoreConnectionInfoVM storeConnectionInfoVM) {
        log.debug("Request to save StoreConnectionInfo : {}", storeConnectionInfoVM);
        StoreConnectionInfo storeConnectionInfo = storeConnectionInfoMapper.toEntity(storeConnectionInfoVM);
        storeConnectionInfo = storeConnectionInfoRepository.save(storeConnectionInfo);
        return storeConnectionInfoMapper.toDto(storeConnectionInfo);
    }

    /**
     * Get all the storeConnectionInfos.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<StoreConnectionInfoDTO> findAll(Pageable pageable) {
        log.debug("Request to get all StoreConnectionInfos");
        return storeConnectionInfoRepository.findAll(pageable)
            .map(storeConnectionInfoMapper::toDto);
    }

    /**
     * Get one storeConnectionInfo by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public StoreConnectionInfoDTO findOne(Long id) {
        log.debug("Request to get StoreConnectionInfo : {}", id);
        StoreConnectionInfo storeConnectionInfo = storeConnectionInfoRepository.findOne(id);
        return storeConnectionInfoMapper.toDto(storeConnectionInfo);
    }

    /**
     * Delete the storeConnectionInfo by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete StoreConnectionInfo : {}", id);
        storeConnectionInfoRepository.delete(id);
    }
}
