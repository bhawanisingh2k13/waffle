package com.hocrox.belgian.service;

import com.hocrox.belgian.domain.*;
import com.hocrox.belgian.repository.SettingRepository;
import com.hocrox.belgian.repository.StoreConnectionInfoRepository;
import com.hocrox.belgian.repository.StoreUserProfileRepository;
import com.hocrox.belgian.repository.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.event.EventListener;
import org.springframework.messaging.simp.stomp.StompHeaderAccessor;
import org.springframework.messaging.simp.user.SimpUser;
import org.springframework.messaging.simp.user.SimpUserRegistry;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import org.springframework.web.socket.messaging.SessionConnectedEvent;
import org.springframework.web.socket.messaging.SessionDisconnectEvent;

import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
@Slf4j
public class WebSocketEventListener {

    private final StoreUserProfileRepository storeUserProfileRepository;
    private final StoreConnectionInfoRepository storeConnectionInfoRepository;
    private final SettingRepository settingRepository;
    private final UserRepository userRepository;
    private final SimpUserRegistry simpUserRegistry;

    public WebSocketEventListener(StoreUserProfileRepository storeUserProfileRepository, StoreConnectionInfoRepository storeConnectionInfoRepository, SettingRepository settingRepository, UserRepository userRepository, SimpUserRegistry simpUserRegistry) {
        this.storeUserProfileRepository = storeUserProfileRepository;
        this.storeConnectionInfoRepository = storeConnectionInfoRepository;
        this.settingRepository = settingRepository;
        this.userRepository = userRepository;
        this.simpUserRegistry = simpUserRegistry;
    }

    @EventListener
    public void handleWebSocketConnectListener(SessionConnectedEvent event) {
        StompHeaderAccessor sha = StompHeaderAccessor.wrap(event.getMessage());
        String storeUsername = ((UserDetails)((UsernamePasswordAuthenticationToken)sha.getUser()).getPrincipal()).getUsername();
        String connectionId = sha.getSessionId();
        log.debug("SHA User : {}, {}", storeUsername, connectionId);
    }

    @EventListener
    public void handleWebSocketDisconnectListener(SessionDisconnectEvent event) {
        StompHeaderAccessor sha = StompHeaderAccessor.wrap(event.getMessage());

        String storeUsername = ((UserDetails)((UsernamePasswordAuthenticationToken)sha.getUser()).getPrincipal()).getUsername();
        String connectionId = sha.getSessionId();
        log.debug("SHA User : {}, {}", storeUsername, connectionId);
    }
}
