package com.hocrox.belgian.service;

import com.hocrox.belgian.domain.Feedback;
import com.hocrox.belgian.repository.FeedbackRepository;
import com.hocrox.belgian.service.dto.FeedbackDTO;
import com.hocrox.belgian.service.mapper.FeedbackMapper;
import com.hocrox.belgian.web.rest.vm.FeedbackVM;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * Service Implementation for managing Feedback.
 */
@Service
@Transactional
public class FeedbackService {

    private final Logger log = LoggerFactory.getLogger(FeedbackService.class);

    private final FeedbackRepository feedbackRepository;

    private final FeedbackMapper feedbackMapper;

    private final ProfileService profileService;

    public FeedbackService(FeedbackRepository feedbackRepository, FeedbackMapper feedbackMapper, ProfileService profileService) {
        this.feedbackRepository = feedbackRepository;
        this.feedbackMapper = feedbackMapper;
        this.profileService = profileService;
    }

    /**
     * Save a feedback.
     *
     * @param feedbackVM the entity to save
     * @return the persisted entity
     */
    public FeedbackDTO save(FeedbackVM feedbackVM) {
        log.debug("Request to save Feedback : {}", feedbackVM);
        Feedback feedback = feedbackMapper.toEntity(feedbackVM);
        feedback.setProfile(profileService.findCurrent());
        feedback = feedbackRepository.save(feedback);
        return feedbackMapper.toDto(feedback);
    }

    /**
     * Get all the feedbacks.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<FeedbackDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Feedback");
        return feedbackRepository.findAll(pageable)
            .map(feedbackMapper::toDto);
    }

    /**
     * Get one feedback by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public FeedbackDTO findOne(Long id) {
        log.debug("Request to get Feedback : {}", id);
        Feedback feedback = feedbackRepository.findOne(id);
        return feedbackMapper.toDto(feedback);
    }

    /**
     * Delete the feedback by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete Feedback : {}", id);
        feedbackRepository.delete(id);
    }
}
