package com.hocrox.belgian.service;

import com.hocrox.belgian.domain.OneTimePassword;
import com.hocrox.belgian.domain.Profile;
import com.hocrox.belgian.repository.OneTimePasswordRepository;
import com.hocrox.belgian.repository.ProfileRepository;
import com.hocrox.belgian.service.dto.OneTimePasswordDTO;
import com.hocrox.belgian.service.util.RandomUtil;
import com.hocrox.belgian.web.rest.errors.InvalidOperationException;
import com.hocrox.belgian.web.rest.vm.OneTimePasswordConfirmVM;
import com.hocrox.belgian.web.rest.vm.OneTimePasswordVM;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing OneTimePassword.
 */
@Service
@Transactional
public class OneTimePasswordService {

    private final Logger log = LoggerFactory.getLogger(OneTimePasswordService.class);

    private final OneTimePasswordRepository oneTimePasswordRepository;

    private final ProfileRepository profileRepository;

    private final PasswordEncoder passwordEncoder;

    private final Msg91 msg91;

    public OneTimePasswordService(OneTimePasswordRepository oneTimePasswordRepository, ProfileRepository profileRepository, PasswordEncoder passwordEncoder, Msg91 msg91) {
        this.oneTimePasswordRepository = oneTimePasswordRepository;
        this.profileRepository = profileRepository;
        this.passwordEncoder = passwordEncoder;
        this.msg91 = msg91;
    }

    /**
     * Save a oneTimePassword.
     *
     * @param oneTimePasswordVM the entity to save
     * @return the persisted entity
     */
    public OneTimePasswordDTO generate(OneTimePasswordVM oneTimePasswordVM) {
        log.debug("Request to save OneTimePassword : {}", oneTimePasswordVM);
        Optional<Profile> profileOptional = profileRepository.findOneByPhoneNumber(oneTimePasswordVM.getPhoneNumber());
        OneTimePassword oneTimePassword = new OneTimePassword();
        OneTimePasswordDTO oneTimePasswordDTO = new OneTimePasswordDTO();
        if (profileOptional.isPresent()) {
            Profile profile = profileOptional.get();
            oneTimePassword.setUser(profile.getUser());
            oneTimePasswordDTO.setNewUser(false);
        }
        return oneTimePasswordDTO;
    }

    public boolean verifyOTP(OneTimePasswordConfirmVM oneTimePasswordConfirmVM) {
        boolean otpMatched = false;
        if (otpMatched) {
            return true;
        }
        throw new InvalidOperationException("Invalid OTP");
    }
}
