package com.hocrox.belgian.service;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.HttpMethod;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.GeneratePresignedUrlRequest;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.hocrox.belgian.config.ApplicationProperties;
import com.hocrox.belgian.config.Constants;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import java.io.File;
import java.net.URL;


/**
 * Created by dell on 11-Feb-16.
 */

@Component
@Slf4j
public class UploadService {

    private final AmazonS3 amazonS3;

    private final ApplicationProperties applicationProperties;

    public UploadService(AmazonS3 amazonS3, ApplicationProperties applicationProperties) {
        this.amazonS3 = amazonS3;
        this.applicationProperties = applicationProperties;
    }

    public String uploadFileToAWS(File file) {
        return uploadFileToAWS(file, null);
    }

    public String uploadFileToAWS(File file, String filePath) {
        String key = file.getName();
        if(StringUtils.isNotBlank(filePath)) {
            key = filePath + "/" + key;
        }
        try {
            log.debug("Uploading a new object to S3 from a file\n");
//            key = System.nanoTime() + "_" + DigestUtils.md5Hex(file.getName()) + "." + FilenameUtils.getExtension(file.getAbsolutePath());
            amazonS3.putObject(new PutObjectRequest(applicationProperties.getAws().getS3Bucket(), key, file).withCannedAcl(CannedAccessControlList.PublicRead));
        } catch (AmazonServiceException ase) {
            log.error("Caught an AmazonServiceException, which means your request made it "
                    + "to Amazon S3, but was rejected with an error response for some reason.");
            log.error("Error Message:    " + ase.getMessage());
            log.error("HTTP Status Code: " + ase.getStatusCode());
            log.error("AWS Error Code:   " + ase.getErrorCode());
            log.error("Error Type:       " + ase.getErrorType());
            log.error("Request ID:       " + ase.getRequestId());
            ase.printStackTrace();
        } catch (AmazonClientException ace) {
            log.error("Caught an AmazonClientException, which means the client encountered "
                    + "a serious internal problem while trying to communicate with S3, "
                    + "such as not being able to access the network.");
            log.error(ace.getMessage(), ace);
        }
        return key;
    }

    public String getObjectPublicUrl(String objectKey) {
        return amazonS3.getUrl(applicationProperties.getAws().getS3Bucket(), objectKey).toString();
    }

    public String getThumbPublicUrl(String objectKey) {
        return amazonS3.getUrl(applicationProperties.getAws().getS3Bucket(), Constants.AWS_THUMBNAIL + "/" + Constants.THUMBNAIL_PREFIX + objectKey).toString();
    }

    public String getObjectTimedUrl(String objectKey){
        java.util.Date expiration = new java.util.Date();
        long msec = expiration.getTime();
        msec += 1000 * 60 * 60; // 1 hour.
        expiration.setTime(msec);
        GeneratePresignedUrlRequest generatePresignedUrlRequest =
                new GeneratePresignedUrlRequest(applicationProperties.getAws().getS3Bucket(), objectKey);
        generatePresignedUrlRequest.setMethod(HttpMethod.GET); // Default.
        generatePresignedUrlRequest.setExpiration(expiration);
        URL s = amazonS3.generatePresignedUrl(generatePresignedUrlRequest);
        return s.toString();
    }
}
