package com.hocrox.belgian.service.mapper;

import com.hocrox.belgian.domain.*;
import com.hocrox.belgian.service.dto.SettingDTO;

import com.hocrox.belgian.web.rest.vm.SettingVM;
import org.mapstruct.*;

/**
 * Mapper for the entity Setting and its DTO SettingDTO.
 */
@Mapper(componentModel = "spring", uses = {WaffleStoreMapper.class, GlobalSettingMapper.class})
public interface SettingMapper extends EntityMapper<SettingDTO, Setting, SettingVM> {

    @Mapping(source = "waffleStore.id", target = "waffleStoreId")
    @Mapping(source = "globalSetting.minOrderQty", target = "minOrderQty")
    @Mapping(source = "globalSetting.maxOrderQty", target = "maxOrderQty")
    @Mapping(source = "globalSetting.androidAppVersion", target = "androidAppVersion")
    SettingDTO toDto(Setting setting);

//    @Mapping(source = "waffleStoreId", target = "waffleStore")
    Setting toEntity(SettingVM settingVM);

    default Setting fromId(Long id) {
        if (id == null) {
            return null;
        }
        Setting setting = new Setting();
        setting.setId(id);
        return setting;
    }
}
