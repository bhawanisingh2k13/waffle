package com.hocrox.belgian.service.mapper;

import com.hocrox.belgian.domain.WaffleStore;
import com.hocrox.belgian.service.UploadService;
import com.hocrox.belgian.service.dto.WaffleStoreContactDTO;
import com.hocrox.belgian.service.dto.WaffleStoreDTO;
import com.hocrox.belgian.web.rest.vm.WaffleStoreVM;
import org.apache.commons.lang3.StringUtils;
import org.hashids.Hashids;
import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Mapper for the entity WaffleStore and its DTO WaffleStoreDTO.
 */
@Mapper(componentModel = "spring", uses = {UserMapper.class})
public abstract class  WaffleStoreMapper implements EntityMapper<WaffleStoreDTO, WaffleStore, WaffleStoreVM> {

    @Autowired
    private UploadService uploadService;

    @Mapping(source = "owner.id", target = "ownerId")
    @Mapping(source = "owner.login", target = "ownerLogin")
    public abstract WaffleStoreDTO toDto(WaffleStore waffleStore);

    public abstract WaffleStoreContactDTO toContactDto(WaffleStore waffleStore);

    public abstract WaffleStore toEntity(WaffleStoreVM waffleStoreVM);

    public WaffleStore fromId(Long id) {
        if (id == null) {
            return null;
        }
        WaffleStore waffleStore = new WaffleStore();
        waffleStore.setId(id);
        return waffleStore;
    }


    @AfterMapping
    void addSecretId(WaffleStore waffleStore, @MappingTarget WaffleStoreDTO waffleStoreDTO) {
        Hashids hashids = new Hashids(WaffleStore.WAFFLE_RANDOM, WaffleStore.WAFFLE_HASH_LENGTH);
        String id = hashids.encode(waffleStore.getId());
        waffleStoreDTO.setStoreId(id);
        if(StringUtils.isNotBlank(waffleStore.getImage())) {
            waffleStoreDTO.setImageUrl(uploadService.getObjectPublicUrl(waffleStore.getImage()));
            waffleStoreDTO.setThumbUrl(uploadService.getThumbPublicUrl(waffleStore.getImage()));
        }
    }

    @AfterMapping
    void updateContact(WaffleStore waffleStore, @MappingTarget WaffleStoreContactDTO waffleStoreContactDTO) {
        if(StringUtils.isNotBlank(waffleStore.getImage())) {
            waffleStoreContactDTO.setImageUrl(uploadService.getObjectPublicUrl(waffleStore.getImage()));
            waffleStoreContactDTO.setThumbUrl(uploadService.getThumbPublicUrl(waffleStore.getImage()));
        }
    }
}
