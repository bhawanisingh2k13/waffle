package com.hocrox.belgian.service.mapper;

import com.hocrox.belgian.domain.*;
import com.hocrox.belgian.service.UploadService;
import com.hocrox.belgian.service.dto.AddonDTO;

import com.hocrox.belgian.web.rest.vm.AddonVM;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.mapstruct.*;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Mapper for the entity Addon and its DTO AddonDTO.
 */

@Slf4j
@Mapper(componentModel = "spring", uses = {AddonCategoryMapper.class}, nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public abstract class AddonMapper implements EntityMapper<AddonDTO, Addon, AddonVM> {

    @Autowired
    private UploadService uploadService;

    @Mapping(source = "category.id", target = "categoryId")
    @Mapping(source = "category.name", target = "categoryName")
    public abstract AddonDTO toDto(Addon addon);

    @Mapping(source = "categoryId", target = "category")
    public abstract Addon toEntity(AddonVM addonVM);

    public Addon fromId(Long id) {
        if (id == null) {
            return null;
        }
        Addon addon = new Addon();
        addon.setId(id);
        return addon;
    }

    @AfterMapping
    public void updateAwsURL(Addon addon, @MappingTarget AddonDTO addonDTO ) {
        if(StringUtils.isNotBlank(addon.getImage())) {
            addonDTO.setImageUrl(uploadService.getObjectPublicUrl(addon.getImage()));
            addonDTO.setThumbUrl(uploadService.getThumbPublicUrl(addon.getImage()));
        }
    }
}
