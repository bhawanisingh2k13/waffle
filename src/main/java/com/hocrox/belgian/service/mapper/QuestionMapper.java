package com.hocrox.belgian.service.mapper;

import com.hocrox.belgian.domain.*;
import com.hocrox.belgian.service.dto.ProfileDTO;
import com.hocrox.belgian.service.dto.QuestionDTO;

import com.hocrox.belgian.web.rest.vm.ProfileVM;
import com.hocrox.belgian.web.rest.vm.QuestionVM;
import org.mapstruct.*;

/**
 * Mapper for the entity Question and its DTO QuestionDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface QuestionMapper extends EntityMapper<QuestionDTO, Question, QuestionVM> {



    QuestionDTO toDto(Question question);

    Question toEntity(QuestionVM questionVM);

    default Question fromId(Long id) {
        if (id == null) {
            return null;
        }
        Question question = new Question();
        question.setId(id);
        return question;
    }
}
