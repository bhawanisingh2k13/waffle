package com.hocrox.belgian.service.mapper;

import com.hocrox.belgian.domain.MenuItem;
import com.hocrox.belgian.service.UploadService;
import com.hocrox.belgian.service.dto.MenuItemDTO;
import com.hocrox.belgian.web.rest.vm.MenuItemVM;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.stream.Collectors;

/**
 * Mapper for the entity MenuItem and its DTO MenuItemDTO.
 */
@Slf4j
@Mapper(componentModel = "spring", uses = {MenuItemCategoryMapper.class, AddonMapper.class})
public abstract class MenuItemMapper implements EntityMapper<MenuItemDTO, MenuItem, MenuItemVM> {

    @Autowired
    private UploadService uploadService;

    @Mapping(source = "category.id", target = "categoryId")
    @Mapping(source = "category.name", target = "categoryName")
    public abstract MenuItemDTO toDto(MenuItem menuItem);

    public MenuItemDTO toEnableDto(MenuItem menuItem, boolean enabledOnly){
        MenuItemDTO menuItemDTO = toDto(menuItem);
        if(!enabledOnly) return menuItemDTO;
        menuItemDTO.setAddons(menuItemDTO.getAddons().stream().filter(addonDTO -> addonDTO.isEnabled()).collect(Collectors.toSet()));
        return menuItemDTO;
    }

    @Mapping(source = "categoryId", target = "category")
    public abstract MenuItem toEntity(MenuItemVM menuItemVM);

    public MenuItem fromId(Long id) {
        if (id == null) {
            return null;
        }
        MenuItem menuItem = new MenuItem();
        menuItem.setId(id);
        return menuItem;
    }

    @AfterMapping
    public void updateAwsURL(MenuItem menuItem, @MappingTarget MenuItemDTO menuItemDTO ) {
        if(StringUtils.isNotBlank(menuItem.getImage())) {
            menuItemDTO.setImageUrl(uploadService.getObjectPublicUrl(menuItem.getImage()));
            menuItemDTO.setThumbUrl(uploadService.getThumbPublicUrl(menuItem.getImage()));
            menuItemDTO.setPanCakeImage(uploadService.getObjectPublicUrl("129940038193341_2937a079b3cd5a2e005deb336946cdaa.png"));
            menuItemDTO.setPanCakeThumbnail(uploadService.getThumbPublicUrl("129940038193341_2937a079b3cd5a2e005deb336946cdaa.png"));
        }
    }
}
