package com.hocrox.belgian.service.mapper;

import com.hocrox.belgian.domain.OrderItemAddon;
import com.hocrox.belgian.service.UploadService;
import com.hocrox.belgian.service.dto.OrderItemAddonDTO;
import com.hocrox.belgian.web.rest.vm.OrderItemAddonVM;
import org.apache.commons.lang3.StringUtils;
import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Mapper for the entity OrderItemAddon and its DTO OrderItemAddonDTO.
 */
@Mapper(componentModel = "spring", uses = {AddonMapper.class, OrderItemMapper.class})
public abstract class OrderItemAddonMapper implements EntityMapper<OrderItemAddonDTO, OrderItemAddon, OrderItemAddonVM> {

    @Autowired
    public UploadService uploadService;

    @Mapping(source = "addon.id", target = "addonId")
    @Mapping(source = "orderItem.id", target = "orderItemId")
    @Mapping(source = "addon.name", target = "name")
    @Mapping(source = "addon.image", target = "image")
    public abstract OrderItemAddonDTO toDto(OrderItemAddon orderItemAddon);

    @Mapping(source = "addonId", target = "addon")
//    @Mapping(source = "orderItemId", target = "orderItem")
    public abstract OrderItemAddon toEntity(OrderItemAddonVM orderItemAddonVM);

    public OrderItemAddon fromId(Long id) {
        if (id == null) {
            return null;
        }
        OrderItemAddon orderItemAddon = new OrderItemAddon();
        orderItemAddon.setId(id);
        return orderItemAddon;
    }

    @AfterMapping
    public void updateAwsURL(OrderItemAddon orderItemAddon, @MappingTarget OrderItemAddonDTO orderItemAddonDTO ) {
        if(StringUtils.isNotBlank(orderItemAddon.getAddon().getImage())) {
            orderItemAddonDTO.setImageUrl(uploadService.getObjectPublicUrl(orderItemAddon.getAddon().getImage()));
            orderItemAddonDTO.setThumbUrl(uploadService.getThumbPublicUrl(orderItemAddon.getAddon().getImage()));
        }
    }
}
