package com.hocrox.belgian.service.mapper;

import com.hocrox.belgian.domain.*;
import com.hocrox.belgian.service.UploadService;
import com.hocrox.belgian.service.dto.MenuItemDTO;
import com.hocrox.belgian.service.dto.OfferDTO;

import com.hocrox.belgian.web.rest.vm.OfferVM;
import org.apache.commons.lang3.StringUtils;
import org.mapstruct.*;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Mapper for the entity Offer and its DTO OfferDTO.
 */
@Mapper(componentModel = "spring", uses = {MenuItemMapper.class, WaffleStoreMapper.class})
public abstract class OfferMapper implements EntityMapper<OfferDTO, Offer, OfferVM> {

    @Autowired
    private UploadService uploadService;

    @Mapping(source = "menuItem.id", target = "menuItemId")
    @Mapping(source = "waffleStore.id", target = "waffleStoreId")
    public abstract OfferDTO toDto(Offer offer);

    @Mapping(source = "menuItemId", target = "menuItem")
    @Mapping(source = "waffleStoreId", target = "waffleStore")
    public abstract Offer toEntity(OfferVM offerVM);

    public Offer fromId(Long id) {
        if (id == null) {
            return null;
        }
        Offer offer = new Offer();
        offer.setId(id);
        return offer;
    }

    @AfterMapping
    public void updateAwsURL(Offer offer, @MappingTarget OfferDTO offerDTO ) {
        if(StringUtils.isNotBlank(offer.getImage())) {
            offerDTO.setImageUrl(uploadService.getObjectPublicUrl(offer.getImage()));
            offerDTO.setThumbUrl(uploadService.getThumbPublicUrl(offer.getImage()));
        }
    }
}
