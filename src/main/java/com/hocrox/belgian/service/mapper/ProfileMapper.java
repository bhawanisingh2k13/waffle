package com.hocrox.belgian.service.mapper;

import com.hocrox.belgian.domain.Profile;
import com.hocrox.belgian.service.dto.ProfileDTO;
import com.hocrox.belgian.web.rest.vm.ProfileVM;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

/**
 * Mapper for the entity Profile and its DTO ProfileDTO.
 */
@Mapper(componentModel = "spring", uses = {UserAddressMapper.class, UserMapper.class})
public interface ProfileMapper extends EntityMapper<ProfileDTO, Profile, ProfileVM> {

    @Mapping(source = "userAddress", target = "userAddress")
    @Mapping(source = "user.id", target = "userId")
    @Mapping(source = "user.login", target = "userLogin")
    @Mapping(source = "user.firstName", target = "firstName")
    @Mapping(source = "user.lastName", target = "lastName")
    @Mapping(source = "user.email", target = "email")
    ProfileDTO toDto(Profile profile);

//    @Mapping(source = "userAddressId", target = "userAddress")
    Profile toEntity(ProfileVM profileVM);

    default Profile fromId(Long id) {
        if (id == null) {
            return null;
        }
        Profile profile = new Profile();
        profile.setId(id);
        return profile;
    }
}
