package com.hocrox.belgian.service.mapper;

import com.hocrox.belgian.domain.MenuItemCategory;
import com.hocrox.belgian.service.UploadService;
import com.hocrox.belgian.service.dto.MenuItemCategoryDTO;
import com.hocrox.belgian.web.rest.vm.MenuItemCategoryVM;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Mapper for the entity MenuItemCategory and its DTO MenuItemCategoryDTO.
 */
@Slf4j
@Mapper(componentModel = "spring", uses = {})
public abstract class MenuItemCategoryMapper implements EntityMapper<MenuItemCategoryDTO, MenuItemCategory, MenuItemCategoryVM> {

    @Autowired
    private UploadService uploadService;

    public abstract MenuItemCategory toEntity(MenuItemCategoryVM menuItemCategoryVM);

    public abstract MenuItemCategoryDTO toDto(MenuItemCategory menuItemCategory);

    public MenuItemCategory fromId(Long id) {
        if (id == null) {
            return null;
        }
        MenuItemCategory menuItemCategory = new MenuItemCategory();
        menuItemCategory.setId(id);
        return menuItemCategory;
    }

    @AfterMapping
    public void updateAwsURL(MenuItemCategory itemCategory, @MappingTarget MenuItemCategoryDTO itemCategoryDTO ) {
        if(StringUtils.isNotBlank(itemCategory.getImage())) {
            itemCategoryDTO.setImageUrl(uploadService.getObjectPublicUrl(itemCategory.getImage()));
            itemCategoryDTO.setThumbUrl(uploadService.getThumbPublicUrl(itemCategory.getImage()));
        }
    }
}
