package com.hocrox.belgian.service.mapper;

import com.hocrox.belgian.domain.*;
import com.hocrox.belgian.service.dto.PickupLocationDTO;

import com.hocrox.belgian.web.rest.vm.PickupLocationVM;
import org.mapstruct.*;

/**
 * Mapper for the entity PickupLocation and its DTO PickupLocationDTO.
 */
@Mapper(componentModel = "spring", uses = {WaffleStoreMapper.class})
public interface PickupLocationMapper extends EntityMapper<PickupLocationDTO, PickupLocation, PickupLocationVM> {

    @Mapping(source = "waffleStore.id", target = "waffleStoreId")
    PickupLocationDTO toDto(PickupLocation pickupLocation); 

    PickupLocation toEntity(PickupLocationVM pickupLocationVM);

    default PickupLocation fromId(Long id) {
        if (id == null) {
            return null;
        }
        PickupLocation pickupLocation = new PickupLocation();
        pickupLocation.setId(id);
        return pickupLocation;
    }
}
