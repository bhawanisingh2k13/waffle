package com.hocrox.belgian.service.mapper;

import com.hocrox.belgian.domain.OrderTrack;
import com.hocrox.belgian.service.dto.OrderTrackDTO;
import com.hocrox.belgian.service.dto.OrderTrackWebDTO;
import com.hocrox.belgian.web.rest.vm.OrderTrackVM;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

/**
 * Mapper for the entity OrderTrack and its DTO OrderTrackDTO.
 */
@Mapper(componentModel = "spring", uses = {WaffleOrderMapper.class})
public interface OrderTrackMapper extends EntityMapper<OrderTrackDTO, OrderTrack, OrderTrackVM> {

    @Mapping(source = "waffleOrder.id", target = "waffleOrderId")
    OrderTrackDTO toDto(OrderTrack orderTrack);

    @Mapping(source = "waffleOrder.id", target = "waffleOrderId")
    @Mapping(source = "longTrackTime", target = "trackTime")
    OrderTrackWebDTO toWebDto(OrderTrack orderTrack);

//    @Mapping(source = "waffleOrderId", target = "waffleOrder")
    OrderTrack toEntity(OrderTrackVM orderTrackVM);

    default OrderTrack fromId(Long id) {
        if (id == null) {
            return null;
        }
        OrderTrack orderTrack = new OrderTrack();
        orderTrack.setId(id);
        return orderTrack;
    }
}
