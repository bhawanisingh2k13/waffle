package com.hocrox.belgian.service.mapper;

import com.hocrox.belgian.domain.*;
import com.hocrox.belgian.service.dto.GlobalSettingDTO;

import com.hocrox.belgian.web.rest.vm.GlobalSettingVM;
import org.mapstruct.*;

/**
 * Mapper for the entity GlobalSetting and its DTO GlobalSettingDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface GlobalSettingMapper extends EntityMapper<GlobalSettingDTO, GlobalSetting, GlobalSettingVM> {

    default GlobalSetting fromId(Long id) {
        if (id == null) {
            return null;
        }
        GlobalSetting globalSetting = new GlobalSetting();
        globalSetting.setId(id);
        return globalSetting;
    }
}
