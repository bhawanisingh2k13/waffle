package com.hocrox.belgian.service.mapper;

import com.hocrox.belgian.domain.StoreUserProfile;
import com.hocrox.belgian.service.UploadService;
import com.hocrox.belgian.service.dto.StoreUserProfileDTO;
import com.hocrox.belgian.web.rest.vm.RegisterStoreUserProfileVM;
import com.hocrox.belgian.web.rest.vm.StoreUserProfileUpdateVM;
import com.hocrox.belgian.web.rest.vm.StoreUserProfileVM;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Mapper for the entity StoreUserProfile and its DTO StoreUserProfileDTO.
 */
@Slf4j
@Mapper(componentModel = "spring", uses = {UserMapper.class, WaffleStoreMapper.class})
public abstract class StoreUserProfileMapper implements EntityMapper<StoreUserProfileDTO, StoreUserProfile, StoreUserProfileVM> {

    @Autowired
    private UploadService uploadService;

    @Mapping(source = "user.id", target = "userId")
    @Mapping(source = "user.firstName", target = "firstName")
    @Mapping(source = "user.lastName", target = "lastName")
    @Mapping(source = "user.email", target = "email")
    @Mapping(source = "user.imageUrl", target = "image")
    @Mapping(source = "user.login", target = "userLogin")
    @Mapping(source = "waffleStore.id", target = "waffleStoreId")
    public abstract StoreUserProfileDTO toDto(StoreUserProfile storeUserProfile);

//    @Mapping(source = "userId", target = "user")
//    @Mapping(target = "user.id", source = "userId")
//    @Mapping(target = "user.firstName", source = "firstName")
//    @Mapping(target = "user.lastName", source = "lastName")
//    @Mapping(target = "user.email", source = "email")
//    @Mapping(target = "user.imageUrl", source = "imageUrl")
    public abstract StoreUserProfile toEntity(StoreUserProfileVM storeUserProfileVM);

   public abstract StoreUserProfile toEntity(StoreUserProfileUpdateVM storeUserProfileUpdateVM);

    public abstract StoreUserProfile toEntity(RegisterStoreUserProfileVM storeUserProfileVM);

    public StoreUserProfile fromId(Long id) {
        if (id == null) {
            return null;
        }
        StoreUserProfile storeUserProfile = new StoreUserProfile();
        storeUserProfile.setId(id);
        return storeUserProfile;
    }

    @AfterMapping
    public void updateAwsURL( StoreUserProfile storeUserProfile, @MappingTarget  StoreUserProfileDTO storeUserProfileDTO) {
        if(StringUtils.isNotBlank(storeUserProfile.getUser().getImageUrl())) {
            storeUserProfileDTO.setImageUrl(uploadService.getObjectPublicUrl(storeUserProfile.getUser().getImageUrl()));
            storeUserProfileDTO.setThumbUrl(uploadService.getThumbPublicUrl(storeUserProfile.getUser().getImageUrl()));
        }
    }
}
