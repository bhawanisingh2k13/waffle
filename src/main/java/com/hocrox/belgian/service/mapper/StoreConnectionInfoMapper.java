package com.hocrox.belgian.service.mapper;

import com.hocrox.belgian.domain.*;
import com.hocrox.belgian.service.dto.StoreConnectionInfoDTO;

import com.hocrox.belgian.web.rest.vm.StoreConnectionInfoVM;
import org.mapstruct.*;

/**
 * Mapper for the entity StoreConnectionInfo and its DTO StoreConnectionInfoDTO.
 */
@Mapper(componentModel = "spring", uses = {WaffleStoreMapper.class})
public interface StoreConnectionInfoMapper extends EntityMapper<StoreConnectionInfoDTO, StoreConnectionInfo, StoreConnectionInfoVM> {

    @Mapping(source = "waffleStore.id", target = "waffleStoreId")
    StoreConnectionInfoDTO toDto(StoreConnectionInfo storeConnectionInfo);

    @Mapping(source = "waffleStoreId", target = "waffleStore")
    StoreConnectionInfo toEntity(StoreConnectionInfoVM storeConnectionInfoVM);

    default StoreConnectionInfo fromId(Long id) {
        if (id == null) {
            return null;
        }
        StoreConnectionInfo storeConnectionInfo = new StoreConnectionInfo();
        storeConnectionInfo.setId(id);
        return storeConnectionInfo;
    }
}
