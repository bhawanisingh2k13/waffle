package com.hocrox.belgian.service.mapper;

import com.hocrox.belgian.domain.WaffleOrder;
import com.hocrox.belgian.service.dto.WaffleOrderDTO;
import com.hocrox.belgian.web.rest.vm.WaffleOrderVM;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Mapper for the entity WaffleOrder and its DTO WaffleOrderDTO.
 */
@Slf4j
@Mapper(componentModel = "spring", uses = {UserAddressMapper.class, StoreUserProfileMapper.class, ProfileMapper.class, OrderItemMapper.class, PickupLocationMapper.class})
public abstract class   WaffleOrderMapper implements EntityMapper<WaffleOrderDTO, WaffleOrder, WaffleOrderVM> {

    @Autowired
    private StoreUserProfileMapper storeUserProfileMapper;

    @Mapping(source = "profile", target = "profile")
    @Mapping(source = "storeUserProfile", target = "deliveryBoy")
    @Mapping(source = "orderItems", target = "orderItems")
    @Mapping(source = "userAddress", target = "userAddress")
    public abstract WaffleOrderDTO toDto(WaffleOrder waffleOrder);

    @Mapping(target = "orderItems", ignore = true)
    @Mapping(source = "userAddressId", target = "userAddress")
    @Mapping(source = "pickupLocationId", target = "pickupLocation")
    public abstract WaffleOrder toEntity(WaffleOrderVM waffleOrderVM);

    WaffleOrder fromId(Long id) {
        if (id == null) {
            return null;
        }
        WaffleOrder waffleOrder = new WaffleOrder();
        waffleOrder.setId(id);
        return waffleOrder;
    }

    @AfterMapping
    public void addItems(WaffleOrder waffleOrder, @MappingTarget WaffleOrderDTO waffleOrderDTO ) {
        if(waffleOrder.getStoreUserProfile() != null) {
            waffleOrderDTO.setDeliveryBoy(storeUserProfileMapper.toDto(waffleOrder.getStoreUserProfile()));
        }

        if(StringUtils.isBlank(waffleOrder.getOrderId())) {
            waffleOrderDTO.setOrderId(WaffleOrder.HASHIDS.encode(waffleOrder.getId()));
        }
    }


}
