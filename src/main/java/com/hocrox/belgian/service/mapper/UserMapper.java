package com.hocrox.belgian.service.mapper;

import com.hocrox.belgian.domain.Authority;
import com.hocrox.belgian.domain.User;
import com.hocrox.belgian.service.dto.SimpleUserDTO;
import com.hocrox.belgian.service.dto.UserDTO;

import com.hocrox.belgian.web.rest.vm.UserVM;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Mapper for the entity User and its DTO called UserDTO.
 *
 * Normal mappers are generated using MapStruct, this one is hand-coded as MapStruct
 * support is still in beta, and requires a manual step with an IDE.
 */
@Service
public class UserMapper {

    public UserDTO userToUserDTO(User user) {
        return new UserDTO(user);
    }

    public SimpleUserDTO userToSimpleUserDTO(User user) {
        return new SimpleUserDTO(user);
    }

    public List<UserDTO> usersToUserDTOs(List<User> users) {
        return users.stream()
            .filter(Objects::nonNull)
            .map(this::userToUserDTO)
            .collect(Collectors.toList());
    }

    public User userVMToUser(UserVM userVM) {
        if (userVM == null) {
            return null;
        } else {
            User user = new User();
            user.setId(userVM.getId());
            user.setLogin(userVM.getLogin());
            user.setFirstName(userVM.getFirstName());
            user.setLastName(userVM.getLastName());
            user.setEmail(userVM.getEmail());
            user.setImageUrl(userVM.getImageUrl());
            user.setActivated(userVM.isActivated());
            user.setLangKey(userVM.getLangKey());
//            Set<Authority> authorities = this.authoritiesFromStrings(userVM.getAuthorities());
//            if (authorities != null) {
//                user.setAuthorities(authorities);
//            }
            return user;
        }
    }

    public List<User> userDTOsToUsers(List<UserVM> userVMS) {
        return userVMS.stream()
            .filter(Objects::nonNull)
            .map(this::userVMToUser)
            .collect(Collectors.toList());
    }

    public User userFromId(Long id) {
        if (id == null) {
            return null;
        }
        User user = new User();
        user.setId(id);
        return user;
    }

    public Set<Authority> authoritiesFromStrings(Set<String> strings) {
        return strings.stream().map(string -> {
            Authority auth = new Authority();
            auth.setName(string);
            return auth;
        }).collect(Collectors.toSet());
    }
}
