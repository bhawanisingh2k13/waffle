package com.hocrox.belgian.service.mapper;

import com.hocrox.belgian.domain.*;
import com.hocrox.belgian.service.dto.UserAddressDTO;

import com.hocrox.belgian.web.rest.vm.UserAddressVM;
import org.mapstruct.*;

/**
 * Mapper for the entity UserAddress and its DTO UserAddressDTO.
 */
@Mapper(componentModel = "spring", uses = {ProfileMapper.class})
public interface UserAddressMapper extends EntityMapper<UserAddressDTO, UserAddress, UserAddressVM> {

    @Mapping(source = "profile.id", target = "profileId")
    UserAddressDTO toDto(UserAddress userAddress); 

    @Mapping(source = "profileId", target = "profile")
    UserAddress toEntity(UserAddressVM userAddressVM);

    default UserAddress fromId(Long id) {
        if (id == null) {
            return null;
        }
        UserAddress userAddress = new UserAddress();
        userAddress.setId(id);
        return userAddress;
    }
}
