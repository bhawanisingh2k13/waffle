package com.hocrox.belgian.service.mapper;

import com.hocrox.belgian.domain.*;
import com.hocrox.belgian.service.UploadService;
import com.hocrox.belgian.service.dto.MenuItemDTO;
import com.hocrox.belgian.service.dto.WaffleNotificationDTO;

import com.hocrox.belgian.web.rest.vm.WaffleNotificationVM;
import org.apache.commons.lang3.StringUtils;
import org.mapstruct.*;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Mapper for the entity WaffleNotification and its DTO WaffleNotificationDTO.
 */
@Mapper(componentModel = "spring", uses = {WaffleStoreMapper.class})
public abstract class WaffleNotificationMapper implements EntityMapper<WaffleNotificationDTO, WaffleNotification, WaffleNotificationVM> {


    @Autowired
    private UploadService uploadService;

    @Mapping(source = "waffleStore.id", target = "waffleStoreId")
    public abstract WaffleNotificationDTO toDto(WaffleNotification waffleNotification);

    @Mapping(source = "waffleStoreId", target = "waffleStore")
    public abstract WaffleNotification toEntity(WaffleNotificationVM waffleNotificationVM);

    public WaffleNotification fromId(Long id) {
        if (id == null) {
            return null;
        }
        WaffleNotification waffleNotification = new WaffleNotification();
        waffleNotification.setId(id);
        return waffleNotification;
    }

    @AfterMapping
    public void updateAwsURL(WaffleNotification waffleNotification, @MappingTarget WaffleNotificationDTO waffleNotificationDTO ) {
        if(StringUtils.isNotBlank(waffleNotification.getImage())) {
            waffleNotificationDTO.setImageUrl(uploadService.getObjectPublicUrl(waffleNotification.getImage()));
            waffleNotificationDTO.setThumbUrl(uploadService.getThumbPublicUrl(waffleNotification.getImage()));
        }
    }
}
