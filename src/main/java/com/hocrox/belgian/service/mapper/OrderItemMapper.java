package com.hocrox.belgian.service.mapper;

import com.hocrox.belgian.domain.OrderItem;
import com.hocrox.belgian.service.UploadService;
import com.hocrox.belgian.service.dto.OrderItemDTO;
import com.hocrox.belgian.web.rest.vm.OrderItemVM;
import org.apache.commons.lang3.StringUtils;
import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collections;

/**
 * Mapper for the entity OrderItem and its DTO OrderItemDTO.
 */
@Mapper(componentModel = "spring", uses = {WaffleOrderMapper.class, OrderItemAddonMapper.class, MenuItemMapper.class})
public abstract class OrderItemMapper implements EntityMapper<OrderItemDTO, OrderItem, OrderItemVM> {

    @Autowired
    public UploadService uploadService;

    @Mapping(source = "menuItem.id", target = "menuItemId")
    @Mapping(source = "menuItem.name", target = "name")
    @Mapping(source = "menuItem.image", target = "image")
    @Mapping(source = "waffleOrder.id", target = "waffleOrderId")
    public abstract OrderItemDTO toDto(OrderItem orderItem);

    @Mapping(source = "menuItemId", target = "menuItem")
    @Mapping(target = "orderItemAddons", ignore = true)
    public abstract OrderItem toEntity(OrderItemVM orderItemVM);

    public OrderItem fromId(Long id) {
        if (id == null) {
            return null;
        }
        OrderItem orderItem = new OrderItem();
        orderItem.setId(id);
        return orderItem;
    }

    @AfterMapping
    public void updateAwsURL(OrderItem orderItem, @MappingTarget OrderItemDTO orderItemDTO ) {
        Collections.sort(orderItemDTO.getOrderItemAddons());
        if(StringUtils.isNotBlank(orderItem.getMenuItem().getImage())) {
            orderItemDTO.setImageUrl(uploadService.getObjectPublicUrl(orderItem.getMenuItem().getImage()));
            orderItemDTO.setThumbUrl(uploadService.getThumbPublicUrl(orderItem.getMenuItem().getImage()));
            orderItemDTO.setPanCakeImage(uploadService.getObjectPublicUrl("129940038193341_2937a079b3cd5a2e005deb336946cdaa.png"));
            orderItemDTO.setPanCakeThumbnail(uploadService.getThumbPublicUrl("129940038193341_2937a079b3cd5a2e005deb336946cdaa.png"));
        }
        if(orderItem.getPanCake()) {
            orderItemDTO.setName(orderItemDTO.getName() + " Pancake");
        }
    }
}
