package com.hocrox.belgian.service.mapper;

import com.hocrox.belgian.domain.*;
import com.hocrox.belgian.service.dto.FeedbackDTO;

import com.hocrox.belgian.web.rest.vm.FeedbackVM;
import org.mapstruct.*;

/**
 * Mapper for the entity Feedback and its DTO FeedbackDTO.
 */
@Mapper(componentModel = "spring", uses = {ProfileMapper.class})
public interface FeedbackMapper extends EntityMapper<FeedbackDTO, Feedback, FeedbackVM> {

    @Mapping(source = "profile.user.firstName", target = "name")
    @Mapping(source = "profile.user.email", target = "email")
    FeedbackDTO toDto(Feedback feedback); 

    Feedback toEntity(FeedbackVM feedbackVM);

    default Feedback fromId(Long id) {
        if (id == null) {
            return null;
        }
        Feedback feedback = new Feedback();
        feedback.setId(id);
        return feedback;
    }
}
