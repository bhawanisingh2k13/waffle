package com.hocrox.belgian.service.mapper;

import com.hocrox.belgian.domain.WaffleUserNotification;
import com.hocrox.belgian.service.UploadService;
import com.hocrox.belgian.service.dto.WaffleUserNotificationDTO;
import com.hocrox.belgian.web.rest.vm.WaffleUserNotificationVM;
import org.apache.commons.lang3.StringUtils;
import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Mapper for the entity WaffleUserNotification and its DTO WaffleUserNotificationDTO.
 */
@Mapper(componentModel = "spring", uses = {UserMapper.class, WaffleStoreMapper.class})
public abstract class WaffleUserNotificationMapper implements EntityMapper<WaffleUserNotificationDTO, WaffleUserNotification, WaffleUserNotificationVM> {

    @Autowired
    private UploadService uploadService;

    @Mapping(source = "waffleStore.id", target = "waffleStoreId")
    public abstract WaffleUserNotificationDTO toDto(WaffleUserNotification waffleUserNotification);

    @Mapping(source = "waffleStoreId", target = "waffleStore")
    public abstract WaffleUserNotification toEntity(WaffleUserNotificationVM waffleUserNotificationVM);

    public WaffleUserNotification fromId(Long id) {
        if (id == null) {
            return null;
        }
        WaffleUserNotification waffleUserNotification = new WaffleUserNotification();
        waffleUserNotification.setId(id);
        return waffleUserNotification;
    }

    @AfterMapping
    public void updateAwsURL(WaffleUserNotification waffleNotification, @MappingTarget WaffleUserNotificationDTO waffleUserNotificationDTO ) {
        if(StringUtils.isNotBlank(waffleNotification.getImage())) {
            waffleUserNotificationDTO.setImageUrl(uploadService.getObjectPublicUrl(waffleNotification.getImage()));
            waffleUserNotificationDTO.setThumbUrl(uploadService.getThumbPublicUrl(waffleNotification.getImage()));
        }
    }
}
