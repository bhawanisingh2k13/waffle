package com.hocrox.belgian.service.mapper;

import com.hocrox.belgian.domain.*;
import com.hocrox.belgian.service.dto.OneTimePasswordDTO;

import com.hocrox.belgian.web.rest.vm.OneTimePasswordVM;
import org.mapstruct.*;

/**
 * Mapper for the entity OneTimePassword and its DTO OneTimePasswordDTO.
 */
@Mapper(componentModel = "spring", uses = {UserMapper.class})
public interface OneTimePasswordMapper extends EntityMapper<OneTimePasswordDTO, OneTimePassword, OneTimePasswordVM> {

    OneTimePasswordDTO toDto(OneTimePassword oneTimePassword);

    OneTimePassword toEntity(OneTimePasswordVM oneTimePasswordVM);

    default OneTimePassword fromId(Long id) {
        if (id == null) {
            return null;
        }
        OneTimePassword oneTimePassword = new OneTimePassword();
        oneTimePassword.setId(id);
        return oneTimePassword;
    }
}
