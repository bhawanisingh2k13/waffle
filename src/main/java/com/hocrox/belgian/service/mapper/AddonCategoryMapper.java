package com.hocrox.belgian.service.mapper;

import com.hocrox.belgian.domain.AddonCategory;
import com.hocrox.belgian.service.UploadService;
import com.hocrox.belgian.service.dto.AddonCategoryDTO;
import com.hocrox.belgian.web.rest.vm.AddonCategoryVM;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Iterator;
import java.util.List;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

/**
 * Mapper for the entity AddonCategory and its DTO AddonCategoryDTO.
 */
@Slf4j
@Mapper(componentModel = "spring", uses = {})
public abstract class AddonCategoryMapper implements EntityMapper<AddonCategoryDTO, AddonCategory,  AddonCategory> {

    @Autowired
    private UploadService uploadService;


    public abstract AddonCategory toEntity(AddonCategoryVM addonCategoryVM);

    public abstract AddonCategoryDTO toDto(AddonCategory addonCategory);

    public List<AddonCategoryDTO> toDto(Iterable<AddonCategory> entityList) {
        Iterator<AddonCategory> iteratorToCollection = entityList.iterator();

        return StreamSupport.stream(
                Spliterators.spliteratorUnknownSize(iteratorToCollection,
                        Spliterator.ORDERED), false).map(this::toDto).collect(
                Collectors.toList());
    }

    public AddonCategory fromId(Long id) {
        if (id == null) {
            return null;
        }
        AddonCategory addonCategory = new AddonCategory();
        addonCategory.setId(id);
        return addonCategory;
    }

    @AfterMapping
    public void updateAwsURL(AddonCategory addonCategory, @MappingTarget AddonCategoryDTO addonCategoryDTO ) {
        if(StringUtils.isNotBlank(addonCategory.getImage())) {
            addonCategoryDTO.setImageUrl(uploadService.getObjectPublicUrl(addonCategory.getImage()));
            addonCategoryDTO.setThumbUrl(uploadService.getThumbPublicUrl(addonCategory.getImage()));
        }
    }
}
