package com.hocrox.belgian.service.mapper;

import java.util.Iterator;
import java.util.List;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

/**
 * Contract for a generic dto to entity mapper.
 *
 * @param <D> - DTO type parameter.
 * @param <E> - Entity type parameter.
 */

public interface EntityMapper <D, E, V> {

    public E toEntity(V vm);

    public D toDto(E entity);

    public List <E> toEntity(List<V> vmList);

    public List <D> toDto(List<E> entityList);

    default List<D> toDto(Iterable<E> entityList) {
        Iterator<E> iteratorToCollection = entityList.iterator();

        return StreamSupport.stream(
                Spliterators.spliteratorUnknownSize(iteratorToCollection,
                        Spliterator.ORDERED), false).map(this::toDto).collect(
                Collectors.toList());
    }
}
