package com.hocrox.belgian.service.mapper;

import com.hocrox.belgian.domain.Device;
import com.hocrox.belgian.service.dto.DeviceDTO;
import com.hocrox.belgian.web.rest.vm.DeviceVM;
import org.mapstruct.Mapper;

/**
 * Mapper for the entity StoreUserProfile and its DTO StoreUserProfileDTO.
 */
@Mapper(componentModel = "spring", uses = {UserMapper.class, WaffleStoreMapper.class})
public interface DeviceMapper extends EntityMapper<DeviceDTO, Device, DeviceVM> {

    DeviceDTO toDto(Device device);

    Device toEntity(DeviceVM deviceVM);

    default String map(Object value) {
        return value.toString();
    }

    default Device fromId(Long id) {
        if (id == null) {
            return null;
        }
        Device device = new Device();
        device.setId(id);
        return device;
    }
}
