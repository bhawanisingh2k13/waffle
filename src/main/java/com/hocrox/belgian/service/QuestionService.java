package com.hocrox.belgian.service;

import com.hocrox.belgian.domain.Question;
import com.hocrox.belgian.repository.QuestionRepository;
import com.hocrox.belgian.service.dto.QuestionDTO;
import com.hocrox.belgian.service.mapper.QuestionMapper;
import com.hocrox.belgian.web.rest.vm.QuestionVM;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;


/**
 * Service Implementation for managing Question.
 */
@Service
@Transactional
public class QuestionService {

    private final Logger log = LoggerFactory.getLogger(QuestionService.class);

    private final QuestionRepository questionRepository;

    private final QuestionMapper questionMapper;

    public QuestionService(QuestionRepository questionRepository, QuestionMapper questionMapper) {
        this.questionRepository = questionRepository;
        this.questionMapper = questionMapper;
    }

    /**
     * Save a question.
     *
     * @param questionVM the entity to save
     * @return the persisted entity
     */
    public QuestionDTO save(QuestionVM questionVM) {
        log.debug("Request to save Question : {}", questionVM);
        Question question = questionMapper.toEntity(questionVM);
        question = questionRepository.save(question);
        return questionMapper.toDto(question);
    }

    /**
     * Get all the questions.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<QuestionDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Questions");
        return questionRepository.findAll(pageable)
            .map(questionMapper::toDto);
    }

    /**
     * Get all the questions.
     *
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<QuestionDTO> findAllEnabled() {
        log.debug("Request to get all Questions");
        return questionRepository.findAllByEnabled(true).stream()
            .map(questionMapper::toDto).collect(Collectors.toList());
    }

    /**
     * Get one question by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public QuestionDTO findOne(Long id) {
        log.debug("Request to get Question : {}", id);
        Question question = questionRepository.findOne(id);
        return questionMapper.toDto(question);
    }

    /**
     * Delete the question by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete Question : {}", id);
        questionRepository.delete(id);
    }
}
