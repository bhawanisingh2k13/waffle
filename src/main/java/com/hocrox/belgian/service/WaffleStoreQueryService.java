package com.hocrox.belgian.service;


import com.hocrox.belgian.domain.QWaffleStore;
import com.hocrox.belgian.domain.User_;
import com.hocrox.belgian.domain.WaffleStore;
import com.hocrox.belgian.domain.WaffleStore_;
import com.hocrox.belgian.repository.WaffleStoreRepository;
import com.hocrox.belgian.service.dto.WaffleStoreDTO;
import com.hocrox.belgian.service.mapper.WaffleStoreMapper;
import com.hocrox.belgian.web.rest.vm.search.WaffleStoreSearch;
import com.hocrox.belgian.web.rest.vm.search.criteria.WaffleStoreCriteria;
import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.OrderSpecifier;
import com.querydsl.core.types.Predicate;
import io.github.jhipster.service.QueryService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.data.querydsl.QPageRequest;
import org.springframework.data.querydsl.QSort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Service for executing complex queries for WaffleStore entities in the database.
 * The main input is a {@link WaffleStoreSearch} which get's converted to {@link Specifications},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link WaffleStoreDTO} or a {@link Page} of {@link WaffleStoreDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class WaffleStoreQueryService extends QueryService<WaffleStore> {

    private final Logger log = LoggerFactory.getLogger(WaffleStoreQueryService.class);


    private final WaffleStoreRepository waffleStoreRepository;

    private final WaffleStoreMapper waffleStoreMapper;

    public WaffleStoreQueryService(WaffleStoreRepository waffleStoreRepository, WaffleStoreMapper waffleStoreMapper) {
        this.waffleStoreRepository = waffleStoreRepository;
        this.waffleStoreMapper = waffleStoreMapper;
    }

    /**
     * Return a {@link List} of {@link WaffleStoreDTO} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<WaffleStoreDTO> findByCriteria(WaffleStoreCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specifications<WaffleStore> specification = createSpecification(criteria);
        return waffleStoreMapper.toDto(waffleStoreRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link WaffleStoreDTO} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<WaffleStoreDTO> findByCriteria(WaffleStoreCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specifications<WaffleStore> specification = createSpecification(criteria);
        final Page<WaffleStore> result = waffleStoreRepository.findAll(specification, page);
        return result.map(waffleStoreMapper::toDto);
    }

    /**
     * Function to convert WaffleStoreCriteria to a {@link Specifications}
     */
    private Specifications<WaffleStore> createSpecification(WaffleStoreCriteria criteria) {
        Specifications<WaffleStore> specification = Specifications.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), WaffleStore_.id));
            }
            if (criteria.getShortCode() != null) {
                specification = specification.and(buildStringSpecification(criteria.getShortCode(), WaffleStore_.shortCode));
            }
            if (criteria.getName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getName(), WaffleStore_.name));
            }
            if (criteria.getLat() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getLat(), WaffleStore_.lat));
            }
            if (criteria.getLng() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getLng(), WaffleStore_.lng));
            }
            if (criteria.getStreet() != null) {
                specification = specification.and(buildStringSpecification(criteria.getStreet(), WaffleStore_.street));
            }
            if (criteria.getCity() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCity(), WaffleStore_.city));
            }
            if (criteria.getState() != null) {
                specification = specification.and(buildStringSpecification(criteria.getState(), WaffleStore_.state));
            }
            if (criteria.getCountry() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCountry(), WaffleStore_.country));
            }
            if (criteria.getOwnerId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getOwnerId(), WaffleStore_.owner, User_.id));
            }
        }
        return specification;
    }

    /**
     * Return a {@link List} of {@link WaffleStoreDTO} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<WaffleStoreDTO> findByPredicate(WaffleStoreSearch criteria) {
        log.debug("find by criteria : {}", criteria);
        final Predicate predicate = createPredicate(criteria);
        return waffleStoreMapper.toDto(waffleStoreRepository.findAll(predicate, defaultSort()));
    }

    /**
     * Return a {@link Page} of {@link WaffleStoreDTO} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<WaffleStoreDTO> findByPredicate(WaffleStoreSearch criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Predicate predicate = createPredicate(criteria);
        final Page<WaffleStore> result = waffleStoreRepository.findAll(predicate, defaultPageable(page));
        return result.map(waffleStoreMapper::toDto);
    }

    /**
     * Function to convert WaffleStoreCriteria to a {@link Predicate}
     */
    private Predicate createPredicate(WaffleStoreSearch waffleStoreSearch) {
        QWaffleStore waffleStore = QWaffleStore.waffleStore;
        BooleanBuilder booleanBuilder = new BooleanBuilder();
        if(waffleStoreSearch.getId() != null) {
            booleanBuilder.and(waffleStore.id.eq(waffleStoreSearch.getId()));
        }
        if(waffleStoreSearch.getName() != null) {
            booleanBuilder.and(waffleStore.name.contains(waffleStoreSearch.getName()));
        }
        if(waffleStoreSearch.getStreet() != null) {
            booleanBuilder.and(waffleStore.street.eq(waffleStoreSearch.getStreet()));
        }
        if(waffleStoreSearch.getState() != null) {
            booleanBuilder.and(waffleStore.state.eq(waffleStoreSearch.getState()));
        }
        if(waffleStoreSearch.getCity() != null) {
            booleanBuilder.and(waffleStore.city.eq(waffleStoreSearch.getCity()));
        }
        if(waffleStoreSearch.getCountry() != null) {
            booleanBuilder.and(waffleStore.country.eq(waffleStoreSearch.getCountry()));
        }
        if(waffleStoreSearch.getShortCode() != null) {
            booleanBuilder.and(waffleStore.shortCode.eq(waffleStoreSearch.getShortCode()));
        }
        return booleanBuilder;
    }

    private QSort defaultSort() {
        OrderSpecifier<Long> sortId = QWaffleStore.waffleStore.id.desc();
        return new QSort(sortId);
    }

    private QPageRequest defaultPageable(Pageable pageable) {
        return new QPageRequest(pageable.getPageNumber(), pageable.getPageSize(), defaultSort());
    }
}
