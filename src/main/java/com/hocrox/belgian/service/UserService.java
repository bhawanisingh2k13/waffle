package com.hocrox.belgian.service;

import com.hocrox.belgian.config.Constants;
import com.hocrox.belgian.domain.Authority;
import com.hocrox.belgian.domain.Role;
import com.hocrox.belgian.domain.User;
import com.hocrox.belgian.repository.AuthorityRepository;
import com.hocrox.belgian.repository.RoleRepository;
import com.hocrox.belgian.repository.UserRepository;
import com.hocrox.belgian.security.SecurityUtils;
import com.hocrox.belgian.service.dto.UserDTO;
import com.hocrox.belgian.service.util.RandomUtil;
import com.hocrox.belgian.web.rest.errors.InvalidOperationException;
import com.hocrox.belgian.web.rest.vm.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.CacheManager;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Service class for managing users.
 */
@Service
@Transactional
public class UserService {

    private final Logger log = LoggerFactory.getLogger(UserService.class);

    private static final String USERS_CACHE = "users";

    private final UserRepository userRepository;

    private final PasswordEncoder passwordEncoder;

    private final AuthorityRepository authorityRepository;

    private final RoleRepository roleRepository;

    private final CacheManager cacheManager;

    public UserService(UserRepository userRepository, PasswordEncoder passwordEncoder, AuthorityRepository authorityRepository, RoleRepository roleRepository, CacheManager cacheManager) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
        this.authorityRepository = authorityRepository;
        this.roleRepository = roleRepository;
        this.cacheManager = cacheManager;
    }

    public Optional<User> activateRegistration(String key) {
        log.debug("Activating user for activation key {}", key);
        return userRepository.findOneByActivationKey(key)
            .map(user -> {
                // activate given user for the registration key.
                user.setActivated(true);
                user.setActivationKey(null);
                cacheManager.getCache(USERS_CACHE).evict(user.getLogin());
                log.debug("Activated user: {}", user);
                return user;
            });
    }

    public Optional<User> completePasswordReset(String newPassword, String key) {
       log.debug("Reset user password for reset key {}", key);

       return userRepository.findOneByResetKey(key)
           .filter(user -> user.getResetDate().isAfter(Instant.now().minusSeconds(86400)))
           .map(user -> {
                user.setPassword(passwordEncoder.encode(newPassword));
                user.setResetKey(null);
                user.setResetDate(null);
                cacheManager.getCache(USERS_CACHE).evict(user.getLogin());
                return user;
           });
    }

    public Optional<User> requestPasswordReset(String mail) {
        return userRepository.findOneByEmailIgnoreCase(mail)
            .filter(User::getActivated)
            .map(user -> {
                user.setResetKey(RandomUtil.generateResetKey());
                user.setResetDate(Instant.now());
                cacheManager.getCache(USERS_CACHE).evict(user.getLogin());
                return user;
            });
    }

    public User registerStoreUser(StoreUserProfileVM userVM, String password) {

        User newUser = registerUser(userVM, password);
        userRepository.save(newUser);
        //TODO add role checks
        log.debug("Created Information for User: {}", newUser);
        return newUser;
    }

    public User registerUser(UserVM userVM, String password) {

        User newUser = new User();
        validateUser(newUser, userVM);
        Role role = roleRepository.userRole();
        newUser.setRole(role);
        String encryptedPassword = passwordEncoder.encode(password);
        newUser.setLogin(userVM.getLogin());
        newUser.setPassword(encryptedPassword);
        newUser.setFirstName(userVM.getFirstName());
        newUser.setLastName(userVM.getLastName());
        newUser.setEmail(userVM.getEmail());
        newUser.setImageUrl(userVM.getImageUrl());
        newUser.setLangKey(userVM.getLangKey());
        newUser.setActivated(true);
        newUser.setActivationKey(RandomUtil.generateActivationKey());
        userRepository.save(newUser);
        log.debug("Created Information for User: {}", newUser);
        return newUser;
    }

    public User createEndUser(ManagedUserVM managedUserVM) {
        User user = registerUser(managedUserVM, managedUserVM.getPassword());
        return user;
    }

    /**
     * Update all information for a specific user, and return the modified user.
     *
     * @param userVM user to update
     * @return updated user
     */
    public User updateEndUser(UserVM userVM) {
        User user = currentUser();
        validateUser(user, userVM);
        user.setFirstName(userVM.getFirstName());
        user.setLastName(userVM.getLastName());
        user.setEmail(userVM.getEmail());
        user.setImageUrl(userVM.getImageUrl());
        user.setActivated(userVM.isActivated());
        user.setLangKey(userVM.getLangKey());
        cacheManager.getCache(USERS_CACHE).evict(user.getLogin());
        log.debug("Changed Information for User: {}", user);
        return user;
    }
    public User createUser(UserVM userVM) {
        User user = new User();
        validateUser(user, userVM);
        Role role = roleRepository.userRole();
        user.setRole(role);
        user.setLogin(userVM.getLogin());
        user.setFirstName(userVM.getFirstName());
        user.setLastName(userVM.getLastName());
        user.setEmail(userVM.getEmail());
        user.setImageUrl(userVM.getImageUrl());
        if (userVM.getLangKey() == null) {
            user.setLangKey(Constants.DEFAULT_LANGUAGE); // default language
        } else {
            user.setLangKey(userVM.getLangKey());
        }
        if (userVM.getRoleId() != null) {
            Optional<Role> roleOptional = roleRepository.findOneById(userVM.getRoleId());
            if(roleOptional.isPresent()) {
                user.setRole(roleOptional.get());
            } else {
                //todo fix
                throw new InvalidOperationException("Invalid Role Selected");
            }
        }
        String encryptedPassword = passwordEncoder.encode(RandomUtil.generatePassword());
        user.setPassword(encryptedPassword);
        user.setResetKey(RandomUtil.generateResetKey());
        user.setResetDate(Instant.now());
        user.setActivated(true);
        userRepository.save(user);
        log.debug("Created Information for User: {}", user);
        return user;
    }

    /**
     * Update basic information (first name, last name, email, language) for the current user.
     *
     * @param firstName first name of user
     * @param lastName last name of user
     * @param email email id of user
     * @param langKey language key
     * @param imageUrl image URL of user
     */
    public void updateUser(String firstName, String lastName, String email, String langKey, String imageUrl) {
        SecurityUtils.getCurrentUserLogin()
            .flatMap(userRepository::findOneByLogin)
            .ifPresent(user -> {
                validateUser(user, email);
                user.setFirstName(firstName);
                user.setLastName(lastName);
                user.setEmail(email);
                user.setLangKey(langKey);
                user.setImageUrl(imageUrl);
                cacheManager.getCache(USERS_CACHE).evict(user.getLogin());
                log.debug("Changed Information for User: {}", user);
            });
    }

    /**
     * Update all information for a specific user, and return the modified user.
     *
     * @param userVM user to update
     * @return updated user
     */
    public Optional<UserDTO> updateUser(UserVM userVM) {
        return Optional.of(userRepository
            .findOne(userVM.getId()))
            .map(user -> {
                validateUser(user, userVM);
                Optional<Role> roleOptional = roleRepository.findOneById(userVM.getRoleId());
                if(roleOptional.isPresent()) {
                    user.setRole(roleOptional.get());
                } else {
                    throw new InvalidOperationException("Invalid role");
                }
                user.setLogin(userVM.getLogin());
                user.setFirstName(userVM.getFirstName());
                user.setLastName(userVM.getLastName());
                user.setEmail(userVM.getEmail());
                user.setImageUrl(userVM.getImageUrl());
                user.setActivated(userVM.isActivated());
                user.setLangKey(userVM.getLangKey());
                cacheManager.getCache(USERS_CACHE).evict(user.getLogin());
                log.debug("Changed Information for User: {}", user);
                return user;
            })
            .map(UserDTO::new);
    }

    public Optional<UserDTO> updateUser(StoreUserProfileUpdateVM userVM) {
        return userRepository
                .findOneByLogin(userVM.getLogin())
                .map(user -> {
                    validateUser(user, userVM);
                    Optional<Role> roleOptional = roleRepository.findOneById(userVM.getRoleId());
                    roleOptional.ifPresent(user::setRole);
                    user.setFirstName(userVM.getFirstName());
                    user.setLastName(userVM.getLastName());
                    user.setEmail(userVM.getEmail());
                    user.setImageUrl(userVM.getImageUrl());
//                    user.setActivated(userVM.isActivated());
                    user.setLangKey(userVM.getLangKey());
                    cacheManager.getCache(USERS_CACHE).evict(user.getLogin());
                    log.debug("Changed Information for User: {}", user);
                    return user;
                })
                .map(UserDTO::new);
    }

    public void deleteUser(String login) {
        userRepository.findOneByLogin(login).ifPresent(user -> {
            userRepository.delete(user);
            cacheManager.getCache(USERS_CACHE).evict(login);
            log.debug("Deleted User: {}", user);
        });
    }

    public void changePassword(String password) {
        SecurityUtils.getCurrentUserLogin()
            .flatMap(userRepository::findOneByLogin)
            .ifPresent(user -> {
                String encryptedPassword = passwordEncoder.encode(password);
                user.setPassword(encryptedPassword);
                cacheManager.getCache(USERS_CACHE).evict(user.getLogin());
                log.debug("Changed password for User: {}", user);
            });
    }

    public User changePassword(ChangePasswordVM changePasswordDTO) {
        User user = currentUser();
        String dbOldPassword = user.getPassword();
        if (passwordEncoder.matches(changePasswordDTO.getOldPassword(), dbOldPassword)) {
            String encryptedPassword = passwordEncoder.encode(changePasswordDTO.getNewPassword());
            user.setPassword(encryptedPassword);
            userRepository.save(user);
            return user;
        }
        throw new InvalidOperationException("Invalid Old password");
        //TODO fix this
//        FieldError fieldError = new FieldError(ChangePasswordVM.VM_NAME, "oldPassword", "Invalid Old password");
//        throw new ValidationException(fieldError);
    }

    @Transactional(readOnly = true)
    public Page<UserDTO> getAllManagedUsers(Pageable pageable) {
        return userRepository.findAllByLoginNot(pageable, Constants.ANONYMOUS_USER).map(UserDTO::new);
    }

    @Transactional(readOnly = true)
    public Optional<User> getUserWithAuthoritiesByLogin(String login) {
        return userRepository.findOneByLogin(login);
    }

    @Transactional(readOnly = true)
    public Optional<User> getUserWithAuthorities(Long id) {
        return userRepository.findOneWithAuthoritiesById(id);
    }

    @Transactional(readOnly = true)
    public Optional<User> getUserWithAuthorities() {
        return SecurityUtils.getCurrentUserLogin().flatMap(userRepository::findOneByLogin);
    }

    /**
     * Not activated users should be automatically deleted after 3 days.
     * <p>
     * This is scheduled to get fired everyday, at 01:00 (am).
     */
    @Scheduled(cron = "0 0 1 * * ?")
    public void removeNotActivatedUsers() {
        List<User> users = userRepository.findAllByActivatedIsFalseAndCreatedDateBefore(Instant.now().minus(3, ChronoUnit.DAYS));
        for (User user : users) {
            log.debug("Deleting not activated user {}", user.getLogin());
            userRepository.delete(user);
            cacheManager.getCache(USERS_CACHE).evict(user.getLogin());
        }
    }

    /**
     * @return a list of all the authorities
     */
    public List<String> getAuthorities() {
        return authorityRepository.findAll().stream().map(Authority::getName).collect(Collectors.toList());
    }

    public User currentUser() {
        Optional<String> nameOptional = SecurityUtils.getCurrentUserLogin();
        if(nameOptional.isPresent()) {

            String name = nameOptional.get();
            Optional<User> userOptional = userRepository.findOneByLogin(name);
            if(!userOptional.isPresent()) {
                throw new AccessDeniedException("User not logged in");
            } else if(userOptional.get().getLogin().equalsIgnoreCase("anonymousUser")) {
                throw new AccessDeniedException("Anonymous User not allowed");
            } else if(userOptional.get().getLogin().equalsIgnoreCase("system")) {
                throw new AccessDeniedException("System User not allowed");
            }
            return userOptional.get();
        } else {
            throw new AccessDeniedException("User not logged in");
        }
    }

    public boolean validateUser(User user, String email) {
        Optional<User> userOptional = userRepository.findOneByEmail(email);
        log.debug("USER OPTIONAL : {}", userOptional.isPresent());
        if (userOptional.isPresent() && !userOptional.get().getId().equals(user.getId())) {
            throw new InvalidOperationException(("Email already in use"));
        }
        return true;
    }

    public boolean validateUser(User user, UserVM userVM) {
        Optional<User> userOptional = userRepository.findOneByLogin(userVM.getLogin().toLowerCase());
        if (userOptional.isPresent() && !userOptional.get().getId().equals(user.getId())) {
            throw new InvalidOperationException("Username already in use");
        }
        userOptional = userRepository.findOneByEmail(userVM.getEmail());
        log.debug("USER OPTIONAL : {}", userOptional.isPresent());
        if (userOptional.isPresent() && !userOptional.get().getId().equals(user.getId())) {
            throw new InvalidOperationException(("Email already in use"));
        }
        return true;
    }

    public boolean validateUser(User user, StoreUserProfileUpdateVM userVM) {
        Optional<User> userOptional = userRepository.findOneByEmail(userVM.getEmail());
        log.debug("USER OPTIONAL : {}", userOptional.isPresent());
        if (userOptional.isPresent() && !userOptional.get().getId().equals(user.getId())) {
            throw new InvalidOperationException(("Email already in use"));
        }
        return true;
    }
}
