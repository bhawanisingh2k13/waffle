package com.hocrox.belgian.service;


import com.hocrox.belgian.domain.Device;
import com.hocrox.belgian.repository.DeviceRepository;
import com.hocrox.belgian.service.dto.DeviceDTO;
import com.hocrox.belgian.service.mapper.DeviceMapper;
import com.hocrox.belgian.web.rest.vm.DeviceVM;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by bhawanisingh on 16/09/15.
 */
@Slf4j
@Service
@Transactional
public class DeviceService {


    private final DeviceRepository deviceRepository;
    private final DeviceMapper deviceMapper;
    private final UserService userService;


    public DeviceService(DeviceRepository deviceRepository, DeviceMapper deviceMapper, UserService userService) {
        this.deviceRepository = deviceRepository;
        this.deviceMapper = deviceMapper;
        this.userService = userService;
    }

    /**
     * Save a device.
     *
     * @param deviceVM the entity to save
     * @return the persisted entity
     */
    public DeviceDTO save(DeviceVM deviceVM) {
        log.debug("Request to save Device : {}", deviceVM);
        deviceRepository.deleteByDeviceID(deviceVM.getDeviceID());
        Device device = deviceMapper.toEntity(deviceVM);
        device.setOwner(userService.currentUser());
        device = deviceRepository.save(device);
        return deviceMapper.toDto(device);
    }

    /**
     * Delete the  device by deviceId.
     *
     * @param deviceId the id of the entity
     */
    public void delete(String deviceId) {
        log.debug("Request to delete device : {}", deviceId);
        deviceRepository.deleteByDeviceID(deviceId);
    }
}
