package com.hocrox.belgian.service;


import com.hocrox.belgian.domain.User_;
import com.hocrox.belgian.domain.WaffleStore_;
import com.hocrox.belgian.domain.WaffleUserNotification;
import com.hocrox.belgian.domain.WaffleUserNotification_;
import com.hocrox.belgian.repository.WaffleUserNotificationRepository;
import com.hocrox.belgian.service.dto.WaffleUserNotificationCriteria;
import com.hocrox.belgian.service.dto.WaffleUserNotificationDTO;
import com.hocrox.belgian.service.mapper.WaffleUserNotificationMapper;
import io.github.jhipster.service.QueryService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Service for executing complex queries for WaffleUserNotification entities in the database.
 * The main input is a {@link WaffleUserNotificationCriteria} which get's converted to {@link Specifications},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link WaffleUserNotificationDTO} or a {@link Page} of {@link WaffleUserNotificationDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class WaffleUserNotificationQueryService extends QueryService<WaffleUserNotification> {

    private final Logger log = LoggerFactory.getLogger(WaffleUserNotificationQueryService.class);


    private final WaffleUserNotificationRepository waffleUserNotificationRepository;

    private final WaffleUserNotificationMapper waffleUserNotificationMapper;

    public WaffleUserNotificationQueryService(WaffleUserNotificationRepository waffleUserNotificationRepository, WaffleUserNotificationMapper waffleUserNotificationMapper) {
        this.waffleUserNotificationRepository = waffleUserNotificationRepository;
        this.waffleUserNotificationMapper = waffleUserNotificationMapper;
    }

    /**
     * Return a {@link List} of {@link WaffleUserNotificationDTO} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<WaffleUserNotificationDTO> findByCriteria(WaffleUserNotificationCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specifications<WaffleUserNotification> specification = createSpecification(criteria);
        return waffleUserNotificationMapper.toDto(waffleUserNotificationRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link WaffleUserNotificationDTO} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<WaffleUserNotificationDTO> findByCriteria(WaffleUserNotificationCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specifications<WaffleUserNotification> specification = createSpecification(criteria);
        final Page<WaffleUserNotification> result = waffleUserNotificationRepository.findAll(specification, page);
        return result.map(waffleUserNotificationMapper::toDto);
    }

    /**
     * Function to convert WaffleUserNotificationCriteria to a {@link Specifications}
     */
    private Specifications<WaffleUserNotification> createSpecification(WaffleUserNotificationCriteria criteria) {
        Specifications<WaffleUserNotification> specification = Specifications.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), WaffleUserNotification_.id));
            }
            if (criteria.getNotiBody() != null) {
                specification = specification.and(buildStringSpecification(criteria.getNotiBody(), WaffleUserNotification_.notiBody));
            }
            if (criteria.getTitle() != null) {
                specification = specification.and(buildStringSpecification(criteria.getTitle(), WaffleUserNotification_.title));
            }
            if (criteria.getImage() != null) {
                specification = specification.and(buildStringSpecification(criteria.getImage(), WaffleUserNotification_.image));
            }
            if (criteria.getSentOn() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getSentOn(), WaffleUserNotification_.sentOn));
            }
            if (criteria.getScheduledTime() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getScheduledTime(), WaffleUserNotification_.scheduledTime));
            }
            if (criteria.getNotiStatus() != null) {
                specification = specification.and(buildSpecification(criteria.getNotiStatus(), WaffleUserNotification_.notiStatus));
            }
            if (criteria.getUserId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getUserId(), WaffleUserNotification_.users, User_.id));
            }
            if (criteria.getWaffleStoreId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getWaffleStoreId(), WaffleUserNotification_.waffleStore, WaffleStore_.id));
            }
        }
        return specification;
    }

}
