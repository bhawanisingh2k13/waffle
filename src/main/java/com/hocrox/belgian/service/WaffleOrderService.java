package com.hocrox.belgian.service;

import com.hocrox.belgian.domain.*;
import com.hocrox.belgian.domain.enumeration.*;
import com.hocrox.belgian.messaging.Notification;
import com.hocrox.belgian.repository.*;
import com.hocrox.belgian.service.dto.WaffleOrderDTO;
import com.hocrox.belgian.service.mapper.WaffleOrderMapper;
import com.hocrox.belgian.service.mapper.WaffleStoreMapper;
import com.hocrox.belgian.service.util.RandomUtil;
import com.hocrox.belgian.web.rest.errors.InvalidOperationException;
import com.hocrox.belgian.web.rest.vm.OrderItemAddonVM;
import com.hocrox.belgian.web.rest.vm.OrderItemVM;
import com.hocrox.belgian.web.rest.vm.WaffleOrderVM;
import com.hocrox.belgian.web.rest.vm.search.criteria.WaffleOrderCriteria;
import com.razorpay.Payment;
import com.razorpay.RazorpayClient;
import com.razorpay.RazorpayException;
import org.apache.commons.lang3.StringUtils;
import org.hashids.Hashids;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.messaging.simp.SimpMessageSendingOperations;
import org.springframework.scheduling.annotation.Async;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.time.Instant;
import java.util.*;
import java.util.stream.Collectors;


/**
 * Service Implementation for managing WaffleOrder.
 */
@Service
@Transactional
public class WaffleOrderService {

    private static final String USERS_CACHE = "users";

    private final Logger log = LoggerFactory.getLogger(WaffleOrderService.class);

    private final WaffleOrderRepository waffleOrderRepository;

    private final OrderItemRepository orderItemRepository;

    private final StoreUserProfileRepository storeUserProfileRepository;

    private final ProfileRepository profileRepository;

    private final WaffleOrderMapper waffleOrderMapper;

    private final WaffleStoreMapper waffleStoreMapper;

    private final UserService userService;

    private final Notification notification;


    private final SimpMessageSendingOperations messagingTemplate;

    private final WaffleOrderQueryService waffleOrderQueryService;

    private final ExcelService excelService;

    public WaffleOrderService(WaffleOrderRepository waffleOrderRepository, OrderItemRepository orderItemRepository, StoreUserProfileRepository storeUserProfileRepository, ProfileRepository profileRepository, WaffleOrderMapper waffleOrderMapper, WaffleStoreMapper waffleStoreMapper, UserService userService, Notification notification, SimpMessageSendingOperations messagingTemplate, WaffleOrderQueryService waffleOrderQueryService, ExcelService excelService) {
        this.waffleOrderRepository = waffleOrderRepository;
        this.orderItemRepository = orderItemRepository;
        this.storeUserProfileRepository = storeUserProfileRepository;
        this.profileRepository = profileRepository;
        this.waffleOrderMapper = waffleOrderMapper;
        this.waffleStoreMapper = waffleStoreMapper;
        this.userService = userService;
        this.notification = notification;
        this.messagingTemplate = messagingTemplate;
        this.waffleOrderQueryService = waffleOrderQueryService;
        this.excelService = excelService;
    }

    /**
     * Save a waffleOrder.
     *
     * @param waffleOrderVM the entity to save
     * @return the persisted entity
     */
    public WaffleOrderDTO save(WaffleOrderVM waffleOrderVM) {
        log.debug("Request to save WaffleOrder : {}", waffleOrderVM);
        Optional<WaffleOrder> waffleOrderOptional = waffleOrderRepository.findOneByCartToken(waffleOrderVM.getCartToken());
        if(!waffleOrderOptional.isPresent()) {
            throw new InvalidOperationException("Invalid order selected");
        }
        WaffleOrder waffleOrder = waffleOrderOptional.get();

        waffleOrder = waffleOrderRepository.save(waffleOrder);
        WaffleOrderDTO waffleOrderDTO = waffleOrderMapper.toDto(waffleOrder);

        publishEvent(waffleOrder.getId());
        return waffleOrderDTO;
    }

    public void testData() {
        publishEvent(10l);
    }

    public WaffleOrderDTO getOrder(String orderId) {
        Optional<WaffleOrder> orderOptional = waffleOrderRepository.findOneByCartToken(orderId);
        if(orderOptional.isPresent()) {
            return buildDto(orderOptional.get());
        } else {
            throw new InvalidOperationException("Invalid cart");
        }
    }

    public WaffleOrderDTO placeOrder(Long orderId, String razorPayId) {
        Optional<WaffleOrder> waffleOrderOptional = waffleOrderRepository.findOneById(orderId);
        if(!waffleOrderOptional.isPresent()) {
            throw new InvalidOperationException("Invalid order selected");
        }

        WaffleOrder waffleOrder = waffleOrderOptional.get();

        WaffleOrderDTO waffleOrderDTO =  waffleOrderMapper.toDto(waffleOrder);
        publishEvent(waffleOrder.getId());
        return waffleOrderDTO;
    }

    public WaffleOrderDTO updateStoreOrder(Long orderId, String storeOrderNumber) {
        Optional<WaffleOrder> waffleOrderOptional = waffleOrderRepository.findOneById(orderId);
        if(!waffleOrderOptional.isPresent()) {
            throw new InvalidOperationException("Invalid order selected");
        }
        WaffleOrder waffleOrder = waffleOrderOptional.get();
        WaffleOrderDTO waffleOrderDTO =  waffleOrderMapper.toDto(waffleOrder);
        return waffleOrderDTO;
    }
    /**
     * Get all the waffleOrders.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<WaffleOrderDTO> findAll(Pageable pageable) {
        log.debug("Request to get all WaffleOrders");
        return waffleOrderRepository.findAll(pageable)
            .map(waffleOrderMapper::toDto);
    }

    @Transactional(readOnly = true)
    public Page<WaffleOrderDTO> findTodayOrderByStoreDriver(Pageable pageable) {
        log.debug("Request to get all WaffleOrders");
        User driver = userService.currentUser();
        Optional<StoreUserProfile> storeUserProfileOptional = storeUserProfileRepository.findOneByUser(driver);
        if(!storeUserProfileOptional.isPresent()) {
            throw new AccessDeniedException("You are not driver");
        }
        Instant instant = Instant.now();
        ArrayList<OrderStatus> orderStatuses = new ArrayList<>();
        return waffleOrderRepository.findAllByStoreUserProfileAndOrderStatusNotInOrderByIdDesc(storeUserProfileOptional.get(), orderStatuses, pageable)
                .map(waffleOrderMapper::toDto);
    }

    @Transactional(readOnly = true)
    public Page<WaffleOrderDTO> findAllByStoreDriver(Pageable pageable) {
        log.debug("Request to get all WaffleOrders");
        User driver = userService.currentUser();
        Optional<StoreUserProfile> storeUserProfileOptional = storeUserProfileRepository.findOneByUser(driver);
        if(!storeUserProfileOptional.isPresent()) {
            throw new AccessDeniedException("You are not driver");
        }
        ArrayList<OrderStatus> orderStatuses = new ArrayList<>();
        return waffleOrderRepository.findAllByStoreUserProfileAndOrderStatusNotInOrderByIdDesc(storeUserProfileOptional.get(), orderStatuses, pageable)
                .map(waffleOrderMapper::toDto);
    }

    @Transactional(readOnly = true)
    public Page<WaffleOrderDTO> findAllByStoreDriverFilter(Date startDate, Date endDate, Pageable pageable) {
        log.debug("Request to get all WaffleOrders");
        User driver = userService.currentUser();
        Optional<StoreUserProfile> storeUserProfileOptional = storeUserProfileRepository.findOneByUser(driver);
        if(!storeUserProfileOptional.isPresent()) {
            throw new AccessDeniedException("You are not driver");
        }
        ArrayList<OrderStatus> orderStatuses = new ArrayList<>();
        return waffleOrderRepository.findAllByStoreUserProfileAndOrderStatusNotInOrderByIdDesc(storeUserProfileOptional.get(), orderStatuses, pageable)
            .map(waffleOrderMapper::toDto);
    }

    @Transactional(readOnly = true)
    public Page<WaffleOrderDTO> findAllByUser(Pageable pageable) {
        log.debug("Request to get all WaffleOrders");
        User user = userService.currentUser();
        Profile profile = profileRepository.findOneByUser(user);
        if(profile == null) {
            throw new AccessDeniedException("You are not the authorised user");
        }
        ArrayList<OrderStatus> orderStatuses = new ArrayList<>();
        return waffleOrderRepository.findAllByProfileAndOrderStatusNotInOrderByIdDesc(profile, orderStatuses, pageable)
                .map(waffleOrderMapper::toDto);
    }

    /**
     * Get one waffleOrder by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public WaffleOrderDTO findOne(Long id) {
        log.debug("Request to get WaffleOrder : {}", id);
        WaffleOrder waffleOrder = waffleOrderRepository.findOne(id);
        return waffleOrderMapper.toDto(waffleOrder);
    }

    @Transactional(readOnly = true)
    public File mailExcel(WaffleOrderCriteria criteria) {
        List<WaffleOrderDTO> waffleOrders = waffleOrderQueryService.findByCriteria(criteria);
        return excelService.getWaffleOrder(waffleOrders);
    }

    /**
     * Delete the waffleOrder by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete WaffleOrder : {}", id);
        waffleOrderRepository.delete(id);
    }

    public WaffleOrderDTO updateOrderStatus(Long orderId, OrderStatus orderStatus) {
        WaffleOrder waffleOrder = waffleOrderRepository.findOne(orderId);

        User user = userService.currentUser();
        Optional<StoreUserProfile> storeUserProfileOptional = storeUserProfileRepository.findOneByUser(user);
        if(!storeUserProfileOptional.isPresent()) {
            throw new AccessDeniedException("You are not the authorized person");
        }

        if(waffleOrder.getWaffleStore().getId() != storeUserProfileOptional.get().getWaffleStore().getId()) {
            throw new AccessDeniedException("You have selected the wrong order");
        }

        if(waffleOrder.getOrderStatus() == OrderStatus.CANCELLED) {
            throw new InvalidOperationException("Order is already cancelled");
        }
        if(waffleOrder.getOrderStatus().getValue() < orderStatus.getValue()) {
            waffleOrder.setOrderStatus(orderStatus);
            waffleOrder = waffleOrderRepository.save(waffleOrder);

            Profile profile = waffleOrder.getProfile();
            List<StoreUserProfile> storeUserProfiles;
            List<User> users;
            switch (orderStatus) {
                case PREPARING:
                    notification.pushNotification(profile.getUser(), NotificationType.O_PREP, waffleOrder.getId(), "Your order with order number " + waffleOrder.getId() + " is now preparing");
                    break;
                case CANCELLED:
                    notification.pushNotification(profile.getUser(), NotificationType.O_CNCL, waffleOrder.getId(), "Your order with order number " + waffleOrder.getId() + " has been cancelled");
                    //todo process refund
                    break;
                case OUT_FOR_DELIVERY:
                    waffleOrder.setOutForDeliveryTime(Instant.now());
                    notification.pushNotification(profile.getUser(), NotificationType.O_OFD, waffleOrder.getId(), "Your order with order number " + waffleOrder.getId() + " is now out for delivery");
                    break;
                case DELIVERED:
                    waffleOrder.setDeliveryTime(Instant.now());
                    // todo process split payment
                    break;
            }
            return waffleOrderMapper.toDto(waffleOrder);
        }
        else if(orderStatus == OrderStatus.CANCELLED) {
            throw new InvalidOperationException("You can't cancel order now");
        } else{
            throw new InvalidOperationException("Please choose other status for the order");
        }
    }

    public WaffleOrderDTO assignDeliveryBoy(Long deliveryBoyId, Long orderId) {
        WaffleOrder waffleOrder = waffleOrderRepository.findOne(orderId);
        throw new InvalidOperationException("Selected user is not delivery boy");
    }

    @Async
    public void publishEvent(Long orderId) {
        WaffleOrder waffleOrder = waffleOrderRepository.findOne(orderId);
        WaffleOrderDTO waffleOrderDTO = waffleOrderMapper.toDto(waffleOrder);
        log.debug("Sending waffle order data {}", waffleOrderDTO);
        Hashids hashids = new Hashids(WaffleStore.WAFFLE_RANDOM, WaffleStore.WAFFLE_HASH_LENGTH);
        messagingTemplate.convertAndSend("/topic/waffle-orders/noti/" +  hashids.encode(waffleOrder.getWaffleStore().getId()), waffleOrderDTO);
        messagingTemplate.convertAndSend("/topic/waffle-orders/" +  hashids.encode(waffleOrder.getWaffleStore().getId()), waffleOrderDTO);
    }

//     Order Management

    public WaffleOrderDTO removeItem(String orderId, Long cartItemId) {
        Optional<OrderItem> orderItemOptional = orderItemRepository.findOneByWaffleOrderCartTokenAndId(orderId, cartItemId);
        if(orderItemOptional.isPresent()){
            OrderItem orderItem = orderItemOptional.get();
            WaffleOrder waffleOrder = orderItem.getWaffleOrder();
            waffleOrderRepository.save(waffleOrder);
            orderItem.setWaffleOrder(null);
            return buildDto(waffleOrder);
        } else {
            throw new InvalidOperationException("Invalid cart item specified");
        }
    }

    public WaffleOrderDTO updateOrder(String cartToken, Long cartItemId, Integer qty) {
        Optional<OrderItem> orderItemOptional = orderItemRepository.findOneByWaffleOrderCartTokenAndId(cartToken, cartItemId);
        if(!orderItemOptional.isPresent()) {
            throw new InvalidOperationException("Item not in your cart");
        }
        OrderItem orderItem = orderItemOptional.get();
        return buildDto(updateOrder(orderItem, qty));
    }

    public WaffleOrderDTO addItem(OrderItemVM orderItemVM) {
        WaffleOrder waffleOrder;
        if(StringUtils.isNotBlank(orderItemVM.getCartToken())) {
            Optional<WaffleOrder> orderOptional = waffleOrderRepository.findOneByCartToken(orderItemVM.getCartToken());
            waffleOrder = orderOptional.orElseGet(() -> startOrder(orderItemVM));
        } else {
            waffleOrder = startOrder(orderItemVM);
        }
        return buildDto(waffleOrder);
    }

    private OrderItem itemMatched(OrderItemVM orderItemVM) {
        List<OrderItem> orderItems = orderItemRepository.findAllByWaffleOrderCartTokenAndMenuItemId(orderItemVM.getCartToken(), orderItemVM.getMenuItemId());
        for(OrderItem oi :  orderItems) {

        }
        return null;
    }

    private boolean itemAddonMatched(OrderItemVM orderItemVM, List<OrderItemAddon> orderItemAddons) {
        boolean matched = true;

        return matched;
    }

    private WaffleOrder startOrder(OrderItemVM cartItemReqDTO) {
        WaffleOrder order = new WaffleOrder();
        WaffleStore waffleStore = waffleStoreMapper.fromId(cartItemReqDTO.getWaffleStoreId());
        order.setWaffleStore(waffleStore);
        return waffleOrderRepository.save(order);
    }

    private WaffleOrder updateOrder(OrderItem orderItem, Integer qty) {
        WaffleOrder waffleOrder = orderItem.getWaffleOrder();
        waffleOrderRepository.save(waffleOrder);
        return waffleOrder;
    }

    private WaffleOrderDTO buildDto(WaffleOrder waffleOrder) {
        return waffleOrderMapper.toDto(waffleOrder);
    }
}
