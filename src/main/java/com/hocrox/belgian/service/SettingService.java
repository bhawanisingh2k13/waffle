package com.hocrox.belgian.service;

import com.hocrox.belgian.domain.Setting;
import com.hocrox.belgian.domain.StoreUserProfile;
import com.hocrox.belgian.domain.User;
import com.hocrox.belgian.repository.SettingRepository;
import com.hocrox.belgian.repository.StoreUserProfileRepository;
import com.hocrox.belgian.service.dto.SettingDTO;
import com.hocrox.belgian.service.mapper.GlobalSettingMapper;
import com.hocrox.belgian.service.mapper.SettingMapper;
import com.hocrox.belgian.web.rest.errors.InvalidOperationException;
import com.hocrox.belgian.web.rest.vm.SettingVM;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;


/**
 * Service Implementation for managing Setting.
 */
@Service
@Transactional
public class SettingService {

    private final Logger log = LoggerFactory.getLogger(SettingService.class);

    private final SettingRepository settingRepository;

    private final SettingMapper settingMapper;

    private final UserService userService;

    private final StoreUserProfileRepository storeUserProfileRepository;

    private final GlobalSettingMapper globalSettingMapper;

    public SettingService(SettingRepository settingRepository, SettingMapper settingMapper, UserService userService, StoreUserProfileRepository storeUserProfileRepository, GlobalSettingMapper globalSettingMapper) {
        this.settingRepository = settingRepository;
        this.settingMapper = settingMapper;
        this.userService = userService;
        this.storeUserProfileRepository = storeUserProfileRepository;
        this.globalSettingMapper = globalSettingMapper;
    }

    /**
     * Save a setting.
     *
     * @param settingVM the entity to save
     * @return the persisted entity
     */
    public SettingDTO save(SettingVM settingVM) {
        log.debug("Request to save Setting : {}", settingVM);
        Setting setting = settingMapper.toEntity(settingVM);
        User user = userService.currentUser();
        Optional<StoreUserProfile> storeUserProfileOptional = storeUserProfileRepository.findOneByUser(user);
        if(!storeUserProfileOptional.isPresent()) {
            throw new InvalidOperationException("You are not allowed for this operation");
        }
        return settingMapper.toDto(setting);
    }

    /**
     * Get all the settings.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<SettingDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Settings");
        return settingRepository.findAll(pageable)
            .map(settingMapper::toDto);
    }

    /**
     * Get one setting by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public SettingDTO findOne(Long id) {
        log.debug("Request to get Setting : {}", id);
        Setting setting = settingRepository.findOne(id);
        return settingMapper.toDto(setting);
    }


    /**
     * Get one setting by store id.
     *
     * @param storeId the id of the store
     * @return the entity
     */
    public SettingDTO findOneUsingStoreId(Long storeId) {
        log.debug("Request to get Setting of store with id : {}", storeId);
        Setting setting = settingRepository.findOneByWaffleStoreId(storeId);
        return settingMapper.toDto(setting);
    }
    /**
     * Delete the setting by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete Setting : {}", id);
        settingRepository.delete(id);
    }
}
