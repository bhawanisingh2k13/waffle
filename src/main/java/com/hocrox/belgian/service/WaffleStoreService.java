package com.hocrox.belgian.service;

import com.hocrox.belgian.domain.StoreUserProfile;
import com.hocrox.belgian.domain.User;
import com.hocrox.belgian.domain.WaffleStore;
import com.hocrox.belgian.repository.StoreUserProfileRepository;
import com.hocrox.belgian.repository.WaffleStoreRepository;
import com.hocrox.belgian.service.dto.WaffleStoreContactDTO;
import com.hocrox.belgian.service.dto.WaffleStoreDTO;
import com.hocrox.belgian.service.mapper.WaffleStoreMapper;
import com.hocrox.belgian.web.rest.vm.WaffleStoreVM;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;


/**
 * Service Implementation for managing WaffleStore.
 */
@Service
@Transactional
public class WaffleStoreService {

    private final Logger log = LoggerFactory.getLogger(WaffleStoreService.class);

    private final WaffleStoreRepository waffleStoreRepository;

    private final WaffleStoreMapper waffleStoreMapper;

    private final UserService userService;

    private final StoreUserProfileRepository storeUserProfileRepository;

    public WaffleStoreService(WaffleStoreRepository waffleStoreRepository, WaffleStoreMapper waffleStoreMapper, UserService userService, StoreUserProfileRepository storeUserProfileRepository) {
        this.waffleStoreRepository = waffleStoreRepository;
        this.waffleStoreMapper = waffleStoreMapper;
        this.userService = userService;
        this.storeUserProfileRepository = storeUserProfileRepository;
    }

    /**
     * Save a waffleStore.
     *
     * @param waffleStoreVM the entity to save
     * @return the persisted entity
     */
    public WaffleStoreDTO create(WaffleStoreVM waffleStoreVM) {
        log.debug("Request to save WaffleStore : {}", waffleStoreVM);
        WaffleStore waffleStore = waffleStoreMapper.toEntity(waffleStoreVM);
        User user = userService.currentUser();
        Optional<StoreUserProfile> storeUserProfileOptional = storeUserProfileRepository.findOneByUser(user);
        if(!storeUserProfileOptional.isPresent()) {
            throw new AccessDeniedException("You are not authorized for this");
        }
        StoreUserProfile storeUserProfile = storeUserProfileOptional.get();
        waffleStore.setOwner(user);
        waffleStore = waffleStoreRepository.save(waffleStore);
        storeUserProfile.setWaffleStore(waffleStore);
        storeUserProfileRepository.save(storeUserProfile);
        return waffleStoreMapper.toDto(waffleStore);
    }

    public WaffleStoreDTO update(WaffleStoreVM waffleStoreVM) {
        //todo its jugaad fix it
        log.debug("Request to update WaffleStore : {}", waffleStoreVM);
        WaffleStore waffleStore = waffleStoreMapper.toEntity(waffleStoreVM);
        User user = userService.currentUser();
        Optional<StoreUserProfile> storeUserProfileOptional = storeUserProfileRepository.findOneByUser(user);
        if(!storeUserProfileOptional.isPresent()) {
            throw new AccessDeniedException("You are not authorized for this");
        }
        waffleStore.setOwner(user);
        waffleStore = waffleStoreRepository.save(waffleStore);
        return waffleStoreMapper.toDto(waffleStore);
    }

    /**
     * Get all the waffleStores.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<WaffleStoreDTO> findAll(Pageable pageable) {
        log.debug("Request to get all WaffleStores");
        return waffleStoreRepository.findAll(pageable)
            .map(waffleStoreMapper::toDto);
    }

    /**
     * Get all the waffleStores contact info.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<WaffleStoreContactDTO> findAllForContact(Pageable pageable) {
        log.debug("Request to get all WaffleStores contact dto");
        return waffleStoreRepository.findAll(pageable)
                .map(waffleStoreMapper::toContactDto);
    }

    /**
     * Get one waffleStore by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public WaffleStoreDTO findOne(Long id) {
        log.debug("Request to get WaffleStore : {}", id);
        WaffleStore waffleStore = waffleStoreRepository.findOne(id);
        return waffleStoreMapper.toDto(waffleStore);
    }

    /**
     * Delete the waffleStore by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete WaffleStore : {}", id);
        waffleStoreRepository.delete(id);
    }

    @Transactional(readOnly = true)
    public WaffleStoreDTO findOneByCurrentUser() {
        User user = userService.currentUser();
        Optional<StoreUserProfile> storeUserProfileOptional = storeUserProfileRepository.findOneByUser(user);
        if(!storeUserProfileOptional.isPresent()) {
            throw new AccessDeniedException("You are not authorized for this");
        }
        return waffleStoreMapper.toDto(storeUserProfileOptional.get().getWaffleStore());
    }
}
