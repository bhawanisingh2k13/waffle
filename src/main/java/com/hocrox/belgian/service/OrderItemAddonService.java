package com.hocrox.belgian.service;

import com.hocrox.belgian.domain.OrderItemAddon;
import com.hocrox.belgian.repository.OrderItemAddonRepository;
import com.hocrox.belgian.service.dto.OrderItemAddonDTO;
import com.hocrox.belgian.service.mapper.OrderItemAddonMapper;
import com.hocrox.belgian.web.rest.vm.OrderItemAddonVM;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * Service Implementation for managing OrderItemAddon.
 */
@Service
@Transactional
public class OrderItemAddonService {

    private final Logger log = LoggerFactory.getLogger(OrderItemAddonService.class);

    private final OrderItemAddonRepository orderItemAddonRepository;

    private final OrderItemAddonMapper orderItemAddonMapper;

    public OrderItemAddonService(OrderItemAddonRepository orderItemAddonRepository, OrderItemAddonMapper orderItemAddonMapper) {
        this.orderItemAddonRepository = orderItemAddonRepository;
        this.orderItemAddonMapper = orderItemAddonMapper;
    }

    /**
     * Save a orderItemAddon.
     *
     * @param orderItemAddonVM the entity to save
     * @return the persisted entity
     */
    public OrderItemAddonDTO save(OrderItemAddonVM orderItemAddonVM) {
        log.debug("Request to save OrderItemAddon : {}", orderItemAddonVM);
        OrderItemAddon orderItemAddon = orderItemAddonMapper.toEntity(orderItemAddonVM);
        orderItemAddon = orderItemAddonRepository.save(orderItemAddon);
        return orderItemAddonMapper.toDto(orderItemAddon);
    }

    /**
     * Get all the orderItemAddons.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<OrderItemAddonDTO> findAll(Pageable pageable) {
        log.debug("Request to get all OrderItemAddons");
        return orderItemAddonRepository.findAll(pageable)
            .map(orderItemAddonMapper::toDto);
    }

    /**
     * Get one orderItemAddon by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public OrderItemAddonDTO findOne(Long id) {
        log.debug("Request to get OrderItemAddon : {}", id);
        OrderItemAddon orderItemAddon = orderItemAddonRepository.findOne(id);
        return orderItemAddonMapper.toDto(orderItemAddon);
    }

    /**
     * Delete the orderItemAddon by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete OrderItemAddon : {}", id);
        orderItemAddonRepository.delete(id);
    }
}
