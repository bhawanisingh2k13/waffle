package com.hocrox.belgian.service;


import com.hocrox.belgian.domain.Addon_;
import com.hocrox.belgian.domain.MenuItem;
import com.hocrox.belgian.domain.MenuItemCategory_;
import com.hocrox.belgian.domain.MenuItem_;
import com.hocrox.belgian.repository.MenuItemRepository;
import com.hocrox.belgian.service.dto.MenuItemCriteria;
import com.hocrox.belgian.service.dto.MenuItemDTO;
import com.hocrox.belgian.service.mapper.MenuItemMapper;
import io.github.jhipster.service.QueryService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Service for executing complex queries for MenuItem entities in the database.
 * The main input is a {@link MenuItemCriteria} which get's converted to {@link Specifications},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link MenuItemDTO} or a {@link Page} of {@link MenuItemDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class MenuItemQueryService extends QueryService<MenuItem> {

    private final Logger log = LoggerFactory.getLogger(MenuItemQueryService.class);


    private final MenuItemRepository menuItemRepository;

    private final MenuItemMapper menuItemMapper;

    public MenuItemQueryService(MenuItemRepository menuItemRepository, MenuItemMapper menuItemMapper) {
        this.menuItemRepository = menuItemRepository;
        this.menuItemMapper = menuItemMapper;
    }

    /**
     * Return a {@link List} of {@link MenuItemDTO} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<MenuItemDTO> findByCriteria(MenuItemCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specifications<MenuItem> specification = createSpecification(criteria, true);
        return menuItemMapper.toDto(menuItemRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link MenuItemDTO} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<MenuItemDTO> findByCriteria(MenuItemCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specifications<MenuItem> specification = createSpecification(criteria, true);
        final Page<MenuItem> result = menuItemRepository.findAll(specification, page);
        return result.map(menuItemMapper::toDto);
    }

    @Transactional(readOnly = true)
    public Page<MenuItemDTO> findByCriteriaComplete(MenuItemCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specifications<MenuItem> specification = createSpecification(criteria, false);
        final Page<MenuItem> result = menuItemRepository.findAll(specification, page);
        return result.map(menuItemMapper::toDto);
    }

    /**
     * Function to convert MenuItemCriteria to a {@link Specifications}
     */
    private Specifications<MenuItem> createSpecification(MenuItemCriteria criteria, boolean includeEnable) {
        Specifications<MenuItem> specification = Specifications.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), MenuItem_.id));
            }
            if (criteria.getName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getName(), MenuItem_.name));
            }
            if (criteria.getPrice() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getPrice(), MenuItem_.price));
            }
            if (criteria.getCategoryId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getCategoryId(), MenuItem_.category, MenuItemCategory_.id));
            }
            if (criteria.getAddonId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getAddonId(), MenuItem_.addons, Addon_.id));
            }
            if(includeEnable) {
                specification = specification.and(equalsSpecification(MenuItem_.enabled, includeEnable));
            }
        }
        return specification;
    }

}
