package com.hocrox.belgian.service;

import com.hocrox.belgian.domain.PickupLocation;
import com.hocrox.belgian.domain.StoreUserProfile;
import com.hocrox.belgian.domain.User;
import com.hocrox.belgian.repository.PickupLocationRepository;
import com.hocrox.belgian.repository.StoreUserProfileRepository;
import com.hocrox.belgian.service.dto.PickupLocationDTO;
import com.hocrox.belgian.service.mapper.PickupLocationMapper;
import com.hocrox.belgian.web.rest.vm.PickupLocationVM;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing PickupLocation.
 */
@Service
@Transactional
public class PickupLocationService {

    private final Logger log = LoggerFactory.getLogger(PickupLocationService.class);

    private final PickupLocationRepository pickupLocationRepository;

    private final PickupLocationMapper pickupLocationMapper;

    private final UserService userService;

    private final StoreUserProfileRepository storeUserProfileRepository;

    public PickupLocationService(PickupLocationRepository pickupLocationRepository, PickupLocationMapper pickupLocationMapper, UserService userService, StoreUserProfileRepository storeUserProfileRepository) {
        this.pickupLocationRepository = pickupLocationRepository;
        this.pickupLocationMapper = pickupLocationMapper;
        this.userService = userService;
        this.storeUserProfileRepository = storeUserProfileRepository;
    }

    /**
     * Save a pickupLocation.
     *
     * @param pickupLocationVM the entity to save
     * @return the persisted entity
     */
    public PickupLocationDTO save(PickupLocationVM pickupLocationVM) {
        log.debug("Request to save PickupLocation : {}", pickupLocationVM);
        PickupLocation pickupLocation = pickupLocationMapper.toEntity(pickupLocationVM);
        User user = userService.currentUser();
        Optional<StoreUserProfile> storeUserProfileOptional = storeUserProfileRepository.findOneByUser(user);
        if(!storeUserProfileOptional.isPresent()) {
            throw new AccessDeniedException("You are not authorized for this");
        }
        StoreUserProfile storeUserProfile = storeUserProfileOptional.get();
        if(storeUserProfile.getWaffleStore() == null) {
            throw new AccessDeniedException("You don't own a store, you are : " + storeUserProfile.getUser().getLogin());
        }
        pickupLocation.setWaffleStore(storeUserProfile.getWaffleStore());
        pickupLocation = pickupLocationRepository.save(pickupLocation);
        return pickupLocationMapper.toDto(pickupLocation);
    }

    /**
     * Get all the pickupLocations.
     *
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<PickupLocationDTO> findAllForAdmin() {
        log.debug("Request to get all PickupLocations");
        return pickupLocationRepository.findAll().stream()
            .map(pickupLocationMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    @Transactional(readOnly = true)
    public List<PickupLocationDTO> findAll() {
        log.debug("Request to get all PickupLocations");
        return pickupLocationRepository.findAllByEnabled(true).stream()
                .map(pickupLocationMapper::toDto)
                .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     * Get one pickupLocation by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public PickupLocationDTO findOne(Long id) {
        log.debug("Request to get PickupLocation : {}", id);
        PickupLocation pickupLocation = pickupLocationRepository.findOne(id);
        return pickupLocationMapper.toDto(pickupLocation);
    }

    /**
     * Delete the pickupLocation by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete PickupLocation : {}", id);
        pickupLocationRepository.delete(id);
    }
}
