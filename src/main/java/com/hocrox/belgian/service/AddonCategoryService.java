package com.hocrox.belgian.service;

import com.hocrox.belgian.domain.AddonCategory;
import com.hocrox.belgian.repository.AddonCategoryRepository;
import com.hocrox.belgian.service.dto.AddonCategoryDTO;
import com.hocrox.belgian.service.mapper.AddonCategoryMapper;
import com.hocrox.belgian.web.rest.vm.AddonCategoryVM;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;


/**
 * Service Implementation for managing AddonCategory.
 */
@Service
@Transactional
public class AddonCategoryService {

    private final Logger log = LoggerFactory.getLogger(AddonCategoryService.class);

    private final AddonCategoryRepository addonCategoryRepository;

    private final AddonCategoryMapper addonCategoryMapper;

    public AddonCategoryService(AddonCategoryRepository addonCategoryRepository, AddonCategoryMapper addonCategoryMapper) {
        this.addonCategoryRepository = addonCategoryRepository;
        this.addonCategoryMapper = addonCategoryMapper;
    }

    /**
     * Save a addonCategory.
     *
     * @param addonCategoryVM the entity to save
     * @return the persisted entity
     */
    public AddonCategoryDTO save(AddonCategoryVM addonCategoryVM) {
        log.debug("Request to save AddonCategory : {}", addonCategoryVM);
        AddonCategory addonCategory = addonCategoryMapper.toEntity(addonCategoryVM);
        addonCategory = addonCategoryRepository.save(addonCategory);
        return addonCategoryMapper.toDto(addonCategory);
    }

    /**
     * Get all the addonCategories.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<AddonCategoryDTO> findAll(Pageable pageable) {
        log.debug("Request to get all AddonCategories");
        return addonCategoryRepository.findAll(pageable)
            .map(addonCategoryMapper::toDto);
    }

    @Transactional(readOnly = true)
    public List<AddonCategoryDTO> findAll() {
        log.debug("Request to get all AddonCategories");
        return addonCategoryRepository.findAll().stream()
                .map(addonCategoryMapper::toDto).collect(Collectors.toList());
    }

    /**
     * Get one addonCategory by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public AddonCategoryDTO findOne(Long id) {
        log.debug("Request to get AddonCategory : {}", id);
        AddonCategory addonCategory = addonCategoryRepository.findOne(id);
        return addonCategoryMapper.toDto(addonCategory);
    }

    /**
     * Delete the addonCategory by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete AddonCategory : {}", id);
        addonCategoryRepository.delete(id);
    }
}
