package com.hocrox.belgian.service;

import com.hocrox.belgian.service.dto.OrderItemAddonDTO;
import com.hocrox.belgian.service.dto.OrderItemDTO;
import com.hocrox.belgian.service.dto.WaffleOrderDTO;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by bhawanisingh on 12/04/18 4:56 PM.
 */
@Service
public class ExcelService {
    private static final SimpleDateFormat formatter = new SimpleDateFormat("dd MM yyyy HH:mm:ss");

    public File getWaffleOrder(List<WaffleOrderDTO> waffleOrders) {
        File file = new File("orders-" + System.nanoTime() +".xlsx");
        try {
            XSSFWorkbook workbook = new XSSFWorkbook();

            XSSFSheet sheet = workbook.createSheet("Orders");// creating a blank sheet
            Row row = sheet.createRow(0);
            row.setHeight((short)-1);
            Cell cell = row.createCell(0);
            cell.setCellValue("S. No.");
            cell = row.createCell(1);
            cell.setCellValue("Customer");
            cell = row.createCell(2);
            cell.setCellValue("Order Type");
            cell = row.createCell(3);
            cell.setCellValue("Payment Type");
            cell = row.createCell(4);
            cell.setCellValue("Order Items");
            cell = row.createCell(5);
            cell.setCellValue("Order Time");
            cell = row.createCell(6);
            cell.setCellValue("Delivery Time");
            cell = row.createCell(7);
            cell.setCellValue("Order Amount");
            cell = row.createCell(8);
            cell.setCellValue("Delivered By");
            int rowNum = 1;
            XSSFCellStyle style = workbook.createCellStyle();
            style.setWrapText(true);
            row.setRowStyle(style);
            for(WaffleOrderDTO waffleOrder : waffleOrders) {
                row = sheet.createRow(rowNum);
                row.setHeight((short)-1);
                cell = row.createCell(0);
                cell.setCellValue(rowNum);
                String customer = waffleOrder.getProfile().getFirstName() +
                        "\n" +
                        waffleOrder.getProfile().getEmail() +
                        "\n" +
                        waffleOrder.getProfile().getPhoneNumber();
                cell = row.createCell(1);
                cell.setCellValue(customer);
                cell = row.createCell(2);
                cell.setCellValue(waffleOrder.getOrderType().getName());
                cell = row.createCell(3);
                cell.setCellValue(waffleOrder.getPaymentType().getName());

                StringBuilder orderItemValue = new StringBuilder();
                for(OrderItemDTO orderItem: waffleOrder.getOrderItems()) {
                    orderItemValue.append(orderItem.getName())
                            .append("\t")
                            .append(orderItem.getTotalPrice())
                            .append(" (")
                            .append(orderItem.getQuantity())
                            .append(")\n");
                    for(OrderItemAddonDTO orderItemAddon: orderItem.getOrderItemAddons()) {
                        orderItemValue.append(orderItemAddon.getName())
                                .append("\t")
                                .append(orderItemAddon.getPrice())
                                .append("\n");
                    }
                }
                cell = row.createCell(4);
                cell.setCellValue(orderItemValue.toString());
                cell = row.createCell(5);
                if(waffleOrder.getDeliveryTime() != null) {
                    cell.setCellValue(formatter.format(Date.from(waffleOrder.getOrderTime())));
                } else {
                    cell.setCellValue("");
                }
                cell = row.createCell(6);
                if(waffleOrder.getDeliveryTime() != null) {
                    cell.setCellValue(formatter.format(Date.from(waffleOrder.getDeliveryTime())));
                } else {
                    cell.setCellValue("");
                }
                cell = row.createCell(7);
                cell.setCellValue(waffleOrder.getFinalPrice());
                cell = row.createCell(8);
                if(waffleOrder.getDeliveryBoy() != null) {
                    cell.setCellValue(waffleOrder.getDeliveryBoy().getFirstName());
                } else {
                    cell.setCellValue("");
                }
                row.setRowStyle(style);
                ++rowNum;

            }
            for(int i = 0; i < 9; ++i) {
                sheet.autoSizeColumn(i);
            }
            makeRowBold(workbook, sheet.getRow(0));
            sheet.createFreezePane(0, 1);
            FileOutputStream out = new FileOutputStream(file);
            workbook.write(out);
            out.close();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return file;

    }

    public static void makeRowBold(Workbook wb, Row row){
        CellStyle style = wb.createCellStyle();//Create style
        Font font = wb.createFont();//Create font
        font.setBold(true);//Make font bold
        style.setFont(font);//set it to bold

        for(int i = 0; i < row.getLastCellNum(); i++){//For each cell in the row
            row.getCell(i).setCellStyle(style);//Set the style
        }
    }
}
