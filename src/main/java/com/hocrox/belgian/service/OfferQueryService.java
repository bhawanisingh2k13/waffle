package com.hocrox.belgian.service;

import com.hocrox.belgian.domain.MenuItem_;
import com.hocrox.belgian.domain.Offer;
import com.hocrox.belgian.domain.Offer_;
import com.hocrox.belgian.domain.WaffleStore_;
import com.hocrox.belgian.repository.OfferRepository;
import com.hocrox.belgian.service.dto.OfferDTO;
import com.hocrox.belgian.service.mapper.OfferMapper;
import com.hocrox.belgian.web.rest.vm.search.criteria.OfferCriteria;
import io.github.jhipster.service.QueryService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Service for executing complex queries for Offer entities in the database.
 * The main input is a {@link OfferCriteria} which get's converted to {@link Specifications},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link OfferDTO} or a {@link Page} of {@link OfferDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class OfferQueryService extends QueryService<Offer> {

    private final Logger log = LoggerFactory.getLogger(OfferQueryService.class);


    private final OfferRepository offerRepository;

    private final OfferMapper offerMapper;

    public OfferQueryService(OfferRepository offerRepository, OfferMapper offerMapper) {
        this.offerRepository = offerRepository;
        this.offerMapper = offerMapper;
    }

    /**
     * Return a {@link List} of {@link OfferDTO} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<OfferDTO> findByCriteria(OfferCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specifications<Offer> specification = createSpecification(criteria);
        return offerMapper.toDto(offerRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link OfferDTO} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<OfferDTO> findByCriteria(OfferCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specifications<Offer> specification = createSpecification(criteria);
        final Page<Offer> result = offerRepository.findAll(specification, page);
        return result.map(offerMapper::toDto);
    }

    /**
     * Function to convert OfferCriteria to a {@link Specifications}
     */
    private Specifications<Offer> createSpecification(OfferCriteria criteria) {
        Specifications<Offer> specification = Specifications.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), Offer_.id));
            }
            if (criteria.getToDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getToDate(), Offer_.toDate));
            }
            if (criteria.getFromDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getFromDate(), Offer_.fromDate));
            }
            if (criteria.getCouponCode() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCouponCode(), Offer_.couponCode));
            }
            if (criteria.getBill() != null) {
                specification = specification.and(buildSpecification(criteria.getBill(), Offer_.bill));
            }
            if (criteria.getGlobal() != null) {
                specification = specification.and(buildSpecification(criteria.getGlobal(), Offer_.global));
            }
            if (criteria.getImage() != null) {
                specification = specification.and(buildStringSpecification(criteria.getImage(), Offer_.image));
            }
            if (criteria.getPercentOff() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getPercentOff(), Offer_.percentOff));
            }
            if (criteria.getMenuItemId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getMenuItemId(), Offer_.menuItem, MenuItem_.id));
            }
            if (criteria.getWaffleStoreId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getWaffleStoreId(), Offer_.waffleStore, WaffleStore_.id));
            }
        }
        return specification;
    }

}
