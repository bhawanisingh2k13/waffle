package com.hocrox.belgian.service.dto;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the Setting entity.
 */
@Slf4j
@Getter
@Setter
@ToString
@NoArgsConstructor
public class SettingDTO implements Serializable {

    private Long id;

    private Boolean storeAvailable;

    private Boolean delayedDelivery;

    private String delayedDeliveryReason;

    private Integer delayedDeliveryTime;

    private Boolean deliveryAvailable;

    private Boolean codAvailable;

    private Boolean odtAvailable;

    private Long deliveryRadius = 0L;

    private Float deliveryCharges = 0.0F;

    private Float handlingCharges = 0.0F;

    private Float packagingCharges = 0.0F;

    private Float panCakePackagingCharges = 0.0F;

    private Long androidAppVersion;

    private Long waffleStoreId;

    private Integer minOrderQty = 2;

    private Integer maxOrderQty = 20;

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        SettingDTO settingDTO = (SettingDTO) o;
        if(settingDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), settingDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

}
