package com.hocrox.belgian.service.dto;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

import javax.persistence.Lob;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the AddonCategory entity.
 */
@Slf4j
@Getter
@Setter
@ToString
@NoArgsConstructor
public class AddonCategoryDTO implements Serializable {

    private Long id;

    @NotNull
    private String name;

    @Lob
    private String image;

    private String imageUrl;

    private String thumbUrl;

    private boolean enabled = true;

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        AddonCategoryDTO addonCategoryDTO = (AddonCategoryDTO) o;
        return addonCategoryDTO.getId() != null && getId() != null && Objects.equals(getId(), addonCategoryDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

}
