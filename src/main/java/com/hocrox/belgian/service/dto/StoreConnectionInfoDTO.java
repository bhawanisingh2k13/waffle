package com.hocrox.belgian.service.dto;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;

/**
 * A DTO for the StoreConnectionInfo entity.
 */
@Slf4j
@Getter
@Setter
@ToString
@NoArgsConstructor
public class StoreConnectionInfoDTO implements Serializable {

    private Long id;

    @NotNull
    private String connectionId;

    private Long waffleStoreId;

    private Instant createdDate;

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        StoreConnectionInfoDTO storeConnectionInfoDTO = (StoreConnectionInfoDTO) o;
        if(storeConnectionInfoDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), storeConnectionInfoDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

}
