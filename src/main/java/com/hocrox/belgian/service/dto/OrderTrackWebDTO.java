package com.hocrox.belgian.service.dto;


import com.hocrox.belgian.domain.enumeration.OrderType;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the OrderTrack entity.
 */
@Slf4j
@Getter
@Setter
@ToString
@NoArgsConstructor
public class OrderTrackWebDTO implements Serializable {

    private Long id;

    @NotNull
    private Double lat;

    @NotNull
    private Double lng;

    private Long trackTime;

    private Long waffleOrderId;

    private boolean deliveryBoy;

    private String driverTimeInSec = "";

    private String driverDstInM = "";

    private String userTimeInSec = "";

    private String userDstInM = "";

    private OrderType orderType;

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        OrderTrackWebDTO orderTrackDTO = (OrderTrackWebDTO) o;
        if(orderTrackDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), orderTrackDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }
}
