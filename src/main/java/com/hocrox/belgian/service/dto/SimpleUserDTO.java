package com.hocrox.belgian.service.dto;

import com.hocrox.belgian.domain.User;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.validator.constraints.Email;

import javax.validation.constraints.Size;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class SimpleUserDTO {

    private String login;

    private String firstName;

    private String lastName;

    private String email;

    private String imageUrl;

    public SimpleUserDTO(User user) {
        this.login = user.getLogin();
        this.firstName = user.getFirstName();
        this.lastName = user.getLastName();
        this.email = user.getEmail();;
        this.imageUrl = user.getImageUrl();
    }
}
