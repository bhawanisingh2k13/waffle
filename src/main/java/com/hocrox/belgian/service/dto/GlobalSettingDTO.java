package com.hocrox.belgian.service.dto;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the GlobalSetting entity.
 */
@Slf4j
@Getter
@Setter
@ToString
@NoArgsConstructor
public class GlobalSettingDTO implements Serializable {

    private Long id;

    @NotNull
    private Integer minOrderQty;

    @NotNull
    private Integer maxOrderQty;

    @NotNull
    private Long androidAppVersion;

    private Float panCakePrice = 0f;

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        GlobalSettingDTO globalSettingDTO = (GlobalSettingDTO) o;
        if(globalSettingDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), globalSettingDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }
}
