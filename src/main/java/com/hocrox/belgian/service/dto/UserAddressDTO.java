package com.hocrox.belgian.service.dto;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the UserAddress entity.
 */
@Slf4j
@Getter
@Setter
@ToString
@NoArgsConstructor
public class UserAddressDTO implements Serializable {

    private Long id;

    private String street;

    private String formattedAddress;

    private String flat;

    private Double lat;

    private Double lng;

    private String city;

    private String state;

    private String country;

    private Boolean fromLocation;

    private Long profileId;

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        UserAddressDTO userAddressDTO = (UserAddressDTO) o;
        if(userAddressDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), userAddressDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

}
