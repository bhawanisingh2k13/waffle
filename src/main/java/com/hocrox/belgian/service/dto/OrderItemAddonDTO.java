package com.hocrox.belgian.service.dto;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the OrderItemAddon entity.
 */
@Slf4j
@Getter
@Setter
@ToString
@NoArgsConstructor
public class OrderItemAddonDTO implements Serializable, Comparable<OrderItemAddonDTO> {

    private Long id;

    private Double price;

    private Double offer;

    private Double finalPrice;

    private Long addonId;

//    private AddonDTO addon;

    private String name;

    private String image;

    private String imageUrl;

    private String thumbUrl;

    private Long orderItemId;

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        OrderItemAddonDTO orderItemAddonDTO = (OrderItemAddonDTO) o;
        return orderItemAddonDTO.getId() != null && getId() != null && Objects.equals(getId(), orderItemAddonDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public int compareTo(OrderItemAddonDTO orderItemAddonDTO) {
        return this.getId().compareTo( orderItemAddonDTO.getId());
    }
}
