package com.hocrox.belgian.service.dto;

import com.hocrox.belgian.domain.enumeration.NotiStatus;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.InstantFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;

import java.io.Serializable;




/**
 * Criteria class for the WaffleUserNotification entity. This class is used in WaffleUserNotificationResource to
 * receive all the possible filtering options from the Http GET request parameters.
 * For example the following could be a valid requests:
 * <code> /waffle-user-notifications?id.greaterThan=5&amp;attr1.contains=something&amp;attr2.specified=false</code>
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class WaffleUserNotificationCriteria implements Serializable {
    /**
     * Class for filtering NotiStatus
     */
    public static class NotiStatusFilter extends Filter<NotiStatus> {
    }

    private static final long serialVersionUID = 1L;


    private LongFilter id;

    private StringFilter notiBody;

    private StringFilter title;

    private StringFilter image;

    private InstantFilter sentOn;

    private InstantFilter scheduledTime;

    private NotiStatusFilter notiStatus;

    private LongFilter userId;

    private LongFilter waffleStoreId;

    public WaffleUserNotificationCriteria() {
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getNotiBody() {
        return notiBody;
    }

    public void setNotiBody(StringFilter notiBody) {
        this.notiBody = notiBody;
    }

    public StringFilter getTitle() {
        return title;
    }

    public void setTitle(StringFilter title) {
        this.title = title;
    }

    public StringFilter getImage() {
        return image;
    }

    public void setImage(StringFilter image) {
        this.image = image;
    }

    public InstantFilter getSentOn() {
        return sentOn;
    }

    public void setSentOn(InstantFilter sentOn) {
        this.sentOn = sentOn;
    }

    public InstantFilter getScheduledTime() {
        return scheduledTime;
    }

    public void setScheduledTime(InstantFilter scheduledTime) {
        this.scheduledTime = scheduledTime;
    }

    public NotiStatusFilter getNotiStatus() {
        return notiStatus;
    }

    public void setNotiStatus(NotiStatusFilter notiStatus) {
        this.notiStatus = notiStatus;
    }

    public LongFilter getUserId() {
        return userId;
    }

    public void setUserId(LongFilter userId) {
        this.userId = userId;
    }

    public LongFilter getWaffleStoreId() {
        return waffleStoreId;
    }

    public void setWaffleStoreId(LongFilter waffleStoreId) {
        this.waffleStoreId = waffleStoreId;
    }

    @Override
    public String toString() {
        return "WaffleUserNotificationCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (notiBody != null ? "notiBody=" + notiBody + ", " : "") +
                (title != null ? "title=" + title + ", " : "") +
                (image != null ? "image=" + image + ", " : "") +
                (sentOn != null ? "sentOn=" + sentOn + ", " : "") +
                (scheduledTime != null ? "scheduledTime=" + scheduledTime + ", " : "") +
                (notiStatus != null ? "notiStatus=" + notiStatus + ", " : "") +
                (userId != null ? "userId=" + userId + ", " : "") +
                (waffleStoreId != null ? "waffleStoreId=" + waffleStoreId + ", " : "") +
            "}";
    }

}
