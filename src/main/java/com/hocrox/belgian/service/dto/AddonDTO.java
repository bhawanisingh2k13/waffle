package com.hocrox.belgian.service.dto;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

import javax.persistence.Lob;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the Addon entity.
 */
@Slf4j
@Getter
@Setter
@ToString
@NoArgsConstructor
public class AddonDTO implements Serializable {

    private Long id;

    private String name;

    @Lob
    private String image;

    private String imageUrl;

    private String thumbUrl;

    private Double price;

    private Long categoryId;

    private String categoryName;

    private boolean enabled = true;

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        AddonDTO addonDTO = (AddonDTO) o;
        return addonDTO.getId() != null && getId() != null && Objects.equals(getId(), addonDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

}
