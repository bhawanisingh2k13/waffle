package com.hocrox.belgian.service.dto;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;

/**
 * A DTO for the OrderTrack entity.
 */
@Slf4j
@Getter
@Setter
@ToString
@NoArgsConstructor
public class OrderTrackDTO implements Serializable {

    private Long id;

    @NotNull
    private Double lat;

    @NotNull
    private Double lng;

    private Instant trackTime;

    private Long waffleOrderId;

    private boolean deliveryBoy;

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        OrderTrackDTO orderTrackDTO = (OrderTrackDTO) o;
        if(orderTrackDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), orderTrackDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }
}
