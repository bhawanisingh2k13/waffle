package com.hocrox.belgian.service.dto;

import com.hocrox.belgian.domain.enumeration.DeviceType;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

/**
 * Created by bhawanisingh on 03/11/17 6:28 PM.
 */

@Getter
@Setter
@ToString
@Slf4j
public class DeviceDTO {

    private Long id;

    private String deviceID;

    private String messagingID;

    private String deviceName;

    private SimpleUserDTO owner;

    private DeviceType deviceType;

}
