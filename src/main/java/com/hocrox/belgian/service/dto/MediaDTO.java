package com.hocrox.belgian.service.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Created by bhawanisingh on 22/12/17 5:32 PM.
 */

@Getter
@Setter
@ToString
public class MediaDTO {

    private String link;

    private String key;

    public MediaDTO(String link, String key) {
        this.link = link;
        this.key = key;
    }
}
