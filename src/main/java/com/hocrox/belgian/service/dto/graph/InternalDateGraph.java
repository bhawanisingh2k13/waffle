package com.hocrox.belgian.service.dto.graph;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.time.Instant;

/**
 * Created by bhawanisingh on 06/12/17 3:56 PM.
 */
@Getter
@Setter
@ToString
public class InternalDateGraph implements Serializable, Comparable<InternalDateGraph> {
    private Instant value;
    private Double count;

    public InternalDateGraph(Instant value, Long count) {
        this.value = value;
        this.count = count.doubleValue();
    }

    public InternalDateGraph(Instant value, Double count) {
        this.value = value;
        this.count = count;
    }

    @Override
    public int compareTo(InternalDateGraph dateGraph) {
        return value.compareTo(dateGraph.value);
    }
}
