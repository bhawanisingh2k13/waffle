package com.hocrox.belgian.service.dto;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

import java.io.Serializable;

/**
 * A DTO for the WaffleStore entity.
 */
@Slf4j
@Getter
@Setter
@ToString
@NoArgsConstructor
public class WaffleStoreContactDTO implements Serializable {

    private String name;

    private String description;

    private Double lat;

    private Double lng;

    private String street;

    private String city;

    private String state;

    private String country;

    private String formattedAddress;

    private String email;

    private String phoneNumber;

    private String image;

    private String imageUrl;

    private String thumbUrl;

}
