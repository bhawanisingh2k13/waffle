package com.hocrox.belgian.service.dto;


import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

import java.io.Serializable;

/**
 * A DTO for the Feedback entity.
 */
@Getter
@Setter
@ToString
@Slf4j
public class FeedbackDTO implements Serializable {

    private Long id;

    private String description;

    private String title;

    private String name;

    private String email;
}
