package com.hocrox.belgian.service.dto.graph;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by bhawanisingh on 06/12/17 3:56 PM.
 */
@Getter
@Setter
@ToString
public class DateGraph implements Serializable, Comparable<DateGraph> {
    private Date value;
    private Double count;

    public DateGraph(Date value, Long count) {
        this.value = value;
        this.count = count.doubleValue();
    }

    public DateGraph(Date value, Double count) {
        this.value = value;
        this.count = count;
    }

    @Override
    public int compareTo(DateGraph dateGraph) {
        return value.compareTo(dateGraph.value);
    }
}
