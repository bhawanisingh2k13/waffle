package com.hocrox.belgian.service.dto;


import com.hocrox.belgian.domain.enumeration.OrderStatus;
import com.hocrox.belgian.domain.enumeration.OrderType;
import com.hocrox.belgian.domain.enumeration.PaymentType;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;
import java.util.Set;
import java.util.TreeSet;

/**
 * A DTO for the WaffleOrder entity.
 */
@Slf4j
@Getter
@Setter
@ToString
@NoArgsConstructor
public class WaffleOrderDTO implements Serializable {

    private Long id;

    private String orderId;

    private String name;

    private String cartToken;

    private String storeOrderNumber;

    private String phoneNumber;

    private String email;

    @NotNull
    private OrderType orderType;

    @NotNull
    private PaymentType paymentType;

    private String couponCode;

    private Instant expectedDeliveryTime;

    private Instant deliveryTime;

    private Instant outForDeliveryTime;

    private Instant orderTime;

    @NotNull
    private OrderStatus orderStatus;

    private Double discount;

    private Float cgst;

    private Float sgst;

    private Double totalPrice;

    private Double finalPrice;

    private Long packagingCharges;

    private UserAddressDTO userAddress;

    private StoreUserProfileDTO deliveryBoy;

    private ProfileDTO profile;

    private Set<OrderItemDTO> orderItems = new TreeSet<>();

    private PickupLocationDTO pickupLocation;

    private String driverTimeInSec = "";

    private String driverDstInM = "";

    private String userTimeInSec = "";

    private String userDstInM = "";

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        WaffleOrderDTO waffleOrderDTO = (WaffleOrderDTO) o;
        return waffleOrderDTO.getId() != null && getId() != null && Objects.equals(getId(), waffleOrderDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }
}
