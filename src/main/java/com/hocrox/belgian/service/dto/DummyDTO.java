package com.hocrox.belgian.service.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Created by bhawanisingh on 22/02/16.
 */
@Getter
@Setter
@AllArgsConstructor
@ToString
public class DummyDTO {

    private String reason;
}
