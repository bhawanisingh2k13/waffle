package com.hocrox.belgian.service.dto.graph;

import lombok.*;
import lombok.extern.slf4j.Slf4j;

import java.util.List;

/**
 * Created by bhawanisingh on 11/01/18 12:28 PM.
 */
@Slf4j
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class StoreStatsDTO {

    private Long totalOrders = 0L;

    private Long totalSales = 0L;

    private Long totalMonthlyOrders = 0L;

    private Long totalMonthlySales = 0L;

    List<DateGraph> dateGraphs;

}
