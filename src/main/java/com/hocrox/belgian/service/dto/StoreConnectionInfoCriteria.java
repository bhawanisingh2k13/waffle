package com.hocrox.belgian.service.dto;

import java.io.Serializable;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;






/**
 * Criteria class for the StoreConnectionInfo entity. This class is used in StoreConnectionInfoResource to
 * receive all the possible filtering options from the Http GET request parameters.
 * For example the following could be a valid requests:
 * <code> /store-connection-infos?id.greaterThan=5&amp;attr1.contains=something&amp;attr2.specified=false</code>
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class StoreConnectionInfoCriteria implements Serializable {
    private static final long serialVersionUID = 1L;


    private LongFilter id;

    private StringFilter connectionId;

    private LongFilter waffleStoreId;

    public StoreConnectionInfoCriteria() {
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getConnectionId() {
        return connectionId;
    }

    public void setConnectionId(StringFilter connectionId) {
        this.connectionId = connectionId;
    }

    public LongFilter getWaffleStoreId() {
        return waffleStoreId;
    }

    public void setWaffleStoreId(LongFilter waffleStoreId) {
        this.waffleStoreId = waffleStoreId;
    }

    @Override
    public String toString() {
        return "StoreConnectionInfoCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (connectionId != null ? "connectionId=" + connectionId + ", " : "") +
                (waffleStoreId != null ? "waffleStoreId=" + waffleStoreId + ", " : "") +
            "}";
    }

}
