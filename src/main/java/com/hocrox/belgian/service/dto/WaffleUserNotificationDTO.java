package com.hocrox.belgian.service.dto;


import com.hocrox.belgian.domain.enumeration.NotiStatus;
import com.hocrox.belgian.domain.enumeration.NotificationType;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.Instant;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * A DTO for the WaffleUserNotification entity.
 */
@Slf4j
@Getter
@Setter
@ToString
@NoArgsConstructor
public class WaffleUserNotificationDTO implements Serializable {

    private Long id;

    private String notiBody;

    private String title;

    private NotificationType type = NotificationType.BLANK;

    private String image;

    private String imageUrl;

    private String thumbUrl;

    private boolean background;

    private boolean toAll;

    private String notiId;

    private long refId;

    private Instant sentOn;

    private long apv = -1; // apv is app version

    private long apvt = 0; // -1 Less than apv, 0 exact apv, +1 greater than apv

    @NotNull
    private Instant scheduledTime;

    @NotNull
    private NotiStatus notiStatus;

    private Set<SimpleUserDTO> users = new HashSet<>();

    private Long waffleStoreId;

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        WaffleUserNotificationDTO waffleUserNotificationDTO = (WaffleUserNotificationDTO) o;
        if(waffleUserNotificationDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), waffleUserNotificationDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

}
