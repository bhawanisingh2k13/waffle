package com.hocrox.belgian.service.dto;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * A DTO for the MenuItem entity.
 */
@Slf4j
@Getter
@Setter
@ToString
@NoArgsConstructor
public class MenuItemDTO implements Serializable {

    private Long id;

    private String name;

    private Float price;

    private String image;

    private String imageUrl;

    private String thumbUrl;

    private String description;

    private Boolean panCake = false;

    private String panCakeImage;

    private String panCakeThumbnail;

    private Long categoryId;

    private String categoryName;

    private Set<AddonDTO> addons = new HashSet<>();

    private boolean enabled = true;

    private boolean enablePackagingCost = true;

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        MenuItemDTO menuItemDTO = (MenuItemDTO) o;
        if(menuItemDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), menuItemDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

}
