package com.hocrox.belgian.service.dto;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

import javax.persistence.Lob;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the WaffleStore entity.
 */
@Slf4j
@Getter
@Setter
@ToString
@NoArgsConstructor
public class WaffleStoreDTO implements Serializable {

    private Long id;

    private String storeId;

    @NotNull
    private String shortCode;

    @NotNull
    private String name;

    @Lob
    private String description;

    @NotNull
    private Double lat;

    @NotNull
    private Double lng;

    @NotNull
    private String street;

    @NotNull
    private String city;

    @NotNull
    private String state;

    @NotNull
    private String country;

    @NotNull
    private String formattedAddress;

    private Long ownerId;

    private String ownerLogin;

    private String email;

    private String phoneNumber;

    private String image;

    private String imageUrl;

    private String thumbUrl;

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        WaffleStoreDTO waffleStoreDTO = (WaffleStoreDTO) o;
        return waffleStoreDTO.getId() != null && getId() != null && Objects.equals(getId(), waffleStoreDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }
}
