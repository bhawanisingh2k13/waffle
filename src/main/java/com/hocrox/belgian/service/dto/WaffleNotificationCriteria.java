package com.hocrox.belgian.service.dto;

import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;

import java.io.Serializable;






/**
 * Criteria class for the WaffleNotification entity. This class is used in WaffleNotificationResource to
 * receive all the possible filtering options from the Http GET request parameters.
 * For example the following could be a valid requests:
 * <code> /waffle-notifications?id.greaterThan=5&amp;attr1.contains=something&amp;attr2.specified=false</code>
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class WaffleNotificationCriteria implements Serializable {
    private static final long serialVersionUID = 1L;


    private LongFilter id;

    private StringFilter notiBody;

    private StringFilter title;

    private StringFilter image;

    private LongFilter waffleStoreId;

    public WaffleNotificationCriteria() {
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getNotiBody() {
        return notiBody;
    }

    public void setNotiBody(StringFilter notiBody) {
        this.notiBody = notiBody;
    }

    public StringFilter getTitle() {
        return title;
    }

    public void setTitle(StringFilter title) {
        this.title = title;
    }

    public StringFilter getImage() {
        return image;
    }

    public void setImage(StringFilter image) {
        this.image = image;
    }

    public LongFilter getWaffleStoreId() {
        return waffleStoreId;
    }

    public void setWaffleStoreId(LongFilter waffleStoreId) {
        this.waffleStoreId = waffleStoreId;
    }

    @Override
    public String toString() {
        return "WaffleNotificationCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (notiBody != null ? "notiBody=" + notiBody + ", " : "") +
                (title != null ? "title=" + title + ", " : "") +
                (image != null ? "image=" + image + ", " : "") +
                (waffleStoreId != null ? "waffleStoreId=" + waffleStoreId + ", " : "") +
            "}";
    }

}
