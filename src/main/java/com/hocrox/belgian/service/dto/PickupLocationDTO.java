package com.hocrox.belgian.service.dto;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the PickupLocation entity.
 */
@Slf4j
@Getter
@Setter
@ToString
@NoArgsConstructor
public class PickupLocationDTO implements Serializable {

    private Long id;

    @NotNull
    private Double lat;

    @NotNull
    private Double lng;

    @NotNull
    private String name;

    @NotNull
    private String formattedAddress;

    @NotNull
    private Boolean active;

    private boolean enabled = true;

    private Long waffleStoreId;

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        PickupLocationDTO pickupLocationDTO = (PickupLocationDTO) o;
        return pickupLocationDTO.getId() != null && getId() != null && Objects.equals(getId(), pickupLocationDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

}
