package com.hocrox.belgian.service.dto;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

import java.io.Serializable;

/**
 * A DTO for the OneTimePassword entity.
 */
@Slf4j
@Getter
@Setter
@ToString
@NoArgsConstructor
public class OneTimePasswordDTO implements Serializable {

    private String message;

    private String otpHalf;

    private boolean newUser = true;

}
