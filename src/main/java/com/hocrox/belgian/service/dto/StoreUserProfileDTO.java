package com.hocrox.belgian.service.dto;


import com.hocrox.belgian.domain.enumeration.StoreRoleType;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the StoreUserProfile entity.
 */
@Slf4j
@Getter
@Setter
@ToString
@NoArgsConstructor
public class StoreUserProfileDTO implements Serializable {

    private Long id;

    private String license;

    private String phoneNumber;

    private StoreRoleType roleType;

    private Long waffleStoreId;

    private Long userId;

    private String userLogin;

    private String firstName;

    private String lastName;

    private String email;

    private String image;

    private String imageUrl;

    private String thumbUrl;

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        StoreUserProfileDTO storeUserProfileDTO = (StoreUserProfileDTO) o;
        return storeUserProfileDTO.getId() != null && getId() != null && Objects.equals(getId(), storeUserProfileDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

}
