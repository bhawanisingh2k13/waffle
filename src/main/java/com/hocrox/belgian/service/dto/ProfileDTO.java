package com.hocrox.belgian.service.dto;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

/**
 * A DTO for the Profile entity.
 */
@Slf4j
@Getter
@Setter
@ToString
@NoArgsConstructor
public class ProfileDTO implements Serializable {

    private Long id;

    @NotNull
    private String phoneNumber;

    private Date dateOfBirth;

    private UserAddressDTO userAddress;

    private SimpleUserDTO user;

    private Long userId;

    private String userLogin;

    private String firstName;

    private String lastName;

    private String email;

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ProfileDTO profileDTO = (ProfileDTO) o;
        if(profileDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), profileDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

}
