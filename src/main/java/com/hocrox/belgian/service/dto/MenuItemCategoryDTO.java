package com.hocrox.belgian.service.dto;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

import javax.persistence.Lob;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the MenuItemCategory entity.
 */
@Slf4j
@Getter
@Setter
@ToString
@NoArgsConstructor
public class MenuItemCategoryDTO implements Serializable {

    private Long id;

    private String name;

    @Lob
    private String image;

    private String imageUrl;

    private String thumbUrl;

    private boolean enabled = true;

    private boolean enablePackagingCost = true;

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        MenuItemCategoryDTO menuItemCategoryDTO = (MenuItemCategoryDTO) o;
        return menuItemCategoryDTO.getId() != null && getId() != null && Objects.equals(getId(), menuItemCategoryDTO.getId());
    }

}
