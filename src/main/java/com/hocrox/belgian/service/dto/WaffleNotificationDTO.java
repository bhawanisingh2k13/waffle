package com.hocrox.belgian.service.dto;


import com.hocrox.belgian.domain.enumeration.NotificationType;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the WaffleNotification entity.
 */
@Slf4j
@Getter
@Setter
@ToString
@NoArgsConstructor
public class WaffleNotificationDTO implements Serializable {

    private Long id;

    private String notiBody;

    private String imageUrl;

    private String thumbUrl;

    private boolean background = false;

    private String title;

    private NotificationType type = NotificationType.BLANK;

    private String notiId;

    private long refId = 0;

    private long apv = -1; // apv is app version

    private long apvt = 0; // -1 Less than apv, 0 exact apv, +1 greater than apv

    private Long waffleStoreId;

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        WaffleNotificationDTO waffleNotificationDTO = (WaffleNotificationDTO) o;
        if(waffleNotificationDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), waffleNotificationDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }
}
