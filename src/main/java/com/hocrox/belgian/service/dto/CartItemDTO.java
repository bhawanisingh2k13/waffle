package com.hocrox.belgian.service.dto;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class CartItemDTO {

    private Long cartItemId;
    private long itemQuantity;
    private float totalPrice;
    private String description;

    private OrderItemDTO orderItem;

    private List<OrderItemAddonDTO> orderAddOnList;

}
