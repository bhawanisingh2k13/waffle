package com.hocrox.belgian.service.dto;


import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.hocrox.belgian.config.serializer.LocalDateSerializer;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

import javax.persistence.Lob;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

/**
 * A DTO for the Offer entity.
 */
@Slf4j
@Getter
@Setter
@ToString
@NoArgsConstructor
public class OfferDTO implements Serializable {

    private Long id;

    @JsonSerialize(using = LocalDateSerializer.class)
    private LocalDate toDate;

    @JsonSerialize(using = LocalDateSerializer.class)
    private LocalDate fromDate;

    private Double maxDiscountAmount;

    private String couponCode;

    private Boolean bill;

    private Boolean global;

    @Lob
    private String description;

    private String image;

    private String imageUrl;

    private String thumbUrl;

    @NotNull
    private Integer percentOff;

    private Long menuItemId;

    private Long waffleStoreId;

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        OfferDTO offerDTO = (OfferDTO) o;
        if(offerDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), offerDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }
}
