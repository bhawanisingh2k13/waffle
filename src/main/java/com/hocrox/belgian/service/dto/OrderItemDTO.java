package com.hocrox.belgian.service.dto;


import lombok.*;
import lombok.extern.slf4j.Slf4j;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;

/**
 * A DTO for the OrderItem entity.
 */
@Slf4j
@Getter
@Setter
@ToString
@NoArgsConstructor
public class OrderItemDTO implements Serializable, Comparable<OrderItemDTO> {

    private Long id;

    private Integer quantity;

    private Double pricePerQty;

    private Double totalPrice;

    private Double offer;

    private Double finalPrice;

    private Long menuItemId;

    private String name;

    private String image;

    private String imageUrl;

    private String thumbUrl;

    private Boolean panCake = false;

    private String panCakeImage;

    private String panCakeThumbnail;

    private Long waffleOrderId;

    private List<OrderItemAddonDTO> orderItemAddons;


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        OrderItemDTO orderItemDTO = (OrderItemDTO) o;
        return orderItemDTO.getId() != null && getId() != null && Objects.equals(getId(), orderItemDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public int compareTo(OrderItemDTO o) {
        return this.getId().compareTo( o.getId());
    }
}
