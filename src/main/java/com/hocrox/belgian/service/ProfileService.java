package com.hocrox.belgian.service;

import com.hocrox.belgian.domain.OneTimePassword;
import com.hocrox.belgian.domain.Profile;
import com.hocrox.belgian.domain.User;
import com.hocrox.belgian.repository.OneTimePasswordRepository;
import com.hocrox.belgian.repository.ProfileRepository;
import com.hocrox.belgian.repository.UserAddressRepository;
import com.hocrox.belgian.service.dto.ProfileDTO;
import com.hocrox.belgian.service.mapper.ProfileMapper;
import com.hocrox.belgian.service.mapper.UserAddressMapper;
import com.hocrox.belgian.service.util.RandomUtil;
import com.hocrox.belgian.web.rest.errors.InvalidOperationException;
import com.hocrox.belgian.web.rest.vm.ProfileVM;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;


/**
 * Service Implementation for managing Profile.
 */
@Service
@Transactional
public class ProfileService {

    private final Logger log = LoggerFactory.getLogger(ProfileService.class);

    private final UserService userService;

    private final ProfileRepository profileRepository;

    private final UserAddressRepository userAddressRepository;

    private final ProfileMapper profileMapper;

    private final UserAddressMapper userAddressMapper;

    private final OneTimePasswordRepository oneTimePasswordRepository;

    public ProfileService(UserService userService, ProfileRepository profileRepository, UserAddressRepository userAddressRepository, ProfileMapper profileMapper, UserAddressMapper userAddressMapper, OneTimePasswordRepository oneTimePasswordRepository) {
        this.userService = userService;
        this.profileRepository = profileRepository;
        this.userAddressRepository = userAddressRepository;
        this.profileMapper = profileMapper;
        this.userAddressMapper = userAddressMapper;
        this.oneTimePasswordRepository = oneTimePasswordRepository;
    }

    /**
     * Save a profile.
     *
     * @param profileVM the entity to save
     * @return the persisted entity
     */
    public ProfileDTO create(ProfileVM profileVM) {
        log.debug("Request to save Profile : {}", profileVM);
        profileVM.setLogin(RandomUtil.generateUsername());

        Optional<Profile> profileOptional = profileRepository.findOneByPhoneNumber(profileVM.getPhoneNumber());
        if(profileOptional.isPresent()){
            throw new InvalidOperationException("Mobile number already in use");
        }
        Profile profile = profileMapper.toEntity(profileVM);
        return profileMapper.toDto(profile);
    }

    /**
     * Save a profile.
     *
     * @param profileVM the entity to save
     * @return the persisted entity
     */
    public ProfileDTO update(ProfileVM profileVM) {
        log.debug("Request to save Profile : {}", profileVM);
        User user = userService.currentUser();
        Profile profile = profileRepository.findOneByUser(user);
        if(profile == null) {
            return create(profileVM);
        } else {
            profile = profileRepository.save(profile);
            return profileMapper.toDto(profile);
        }
    }

    /**
     * Get all the profiles.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<ProfileDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Profiles");
        return profileRepository.findAll(pageable)
            .map(profileMapper::toDto);
    }

    /**
     * Get one profile by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public ProfileDTO findOne(Long id) {
        log.debug("Request to get Profile : {}", id);
        Profile profile = profileRepository.findOne(id);
        return profileMapper.toDto(profile);
    }

    /**
     * Delete the profile by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete Profile : {}", id);
        profileRepository.delete(id);
    }

    @Transactional(readOnly = true)
    public ProfileDTO findCurrentDTO() {
        return profileMapper.toDto(findCurrent());
    }

    @Transactional(readOnly = true)
    public Profile findCurrent() {
        return profileRepository.findOneByUser(userService.currentUser());
    }
}
