package com.hocrox.belgian.service;

import com.hocrox.belgian.domain.StoreUserProfile;
import com.hocrox.belgian.domain.User;
import com.hocrox.belgian.domain.WaffleStore;
import com.hocrox.belgian.domain.enumeration.AccountType;
import com.hocrox.belgian.domain.enumeration.StoreRoleType;
import com.hocrox.belgian.repository.StoreUserProfileRepository;
import com.hocrox.belgian.repository.UserRepository;
import com.hocrox.belgian.security.HocroxAuthenticationToken;
import com.hocrox.belgian.security.jwt.JWTToken;
import com.hocrox.belgian.security.jwt.TokenProvider;
import com.hocrox.belgian.service.dto.StoreUserProfileDTO;
import com.hocrox.belgian.service.mapper.StoreUserProfileMapper;
import com.hocrox.belgian.service.util.RandomUtil;
import com.hocrox.belgian.web.rest.errors.InvalidOperationException;
import com.hocrox.belgian.web.rest.vm.LoginVM;
import com.hocrox.belgian.web.rest.vm.RegisterStoreUserProfileVM;
import com.hocrox.belgian.web.rest.vm.StoreUserProfileUpdateVM;
import com.hocrox.belgian.web.rest.vm.StoreUserProfileVM;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;


/**
 * Service Implementation for managing StoreUserProfile.
 */
@Service
@Transactional
public class StoreUserProfileService {

    private final Logger log = LoggerFactory.getLogger(StoreUserProfileService.class);

    private final StoreUserProfileRepository storeUserProfileRepository;

    private final StoreUserProfileMapper storeUserProfileMapper;

    private final UserService userService;

    private final UserRepository userRepository;

    private final TokenProvider tokenProvider;

    private final AuthenticationManager authenticationManager;

    private final MailService mailService;

    public StoreUserProfileService(StoreUserProfileRepository storeUserProfileRepository, StoreUserProfileMapper storeUserProfileMapper, UserService userService, UserRepository userRepository, TokenProvider tokenProvider, AuthenticationManager authenticationManager, MailService mailService) {
        this.storeUserProfileRepository = storeUserProfileRepository;
        this.storeUserProfileMapper = storeUserProfileMapper;
        this.userService = userService;
        this.userRepository = userRepository;
        this.tokenProvider = tokenProvider;
        this.authenticationManager = authenticationManager;
        this.mailService = mailService;
    }

    /**
     * Save a storeUserProfile.
     *
     * @param storeUserProfileVM the entity to save
     * @return the persisted entity
     */
    public StoreUserProfileDTO save(StoreUserProfileVM storeUserProfileVM) {
        log.debug("Request to save StoreUserProfile : {}", storeUserProfileVM);
        StoreUserProfile storeUserProfile = storeUserProfileMapper.toEntity(storeUserProfileVM);
        storeUserProfile = storeUserProfileRepository.save(storeUserProfile);
        return storeUserProfileMapper.toDto(storeUserProfile);
    }


    public void create(RegisterStoreUserProfileVM registerStoreUserProfileVM) {
        log.debug("Request to save StoreUserProfile : {}", registerStoreUserProfileVM);
        User user = userService.registerUser(registerStoreUserProfileVM, registerStoreUserProfileVM.getPassword());
        StoreUserProfile storeUserProfile = storeUserProfileMapper.toEntity(registerStoreUserProfileVM);
        storeUserProfile.setRoleType(StoreRoleType.OWNER);
        storeUserProfile.setUser(user);
        storeUserProfileRepository.save(storeUserProfile);
    }

    /**
     * Get all the storeUserProfiles.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<StoreUserProfileDTO> findAll(Pageable pageable) {
        log.debug("Request to get all StoreUserProfiles");
        return storeUserProfileRepository.findAll(pageable)
            .map(storeUserProfileMapper::toDto);
    }

    /**
     * Get one storeUserProfile by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public StoreUserProfileDTO findOne(Long id) {
        log.debug("Request to get StoreUserProfile : {}", id);
        StoreUserProfile storeUserProfile = storeUserProfileRepository.findOne(id);
        return storeUserProfileMapper.toDto(storeUserProfile);
    }

    @Transactional(readOnly = true)
    public StoreUserProfileDTO findCurrent() {
        log.debug("Request to get current StoreUserProfile : {}" );
        Optional<StoreUserProfile> storeUserProfileOptional = storeUserProfileRepository.findOneByUser(userService.currentUser());
        if(storeUserProfileOptional.isPresent()) return storeUserProfileMapper.toDto(storeUserProfileOptional.get());
        else throw new InvalidOperationException("Sorry you are not linked to any store");
    }

    /**
     * Delete the storeUserProfile by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete StoreUserProfile : {}", id);
        storeUserProfileRepository.delete(id);
    }

    public JWTToken getSecurityTokenAfterAuthentication(LoginVM loginVM) {
        String username = loginVM.getUsername();
        String password = loginVM.getPassword();
        AccountType accountType = loginVM.getAccountType();
        HocroxAuthenticationToken token = new HocroxAuthenticationToken(username, password, accountType);
        Authentication authentication = this.authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        String jwt = tokenProvider.createToken(authentication, loginVM.getRememberMe());
        JWTToken jwtToken = new JWTToken(jwt);
        return jwtToken;
    }

    public StoreUserProfileDTO createUser(StoreUserProfileVM storeUserProfileVM) {
        if(storeUserProfileVM.getRoleType() == null) {
            //todo throw violation
            throw new InvalidOperationException("Please specify role");
        }
        User currentUser = userService.currentUser();
        Optional<StoreUserProfile> currentStoreUserProfileOptional = storeUserProfileRepository.findOneByUser(currentUser);
        String password = RandomUtil.generatePassword();
        User user = userService.registerStoreUser(storeUserProfileVM, password);
        StoreUserProfile storeUserProfile = storeUserProfileMapper.toEntity(storeUserProfileVM);

        mailService.sendCreationEmail(user, password);
        return storeUserProfileMapper.toDto(storeUserProfile);
    }

    public StoreUserProfileDTO updateUser(StoreUserProfileUpdateVM storeUserProfileVM) {

        User currentUser = userService.currentUser();
        Optional<StoreUserProfile> currentStoreUserProfileOptional = storeUserProfileRepository.findOneByUser(currentUser);
        if(!currentStoreUserProfileOptional.isPresent()) {
            throw new AccessDeniedException("You are not authorized for this action");
        }
        StoreUserProfile storeUserProfile = storeUserProfileRepository.findOne(storeUserProfileVM.getId());
        return storeUserProfileMapper.toDto(storeUserProfile);
    }
}
