package com.hocrox.belgian.service;

import com.google.maps.GeoApiContext;
import com.hocrox.belgian.domain.OrderTrack;
import com.hocrox.belgian.domain.WaffleOrder;
import com.hocrox.belgian.domain.WaffleStore;
import com.hocrox.belgian.domain.enumeration.OrderStatus;
import com.hocrox.belgian.domain.enumeration.OrderType;
import com.hocrox.belgian.repository.OrderTrackRepository;
import com.hocrox.belgian.repository.StoreUserProfileRepository;
import com.hocrox.belgian.repository.WaffleOrderRepository;
import com.hocrox.belgian.service.dto.OrderTrackDTO;
import com.hocrox.belgian.service.dto.OrderTrackWebDTO;
import com.hocrox.belgian.service.mapper.OrderTrackMapper;
import com.hocrox.belgian.service.util.RandomUtil;
import com.hocrox.belgian.web.rest.vm.OrderTrackVM;
import org.hashids.Hashids;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.messaging.simp.SimpMessageSendingOperations;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;


/**
 * Service Implementation for managing OrderTrack.
 */
@Service
@Transactional
public class OrderTrackService {

    private final Logger log = LoggerFactory.getLogger(OrderTrackService.class);

    private final GeoApiContext context = new GeoApiContext.Builder().apiKey("AIzaSyCnHWGnysV-f_6IcVevwu4N8Osq0lzmEG8 ").build();

    private final OrderTrackRepository orderTrackRepository;

    private final UserService userService;

    private final StoreUserProfileRepository storeUserProfileRepository;

    private final WaffleOrderRepository waffleOrderRepository;

    private final OrderTrackMapper orderTrackMapper;

    private final SimpMessageSendingOperations messagingTemplate;

    public OrderTrackService(OrderTrackRepository orderTrackRepository, UserService userService, StoreUserProfileRepository storeUserProfileRepository, WaffleOrderRepository waffleOrderRepository, OrderTrackMapper orderTrackMapper, SimpMessageSendingOperations messagingTemplate) {
        this.orderTrackRepository = orderTrackRepository;
        this.userService = userService;
        this.storeUserProfileRepository = storeUserProfileRepository;
        this.waffleOrderRepository = waffleOrderRepository;
        this.orderTrackMapper = orderTrackMapper;
        this.messagingTemplate = messagingTemplate;
    }

    /**
     * Save a orderTrack.
     *
     * @param orderTrackVM the entity to save
     */
    public void save(OrderTrackVM orderTrackVM) {
        log.debug("Request to save OrderTrack : {}", orderTrackVM);
        for(Long waffleOrderId : orderTrackVM.getWaffleOrderIds()) {
            OrderTrack orderTrack = orderTrackMapper.toEntity(orderTrackVM);
            WaffleOrder waffleOrder =  waffleOrderRepository.findOne(waffleOrderId);
            orderTrack.setWaffleOrder(waffleOrder);
            orderTrack = orderTrackRepository.save(orderTrack);
            pushOrderTrack(orderTrack);
        }
//        return orderTrackMapper.toDto(orderTrack);
    }

    /**
     * Get all the orderTracks.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<OrderTrackDTO> findAll(Pageable pageable) {
        log.debug("Request to get all OrderTracks");
        return orderTrackRepository.findAll(pageable)
            .map(orderTrackMapper::toDto);
    }

    @Transactional(readOnly = true)
    public List<OrderTrackDTO> findAll(Long orderId, Long lastTime, boolean isDriver) {
//        Instant instant = Instant.ofEpochMilli(lastTime * 1000);
        log.debug("Request to get all OrderTracks");
        return orderTrackRepository.findAllByWaffleOrderIdAndDeliveryBoyAndLongTrackTimeGreaterThanOrderByIdAsc(orderId, isDriver, lastTime).stream()
                .map(orderTrackMapper::toDto).collect(Collectors.toList());
    }


    @Transactional(readOnly = true)
    public List<OrderTrackDTO> findAll(Long orderId, boolean isDriver) {
//        Instant instant = Instant.ofEpochMilli(lastTime * 1000);
        log.debug("Request to get all OrderTracks");
        return orderTrackRepository.findTopOneByWaffleOrderIdAndDeliveryBoyOrderByLongTrackTimeDesc(orderId, isDriver).stream()
                .map(orderTrackMapper::toDto).collect(Collectors.toList());
    }

    /**
     * Get one orderTrack by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public OrderTrackDTO findOne(Long id) {
        log.debug("Request to get OrderTrack : {}", id);
        OrderTrack orderTrack = orderTrackRepository.findOne(id);
        return orderTrackMapper.toDto(orderTrack);
    }

    /**
     * Delete the orderTrack by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete OrderTrack : {}", id);
        orderTrackRepository.delete(id);
    }

    private void pushOrderTrack(OrderTrack orderTrack) {
        OrderTrackWebDTO waffleOrderDTO = orderTrackMapper.toWebDto(orderTrack);
        WaffleOrder waffleOrder = orderTrack.getWaffleOrder();
        waffleOrderDTO.setOrderType(waffleOrder.getOrderType());

        //For driver drop off
        if(waffleOrder.getOrderType() == OrderType.DROP_OFF && waffleOrder.getOrderStatus() == OrderStatus.OUT_FOR_DELIVERY) {

        }

        //For user online drive thru
        if(waffleOrder.getOrderType() == OrderType.ONLINE_DRIVE_THRU && (waffleOrder.getOrderStatus() != OrderStatus.CANCELLED && waffleOrder.getOrderStatus() !=  OrderStatus.DELIVERED) && !orderTrack.isDeliveryBoy()) {

        }

        //For driver online drive thru
        if(waffleOrder.getOrderType() == OrderType.ONLINE_DRIVE_THRU && waffleOrder.getOrderStatus() == OrderStatus.OUT_FOR_DELIVERY && orderTrack.isDeliveryBoy()) {

        }

        Hashids hashids = new Hashids(WaffleStore.WAFFLE_RANDOM, WaffleStore.WAFFLE_HASH_LENGTH);
        messagingTemplate.convertAndSend("/topic/track-orders/" +  hashids.encode(waffleOrder.getWaffleStore().getId()), waffleOrderDTO);
    }

}
