package com.hocrox.belgian.service;

import com.hocrox.belgian.domain.GlobalSetting;
import com.hocrox.belgian.repository.GlobalSettingRepository;
import com.hocrox.belgian.service.dto.GlobalSettingDTO;
import com.hocrox.belgian.service.mapper.GlobalSettingMapper;
import com.hocrox.belgian.web.rest.vm.GlobalSettingVM;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing GlobalSetting.
 */
@Service
@Transactional
public class GlobalSettingService {

    private final Logger log = LoggerFactory.getLogger(GlobalSettingService.class);

    private final GlobalSettingRepository globalSettingRepository;

    private final GlobalSettingMapper globalSettingMapper;

    public GlobalSettingService(GlobalSettingRepository globalSettingRepository, GlobalSettingMapper globalSettingMapper) {
        this.globalSettingRepository = globalSettingRepository;
        this.globalSettingMapper = globalSettingMapper;
    }

    /**
     * Save a globalSetting.
     *
     * @param globalSettingVM the entity to save
     * @return the persisted entity
     */
    public GlobalSettingDTO save(GlobalSettingVM globalSettingVM) {
        log.debug("Request to save GlobalSetting : {}", globalSettingVM);
        GlobalSetting globalSetting = globalSettingMapper.toEntity(globalSettingVM);
        globalSetting.setId(1l);
        globalSetting = globalSettingRepository.save(globalSetting);
        return globalSettingMapper.toDto(globalSetting);
    }

    /**
     * Get one globalSetting by id.
     *
     * @return the entity
     */
    @Transactional(readOnly = true)
    public GlobalSettingDTO findOne() {
        log.debug("Request to get GlobalSetting");
        GlobalSetting globalSetting = globalSettingRepository.findOne(1l);
        return globalSettingMapper.toDto(globalSetting);
    }

}
