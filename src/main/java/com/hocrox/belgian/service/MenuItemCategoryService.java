package com.hocrox.belgian.service;

import com.hocrox.belgian.domain.MenuItemCategory;
import com.hocrox.belgian.repository.MenuItemCategoryRepository;
import com.hocrox.belgian.service.dto.MenuItemCategoryDTO;
import com.hocrox.belgian.service.mapper.MenuItemCategoryMapper;
import com.hocrox.belgian.web.rest.vm.MenuItemCategoryVM;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;


/**
 * Service Implementation for managing MenuItemCategory.
 */
@Service
@Transactional
public class MenuItemCategoryService {

    private final Logger log = LoggerFactory.getLogger(MenuItemCategoryService.class);

    private final MenuItemCategoryRepository menuItemCategoryRepository;

    private final MenuItemCategoryMapper menuItemCategoryMapper;

    public MenuItemCategoryService(MenuItemCategoryRepository menuItemCategoryRepository, MenuItemCategoryMapper menuItemCategoryMapper) {
        this.menuItemCategoryRepository = menuItemCategoryRepository;
        this.menuItemCategoryMapper = menuItemCategoryMapper;
    }

    /**
     * Save a menuItemCategory.
     *
     * @param menuItemCategoryVM the entity to save
     * @return the persisted entity
     */
    public MenuItemCategoryDTO save(MenuItemCategoryVM menuItemCategoryVM) {
        log.debug("Request to save MenuItemCategory : {}", menuItemCategoryVM);
        MenuItemCategory menuItemCategory = menuItemCategoryMapper.toEntity(menuItemCategoryVM);
        menuItemCategory = menuItemCategoryRepository.save(menuItemCategory);
        return menuItemCategoryMapper.toDto(menuItemCategory);
    }

    /**
     * Get all the menuItemCategories.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<MenuItemCategoryDTO> findAll(Pageable pageable) {
        log.debug("Request to get all MenuItemCategories");
        return menuItemCategoryRepository.findAll(pageable)
            .map(menuItemCategoryMapper::toDto);
    }

    @Transactional(readOnly = true)
    public List<MenuItemCategoryDTO> findAll() {
        log.debug("Request to get all MenuItemCategories");
        return menuItemCategoryRepository.findAll().stream()
                .map(menuItemCategoryMapper::toDto).collect(Collectors.toList());
    }

    /**
     * Get one menuItemCategory by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public MenuItemCategoryDTO findOne(Long id) {
        log.debug("Request to get MenuItemCategory : {}", id);
        MenuItemCategory menuItemCategory = menuItemCategoryRepository.findOne(id);
        return menuItemCategoryMapper.toDto(menuItemCategory);
    }

    /**
     * Delete the menuItemCategory by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete MenuItemCategory : {}", id);
        menuItemCategoryRepository.delete(id);
    }
}
