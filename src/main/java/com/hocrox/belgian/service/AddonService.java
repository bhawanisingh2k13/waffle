package com.hocrox.belgian.service;

import com.hocrox.belgian.domain.Addon;
import com.hocrox.belgian.repository.AddonRepository;
import com.hocrox.belgian.service.dto.AddonDTO;
import com.hocrox.belgian.service.mapper.AddonMapper;
import com.hocrox.belgian.web.rest.vm.AddonVM;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;


/**
 * Service Implementation for managing Addon.
 */
@Service
@Transactional
public class AddonService {
//funding
    private final Logger log = LoggerFactory.getLogger(AddonService.class);

    private final AddonRepository addonRepository;

    private final AddonMapper addonMapper;

    public AddonService(AddonRepository addonRepository, AddonMapper addonMapper) {
        this.addonRepository = addonRepository;
        this.addonMapper = addonMapper;
    }

    /**
     * Save a addon.
     *
     * @param addonVM the entity to save
     * @return the persisted entity
     */
    public AddonDTO save(AddonVM addonVM) {
        log.debug("Request to save Addon : {}", addonVM);
        Addon addon = addonMapper.toEntity(addonVM);
        addon = addonRepository.save(addon);
        return addonMapper.toDto(addon);
    }

    /**
     * Get all the addons.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<AddonDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Addons");
        return addonRepository.findAll(pageable)
            .map(addonMapper::toDto);
    }

    @Transactional(readOnly = true)
    public List<AddonDTO> findAll() {
        log.debug("Request to get all Addons");
        return addonRepository.findAll().stream()
                .map(addonMapper::toDto).collect(Collectors.toList());
    }

    /**
     * Get one addon by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public AddonDTO findOne(Long id) {
        log.debug("Request to get Addon : {}", id);
        Addon addon = addonRepository.findOne(id);
        return addonMapper.toDto(addon);
    }

    /**
     * Delete the addon by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete Addon : {}", id);
        addonRepository.delete(id);
    }
}
