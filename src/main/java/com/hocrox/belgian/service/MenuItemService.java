package com.hocrox.belgian.service;

import com.hocrox.belgian.domain.MenuItem;
import com.hocrox.belgian.repository.MenuItemRepository;
import com.hocrox.belgian.service.dto.MenuItemDTO;
import com.hocrox.belgian.service.mapper.MenuItemMapper;
import com.hocrox.belgian.web.rest.vm.MenuItemVM;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;


/**
 * Service Implementation for managing MenuItem.
 */
@Service
@Transactional
public class MenuItemService {

    private final Logger log = LoggerFactory.getLogger(MenuItemService.class);

    private final MenuItemRepository menuItemRepository;

    private final MenuItemMapper menuItemMapper;

    public MenuItemService(MenuItemRepository menuItemRepository, MenuItemMapper menuItemMapper) {
        this.menuItemRepository = menuItemRepository;
        this.menuItemMapper = menuItemMapper;
    }

    /**
     * Save a menuItem.
     *
     * @param menuItemVM the entity to save
     * @return the persisted entity
     */
    public MenuItemDTO save(MenuItemVM menuItemVM) {
        log.debug("Request to save MenuItem : {}", menuItemVM);
        MenuItem menuItem = menuItemMapper.toEntity(menuItemVM);
        menuItem = menuItemRepository.save(menuItem);
        return menuItemMapper.toDto(menuItem);
    }

    /**
     * Get all the menuItems.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<MenuItemDTO> findAll(Pageable pageable) {
        log.debug("Request to get all MenuItems");
        return menuItemRepository.findAll(pageable)
            .map(menuItemMapper::toDto);
    }

    @Transactional(readOnly = true)
    public List<MenuItemDTO> findAll() {
        log.debug("Request to get all MenuItems");
        return menuItemRepository.findAll().stream()
                .map(menuItemMapper::toDto).collect(Collectors.toList());
    }

    @Transactional(readOnly = true)
    public List<MenuItemDTO> findAllUsingCategory(Long catId) {
        return menuItemRepository.findAllByCategoryIdOrderByIdDesc(catId).stream()
                .map(menuItemMapper::toDto).collect(Collectors.toList());
    }

    @Transactional(readOnly = true)
    public List<MenuItemDTO> findAllUsingCategoryEnable(Long catId) {
        return menuItemRepository.findAllByCategoryIdAndEnabledOrderBySortingIdDesc(catId, true).stream()
            .map(menuItemMapper::toDto).collect(Collectors.toList());
    }

    /**
     * Get one menuItem by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public MenuItemDTO findOne(Long id, boolean enabledOnly) {
        log.debug("Request to get MenuItem : {}", id);
        MenuItem menuItem = menuItemRepository.findOneWithEagerRelationships(id);
        return menuItemMapper.toEnableDto(menuItem, enabledOnly);
    }

    /**
     * Delete the menuItem by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete MenuItem : {}", id);
        menuItemRepository.delete(id);
    }
}
