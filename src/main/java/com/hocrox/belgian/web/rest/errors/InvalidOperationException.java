package com.hocrox.belgian.web.rest.errors;

import org.zalando.problem.AbstractThrowableProblem;
import org.zalando.problem.Status;

/**
 * Created by bhawanisingh on 05/01/18 2:18 PM.
 */
public class InvalidOperationException extends AbstractThrowableProblem {

    public InvalidOperationException(String message) {
        super(ErrorConstants.INVALID_OPERATION_TYPE, message, Status.BAD_REQUEST);
    }
}
