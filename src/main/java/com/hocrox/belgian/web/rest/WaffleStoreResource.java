package com.hocrox.belgian.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.hocrox.belgian.service.UserService;
import com.hocrox.belgian.service.WaffleStoreQueryService;
import com.hocrox.belgian.service.WaffleStoreService;
import com.hocrox.belgian.service.dto.WaffleStoreContactDTO;
import com.hocrox.belgian.service.dto.WaffleStoreDTO;
import com.hocrox.belgian.web.rest.errors.BadRequestAlertException;
import com.hocrox.belgian.web.rest.util.HeaderUtil;
import com.hocrox.belgian.web.rest.util.PaginationUtil;
import com.hocrox.belgian.web.rest.vm.WaffleStoreVM;
import com.hocrox.belgian.web.rest.vm.search.WaffleStoreSearch;
import com.hocrox.belgian.web.rest.vm.search.criteria.WaffleStoreCriteria;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing WaffleStore.
 */
@RestController
@RequestMapping("/api/waffle-stores")
public class WaffleStoreResource {

    private final Logger log = LoggerFactory.getLogger(WaffleStoreResource.class);

    private static final String ENTITY_NAME = "waffleStore";

    private final WaffleStoreService waffleStoreService;

    private final WaffleStoreQueryService waffleStoreQueryService;

    private final UserService userService;

    public WaffleStoreResource(WaffleStoreService waffleStoreService, WaffleStoreQueryService waffleStoreQueryService, UserService userService) {
        this.waffleStoreService = waffleStoreService;
        this.waffleStoreQueryService = waffleStoreQueryService;
        this.userService = userService;
    }

    /**
     * POST  /waffle-stores : Create a new waffleStore.
     *
     * @param waffleStoreVM the waffleStoreVM to create
     * @return the ResponseEntity with status 201 (Created) and with body the new waffleStoreVM, or with status 400 (Bad Request) if the waffleStore has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping()
    @Timed
    public ResponseEntity<WaffleStoreDTO> createWaffleStore(@Valid @RequestBody WaffleStoreVM waffleStoreVM) throws URISyntaxException {
        log.debug("REST request to save WaffleStore : {}", waffleStoreVM);
        if (waffleStoreVM.getId() != null) {
            throw new BadRequestAlertException("A new waffleStore cannot already have an ID", ENTITY_NAME, "idexists");
        }
        WaffleStoreDTO result = waffleStoreService.create(waffleStoreVM);
        return ResponseEntity.created(new URI("/api/waffle-stores/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /waffle-stores : Updates an existing waffleStore.
     *
     * @param waffleStoreVM the waffleStoreVM to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated waffleStoreVM,
     * or with status 400 (Bad Request) if the waffleStoreVM is not valid,
     * or with status 500 (Internal Server Error) if the waffleStoreVM couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping()
    @Timed
    public ResponseEntity<WaffleStoreDTO> updateWaffleStore(@Valid @RequestBody WaffleStoreVM waffleStoreVM) throws URISyntaxException {
        log.debug("REST request to update WaffleStore : {}", waffleStoreVM);
        if (waffleStoreVM.getId() == null) {
            return createWaffleStore(waffleStoreVM);
        }
        WaffleStoreDTO result = waffleStoreService.update(waffleStoreVM);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, waffleStoreVM.getId().toString()))
            .body(result);
    }

    /**
     * GET  /waffle-stores/search : get all the waffleStores.
     *
     * @param pageable the pagination information
     * @param waffleStoreCriteria the criterias which the requested entities should match
     * @return the ResponseEntity with status 200 (OK) and the list of waffleStores in body
     */
    @GetMapping("/search")
    @Timed
    public ResponseEntity<List<WaffleStoreDTO>> getAllWaffleStores(WaffleStoreCriteria waffleStoreCriteria, Pageable pageable) {
        log.debug("REST request to get WaffleStores by waffleStoreCriteria: {}", waffleStoreCriteria);
        Page<WaffleStoreDTO> page = waffleStoreQueryService.findByCriteria(waffleStoreCriteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/waffle-stores");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /waffle-stores : get all the waffleStores.
     *
     * @param pageable the pagination information
     * @param waffleStoreSearch the criterias which the requested entities should match
     * @return the ResponseEntity with status 200 (OK) and the list of waffleStores in body
     */
    @GetMapping()
    @Timed
    public ResponseEntity<List<WaffleStoreDTO>> getAllWaffleStores(WaffleStoreSearch waffleStoreSearch, Pageable pageable) {
        log.debug("REST request to get WaffleStores by waffleStoreSearch: {}", waffleStoreSearch);
        Page<WaffleStoreDTO> page = waffleStoreQueryService.findByPredicate(waffleStoreSearch, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/waffle-stores");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/contact")
    @Timed
    public ResponseEntity<List<WaffleStoreContactDTO>> getAllWaffleStoresContact( Pageable pageable) {
        Page<WaffleStoreContactDTO> page = waffleStoreService.findAllForContact(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/waffle-stores");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /waffle-stores/current : get the waffleStore of current user.
     *
     * @return the ResponseEntity with status 200 (OK) and with body the waffleStoreDTO, or with status 404 (Not Found)
     */
    @GetMapping("/current")
    @Timed
    public ResponseEntity<WaffleStoreDTO> getWaffleStore() {
        log.debug("REST request to get WaffleStore");
        WaffleStoreDTO waffleStoreDTO = waffleStoreService.findOneByCurrentUser();
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(waffleStoreDTO));
    }

    /**
     * GET  /waffle-stores/:id : get the "id" waffleStore.
     *
     * @param id the id of the waffleStoreDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the waffleStoreDTO, or with status 404 (Not Found)
     */
    @GetMapping("/{id}")
    @Timed
    public ResponseEntity<WaffleStoreDTO> getWaffleStore(@PathVariable Long id) {
        log.debug("REST request to get WaffleStore : {}", id);
        WaffleStoreDTO waffleStoreDTO = waffleStoreService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(waffleStoreDTO));
    }

    /**
     * DELETE  /waffle-stores/:id : delete the "id" waffleStore.
     *
     * @param id the id of the waffleStoreDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/{id}")
    @Timed
    public ResponseEntity<Void> deleteWaffleStore(@PathVariable Long id) {
        log.debug("REST request to delete WaffleStore : {}", id);
        waffleStoreService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
