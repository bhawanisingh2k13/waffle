package com.hocrox.belgian.web.rest.vm;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

import java.io.Serializable;

/**
 * A DTO for the StoreUserProfile entity.
 */
@Slf4j
@Getter
@Setter
@ToString
@NoArgsConstructor
public class RegisterStoreUserProfileVM extends StoreUserProfileVM implements Serializable {

    public static final String VM_NAME = "registerStoreUserProfileVM";

    private String password;
}
