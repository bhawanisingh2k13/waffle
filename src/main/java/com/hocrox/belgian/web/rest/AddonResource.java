package com.hocrox.belgian.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.hocrox.belgian.service.AddonService;
import com.hocrox.belgian.web.rest.errors.BadRequestAlertException;
import com.hocrox.belgian.web.rest.util.HeaderUtil;
import com.hocrox.belgian.web.rest.util.PaginationUtil;
import com.hocrox.belgian.service.dto.AddonDTO;
import com.hocrox.belgian.web.rest.vm.AddonVM;
import com.hocrox.belgian.web.rest.vm.search.AddonSearch;
import com.hocrox.belgian.service.AddonQueryService;
import com.hocrox.belgian.web.rest.vm.search.criteria.AddonCriteria;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Addon.
 */
@RestController
@RequestMapping("/api/addons")
public class AddonResource {

    private final Logger log = LoggerFactory.getLogger(AddonResource.class);

    private static final String ENTITY_NAME = "addon";

    private final AddonService addonService;

    private final AddonQueryService addonQueryService;

    public AddonResource(AddonService addonService, AddonQueryService addonQueryService) {
        this.addonService = addonService;
        this.addonQueryService = addonQueryService;
    }

    /**
     * POST  /addons : Create a new addon.
     *
     * @param addonVM the addonVM to create
     * @return the ResponseEntity with status 201 (Created) and with body the new addonVM, or with status 400 (Bad Request) if the addon has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping()
    @Timed
    public ResponseEntity<AddonDTO> createAddon(@Valid @RequestBody AddonVM addonVM) throws URISyntaxException {
        log.debug("REST request to save Addon : {}", addonVM);
        if (addonVM.getId() != null) {
            throw new BadRequestAlertException("A new addon cannot already have an ID", ENTITY_NAME, "idexists");
        }
        AddonDTO result = addonService.save(addonVM);
        return ResponseEntity.created(new URI("/api/addons/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /addons : Updates an existing addon.
     *
     * @param addonVM the addonVM to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated addonVM,
     * or with status 400 (Bad Request) if the addonVM is not valid,
     * or with status 500 (Internal Server Error) if the addonVM couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping()
    @Timed
    public ResponseEntity<AddonDTO> updateAddon(@Valid @RequestBody AddonVM addonVM) throws URISyntaxException {
        log.debug("REST request to update Addon : {}", addonVM);
        if (addonVM.getId() == null) {
            return createAddon(addonVM);
        }
        AddonDTO result = addonService.save(addonVM);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, addonVM.getId().toString()))
            .body(result);
    }

    /**
     * GET  /addons : get all the addons.
     *
     * @param pageable the pagination information
     * @param addonSearch the criterias which the requested entities should match
     * @return the ResponseEntity with status 200 (OK) and the list of addons in body
     */
    @GetMapping()
    @Timed
    public ResponseEntity<List<AddonDTO>> getEnabledAddons(AddonSearch addonSearch, Pageable pageable) {
        log.debug("REST request to get Addons by addonSearch: {}", addonSearch);
        Page<AddonDTO> page = addonQueryService.findByPredicate(addonSearch, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/addons");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/complete")
    @Timed
    public ResponseEntity<List<AddonDTO>> getAllAddons(AddonSearch addonSearch, Pageable pageable) {
        log.debug("REST request to get Addons by addonSearch: {}", addonSearch);
        Page<AddonDTO> page = addonQueryService.findByPredicateComplete(addonSearch, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/addons");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/all")
    @Timed
    public ResponseEntity<List<AddonDTO>> getAllAddons(AddonSearch addonSearch) {
        log.debug("REST request to get Addons");
        List<AddonDTO> page = addonQueryService.findByPredicate(addonSearch);
        return new ResponseEntity<>(page, HttpStatus.OK);
    }

    @GetMapping("/all/complete")
    @Timed
    public ResponseEntity<List<AddonDTO>> getAllAddons() {
        log.debug("REST request to get Addons");
        List<AddonDTO> page = addonService.findAll();
        return new ResponseEntity<>(page, HttpStatus.OK);
    }

    /**
     * GET  /addons/search : get all the addons.
     *
     * @param pageable the pagination information
     * @param addonCriteria the criterias which the requested entities should match
     * @return the ResponseEntity with status 200 (OK) and the list of addons in body
     */
    @GetMapping("/search")
    @Timed
    public ResponseEntity<List<AddonDTO>> getAllAddons(AddonCriteria addonCriteria, Pageable pageable) {
        log.debug("REST request to get Addons by addonCriteria: {}", addonCriteria);
        Page<AddonDTO> page = addonQueryService.findByCriteria(addonCriteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/addons");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /addons/:id : get the "id" addon.
     *
     * @param id the id of the addonDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the addonDTO, or with status 404 (Not Found)
     */
    @GetMapping("/{id}")
    @Timed
    public ResponseEntity<AddonDTO> getAddon(@PathVariable Long id) {
        log.debug("REST request to get Addon : {}", id);
        AddonDTO addonDTO = addonService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(addonDTO));
    }

    /**
     * DELETE  /addons/:id : delete the "id" addon.
     *
     * @param id the id of the addonDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/{id}")
    @Timed
    public ResponseEntity<Void> deleteAddon(@PathVariable Long id) {
        log.debug("REST request to delete Addon : {}", id);
        addonService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
