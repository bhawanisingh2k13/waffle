package com.hocrox.belgian.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.hocrox.belgian.domain.User;
import com.hocrox.belgian.repository.UserRepository;
import com.hocrox.belgian.security.SecurityUtils;
import com.hocrox.belgian.service.MailService;
import com.hocrox.belgian.service.OneTimePasswordService;
import com.hocrox.belgian.service.UserService;
import com.hocrox.belgian.service.dto.DummyDTO;
import com.hocrox.belgian.service.dto.OneTimePasswordDTO;
import com.hocrox.belgian.service.dto.UserDTO;
import com.hocrox.belgian.web.rest.errors.*;
import com.hocrox.belgian.web.rest.vm.*;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.Optional;

/**
 * REST controller for managing the current user's account.
 */
@RestController
@RequestMapping("/api")
public class AccountResource {

    private final Logger log = LoggerFactory.getLogger(AccountResource.class);

    private final UserRepository userRepository;

    private final UserService userService;

    private final MailService mailService;

    private final OneTimePasswordService oneTimePasswordService;

    public AccountResource(UserRepository userRepository, UserService userService, MailService mailService, OneTimePasswordService oneTimePasswordService) {

        this.userRepository = userRepository;
        this.userService = userService;
        this.mailService = mailService;
        this.oneTimePasswordService = oneTimePasswordService;
    }

    /**
     * POST  /register : register the user.
     *
     * @param managedUserVM the managed user View Model
     * @throws InvalidPasswordException 400 (Bad Request) if the password is incorrect
     * @throws EmailAlreadyUsedException 400 (Bad Request) if the email is already used
     * @throws LoginAlreadyUsedException 400 (Bad Request) if the login is already used
     */
    @PostMapping("/register")
    @Timed
    @ResponseStatus(HttpStatus.CREATED)
    public void registerAccount(@Valid @RequestBody ManagedUserVM managedUserVM) {
        if (!checkPasswordLength(managedUserVM.getPassword())) {
            throw new InvalidPasswordException();
        }
        userRepository.findOneByLogin(managedUserVM.getLogin().toLowerCase()).ifPresent(u -> {throw new LoginAlreadyUsedException();});
        userRepository.findOneByEmailIgnoreCase(managedUserVM.getEmail()).ifPresent(u -> {throw new EmailAlreadyUsedException();});
        User user = userService.registerUser(managedUserVM, managedUserVM.getPassword());
        mailService.sendActivationEmail(user);
    }

    /**
     * GET  /activate : activate the registered user.
     *
     * @param key the activation key
     * @throws RuntimeException 500 (Internal Server Error) if the user couldn't be activated
     */
    @GetMapping("/activate")
    @Timed
    public void activateAccount(@RequestParam(value = "key") String key) {
        Optional<User> user = userService.activateRegistration(key);
        if (!user.isPresent()) {
            throw new InternalServerErrorException("No user was found for this reset key");
        }
    }

    /**
     * GET  /authenticate : check if the user is authenticated, and return its login.
     *
     * @param request the HTTP request
     * @return the login if the user is authenticated
     */
    @GetMapping("/authenticate")
    @Timed
    public String isAuthenticated(HttpServletRequest request) {
        log.debug("REST request to check if the current user is authenticated");
        return request.getRemoteUser();
    }

    /**
     * GET  /account : get the current user.
     *
     * @return the current user
     * @throws RuntimeException 500 (Internal Server Error) if the user couldn't be returned
     */
    @GetMapping("/account")
    @Timed
    public UserDTO getAccount() {
        return userService.getUserWithAuthorities()
            .map(UserDTO::new)
            .orElseThrow(() -> new InternalServerErrorException("User could not be found"));
    }

    /**
     * POST  /account : update the current user information.
     *
     * @param userDTO the current user information
     * @throws EmailAlreadyUsedException 400 (Bad Request) if the email is already used
     * @throws RuntimeException 500 (Internal Server Error) if the user login wasn't found
     */
    @PostMapping("/account")
    @Timed
    public void saveAccount(@Valid @RequestBody UserDTO userDTO) {
        final String userLogin = SecurityUtils.getCurrentUserLogin().orElseThrow(() -> new InternalServerErrorException("Current user login not found"));
        Optional<User> existingUser = userRepository.findOneByEmailIgnoreCase(userDTO.getEmail());
        if (existingUser.isPresent() && (!existingUser.get().getLogin().equalsIgnoreCase(userLogin))) {
            throw new EmailAlreadyUsedException();
        }
        Optional<User> user = userRepository.findOneByLogin(userLogin);
        if (!user.isPresent()) {
            throw new InternalServerErrorException("User could not be found");
        }
        userService.updateUser(userDTO.getFirstName(), userDTO.getLastName(), userDTO.getEmail(),
            userDTO.getLangKey(), userDTO.getImageUrl());
   }

    /**
     * POST  /account/change-password : changes the current user's password
     *
     *  @param changePasswordVM have old password and the new password
     * @throws InvalidPasswordException 400 (Bad Request) if the new password is incorrect
     */
    @PostMapping(path = "/account/change-password")
    @Timed
    public ResponseEntity<DummyDTO> changePassword(@Valid @RequestBody ChangePasswordVM changePasswordVM) {
        if (!checkPasswordLength(changePasswordVM.getNewPassword())) {
            throw new InvalidPasswordException();
        }
        userService.changePassword(changePasswordVM);
        return new ResponseEntity<>(new DummyDTO("Password Reset Operation Successful"), HttpStatus.OK);
   }


    /**
     * POST   /account/reset-password/init : Send an email to reset the password of the user
     *
     * @param resetPassInitVM the mail of the user
     * @throws EmailNotFoundException 400 (Bad Request) if the email address is not registered
     */
    @PostMapping(path = "/account/reset-password/init")
    @Timed
    public ResponseEntity<DummyDTO> requestPasswordReset(@Valid @RequestBody ResetPassInitVM resetPassInitVM) {
       mailService.sendPasswordResetMail(
           userService.requestPasswordReset(resetPassInitVM.getMail())
               .orElseThrow(EmailNotFoundException::new)
       );
       return new ResponseEntity<>(new DummyDTO("email was sent"), HttpStatus.OK);
    }

    /**
     * POST   /account/reset-password/finish : Finish to reset the password of the user
     *
     * @param keyAndPassword the generated key and the new password
     * @throws InvalidPasswordException 400 (Bad Request) if the password is incorrect
     * @throws RuntimeException 500 (Internal Server Error) if the password could not be reset
     */
    @PostMapping(path = "/account/reset-password/finish")
    @Timed
    public ResponseEntity<DummyDTO> finishPasswordReset(@RequestBody KeyAndPasswordVM keyAndPassword) {
        if (!checkPasswordLength(keyAndPassword.getNewPassword())) {
            throw new InvalidPasswordException();
        }
        Optional<User> user =
            userService.completePasswordReset(keyAndPassword.getNewPassword(), keyAndPassword.getKey());

        if (!user.isPresent()) {
            throw new InternalServerErrorException("No user was found for this reset key");
        }
        return new ResponseEntity<>(new DummyDTO("Password updated successfully"), HttpStatus.OK);
    }

    /**
     * POST  /generateOTP : Generate a new OTP.
     *
     * @param oneTimePasswordVM OTP for the given mobile number will be generated
     * @return the ResponseEntity with status 200 (OK), or with status 400 (Bad Request) if the phone number is invalid
     */
    @PostMapping("/generateOTP")
    @Timed
    public ResponseEntity<OneTimePasswordDTO> createOneTimePassword(@Valid @RequestBody OneTimePasswordVM oneTimePasswordVM) {
        log.debug("REST request to generate OneTimePassword : {}", oneTimePasswordVM);
        OneTimePasswordDTO oneTimePasswordDTO = oneTimePasswordService.generate(oneTimePasswordVM);
        return new ResponseEntity<>(oneTimePasswordDTO, HttpStatus.OK);
    }


    /**
     * POST  /verifyOTP : Generate a new OTP.
     *
     * @param oneTimePasswordConfirmVM OTP for the given mobile number will be generated
     * @return the ResponseEntity with status 200 (OK), or with status 400 (Bad Request) if the phone number is invalid
     */
    @PostMapping("/verifyOTP")
    @Timed
    public ResponseEntity<DummyDTO> verifyOTP(@Valid @RequestBody OneTimePasswordConfirmVM oneTimePasswordConfirmVM) {
        log.debug("REST request to generate OneTimePassword : {}", oneTimePasswordConfirmVM);
        oneTimePasswordService.verifyOTP(oneTimePasswordConfirmVM);
        return new ResponseEntity<>(new DummyDTO("OTP is valid"), HttpStatus.OK);
    }

    private static boolean checkPasswordLength(String password) {
        return !StringUtils.isEmpty(password) &&
            password.length() >= ManagedUserVM.PASSWORD_MIN_LENGTH &&
            password.length() <= ManagedUserVM.PASSWORD_MAX_LENGTH;
    }
}
