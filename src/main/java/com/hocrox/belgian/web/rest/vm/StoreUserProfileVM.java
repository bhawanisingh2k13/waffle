package com.hocrox.belgian.web.rest.vm;


import com.hocrox.belgian.domain.enumeration.StoreRoleType;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

import javax.persistence.Lob;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * A DTO for the StoreUserProfile entity.
 */
@Slf4j
@Getter
@Setter
@ToString
@NoArgsConstructor
public class StoreUserProfileVM extends UserVM implements Serializable {

    public static final String VM_NAME = "storeUserProfileVM";

    private Long id;

    @Lob
    private String license;

    @NotNull(message = "Please enter a valid phone number")
    @Pattern(regexp = "^[0-9]*$", message = "Please enter a valid phone number")
    @Size(max = 10, min = 10, message = "Phone number must be of 10 digits")
    private String phoneNumber;

    private StoreRoleType roleType;
}
