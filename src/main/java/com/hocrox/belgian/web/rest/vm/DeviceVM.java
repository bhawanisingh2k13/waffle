package com.hocrox.belgian.web.rest.vm;

import com.fasterxml.jackson.databind.JsonNode;
import com.hocrox.belgian.domain.enumeration.DeviceType;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

/**
 * Created by bhawanisingh on 03/11/17 6:30 PM.
 */

@Getter
@ToString
@Slf4j
public class DeviceVM {

    public static final String VM_NAME = "deviceVM";

    @Setter
    private String deviceID;

    private Object messagingID;

    @Setter
    private String deviceName;

    @Setter
    private DeviceType deviceType;

    public void setMessagingID(JsonNode node) {
        if(node.isValueNode()) {
            this.messagingID = node.asText();
        } else {
            this.messagingID = node;
        }
    }
}
