package com.hocrox.belgian.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.hocrox.belgian.service.MenuItemService;
import com.hocrox.belgian.web.rest.errors.BadRequestAlertException;
import com.hocrox.belgian.web.rest.util.HeaderUtil;
import com.hocrox.belgian.web.rest.util.PaginationUtil;
import com.hocrox.belgian.service.dto.MenuItemDTO;
import com.hocrox.belgian.service.dto.MenuItemCriteria;
import com.hocrox.belgian.service.MenuItemQueryService;
import com.hocrox.belgian.web.rest.vm.MenuItemVM;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing MenuItem.
 */
@RestController
@RequestMapping("/api/menu-items")
public class MenuItemResource {

    private final Logger log = LoggerFactory.getLogger(MenuItemResource.class);

    private static final String ENTITY_NAME = "menuItem";

    private final MenuItemService menuItemService;

    private final MenuItemQueryService menuItemQueryService;

    public MenuItemResource(MenuItemService menuItemService, MenuItemQueryService menuItemQueryService) {
        this.menuItemService = menuItemService;
        this.menuItemQueryService = menuItemQueryService;
    }

    /**
     * POST  /menu-items : Create a new menuItem.
     *
     * @param menuItemVM the menuItemVM to create
     * @return the ResponseEntity with status 201 (Created) and with body the new menuItemVM, or with status 400 (Bad Request) if the menuItem has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping()
    @Timed
    public ResponseEntity<MenuItemDTO> createMenuItem(@Valid @RequestBody MenuItemVM menuItemVM) throws URISyntaxException {
        log.debug("REST request to save MenuItem : {}", menuItemVM);
        if (menuItemVM.getId() != null) {
            throw new BadRequestAlertException("A new menuItem cannot already have an ID", ENTITY_NAME, "idexists");
        }
        MenuItemDTO result = menuItemService.save(menuItemVM);
        return ResponseEntity.created(new URI("/api/menu-items/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /menu-items : Updates an existing menuItem.
     *
     * @param menuItemVM the menuItemVM to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated menuItemVM,
     * or with status 400 (Bad Request) if the menuItemVM is not valid,
     * or with status 500 (Internal Server Error) if the menuItemVM couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping()
    @Timed
    public ResponseEntity<MenuItemDTO> updateMenuItem(@Valid @RequestBody MenuItemVM menuItemVM) throws URISyntaxException {
        log.debug("REST request to update MenuItem : {}", menuItemVM);
        if (menuItemVM.getId() == null) {
            return createMenuItem(menuItemVM);
        }
        MenuItemDTO result = menuItemService.save(menuItemVM);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, menuItemVM.getId().toString()))
            .body(result);
    }

    /**
     * GET  /menu-items : get all the menuItems.
     *
     * @param pageable the pagination information
     * @param criteria the criterias which the requested entities should match
     * @return the ResponseEntity with status 200 (OK) and the list of menuItems in body
     */
    @GetMapping()
    @Timed
    public ResponseEntity<List<MenuItemDTO>> getMenuItems(MenuItemCriteria criteria, Pageable pageable) {
        log.debug("REST request to get MenuItems by criteria: {}", criteria);
        Page<MenuItemDTO> page = menuItemQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/menu-items");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/complete")
    @Timed
    public ResponseEntity<List<MenuItemDTO>> getAllMenuItems(MenuItemCriteria criteria, Pageable pageable) {
        log.debug("REST request to get MenuItems by criteria: {}", criteria);
        Page<MenuItemDTO> page = menuItemQueryService.findByCriteriaComplete(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/menu-items");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /menu-items/all : get all the menuItems.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of menuItems in body
     */
    @GetMapping("/all")
    @Timed
    public ResponseEntity<List<MenuItemDTO>> getAllMenuItems(MenuItemCriteria criteria) {
        log.debug("REST request to get MenuItems");
        List<MenuItemDTO> page = menuItemQueryService.findByCriteria(criteria);
        return new ResponseEntity<>(page, HttpStatus.OK);
    }

    /**
     * GET  /menu-items/cat/{catId}/all : get all the menuItems of a category ID.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of menuItems in body
     */
    @GetMapping("/cat/{catId}/all")
    @Timed
    public ResponseEntity<List<MenuItemDTO>> getAllMenuItems(@PathVariable Long catId) {
        log.debug("REST request to get MenuItems : {}", catId);
        List<MenuItemDTO> page = menuItemService.findAllUsingCategory(catId);
        return new ResponseEntity<>(page, HttpStatus.OK);
    }

    /**
     * GET  /menu-items/cat/{catId}/all/enable : get all the menuItems of a category ID.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of menuItems in body
     */
    @GetMapping("/cat/{catId}/all/enable")
    @Timed
    public ResponseEntity<List<MenuItemDTO>> getAllMenuItemsEnable(@PathVariable Long catId) {
        log.debug("REST request to get MenuItems : {}", catId);
        List<MenuItemDTO> page = menuItemService.findAllUsingCategoryEnable(catId);
        return new ResponseEntity<>(page, HttpStatus.OK);
    }


    /**
     * GET  /menu-items/:id : get the "id" menuItem.
     *
     * @param id the id of the menuItemDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the menuItemDTO, or with status 404 (Not Found)
     */
    @GetMapping("/{id}")
    @Timed
    public ResponseEntity<MenuItemDTO> getMenuItem(@PathVariable Long id) {
        log.debug("REST request to get MenuItem : {}", id);
        MenuItemDTO menuItemDTO = menuItemService.findOne(id, true);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(menuItemDTO));
    }

    /**
     * GET  /menu-items/:id/all : get the "id" menuItem.
     *
     * @param id the id of the menuItemDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the menuItemDTO, or with status 404 (Not Found)
     */
    @GetMapping("/{id}/all")
    @Timed
    public ResponseEntity<MenuItemDTO> getAllMenuItem(@PathVariable Long id) {
        log.debug("REST request to get MenuItem : {}", id);
        MenuItemDTO menuItemDTO = menuItemService.findOne(id, false);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(menuItemDTO));
    }

    /**
     * DELETE  /menu-items/:id : delete the "id" menuItem.
     *
     * @param id the id of the menuItemDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/{id}")
    @Timed
    public ResponseEntity<Void> deleteMenuItem(@PathVariable Long id) {
        log.debug("REST request to delete MenuItem : {}", id);
        menuItemService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
