package com.hocrox.belgian.web.rest.vm;


import com.hocrox.belgian.domain.enumeration.NotificationType;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

import java.io.Serializable;

/**
 * A DTO for the AddonCategory entity.
 */
@Slf4j
@Getter
@Setter
@ToString
@NoArgsConstructor
public class WaffleNotificationVM implements Serializable {

    private static final String DEFAULT_TITLE = "Belgian Waffle";

    private Long id;

    private String notiBody;

    private String image;

    private boolean background = false;

    private String title;

    private NotificationType type = NotificationType.BLANK;

    private String notiId;

    private long refId = 0;

    private long apv = -1; // apv is app version

    private long apvt = 0; // -1 Less than apv, 0 exact apv, +1 greater than apv

    private Long waffleStoreId;

}
