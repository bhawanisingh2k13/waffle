package com.hocrox.belgian.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.hocrox.belgian.service.MenuItemCategoryService;
import com.hocrox.belgian.web.rest.errors.BadRequestAlertException;
import com.hocrox.belgian.web.rest.util.HeaderUtil;
import com.hocrox.belgian.web.rest.util.PaginationUtil;
import com.hocrox.belgian.service.dto.MenuItemCategoryDTO;
import com.hocrox.belgian.web.rest.vm.MenuItemCategoryVM;
import com.hocrox.belgian.web.rest.vm.search.MenuItemCategorySearch;
import com.hocrox.belgian.service.MenuItemCategoryQueryService;
import com.hocrox.belgian.web.rest.vm.search.criteria.MenuItemCategoryCriteria;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing MenuItemCategory.
 */
@RestController
@RequestMapping("/api/menu-item-categories")
public class MenuItemCategoryResource {

    private final Logger log = LoggerFactory.getLogger(MenuItemCategoryResource.class);

    private static final String ENTITY_NAME = "menuItemCategory";

    private final MenuItemCategoryService menuItemCategoryService;

    private final MenuItemCategoryQueryService menuItemCategoryQueryService;

    public MenuItemCategoryResource(MenuItemCategoryService menuItemCategoryService, MenuItemCategoryQueryService menuItemCategoryQueryService) {
        this.menuItemCategoryService = menuItemCategoryService;
        this.menuItemCategoryQueryService = menuItemCategoryQueryService;
    }

    /**
     * POST  /menu-item-categories : Create a new menuItemCategory.
     *
     * @param menuItemCategoryVM the menuItemCategoryVM to create
     * @return the ResponseEntity with status 201 (Created) and with body the new menuItemCategoryVM, or with status 400 (Bad Request) if the menuItemCategory has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping()
    @Timed
    public ResponseEntity<MenuItemCategoryDTO> createMenuItemCategory(@RequestBody MenuItemCategoryVM menuItemCategoryVM) throws URISyntaxException {
        log.debug("REST request to save MenuItemCategory : {}", menuItemCategoryVM);
        if (menuItemCategoryVM.getId() != null) {
            throw new BadRequestAlertException("A new menuItemCategory cannot already have an ID", ENTITY_NAME, "idexists");
        }
        MenuItemCategoryDTO result = menuItemCategoryService.save(menuItemCategoryVM);
        return ResponseEntity.created(new URI("/api/menu-item-categories/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /menu-item-categories : Updates an existing menuItemCategory.
     *
     * @param menuItemCategoryVM the menuItemCategoryVM to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated menuItemCategoryVM,
     * or with status 400 (Bad Request) if the menuItemCategoryVM is not valid,
     * or with status 500 (Internal Server Error) if the menuItemCategoryVM couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping()
    @Timed
    public ResponseEntity<MenuItemCategoryDTO> updateMenuItemCategory(@RequestBody MenuItemCategoryVM menuItemCategoryVM) throws URISyntaxException {
        log.debug("REST request to update MenuItemCategory : {}", menuItemCategoryVM);
        if (menuItemCategoryVM.getId() == null) {
            return createMenuItemCategory(menuItemCategoryVM);
        }
        MenuItemCategoryDTO result = menuItemCategoryService.save(menuItemCategoryVM);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, menuItemCategoryVM.getId().toString()))
            .body(result);
    }

    /**
     * GET  /menu-item-categories/search : get all the menuItemCategories.
     *
     * @param pageable the pagination information
     * @param menuItemCategoryCriteria the criterias which the requested entities should match
     * @return the ResponseEntity with status 200 (OK) and the list of menuItemCategories in body
     */
    @GetMapping("/search")
    @Timed
    public ResponseEntity<List<MenuItemCategoryDTO>> getAllMenuItemCategories(MenuItemCategoryCriteria menuItemCategoryCriteria, Pageable pageable) {
        log.debug("REST request to get MenuItemCategories by menuItemCategoryCriteria: {}", menuItemCategoryCriteria);
        Page<MenuItemCategoryDTO> page = menuItemCategoryQueryService.findByCriteria(menuItemCategoryCriteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/menu-item-categories");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /menu-item-categories : get all the menuItemCategories.
     *
     * @param pageable the pagination information
     * @param menuItemCategorySearch the criterias which the requested entities should match
     * @return the ResponseEntity with status 200 (OK) and the list of menuItemCategories in body
     */
    @GetMapping()
    @Timed
    public ResponseEntity<List<MenuItemCategoryDTO>> getMenuItemCategories(MenuItemCategorySearch menuItemCategorySearch, Pageable pageable) {
        log.debug("REST request to get MenuItemCategories by menuItemCategorySearch: {}", menuItemCategorySearch);
        Page<MenuItemCategoryDTO> page = menuItemCategoryQueryService.findByPredicate(menuItemCategorySearch, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/menu-item-categories");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/complete")
    @Timed
    public ResponseEntity<List<MenuItemCategoryDTO>> getAllMenuItemCategories(MenuItemCategorySearch menuItemCategorySearch, Pageable pageable) {
        log.debug("REST request to get MenuItemCategories by menuItemCategorySearch: {}", menuItemCategorySearch);
        Page<MenuItemCategoryDTO> page = menuItemCategoryQueryService.findByPredicateComplete(menuItemCategorySearch, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/menu-item-categories");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /menu-item-categories : get all the menuItemCategories.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of menuItemCategories in body
     */
    @GetMapping("/all")
    @Timed
    public ResponseEntity<List<MenuItemCategoryDTO>> getAllMenuItemCategories(MenuItemCategorySearch menuItemCategorySearch) {
        log.debug("REST request to get MenuItemCategories");
        List<MenuItemCategoryDTO> page = menuItemCategoryQueryService.findByPredicate(menuItemCategorySearch);
        return new ResponseEntity<>(page, HttpStatus.OK);
    }

    /**
     * GET  /menu-item-categories/:id : get the "id" menuItemCategory.
     *
     * @param id the id of the menuItemCategoryDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the menuItemCategoryDTO, or with status 404 (Not Found)
     */
    @GetMapping("/{id}")
    @Timed
    public ResponseEntity<MenuItemCategoryDTO> getMenuItemCategory(@PathVariable Long id) {
        log.debug("REST request to get MenuItemCategory : {}", id);
        MenuItemCategoryDTO menuItemCategoryDTO = menuItemCategoryService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(menuItemCategoryDTO));
    }

    /**
     * DELETE  /menu-item-categories/:id : delete the "id" menuItemCategory.
     *
     * @param id the id of the menuItemCategoryDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/{id}")
    @Timed
    public ResponseEntity<Void> deleteMenuItemCategory(@PathVariable Long id) {
        log.debug("REST request to delete MenuItemCategory : {}", id);
        menuItemCategoryService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
