package com.hocrox.belgian.web.rest.vm;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.Instant;

/**
 * A VM for the StoreConnectionInfo entity.
 */
@Slf4j
@Getter
@Setter
@ToString
@NoArgsConstructor
public class StoreConnectionInfoVM implements Serializable {

    private Long id;

    @NotNull
    private String connectionId;

    private Long waffleStoreId;

    private Instant createdDate;
}
