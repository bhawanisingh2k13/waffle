package com.hocrox.belgian.web.rest.vm;

import com.hocrox.belgian.domain.enumeration.NotificationType;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * Created by hocrox on 12/08/18.
 */
@ToString
@Getter
@Setter
@NoArgsConstructor
public class NotificationVM {

    private static final String DEFAULT_TITLE = "Belgian Waffle";
    private int bck = 0;
    private String body = "";
    private String title = "";
    private String message = "";
    private String imgUrl = "";
    private NotificationType type = NotificationType.BLANK;
    private long apv = -1; // apv is app version
    private long apvt = 0; // -1 Less than apv, 0 exact apv, +1 greater than apv


}
