package com.hocrox.belgian.web.rest.vm;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.Lob;
import javax.validation.constraints.NotNull;

/**
 * Created by bhawanisingh on 15/12/17 1:47 PM.
 */

@Getter
@Setter
@ToString
@Slf4j
public class WaffleStoreVM {

    public static final String VM_NAME = "waffleStoreVM";

    private Long id;

    @NotNull
    private String shortCode;

    @NotNull(message = "Please enter a valid name")
    private String name;

    @Lob
    private String description;

    @NotNull(message = "Invalid latitude")
    private Double lat;

    @NotNull(message = "Invalid Longitude")
    private Double lng;

    @NotNull(message = "Please enter a valid street name")
    private String street;

    @NotNull(message = "Please enter a valid city name")
    private String city;

    @NotNull(message = "Please enter a valid state")
    private String state;

    @NotNull(message = "Invalid country")
    private String country;

    @NotNull(message = "Please choose a valid address")
    private String formattedAddress;

    @NotBlank(message = "Email can't be empty")
    private String email;

    @NotBlank(message = "Phone number can't be empty")
    private String phoneNumber;

    @NotBlank(message = "Image can't be empty")
    private String image;

}
