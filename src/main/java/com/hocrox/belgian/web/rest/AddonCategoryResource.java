package com.hocrox.belgian.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.hocrox.belgian.service.AddonCategoryService;
import com.hocrox.belgian.web.rest.errors.BadRequestAlertException;
import com.hocrox.belgian.web.rest.util.HeaderUtil;
import com.hocrox.belgian.web.rest.util.PaginationUtil;
import com.hocrox.belgian.service.dto.AddonCategoryDTO;
import com.hocrox.belgian.web.rest.vm.AddonCategoryVM;
import com.hocrox.belgian.web.rest.vm.search.AddonCategorySearch;
import com.hocrox.belgian.service.AddonCategoryQueryService;
import com.hocrox.belgian.web.rest.vm.search.criteria.AddonCategoryCriteria;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing AddonCategory.
 */
@RestController
@RequestMapping("/api/addon-categories")
public class AddonCategoryResource {

    private final Logger log = LoggerFactory.getLogger(AddonCategoryResource.class);

    private static final String ENTITY_NAME = "addonCategory";

    private final AddonCategoryService addonCategoryService;

    private final AddonCategoryQueryService addonCategoryQueryService;

    public AddonCategoryResource(AddonCategoryService addonCategoryService, AddonCategoryQueryService addonCategoryQueryService) {
        this.addonCategoryService = addonCategoryService;
        this.addonCategoryQueryService = addonCategoryQueryService;
    }

    /**
     * POST  /addon-categories : Create a new addonCategory.
     *
     * @param addonCategoryVM the addonCategoryVM to create
     * @return the ResponseEntity with status 201 (Created) and with body the new addonCategoryVM, or with status 400 (Bad Request) if the addonCategory has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping()
    @Timed
    public ResponseEntity<AddonCategoryDTO> createAddonCategory(@Valid @RequestBody AddonCategoryVM addonCategoryVM) throws URISyntaxException {
        log.debug("REST request to save AddonCategory : {}", addonCategoryVM);
        if (addonCategoryVM.getId() != null) {
            throw new BadRequestAlertException("A new addonCategory cannot already have an ID", ENTITY_NAME, "idexists");
        }
        AddonCategoryDTO result = addonCategoryService.save(addonCategoryVM);
        return ResponseEntity.created(new URI("/api/addon-categories/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /addon-categories : Updates an existing addonCategory.
     *
     * @param addonCategoryVM the addonCategoryVM to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated addonCategoryVM,
     * or with status 400 (Bad Request) if the addonCategoryVM is not valid,
     * or with status 500 (Internal Server Error) if the addonCategoryVM couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping()
    @Timed
    public ResponseEntity<AddonCategoryDTO> updateAddonCategory(@Valid @RequestBody AddonCategoryVM addonCategoryVM) throws URISyntaxException {
        log.debug("REST request to update AddonCategory : {}", addonCategoryVM);
        if (addonCategoryVM.getId() == null) {
            return createAddonCategory(addonCategoryVM);
        }
        AddonCategoryDTO result = addonCategoryService.save(addonCategoryVM);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, addonCategoryVM.getId().toString()))
            .body(result);
    }

    /**
     * GET  /addon-categories/search : get all the addonCategories.
     *
     * @param pageable the pagination information
     * @param addonCategoryCriteria the criterias which the requested entities should match
     * @return the ResponseEntity with status 200 (OK) and the list of addonCategories in body
     */
    @GetMapping("/search")
    @Timed
    public ResponseEntity<List<AddonCategoryDTO>> getAllAddonCategories(AddonCategoryCriteria addonCategoryCriteria, Pageable pageable) {
        log.debug("REST request to get AddonCategories by addonCategoryCriteria: {}", addonCategoryCriteria);
        Page<AddonCategoryDTO> page = addonCategoryQueryService.findByCriteria(addonCategoryCriteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/addon-categories");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /addon-categories : get all the addonCategories.
     *
     * @param pageable the pagination information
     * @param addonCategorySearch the criterias which the requested entities should match
     * @return the ResponseEntity with status 200 (OK) and the list of addonCategories in body
     */
    @GetMapping()
    @Timed
    public ResponseEntity<List<AddonCategoryDTO>> getAllAddonCategories(AddonCategorySearch addonCategorySearch, Pageable pageable) {
        log.debug("REST request to get AddonCategories by addonCategorySearch: {}", addonCategorySearch);
        Page<AddonCategoryDTO> page = addonCategoryQueryService.findByPredicate(addonCategorySearch, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/addon-categories");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/complete")
    @Timed
    public ResponseEntity<List<AddonCategoryDTO>> getCompleteAddonCategories(AddonCategorySearch addonCategorySearch, Pageable pageable) {
        log.debug("REST request to get AddonCategories by addonCategorySearch: {}", addonCategorySearch);
        Page<AddonCategoryDTO> page = addonCategoryQueryService.findByPredicateComplete(addonCategorySearch, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/addon-categories");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/all")
    @Timed
    public ResponseEntity<List<AddonCategoryDTO>> getAllAddonCategories(AddonCategorySearch addonCategorySearch) {
        log.debug("REST request to get AddonCategories");
        List<AddonCategoryDTO> page = addonCategoryQueryService.findByPredicate(addonCategorySearch);
        return new ResponseEntity<>(page, HttpStatus.OK);
    }

    /**
     * GET  /addon-categories/:id : get the "id" addonCategory.
     *
     * @param id the id of the addonCategoryDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the addonCategoryDTO, or with status 404 (Not Found)
     */
    @GetMapping("/{id}")
    @Timed
    public ResponseEntity<AddonCategoryDTO> getAddonCategory(@PathVariable Long id) {
        log.debug("REST request to get AddonCategory : {}", id);
        AddonCategoryDTO addonCategoryDTO = addonCategoryService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(addonCategoryDTO));
    }

    /**
     * DELETE  /addon-categories/:id : delete the "id" addonCategory.
     *
     * @param id the id of the addonCategoryDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/{id}")
    @Timed
    public ResponseEntity<Void> deleteAddonCategory(@PathVariable Long id) {
        log.debug("REST request to delete AddonCategory : {}", id);
        addonCategoryService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
