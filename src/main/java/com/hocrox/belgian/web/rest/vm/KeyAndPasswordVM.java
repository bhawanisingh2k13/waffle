package com.hocrox.belgian.web.rest.vm;

import lombok.*;

/**
 * View Model object for storing the user's key and password.
 */
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class KeyAndPasswordVM {

    private String key;

    private String newPassword;
}
