package com.hocrox.belgian.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.hocrox.belgian.service.WaffleNotificationService;
import com.hocrox.belgian.web.rest.errors.BadRequestAlertException;
import com.hocrox.belgian.web.rest.util.HeaderUtil;
import com.hocrox.belgian.web.rest.util.PaginationUtil;
import com.hocrox.belgian.service.dto.WaffleNotificationDTO;
import com.hocrox.belgian.service.dto.WaffleNotificationCriteria;
import com.hocrox.belgian.service.WaffleNotificationQueryService;
import com.hocrox.belgian.web.rest.vm.WaffleNotificationVM;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing WaffleNotification.
 */
@RestController
@RequestMapping("/api/notification")
public class WaffleNotificationResource {

    private final Logger log = LoggerFactory.getLogger(WaffleNotificationResource.class);

    private static final String ENTITY_NAME = "waffleNotification";

    private final WaffleNotificationService waffleNotificationService;

    private final WaffleNotificationQueryService waffleNotificationQueryService;

    public WaffleNotificationResource(WaffleNotificationService waffleNotificationService, WaffleNotificationQueryService waffleNotificationQueryService) {
        this.waffleNotificationService = waffleNotificationService;
        this.waffleNotificationQueryService = waffleNotificationQueryService;
    }

    /**
     * POST  /notification : Create a new waffleNotification.
     *
     * @param waffleNotificationVM the waffleNotificationVM to create
     * @return the ResponseEntity with status 201 (Created) and with body the new waffleNotificationVM, or with status 400 (Bad Request) if the waffleNotification has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping()
    @Timed
    public ResponseEntity<WaffleNotificationDTO> createWaffleNotification(@Valid @RequestBody WaffleNotificationVM waffleNotificationVM) throws URISyntaxException {
        log.debug("REST request to save WaffleNotification : {}", waffleNotificationVM);
        if (waffleNotificationVM.getId() != null) {
            throw new BadRequestAlertException("A new waffleNotification cannot already have an ID", ENTITY_NAME, "idexists");
        }
        WaffleNotificationDTO result = waffleNotificationService.save(waffleNotificationVM);
        return ResponseEntity.created(new URI("/api/notification/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /notification : Updates an existing waffleNotification.
     *
     * @param waffleNotificationVM the waffleNotificationVM to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated waffleNotificationVM,
     * or with status 400 (Bad Request) if the waffleNotificationVM is not valid,
     * or with status 500 (Internal Server Error) if the waffleNotificationVM couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping()
    @Timed
    public ResponseEntity<WaffleNotificationDTO> updateWaffleNotification(@Valid @RequestBody WaffleNotificationVM waffleNotificationVM) throws URISyntaxException {
        log.debug("REST request to update WaffleNotification : {}", waffleNotificationVM);
        if (waffleNotificationVM.getId() == null) {
            return createWaffleNotification(waffleNotificationVM);
        }
        WaffleNotificationDTO result = waffleNotificationService.save(waffleNotificationVM);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, waffleNotificationVM.getId().toString()))
            .body(result);
    }

    /**
     * GET  /notification : get all the waffleNotifications.
     *
     * @param pageable the pagination information
     * @param criteria the criterias which the requested entities should match
     * @return the ResponseEntity with status 200 (OK) and the list of waffleNotifications in body
     */
    @GetMapping()
    @Timed
    public ResponseEntity<List<WaffleNotificationDTO>> getAllWaffleNotifications(WaffleNotificationCriteria criteria, Pageable pageable) {
        log.debug("REST request to get WaffleNotifications by criteria: {}", criteria);
        Page<WaffleNotificationDTO> page = waffleNotificationQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/notification");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /notification/:id : get the "id" waffleNotification.
     *
     * @param id the id of the waffleNotificationDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the waffleNotificationDTO, or with status 404 (Not Found)
     */
    @GetMapping("/{id}")
    @Timed
    public ResponseEntity<WaffleNotificationDTO> getWaffleNotification(@PathVariable Long id) {
        log.debug("REST request to get WaffleNotification : {}", id);
        WaffleNotificationDTO waffleNotificationDTO = waffleNotificationService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(waffleNotificationDTO));
    }

    /**
     * DELETE  /notification/:id : delete the "id" waffleNotification.
     *
     * @param id the id of the waffleNotificationDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/{id}")
    @Timed
    public ResponseEntity<Void> deleteWaffleNotification(@PathVariable Long id) {
        log.debug("REST request to delete WaffleNotification : {}", id);
        waffleNotificationService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
