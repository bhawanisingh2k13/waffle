package com.hocrox.belgian.web.rest.vm;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * A DTO for the UserAddress entity.
 */
@Slf4j
@Getter
@Setter
@ToString
@NoArgsConstructor
public class UserAddressVM implements Serializable {

    private Long id;

    @NotBlank(message = "Street name can't be empty")
    private String street;

    @NotBlank(message = "Formatted address name can't be empty")
    private String formattedAddress;

    private String flat;

    @NotNull(message = "Please enter a valid latitude")
    private Double lat;

    @NotNull(message = "Please enter a valid longitude")
    private Double lng;

    @NotBlank(message = "Please enter a valid city")
    private String city;

    @NotBlank(message = "Please enter a valid state")
    private String state;

    @NotBlank(message = "Please enter a valid country")
    private String country;

    private Boolean fromLocation;

    private Long profileId;
    
}
