/**
 * View Models used by Spring MVC REST controllers.
 */
package com.hocrox.belgian.web.rest.vm;
