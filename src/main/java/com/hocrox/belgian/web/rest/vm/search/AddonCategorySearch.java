package com.hocrox.belgian.web.rest.vm.search;

import io.github.jhipster.service.filter.Filter;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

/**
 * Criteria class for the AddonCategory entity. This class is used in AddonCategoryResource to
 * receive all the possible filtering options from the Http GET request parameters.
 * For example the following could be a valid requests:
 * <code> /addon-categories?id.greaterThan=5&amp;attr1.contains=something&amp;attr2.specified=false</code>
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
@Getter
@Setter
@NoArgsConstructor
public class AddonCategorySearch implements Serializable {
    private static final long serialVersionUID = 1L;

    private Long id;

    private String name;
}
