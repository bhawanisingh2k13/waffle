package com.hocrox.belgian.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.hocrox.belgian.service.PickupLocationService;
import com.hocrox.belgian.web.rest.errors.BadRequestAlertException;
import com.hocrox.belgian.web.rest.util.HeaderUtil;
import com.hocrox.belgian.service.dto.PickupLocationDTO;
import com.hocrox.belgian.web.rest.vm.PickupLocationVM;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing PickupLocation.
 */
@RestController
@RequestMapping("/api")
public class PickupLocationResource {

    private final Logger log = LoggerFactory.getLogger(PickupLocationResource.class);

    private static final String ENTITY_NAME = "pickupLocation";

    private final PickupLocationService pickupLocationService;

    public PickupLocationResource(PickupLocationService pickupLocationService) {
        this.pickupLocationService = pickupLocationService;
    }

    /**
     * POST  /pickup-locations : Create a new pickupLocation.
     *
     * @param pickupLocationVM the pickupLocationVM to create
     * @return the ResponseEntity with status 201 (Created) and with body the new pickupLocationVM, or with status 400 (Bad Request) if the pickupLocation has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/pickup-locations")
    @Timed
    public ResponseEntity<PickupLocationDTO> createPickupLocation(@Valid @RequestBody PickupLocationVM pickupLocationVM) throws URISyntaxException {
        log.debug("REST request to save PickupLocation : {}", pickupLocationVM);
        if (pickupLocationVM.getId() != null) {
            throw new BadRequestAlertException("A new pickupLocation cannot already have an ID", ENTITY_NAME, "idexists");
        }
        PickupLocationDTO result = pickupLocationService.save(pickupLocationVM);
        return ResponseEntity.created(new URI("/api/pickup-locations/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /pickup-locations : Updates an existing pickupLocation.
     *
     * @param pickupLocationVM the pickupLocationVM to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated pickupLocationVM,
     * or with status 400 (Bad Request) if the pickupLocationVM is not valid,
     * or with status 500 (Internal Server Error) if the pickupLocationVM couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/pickup-locations")
    @Timed
    public ResponseEntity<PickupLocationDTO> updatePickupLocation(@Valid @RequestBody PickupLocationVM pickupLocationVM) throws URISyntaxException {
        log.debug("REST request to update PickupLocation : {}", pickupLocationVM);
        if (pickupLocationVM.getId() == null) {
            return createPickupLocation(pickupLocationVM);
        }
        PickupLocationDTO result = pickupLocationService.save(pickupLocationVM);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, pickupLocationVM.getId().toString()))
            .body(result);
    }

    /**
     * GET  /pickup-locations : get all the pickupLocations.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of pickupLocations in body
     */
    @GetMapping("/pickup-locations/all")
    @Timed
    public List<PickupLocationDTO> getAllPickupLocationsForAdmin() {
        log.debug("REST request to get all PickupLocations");
        return pickupLocationService.findAllForAdmin();
    }

    @GetMapping("/pickup-locations")
    @Timed
    public List<PickupLocationDTO> getAllPickupLocations() {
        log.debug("REST request to get all PickupLocations");
        return pickupLocationService.findAll();
    }

    /**
     * GET  /pickup-locations/:id : get the "id" pickupLocation.
     *
     * @param id the id of the pickupLocationDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the pickupLocationDTO, or with status 404 (Not Found)
     */
    @GetMapping("/pickup-locations/{id}")
    @Timed
    public ResponseEntity<PickupLocationDTO> getPickupLocation(@PathVariable Long id) {
        log.debug("REST request to get PickupLocation : {}", id);
        PickupLocationDTO pickupLocationDTO = pickupLocationService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(pickupLocationDTO));
    }

    /**
     * DELETE  /pickup-locations/:id : delete the "id" pickupLocation.
     *
     * @param id the id of the pickupLocationDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/pickup-locations/{id}")
    @Timed
    public ResponseEntity<Void> deletePickupLocation(@PathVariable Long id) {
        log.debug("REST request to delete PickupLocation : {}", id);
        pickupLocationService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
