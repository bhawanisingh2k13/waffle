package com.hocrox.belgian.web.rest.vm;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * A DTO for the AddonCategory entity.
 */
@Slf4j
@Getter
@Setter
@ToString
@NoArgsConstructor
public class SettingVM implements Serializable {

    private Long id;

    @NotNull
    private Boolean storeAvailable;

    @NotNull
    private Boolean delayedDelivery;

    private String delayedDeliveryReason;

    private Integer delayedDeliveryTime;

    @NotNull
    private Boolean deliveryAvailable;

    @NotNull
    private Boolean codAvailable;

    @NotNull
    private Boolean odtAvailable;

    private Long deliveryRadius = 0L;

    private Float deliveryCharges = 0.0F;

    private Float handlingCharges = 0.0F;

    private Float packagingCharges = 0.0F;

    private Float panCakePackagingCharges = 0.0F;

    private Long waffleStoreId;
}
