package com.hocrox.belgian.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.hocrox.belgian.service.FeedbackService;
import com.hocrox.belgian.service.dto.DummyDTO;
import com.hocrox.belgian.service.dto.FeedbackDTO;
import com.hocrox.belgian.web.rest.util.HeaderUtil;
import com.hocrox.belgian.web.rest.util.PaginationUtil;
import com.hocrox.belgian.web.rest.vm.FeedbackVM;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;

/**
 * REST controller for managing Feedback.
 */
@RestController
@RequestMapping("/api/feedback")
public class FeedbackResource {

    private final Logger log = LoggerFactory.getLogger(FeedbackResource.class);

    private static final String ENTITY_NAME = "feedback";

    private final FeedbackService feedbackService;

    public FeedbackResource(FeedbackService feedbackService) {
        this.feedbackService = feedbackService;
    }

    /**
     * POST  /feedback : Create a new feedback.
     *
     * @param feedbackVM the feedbackVM to create
     * @return the ResponseEntity with status 201 (Created) and with body the new feedbackVM, or with status 400 (Bad Request) if the feedback has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping()
    @Timed
    public ResponseEntity<DummyDTO> createFeedback(@RequestBody FeedbackVM feedbackVM) throws URISyntaxException {
        log.debug("REST request to save Feedback : {}", feedbackVM);
        feedbackService.save(feedbackVM);
        return ResponseEntity.ok()
            .body(new DummyDTO("Feedback submitted successfully"));
    }

    /**
     * GET  /feedback : get all the feedback.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of feedback in body
     */
    @GetMapping()
    @Timed
    public ResponseEntity<List<FeedbackDTO>> getAllFeedback(Pageable pageable) {
        log.debug("REST request to get a page of Feedback");
        Page<FeedbackDTO> page = feedbackService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/feedback");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * DELETE  /feedbacks/:id : delete the "id" feedback.
     *
     * @param id the id of the feedbackDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/{id}")
    @Timed
    public ResponseEntity<Void> deleteFeedback(@PathVariable Long id) {
        log.debug("REST request to delete Feedback : {}", id);
        feedbackService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
