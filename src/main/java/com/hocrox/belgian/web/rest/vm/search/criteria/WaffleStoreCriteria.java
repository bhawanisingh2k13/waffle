package com.hocrox.belgian.web.rest.vm.search.criteria;

import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

/**
 * Criteria class for the WaffleStore entity. This class is used in WaffleStoreResource to
 * receive all the possible filtering options from the Http GET request parameters.
 * For example the following could be a valid requests:
 * <code> /waffle-stores?id.greaterThan=5&amp;attr1.contains=something&amp;attr2.specified=false</code>
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
@Getter
@Setter
@ToString
@NoArgsConstructor
public class WaffleStoreCriteria implements Serializable {
    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter shortCode;

    private StringFilter name;

    private DoubleFilter lat;

    private DoubleFilter lng;

    private StringFilter street;

    private StringFilter city;

    private StringFilter state;

    private StringFilter country;

    private LongFilter ownerId;

}
