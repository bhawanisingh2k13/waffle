package com.hocrox.belgian.web.rest.vm.search.criteria;

import java.io.Serializable;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;


import io.github.jhipster.service.filter.LocalDateFilter;



/**
 * Criteria class for the Offer entity. This class is used in OfferResource to
 * receive all the possible filtering options from the Http GET request parameters.
 * For example the following could be a valid requests:
 * <code> /offers?id.greaterThan=5&amp;attr1.contains=something&amp;attr2.specified=false</code>
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class OfferCriteria implements Serializable {
    private static final long serialVersionUID = 1L;


    private LongFilter id;

    private LocalDateFilter toDate;

    private LocalDateFilter fromDate;

    private StringFilter couponCode;

    private BooleanFilter bill;

    private BooleanFilter global;

    private StringFilter image;

    private IntegerFilter percentOff;

    private LongFilter menuItemId;

    private LongFilter waffleStoreId;

    public OfferCriteria() {
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public LocalDateFilter getToDate() {
        return toDate;
    }

    public void setToDate(LocalDateFilter toDate) {
        this.toDate = toDate;
    }

    public LocalDateFilter getFromDate() {
        return fromDate;
    }

    public void setFromDate(LocalDateFilter fromDate) {
        this.fromDate = fromDate;
    }

    public StringFilter getCouponCode() {
        return couponCode;
    }

    public void setCouponCode(StringFilter couponCode) {
        this.couponCode = couponCode;
    }

    public BooleanFilter getBill() {
        return bill;
    }

    public void setBill(BooleanFilter bill) {
        this.bill = bill;
    }

    public BooleanFilter getGlobal() {
        return global;
    }

    public void setGlobal(BooleanFilter global) {
        this.global = global;
    }

    public StringFilter getImage() {
        return image;
    }

    public void setImage(StringFilter image) {
        this.image = image;
    }

    public IntegerFilter getPercentOff() {
        return percentOff;
    }

    public void setPercentOff(IntegerFilter percentOff) {
        this.percentOff = percentOff;
    }

    public LongFilter getMenuItemId() {
        return menuItemId;
    }

    public void setMenuItemId(LongFilter menuItemId) {
        this.menuItemId = menuItemId;
    }

    public LongFilter getWaffleStoreId() {
        return waffleStoreId;
    }

    public void setWaffleStoreId(LongFilter waffleStoreId) {
        this.waffleStoreId = waffleStoreId;
    }

    @Override
    public String toString() {
        return "OfferCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (toDate != null ? "toDate=" + toDate + ", " : "") +
                (fromDate != null ? "fromDate=" + fromDate + ", " : "") +
                (couponCode != null ? "couponCode=" + couponCode + ", " : "") +
                (bill != null ? "bill=" + bill + ", " : "") +
                (global != null ? "global=" + global + ", " : "") +
                (image != null ? "image=" + image + ", " : "") +
                (percentOff != null ? "percentOff=" + percentOff + ", " : "") +
                (menuItemId != null ? "menuItemId=" + menuItemId + ", " : "") +
                (waffleStoreId != null ? "waffleStoreId=" + waffleStoreId + ", " : "") +
            "}";
    }

}
