package com.hocrox.belgian.web.rest.vm;


import com.hocrox.belgian.domain.enumeration.NotificationType;
import com.hocrox.belgian.service.dto.SimpleUserDTO;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;

/**
 * A DTO for the AddonCategory entity.
 */
@Slf4j
@Getter
@Setter
@ToString
@NoArgsConstructor
public class WaffleUserNotificationVM implements Serializable {

    private Long id;

    private String notiBody;

    private boolean background = false;

    private String title;

    private NotificationType type = NotificationType.BLANK;

    private String image;

    private String notiId;

    private String refId = "0";

    private long apv = -1; // apv is app version

    private long apvt = 0; // -1 Less than apv, 0 exact apv, +1 greater than apv

    @NotNull
    private Instant scheduledTime = Instant.now(); //.plus(3, ChronoUnit.DAYS);

    private boolean toAll = false;

    private Set<SimpleUserDTO> users = new HashSet<>();

    private Long waffleStoreId = 1L;

}
