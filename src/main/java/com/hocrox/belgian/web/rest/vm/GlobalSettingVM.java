package com.hocrox.belgian.web.rest.vm;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * A DTO for the AddonCategory entity.
 */
@Slf4j
@Getter
@Setter
@ToString
@NoArgsConstructor
public class GlobalSettingVM implements Serializable {

    @NotNull
    private Integer minOrderQty;

    @NotNull
    private Integer maxOrderQty;

    @NotNull
    private Long androidAppVersion;

    private Float panCakePrice = 0f;
}
