package com.hocrox.belgian.web.rest.vm;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.Size;

/**
 * Created by bhawanisingh on 23/10/17 1:10 PM.
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ChangePasswordVM {

    public static final String VM_NAME = "changePasswordVM";

    private String oldPassword;

    @Size(min = 5, max = 30, message = "Invalid password")
    private String newPassword;
}
