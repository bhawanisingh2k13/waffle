package com.hocrox.belgian.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.hocrox.belgian.service.SettingService;
import com.hocrox.belgian.web.rest.errors.BadRequestAlertException;
import com.hocrox.belgian.web.rest.util.HeaderUtil;
import com.hocrox.belgian.web.rest.util.PaginationUtil;
import com.hocrox.belgian.service.dto.SettingDTO;
import com.hocrox.belgian.web.rest.vm.SettingVM;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Setting.
 */
@RestController
@RequestMapping("/api/setting")
public class SettingResource {

    private final Logger log = LoggerFactory.getLogger(SettingResource.class);

    private static final String ENTITY_NAME = "setting";

    private final SettingService settingService;

    public SettingResource(SettingService settingService) {
        this.settingService = settingService;
    }

    /**
     * POST  /setting : Create a new setting.
     *
     * @param settingVM the settingVM to create
     * @return the ResponseEntity with status 201 (Created) and with body the new settingVM, or with status 400 (Bad Request) if the setting has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping()
    @Timed
    public ResponseEntity<SettingDTO> createSetting(@Valid @RequestBody SettingVM settingVM) throws URISyntaxException {
        log.debug("REST request to save Setting : {}", settingVM);
        if (settingVM.getId() != null) {
            throw new BadRequestAlertException("A new setting cannot already have an ID", ENTITY_NAME, "idexists");
        }
        SettingDTO result = settingService.save(settingVM);
        return ResponseEntity.created(new URI("/api/setting/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /setting : Updates an existing setting.
     *
     * @param settingVM the settingVM to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated settingVM,
     * or with status 400 (Bad Request) if the settingVM is not valid,
     * or with status 500 (Internal Server Error) if the settingVM couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping()
    @Timed
    public ResponseEntity<SettingDTO> updateSetting(@Valid @RequestBody SettingVM settingVM) throws URISyntaxException {
        log.debug("REST request to update Setting : {}", settingVM);
        if (settingVM.getId() == null) {
            return createSetting(settingVM);
        }
        SettingDTO result = settingService.save(settingVM);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, settingVM.getId().toString()))
            .body(result);
    }

    /**
     * GET  /setting : get all the settings.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of settings in body
     */
    @GetMapping()
    @Timed
    public ResponseEntity<List<SettingDTO>> getAllSettings(Pageable pageable) {
        log.debug("REST request to get a page of Settings");
        Page<SettingDTO> page = settingService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/setting");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /setting/:id : get the "id" setting.
     *
     * @param id the id of the settingDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the settingDTO, or with status 404 (Not Found)
     */
    @GetMapping("/{id}")
    @Timed
    public ResponseEntity<SettingDTO> getSetting(@PathVariable Long id) {
        log.debug("REST request to get Setting : {}", id);
        SettingDTO settingDTO = settingService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(settingDTO));
    }

    /**
     * GET  /setting/store/:storeId : get the setting based on store id.
     *
     * @param storeId the id of the settingDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the settingDTO, or with status 404 (Not Found)
     */
    @GetMapping("/store/{storeId}")
    @Timed
    public ResponseEntity<SettingDTO> getStoreSetting(@PathVariable Long storeId) {
        log.debug("REST request to get Setting of store with id : {}", storeId);
        SettingDTO settingDTO = settingService.findOneUsingStoreId(storeId);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(settingDTO));
    }

    /**
     * DELETE  /setting/:id : delete the "id" setting.
     *
     * @param id the id of the settingDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/{id}")
    @Timed
    public ResponseEntity<Void> deleteSetting(@PathVariable Long id) {
        log.debug("REST request to delete Setting : {}", id);
        settingService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
