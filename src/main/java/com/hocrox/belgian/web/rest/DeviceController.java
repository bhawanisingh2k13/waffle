package com.hocrox.belgian.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.hocrox.belgian.messaging.android.FcmNotification;
import com.hocrox.belgian.repository.DeviceRepository;
import com.hocrox.belgian.service.DeviceService;
import com.hocrox.belgian.service.dto.DeviceDTO;
import com.hocrox.belgian.service.dto.DummyDTO;
import com.hocrox.belgian.web.rest.vm.DeviceVM;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * Created by bhawanisingh on 03/11/17 6:52 PM.
 */

/**
 * REST controller for managing Device.
 */
@Slf4j
@RestController
@RequestMapping("/api/device")
public class DeviceController {

    private static final String ENTITY_NAME = "device";

    private final DeviceService deviceService;

    private final DeviceRepository deviceRepository;

    private final FcmNotification fcmNotification;

    public DeviceController(DeviceService deviceService, DeviceRepository deviceRepository, FcmNotification fcmNotification) {
        this.deviceService = deviceService;
        this.deviceRepository = deviceRepository;
        this.fcmNotification = fcmNotification;
    }

    /**
     * POST  /device : Create a new device.
     *
     * @param deviceVM the deviceVM to create
     * @return the ResponseEntity with status 201 (Created) and with body the new deviceVM, or with status 400 (Bad Request) if the device has already an ID
     */
    @PostMapping()
    @Timed
    public ResponseEntity<DeviceDTO> createDevice(@RequestBody DeviceVM deviceVM) {
        log.debug("REST request to save Device : {}", deviceVM);
        DeviceDTO result = deviceService.save(deviceVM);
        return new ResponseEntity<>(result, HttpStatus.CREATED);
    }

    /**
     * PUT  /device : Updates an existing device.
     *
     * @param deviceVM the deviceVM to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated deviceDTO,
     * or with status 400 (Bad Request) if the deviceDTO is not valid,
     * or with status 500 (Internal Server Error) if the deviceDTO couldn't be updated
     */
    @PutMapping()
    @Timed
    public ResponseEntity<DeviceDTO> updateDevice(@RequestBody DeviceVM deviceVM) {
        log.debug("REST request to update Device : {}", deviceVM);
        DeviceDTO result = deviceService.save(deviceVM);
        return ResponseEntity.ok().body(result);
    }

    /**
     * DELETE  /device/:id : delete the "id" device.
     *
     * @param id the id of the Device to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/{id}")
    @Timed
    public ResponseEntity<DummyDTO> deleteDevice(@PathVariable String id) {
        log.debug("REST request to delete Device : {}", id);
        deviceService.delete(id);
        return new ResponseEntity<>(new DummyDTO("Device deleted successfully"), HttpStatus.OK);
    }

}
