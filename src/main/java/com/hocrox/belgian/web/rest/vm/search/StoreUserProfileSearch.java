package com.hocrox.belgian.web.rest.vm.search;

import com.hocrox.belgian.domain.enumeration.StoreRoleType;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

/**
 * Criteria class for the StoreUserProfile entity. This class is used in StoreUserProfileResource to
 * receive all the possible filtering options from the Http GET request parameters.
 * For example the following could be a valid requests:
 * <code> /store-user-profiles?id.greaterThan=5&amp;attr1.contains=something&amp;attr2.specified=false</code>
 * As Spring is unable to properly convert the types, unless specific {@link } class are used, we need to use
 * fix type specific filters.
 */
@Getter
@Setter
@NoArgsConstructor
public class StoreUserProfileSearch implements Serializable {
    /**
     * Class for filtering StoreRoleType
     */

    private static final long serialVersionUID = 1L;


    private Long id;

    private String phoneNumber;

    private StoreRoleType roleType;

    private Long userId;

    private Long waffleStoreId;
}
