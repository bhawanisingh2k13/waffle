package com.hocrox.belgian.web.rest.vm;

import com.hocrox.belgian.config.Constants;
import lombok.*;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

/**
 * Created by bhawanisingh on 14/12/17 6:44 PM.
 */
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class UserVM {

    private Long id;

    @NotBlank(message = "Username can't be empty")
    @Pattern(regexp = Constants.LOGIN_REGEX, message = "Please enter a valid username")
    @Size(min = 6, max = 50, message = "Username must be between 6 and 50 characters")
    private String login;

    @Size(max = 50, message = "First name must be less than 50 characters")
    private String firstName;

    @Size(max = 50, message = "Last name must be less than 50 characters")
    private String lastName;

    @Email(message = "Please enter a valid email")
    @Size(min = 5, max = 100, message = "Email must be between 5 and 100 characters")
    private String email;

    @Size(max = 256)
    private String imageUrl;

    private boolean activated = false;

    @Size(min = 2, max = 6)
    private String langKey;

//    private Set<String> authorities;
    private Long roleId;
}
