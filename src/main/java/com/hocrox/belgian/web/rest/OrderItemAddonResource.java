package com.hocrox.belgian.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.hocrox.belgian.service.OrderItemAddonService;
import com.hocrox.belgian.web.rest.errors.BadRequestAlertException;
import com.hocrox.belgian.web.rest.util.HeaderUtil;
import com.hocrox.belgian.web.rest.util.PaginationUtil;
import com.hocrox.belgian.service.dto.OrderItemAddonDTO;
import com.hocrox.belgian.web.rest.vm.OrderItemAddonVM;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing OrderItemAddon.
 */
@RestController
@RequestMapping("/api")
public class OrderItemAddonResource {

    private final Logger log = LoggerFactory.getLogger(OrderItemAddonResource.class);

    private static final String ENTITY_NAME = "orderItemAddon";

    private final OrderItemAddonService orderItemAddonService;

    public OrderItemAddonResource(OrderItemAddonService orderItemAddonService) {
        this.orderItemAddonService = orderItemAddonService;
    }

    /**
     * POST  /order-item-addons : Create a new orderItemAddon.
     *
     * @param orderItemAddonVM the orderItemAddonVM to create
     * @return the ResponseEntity with status 201 (Created) and with body the new orderItemAddonVM, or with status 400 (Bad Request) if the orderItemAddon has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/order-item-addons")
    @Timed
    public ResponseEntity<OrderItemAddonDTO> createOrderItemAddon(@RequestBody OrderItemAddonVM orderItemAddonVM) throws URISyntaxException {
        log.debug("REST request to save OrderItemAddon : {}", orderItemAddonVM);
        OrderItemAddonDTO result = orderItemAddonService.save(orderItemAddonVM);
        return ResponseEntity.created(new URI("/api/order-item-addons/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }


    /**
     * GET  /order-item-addons : get all the orderItemAddons.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of orderItemAddons in body
     */
    @GetMapping("/order-item-addons")
    @Timed
    public ResponseEntity<List<OrderItemAddonDTO>> getAllOrderItemAddons(Pageable pageable) {
        log.debug("REST request to get a page of OrderItemAddons");
        Page<OrderItemAddonDTO> page = orderItemAddonService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/order-item-addons");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /order-item-addons/:id : get the "id" orderItemAddon.
     *
     * @param id the id of the orderItemAddonDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the orderItemAddonDTO, or with status 404 (Not Found)
     */
    @GetMapping("/order-item-addons/{id}")
    @Timed
    public ResponseEntity<OrderItemAddonDTO> getOrderItemAddon(@PathVariable Long id) {
        log.debug("REST request to get OrderItemAddon : {}", id);
        OrderItemAddonDTO orderItemAddonDTO = orderItemAddonService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(orderItemAddonDTO));
    }

    /**
     * DELETE  /order-item-addons/:id : delete the "id" orderItemAddon.
     *
     * @param id the id of the orderItemAddonDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/order-item-addons/{id}")
    @Timed
    public ResponseEntity<Void> deleteOrderItemAddon(@PathVariable Long id) {
        log.debug("REST request to delete OrderItemAddon : {}", id);
        orderItemAddonService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
