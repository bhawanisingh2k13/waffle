package com.hocrox.belgian.web.rest.vm;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the PickupLocation entity.
 */
@Slf4j
@Getter
@Setter
@ToString
@NoArgsConstructor
public class PickupLocationVM implements Serializable {

    private Long id;

    @NotNull(message = "Latitude can't be empty")
    private Double lat;

    @NotNull(message = "Longitude can't be empty")
    private Double lng;

    @NotNull(message = "Name can't be empty")
    private String name;

    @NotNull(message = "Please enter a valid address")
    private String formattedAddress;

    @NotNull
    private Boolean active = false;

    private boolean enabled = true;
    
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        PickupLocationVM pickupLocationDTO = (PickupLocationVM) o;
        if(pickupLocationDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), pickupLocationDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

}
