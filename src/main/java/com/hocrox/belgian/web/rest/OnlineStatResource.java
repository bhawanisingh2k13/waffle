package com.hocrox.belgian.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.hocrox.belgian.service.dto.DummyDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by hocrox on 30/07/18.
 */
@Slf4j
@RestController
@RequestMapping("/api")
public class OnlineStatResource {
    @GetMapping("/online/status")
    @Timed
    public ResponseEntity<DummyDTO> serverOnline() {
        return new ResponseEntity<>(new DummyDTO("Connection OK"), HttpStatus.OK);
    }
}
