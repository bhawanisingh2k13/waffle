package com.hocrox.belgian.web.rest.vm.search.criteria;

import com.hocrox.belgian.domain.enumeration.OrderStatus;
import com.hocrox.belgian.domain.enumeration.OrderType;
import com.hocrox.belgian.domain.enumeration.PaymentType;
import io.github.jhipster.service.filter.*;

import java.io.Serializable;




/**
 * Criteria class for the WaffleOrder entity. This class is used in WaffleOrderResource to
 * receive all the possible filtering options from the Http GET request parameters.
 * For example the following could be a valid requests:
 * <code> /waffle-orders?id.greaterThan=5&amp;attr1.contains=something&amp;attr2.specified=false</code>
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class WaffleOrderCriteria implements Serializable {
    /**
     * Class for filtering OrderType
     */
    public static class OrderTypeFilter extends Filter<OrderType> {
    }

    /**
     * Class for filtering PaymentType
     */
    public static class PaymentTypeFilter extends Filter<PaymentType> {
    }

    /**
     * Class for filtering OrderStatus
     */
    public static class OrderStatusFilter extends Filter<OrderStatus> {
    }

    private static final long serialVersionUID = 1L;


    private LongFilter id;

    private StringFilter name;

    private StringFilter phoneNumber;

    private StringFilter email;

    private OrderTypeFilter orderType;

    private PaymentTypeFilter payementType;

    private StringFilter couponCode;

    private LocalDateFilter expectedDeliveryTime;

    private LocalDateFilter deliveryTime;

    private LocalDateFilter orderTime;

    private OrderStatusFilter orderStatus;

    private DoubleFilter discount;

    private FloatFilter cgst;

    private FloatFilter sgst;

    private DoubleFilter totalPrice;

    private DoubleFilter finalPrice;

    private LongFilter orderItemId;

    private LongFilter userAddressId;

    private LongFilter storeUserProfileId;

    private LongFilter profileId;

    public WaffleOrderCriteria() {
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getName() {
        return name;
    }

    public void setName(StringFilter name) {
        this.name = name;
    }

    public StringFilter getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(StringFilter phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public StringFilter getEmail() {
        return email;
    }

    public void setEmail(StringFilter email) {
        this.email = email;
    }

    public OrderTypeFilter getOrderType() {
        return orderType;
    }

    public void setOrderType(OrderTypeFilter orderType) {
        this.orderType = orderType;
    }

    public PaymentTypeFilter getPayementType() {
        return payementType;
    }

    public void setPayementType(PaymentTypeFilter payementType) {
        this.payementType = payementType;
    }

    public StringFilter getCouponCode() {
        return couponCode;
    }

    public void setCouponCode(StringFilter couponCode) {
        this.couponCode = couponCode;
    }

    public LocalDateFilter getExpectedDeliveryTime() {
        return expectedDeliveryTime;
    }

    public void setExpectedDeliveryTime(LocalDateFilter expectedDeliveryTime) {
        this.expectedDeliveryTime = expectedDeliveryTime;
    }

    public LocalDateFilter getDeliveryTime() {
        return deliveryTime;
    }

    public void setDeliveryTime(LocalDateFilter deliveryTime) {
        this.deliveryTime = deliveryTime;
    }

    public LocalDateFilter getOrderTime() {
        return orderTime;
    }

    public void setOrderTime(LocalDateFilter orderTime) {
        this.orderTime = orderTime;
    }

    public OrderStatusFilter getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(OrderStatusFilter orderStatus) {
        this.orderStatus = orderStatus;
    }

    public DoubleFilter getDiscount() {
        return discount;
    }

    public void setDiscount(DoubleFilter discount) {
        this.discount = discount;
    }

    public FloatFilter getCgst() {
        return cgst;
    }

    public void setCgst(FloatFilter cgst) {
        this.cgst = cgst;
    }

    public FloatFilter getSgst() {
        return sgst;
    }

    public void setSgst(FloatFilter sgst) {
        this.sgst = sgst;
    }

    public DoubleFilter getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(DoubleFilter totalPrice) {
        this.totalPrice = totalPrice;
    }

    public DoubleFilter getFinalPrice() {
        return finalPrice;
    }

    public void setFinalPrice(DoubleFilter finalPrice) {
        this.finalPrice = finalPrice;
    }

    public LongFilter getOrderItemId() {
        return orderItemId;
    }

    public void setOrderItemId(LongFilter orderItemId) {
        this.orderItemId = orderItemId;
    }

    public LongFilter getUserAddressId() {
        return userAddressId;
    }

    public void setUserAddressId(LongFilter userAddressId) {
        this.userAddressId = userAddressId;
    }

    public LongFilter getStoreUserProfileId() {
        return storeUserProfileId;
    }

    public void setStoreUserProfileId(LongFilter storeUserProfileId) {
        this.storeUserProfileId = storeUserProfileId;
    }

    public LongFilter getProfileId() {
        return profileId;
    }

    public void setProfileId(LongFilter profileId) {
        this.profileId = profileId;
    }

    @Override
    public String toString() {
        return "WaffleOrderCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (name != null ? "name=" + name + ", " : "") +
                (phoneNumber != null ? "phoneNumber=" + phoneNumber + ", " : "") +
                (email != null ? "email=" + email + ", " : "") +
                (orderType != null ? "orderType=" + orderType + ", " : "") +
                (payementType != null ? "payementType=" + payementType + ", " : "") +
                (couponCode != null ? "couponCode=" + couponCode + ", " : "") +
                (expectedDeliveryTime != null ? "expectedDeliveryTime=" + expectedDeliveryTime + ", " : "") +
                (deliveryTime != null ? "deliveryTime=" + deliveryTime + ", " : "") +
                (orderTime != null ? "orderTime=" + orderTime + ", " : "") +
                (orderStatus != null ? "orderStatus=" + orderStatus + ", " : "") +
                (discount != null ? "discount=" + discount + ", " : "") +
                (cgst != null ? "cgst=" + cgst + ", " : "") +
                (sgst != null ? "sgst=" + sgst + ", " : "") +
                (totalPrice != null ? "totalPrice=" + totalPrice + ", " : "") +
                (finalPrice != null ? "finalPrice=" + finalPrice + ", " : "") +
                (orderItemId != null ? "orderItemId=" + orderItemId + ", " : "") +
                (userAddressId != null ? "userAddressId=" + userAddressId + ", " : "") +
                (storeUserProfileId != null ? "storeUserProfileId=" + storeUserProfileId + ", " : "") +
                (profileId != null ? "profileId=" + profileId + ", " : "") +
            "}";
    }

}
