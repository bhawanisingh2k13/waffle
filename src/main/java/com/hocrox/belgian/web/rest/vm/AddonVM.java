package com.hocrox.belgian.web.rest.vm;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

import javax.persistence.Lob;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * A DTO for the Addon entity.
 */
@Slf4j
@Getter
@Setter
@ToString
@NoArgsConstructor
public class AddonVM implements Serializable {

    private Long id;

    @NotNull(message = "Please enter a valid name")
    private String name;

    @Lob
    private String image;

    @NotNull(message = "Please enter a valid price")
    private Double price;

    @NotNull(message = "Please select a category")
    private Long categoryId;

    private boolean enabled = true;
}
