package com.hocrox.belgian.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.hocrox.belgian.domain.enumeration.AccountType;
import com.hocrox.belgian.security.jwt.JWTToken;
import com.hocrox.belgian.service.StoreUserProfileQueryService;
import com.hocrox.belgian.service.StoreUserProfileService;
import com.hocrox.belgian.service.dto.StoreUserProfileDTO;
import com.hocrox.belgian.web.rest.errors.BadRequestAlertException;
import com.hocrox.belgian.web.rest.util.HeaderUtil;
import com.hocrox.belgian.web.rest.util.PaginationUtil;
import com.hocrox.belgian.web.rest.vm.LoginVM;
import com.hocrox.belgian.web.rest.vm.RegisterStoreUserProfileVM;
import com.hocrox.belgian.web.rest.vm.StoreUserProfileUpdateVM;
import com.hocrox.belgian.web.rest.vm.StoreUserProfileVM;
import com.hocrox.belgian.web.rest.vm.search.StoreUserProfileSearch;
import com.hocrox.belgian.web.rest.vm.search.criteria.StoreUserProfileCriteria;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing StoreUserProfile.
 */
@RestController
@RequestMapping("/api/store-user-profiles")
public class StoreUserProfileResource {

    private final Logger log = LoggerFactory.getLogger(StoreUserProfileResource.class);

    private static final String ENTITY_NAME = "storeUserProfile";

    private final StoreUserProfileService storeUserProfileService;

    private final StoreUserProfileQueryService storeUserProfileQueryService;

    public StoreUserProfileResource(StoreUserProfileService storeUserProfileService, StoreUserProfileQueryService storeUserProfileQueryService) {
        this.storeUserProfileService = storeUserProfileService;
        this.storeUserProfileQueryService = storeUserProfileQueryService;
    }

    /**
     * POST  /store-user-profiles/register : Register a new storeUserProfile.
     *
     * @param registerStoreUserProfileVM the registerStoreUserProfileVM to create
     * @return the ResponseEntity with status 201 (Created) and with body the new registerStoreUserProfileVM, or with status 400 (Bad Request) if the storeUserProfile has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/register")
    @Timed
    public ResponseEntity<JWTToken> registerStoreOwnerProfile(@Valid @RequestBody RegisterStoreUserProfileVM registerStoreUserProfileVM) throws URISyntaxException {
        log.debug("REST request to save StoreUserProfile : {}", registerStoreUserProfileVM);
        storeUserProfileService.create(registerStoreUserProfileVM);
        LoginVM loginVM = new LoginVM();
        loginVM.setAccountType(AccountType.NORMAL);
        loginVM.setUsername(registerStoreUserProfileVM.getLogin());
        loginVM.setPassword(registerStoreUserProfileVM.getPassword());
        loginVM.setRememberMe(true);
        JWTToken jwtToken = storeUserProfileService.getSecurityTokenAfterAuthentication(loginVM);
        return new ResponseEntity<>(jwtToken, HttpStatus.CREATED);
    }

    /**
     * POST  /store-user-profiles/register : Register a new storeUserProfile.
     *
     * @param registerStoreUserProfileVM the registerStoreUserProfileVM to create
     * @return the ResponseEntity with status 201 (Created) and with body the new registerStoreUserProfileVM, or with status 400 (Bad Request) if the storeUserProfile has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/user")
    @Timed
    public ResponseEntity<StoreUserProfileDTO> registerStoreUserProfile(@Valid @RequestBody StoreUserProfileVM registerStoreUserProfileVM) throws URISyntaxException {
        log.debug("REST request to save StoreUserProfile : {}", registerStoreUserProfileVM);
        StoreUserProfileDTO result = storeUserProfileService.createUser(registerStoreUserProfileVM);
        return new ResponseEntity<>(result, HttpStatus.CREATED);
    }

    /**
     * PUT  /store-user-profiles/user : Save a new storeUserProfile.
     *
     * @param storeUserProfileUpdateVM the storeUserProfileUpdateVM to update
     * @return the ResponseEntity with status 201 (Created) and with body the new storeUserProfileUpdateVM, or with status 400 (Bad Request) if the storeUserProfile has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/user")
    @Timed
    public ResponseEntity<StoreUserProfileDTO> updateUserStoreUserProfile(@Valid @RequestBody StoreUserProfileUpdateVM storeUserProfileUpdateVM) throws URISyntaxException {
        log.debug("REST request to save StoreUserProfile : {}", storeUserProfileUpdateVM);
        StoreUserProfileDTO result = storeUserProfileService.updateUser(storeUserProfileUpdateVM);

        return new ResponseEntity<>(result, HttpStatus.CREATED);
    }

    /**
     * POST  /store-user-profiles/register : Register a new storeUserProfile.
     *
     * @param loginVM the loginVM to create
     * @return the ResponseEntity with status 200 (OK) and with body the new JWTToken, or with status 401 (Unathenticated) if the credentials are wrong
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/authenticate")
    @Timed
    public ResponseEntity<JWTToken> loginStoreUserProfile(@Valid @RequestBody LoginVM loginVM) throws URISyntaxException {
        log.debug("REST request to save StoreUserProfile : {}", loginVM);
        JWTToken jwtToken = storeUserProfileService.getSecurityTokenAfterAuthentication(loginVM);
        return new ResponseEntity<>(jwtToken, HttpStatus.CREATED);
    }

    /**
     * POST  /store-user-profiles : Create a new storeUserProfile.
     *
     * @param storeUserProfileVM the storeUserProfileVM to create
     * @return the ResponseEntity with status 201 (Created) and with body the new storeUserProfileVM, or with status 400 (Bad Request) if the storeUserProfile has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping()
    @Timed
    public ResponseEntity<StoreUserProfileDTO> createStoreUserProfile(@Valid @RequestBody StoreUserProfileVM storeUserProfileVM) throws URISyntaxException {
        log.debug("REST request to save StoreUserProfile : {}", storeUserProfileVM);
        if (storeUserProfileVM.getId() != null) {
            throw new BadRequestAlertException("A new storeUserProfile cannot already have an ID", ENTITY_NAME, "idexists");
        }
        StoreUserProfileDTO result = storeUserProfileService.save(storeUserProfileVM);
        return ResponseEntity.created(new URI("/api/store-user-profiles/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /store-user-profiles : Updates an existing storeUserProfile.
     *
     * @param storeUserProfileVM the storeUserProfileVM to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated storeUserProfileVM,
     * or with status 400 (Bad Request) if the storeUserProfileVM is not valid,
     * or with status 500 (Internal Server Error) if the storeUserProfileVM couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping()
    @Timed
    public ResponseEntity<StoreUserProfileDTO> updateStoreUserProfile(@Valid @RequestBody StoreUserProfileVM storeUserProfileVM) throws URISyntaxException {
        log.debug("REST request to update StoreUserProfile : {}", storeUserProfileVM);
        if (storeUserProfileVM.getId() == null) {
            return createStoreUserProfile(storeUserProfileVM);
        }
        StoreUserProfileDTO result = storeUserProfileService.save(storeUserProfileVM);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, storeUserProfileVM.getId().toString()))
            .body(result);
    }

    /**
     * GET  /store-user-profiles/search : get all the storeUserProfiles.
     *
     * @param pageable the pagination information
     * @param storeUserProfileCriteria the criterias which the requested entities should match
     * @return the ResponseEntity with status 200 (OK) and the list of storeUserProfiles in body
     */
    @GetMapping("/search")
    @Timed
    public ResponseEntity<List<StoreUserProfileDTO>> getAllStoreUserProfiles(StoreUserProfileCriteria storeUserProfileCriteria, Pageable pageable) {
        log.debug("REST request to get StoreUserProfiles by storeUserProfileCriteria: {}", storeUserProfileCriteria);
        Page<StoreUserProfileDTO> page = storeUserProfileQueryService.findByCriteria(storeUserProfileCriteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/store-user-profiles");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /store-user-profiles : get all the storeUserProfiles.
     *
     * @param pageable the pagination information
     * @param storeUserProfileSearch the criterias which the requested entities should match
     * @return the ResponseEntity with status 200 (OK) and the list of storeUserProfiles in body
     */
    @GetMapping()
    @Timed
    public ResponseEntity<List<StoreUserProfileDTO>> getStoreUserProfiles(StoreUserProfileSearch storeUserProfileSearch, Pageable pageable) {
        log.debug("REST request to get StoreUserProfiles by storeUserProfileSearch: {}", storeUserProfileSearch);
        Page<StoreUserProfileDTO> page = storeUserProfileQueryService.findByPredicate(storeUserProfileSearch, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/store-user-profiles");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/all")
    @Timed
    public ResponseEntity<List<StoreUserProfileDTO>> getAllStoreUserProfiles(StoreUserProfileSearch storeUserProfileSearch) {
        log.debug("REST request to get StoreUserProfiles by storeUserProfileSearch: {}", storeUserProfileSearch);
        List<StoreUserProfileDTO> page = storeUserProfileQueryService.findByPredicate(storeUserProfileSearch);
        return new ResponseEntity<>(page, HttpStatus.OK);
    }
    /**
     * GET  /store-user-profiles/:id : get the "id" storeUserProfile.
     *
     * @param id the id of the storeUserProfileDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the storeUserProfileDTO, or with status 404 (Not Found)
     */
    @GetMapping("/{id}")
    @Timed
    public ResponseEntity<StoreUserProfileDTO> getStoreUserProfile(@PathVariable Long id) {
        log.debug("REST request to get StoreUserProfile : {}", id);
        StoreUserProfileDTO storeUserProfileDTO = storeUserProfileService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(storeUserProfileDTO));
    }

    @GetMapping("/current")
    @Timed
    public ResponseEntity<StoreUserProfileDTO> getCurrentStoreUserProfile() {
        log.debug("REST request to get current StoreUserProfile");
        StoreUserProfileDTO storeUserProfileDTO = storeUserProfileService.findCurrent();
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(storeUserProfileDTO));
    }

    /**
     * DELETE  /store-user-profiles/:id : delete the "id" storeUserProfile.
     *
     * @param id the id of the storeUserProfileDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/{id}")
    @Timed
    public ResponseEntity<Void> deleteStoreUserProfile(@PathVariable Long id) {
        log.debug("REST request to delete StoreUserProfile : {}", id);
        storeUserProfileService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
