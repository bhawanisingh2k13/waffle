package com.hocrox.belgian.web.rest.vm;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * A DTO for the OrderTrack entity.
 */
@Slf4j
@Getter
@Setter
@ToString
@NoArgsConstructor
public class OneTimePasswordConfirmVM implements Serializable {

    @NotNull(message = "Please enter a valid mobile number")
    @Size(min = 10, max = 10, message = "Please enter a valid 10 digit mobile number")
    private String phoneNumber;

    @NotNull(message = "Please enter a valid otp")
    @Size(min = 16, max = 16, message = "Please enter a valid 6 digit otp")
    private String otp;
}
