package com.hocrox.belgian.web.rest.vm;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

import javax.persistence.Lob;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

/**
 * A VM for the Offer entity.
 */
@Slf4j
@Getter
@Setter
@ToString
@NoArgsConstructor
public class OfferVM implements Serializable {

    private Long id;

    @NotNull
    private LocalDate toDate;

    @NotNull
    private LocalDate fromDate;

    private Double maxDiscountAmount;

    private String couponCode;

    private Boolean bill;

    private Boolean global;

    @Lob
    private String description;

    private String image;

    @NotNull
    private Integer percentOff;

    private Long menuItemId;

    private Long waffleStoreId;

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        OfferVM offerDTO = (OfferVM) o;
        if(offerDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), offerDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }
}
