package com.hocrox.belgian.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.hocrox.belgian.service.OrderTrackService;
import com.hocrox.belgian.service.dto.OrderTrackDTO;
import com.hocrox.belgian.web.rest.errors.BadRequestAlertException;
import com.hocrox.belgian.web.rest.util.HeaderUtil;
import com.hocrox.belgian.web.rest.util.PaginationUtil;
import com.hocrox.belgian.web.rest.vm.OrderTrackVM;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing OrderTrack.
 */
@RestController
@RequestMapping("/api")
public class OrderTrackResource {

    private final Logger log = LoggerFactory.getLogger(OrderTrackResource.class);

    private static final String ENTITY_NAME = "orderTrack";

    private final OrderTrackService orderTrackService;

    public OrderTrackResource(OrderTrackService orderTrackService) {
        this.orderTrackService = orderTrackService;
    }

    /**
     * POST  /order-tracks : Create a new orderTrack.
     *
     * @param orderTrackVM the orderTrackVM to create
     * @return the ResponseEntity with status 201 (Created) and with body the new orderTrackVM, or with status 400 (Bad Request) if the orderTrack has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/order-tracks")
    @Timed
    public ResponseEntity<OrderTrackDTO> createOrderTrack(@Valid @RequestBody OrderTrackVM orderTrackVM) throws URISyntaxException {
        log.debug("REST request to save OrderTrack : {}", orderTrackVM);
        if (orderTrackVM.getId() != null) {
            throw new BadRequestAlertException("A new orderTrack cannot already have an ID", ENTITY_NAME, "idexists");
        }
        orderTrackService.save(orderTrackVM);
        return new ResponseEntity(HttpStatus.CREATED);
    }

    /**
     * PUT  /order-tracks : Updates an existing orderTrack.
     *
     * @param orderTrackVM the orderTrackVM to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated orderTrackVM,
     * or with status 400 (Bad Request) if the orderTrackVM is not valid,
     * or with status 500 (Internal Server Error) if the orderTrackVM couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/order-tracks")
    @Timed
    public ResponseEntity<OrderTrackDTO> updateOrderTrack(@Valid @RequestBody OrderTrackVM orderTrackVM) throws URISyntaxException {
        log.debug("REST request to update OrderTrack : {}", orderTrackVM);
        orderTrackService.save(orderTrackVM);
        return new ResponseEntity(HttpStatus.OK);
    }

    /**
     * GET  /order-tracks : get all the orderTracks.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of orderTracks in body
     */
    @GetMapping("/order-tracks")
    @Timed
    public ResponseEntity<List<OrderTrackDTO>> getAllOrderTracks(Pageable pageable) {
        log.debug("REST request to get a page of OrderTracks");
        Page<OrderTrackDTO> page = orderTrackService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/order-tracks");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/order-tracks/order/{orderId}/timestamp/{lastTime}/driver")
    @Timed
    public ResponseEntity<List<OrderTrackDTO>> getAllOrderTracksForDriver(@PathVariable Long orderId, @PathVariable Long lastTime) {
        log.debug("REST request to get a page of OrderTracks");
        List<OrderTrackDTO> page = orderTrackService.findAll(orderId, lastTime, true);
        return new ResponseEntity<>(page, HttpStatus.OK);
    }

    @GetMapping("/order-tracks/order/{orderId}/driver")
    @Timed
    public ResponseEntity<List<OrderTrackDTO>> getAllOrderTracksForDriverWithOrderOnly(@PathVariable Long orderId) {
        log.debug("REST request to get a page of OrderTracks");
        List<OrderTrackDTO> page = orderTrackService.findAll(orderId, true);
        return new ResponseEntity<>(page, HttpStatus.OK);
    }

    @GetMapping("/order-tracks/order/{orderId}/timestamp/{lastTime}/user")
    @Timed
    public ResponseEntity<List<OrderTrackDTO>> getAllOrderTracksForUser(@PathVariable Long orderId, @PathVariable Long lastTime) {
        log.debug("REST request to get a page of OrderTracks");
        List<OrderTrackDTO> page = orderTrackService.findAll(orderId, lastTime, false);
        return new ResponseEntity<>(page, HttpStatus.OK);
    }

    @GetMapping("/order-tracks/order/{orderId}/user")
    @Timed
    public ResponseEntity<List<OrderTrackDTO>> getAllOrderTracksFoUserWithOrderOnly(@PathVariable Long orderId) {
        log.debug("REST request to get a page of OrderTracks");
        List<OrderTrackDTO> page = orderTrackService.findAll(orderId, false);
        return new ResponseEntity<>(page, HttpStatus.OK);
    }

    /**
     * GET  /order-tracks/:id : get the "id" orderTrack.
     *
     * @param id the id of the orderTrackDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the orderTrackDTO, or with status 404 (Not Found)
     */
    @GetMapping("/order-tracks/{id}")
    @Timed
    public ResponseEntity<OrderTrackDTO> getOrderTrack(@PathVariable Long id) {
        log.debug("REST request to get OrderTrack : {}", id);
        OrderTrackDTO orderTrackDTO = orderTrackService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(orderTrackDTO));
    }

    /**
     * DELETE  /order-tracks/:id : delete the "id" orderTrack.
     *
     * @param id the id of the orderTrackDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/order-tracks/{id}")
    @Timed
    public ResponseEntity<Void> deleteOrderTrack(@PathVariable Long id) {
        log.debug("REST request to delete OrderTrack : {}", id);
        orderTrackService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
