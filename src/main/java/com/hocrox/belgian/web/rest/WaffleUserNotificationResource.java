package com.hocrox.belgian.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.hocrox.belgian.service.WaffleUserNotificationQueryService;
import com.hocrox.belgian.service.WaffleUserNotificationService;
import com.hocrox.belgian.service.dto.DummyDTO;
import com.hocrox.belgian.service.dto.WaffleUserNotificationCriteria;
import com.hocrox.belgian.service.dto.WaffleUserNotificationDTO;
import com.hocrox.belgian.web.rest.errors.BadRequestAlertException;
import com.hocrox.belgian.web.rest.util.HeaderUtil;
import com.hocrox.belgian.web.rest.util.PaginationUtil;
import com.hocrox.belgian.web.rest.vm.WaffleUserNotificationVM;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing WaffleUserNotification.
 */
@RestController
@RequestMapping("/api/notification/user")
public class WaffleUserNotificationResource {

    private final Logger log = LoggerFactory.getLogger(WaffleUserNotificationResource.class);

    private static final String ENTITY_NAME = "waffleUserNotification";

    private final WaffleUserNotificationService waffleUserNotificationService;

    private final WaffleUserNotificationQueryService waffleUserNotificationQueryService;

    public WaffleUserNotificationResource(WaffleUserNotificationService waffleUserNotificationService, WaffleUserNotificationQueryService waffleUserNotificationQueryService) {
        this.waffleUserNotificationService = waffleUserNotificationService;
        this.waffleUserNotificationQueryService = waffleUserNotificationQueryService;
    }

    /**
     * POST  /notification/user : Create a new waffleUserNotification.
     *
     * @param waffleUserNotificationVM the waffleUserNotificationVM to create
     * @return the ResponseEntity with status 201 (Created) and with body the new waffleUserNotificationVM, or with status 400 (Bad Request) if the waffleUserNotification has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping()
    @Timed
    public ResponseEntity<WaffleUserNotificationDTO> createWaffleUserNotification(@Valid @RequestBody WaffleUserNotificationVM waffleUserNotificationVM) throws URISyntaxException {
        log.debug("REST request to save WaffleUserNotification : {}", waffleUserNotificationVM);
        if (waffleUserNotificationVM.getId() != null) {
            throw new BadRequestAlertException("A new waffleUserNotification cannot already have an ID", ENTITY_NAME, "idexists");
        }
        WaffleUserNotificationDTO result = waffleUserNotificationService.save(waffleUserNotificationVM);
        return ResponseEntity.created(new URI("/api/notification/user/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /notification/user : Updates an existing waffleUserNotification.
     *
     * @param waffleUserNotificationVM the waffleUserNotificationVM to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated waffleUserNotificationVM,
     * or with status 400 (Bad Request) if the waffleUserNotificationVM is not valid,
     * or with status 500 (Internal Server Error) if the waffleUserNotificationVM couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping()
    @Timed
    public ResponseEntity<WaffleUserNotificationDTO> updateWaffleUserNotification(@Valid @RequestBody WaffleUserNotificationVM waffleUserNotificationVM) throws URISyntaxException {
        log.debug("REST request to update WaffleUserNotification : {}", waffleUserNotificationVM);
        if (waffleUserNotificationVM.getId() == null) {
            return createWaffleUserNotification(waffleUserNotificationVM);
        }
        WaffleUserNotificationDTO result = waffleUserNotificationService.save(waffleUserNotificationVM);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, waffleUserNotificationVM.getId().toString()))
            .body(result);
    }

    /**
     * GET  /notification/user : get all the waffleUserNotifications.
     *
     * @param pageable the pagination information
     * @param criteria the criterias which the requested entities should match
     * @return the ResponseEntity with status 200 (OK) and the list of waffleUserNotifications in body
     */
    @GetMapping()
    @Timed
    public ResponseEntity<List<WaffleUserNotificationDTO>> getAllWaffleUserNotifications(WaffleUserNotificationCriteria criteria, Pageable pageable) {
        log.debug("REST request to get WaffleUserNotifications by criteria: {}", criteria);
        Page<WaffleUserNotificationDTO> page = waffleUserNotificationQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/notification/user");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /notification/user/:id : get the "id" waffleUserNotification.
     *
     * @param id the id of the waffleUserNotificationDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the waffleUserNotificationDTO, or with status 404 (Not Found)
     */
    @GetMapping("/{id}")
    @Timed
    public ResponseEntity<WaffleUserNotificationDTO> getWaffleUserNotification(@PathVariable Long id) {
        log.debug("REST request to get WaffleUserNotification : {}", id);
        WaffleUserNotificationDTO waffleUserNotificationDTO = waffleUserNotificationService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(waffleUserNotificationDTO));
    }

    /**
     * DELETE  /notification/user/:id : delete the "id" waffleUserNotification.
     *
     * @param id the id of the waffleUserNotificationDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/{id}")
    @Timed
    public ResponseEntity<Void> deleteWaffleUserNotification(@PathVariable Long id) {
        log.debug("REST request to delete WaffleUserNotification : {}", id);
        waffleUserNotificationService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }


    @GetMapping("/fire")
    @Timed
    public ResponseEntity<DummyDTO> testFireSingle(@RequestParam String username, @RequestParam String message) {
        log.debug("REST request to fire noti for bhawani: {}", message);
        waffleUserNotificationService.fireNotificationSingle(username, message, true);
        return new ResponseEntity<>(new DummyDTO("Notification Fired"), HttpStatus.OK);
    }

    @GetMapping("/fire/text")
    @Timed
    public ResponseEntity<DummyDTO> testFireSingleText(@RequestParam String username, @RequestParam String message) {
        log.debug("REST request to fire noti for bhawani: {}", message);
        waffleUserNotificationService.fireNotificationSingle(username, message, false);
        return new ResponseEntity<>(new DummyDTO("Notification Fired"), HttpStatus.OK);
    }

    @PostMapping("/fire")
    @Timed
    public ResponseEntity<DummyDTO> notificationFireAll(@RequestParam String message) {
        log.debug("REST request to fire noti for all");
        waffleUserNotificationService.fireNotification(message);
        return new ResponseEntity<>(new DummyDTO("Notification Fired"), HttpStatus.OK);
    }
}
