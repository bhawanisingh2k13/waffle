package com.hocrox.belgian.web.rest.vm;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;

import java.io.Serializable;

/**
 * Created by bhawanisingh on 11/01/18 6:53 PM.
 */
@Slf4j
@Getter
@Setter
@ToString
@NoArgsConstructor
public class ResetPassInitVM implements Serializable{

    @Email(message = "Please enter a valid Email")
    @NotBlank(message = "Please enter a valid Email")
    private String mail;
}
