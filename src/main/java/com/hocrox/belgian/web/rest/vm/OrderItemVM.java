package com.hocrox.belgian.web.rest.vm;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

/**
 * A DTO for the OrderItem entity.
 */
@Slf4j
@Getter
@Setter
@ToString
@NoArgsConstructor
public class OrderItemVM implements Serializable {

    private Long id;

    private String cartToken;

    @NotNull(message = "Please specify the quantity")
    private Integer quantity;

    private Boolean panCake = false;

    private Double pricePerQty;

//    private Double totalPrice;

//    private Double offer;

//    private Double finalPrice;

    private Long menuItemId;

    private Long waffleStoreId = 1L;

    private List<OrderItemAddonVM> orderItemAddons;
}
