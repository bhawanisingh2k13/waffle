package com.hocrox.belgian.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.hocrox.belgian.domain.enumeration.AccountType;
import com.hocrox.belgian.security.jwt.JWTToken;
import com.hocrox.belgian.service.OneTimePasswordService;
import com.hocrox.belgian.service.ProfileQueryService;
import com.hocrox.belgian.service.ProfileService;
import com.hocrox.belgian.service.UserJWTService;
import com.hocrox.belgian.service.dto.ProfileCriteria;
import com.hocrox.belgian.service.dto.ProfileDTO;
import com.hocrox.belgian.web.rest.util.HeaderUtil;
import com.hocrox.belgian.web.rest.util.PaginationUtil;
import com.hocrox.belgian.web.rest.vm.LoginVM;
import com.hocrox.belgian.web.rest.vm.OneTimePasswordConfirmVM;
import com.hocrox.belgian.web.rest.vm.ProfileVM;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Profile.
 */
@RestController
@RequestMapping("/api")
public class ProfileResource {

    private final Logger log = LoggerFactory.getLogger(ProfileResource.class);

    private static final String ENTITY_NAME = "profile";

    private final ProfileService profileService;

    private final ProfileQueryService profileQueryService;

    private final UserJWTService userJWTService;

    private final OneTimePasswordService oneTimePasswordService;

    public ProfileResource(ProfileService profileService, ProfileQueryService profileQueryService, UserJWTService userJWTService, OneTimePasswordService oneTimePasswordService) {
        this.profileService = profileService;
        this.profileQueryService = profileQueryService;
        this.userJWTService = userJWTService;
        this.oneTimePasswordService = oneTimePasswordService;
    }

    /**
     * POST  /profiles : Create a new profile.
     *
     * @param profileVM the profileDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new profileDTO, or with status 400 (Bad Request) if the profile has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/user/register")
    @Timed
    public ResponseEntity<JWTToken> createProfile(@Valid @RequestBody ProfileVM profileVM) throws URISyntaxException {
        log.debug("REST request to save Profile : {}", profileVM);
        OneTimePasswordConfirmVM oneTimePasswordConfirmVM = new OneTimePasswordConfirmVM();
        oneTimePasswordConfirmVM.setOtp(profileVM.getPassword());
        oneTimePasswordConfirmVM.setPhoneNumber(profileVM.getPhoneNumber());
        oneTimePasswordService.verifyOTP(oneTimePasswordConfirmVM);

        profileService.create(profileVM);
        LoginVM loginVM = new LoginVM();
        loginVM.setAccountType(AccountType.OTP);
        loginVM.setUsername(profileVM.getPhoneNumber());
        loginVM.setPassword(profileVM.getPassword());
        loginVM.setRememberMe(true);
        JWTToken jwtToken = userJWTService.getSecurityTokenAfterAuthentication(loginVM);
        return new ResponseEntity<>(jwtToken, HttpStatus.CREATED);
    }

    /**
     * PUT  /profiles : Updates an existing profile.
     *
     * @param profileVM the profileVM to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated profileVM,
     * or with status 400 (Bad Request) if the profileVM is not valid,
     * or with status 500 (Internal Server Error) if the profileVM couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/profiles")
    @Timed
    public ResponseEntity<ProfileDTO> updateProfile(@Valid @RequestBody ProfileVM profileVM) throws URISyntaxException {
        log.debug("REST request to update Profile : {}", profileVM);
        ProfileDTO result = profileService.update(profileVM);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, profileVM.getId().toString()))
            .body(result);
    }

    /**
     * GET  /profiles : get all the profiles.
     *
     * @param pageable the pagination information
     * @param criteria the criterias which the requested entities should match
     * @return the ResponseEntity with status 200 (OK) and the list of profiles in body
     */
    @GetMapping("/profiles")
    @Timed
    public ResponseEntity<List<ProfileDTO>> getAllProfiles(ProfileCriteria criteria, Pageable pageable) {
        log.debug("REST request to get Profiles by criteria: {}", criteria);
        Page<ProfileDTO> page = profileQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/profiles");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/profiles/current")
    @Timed
    public ResponseEntity<ProfileDTO> getCurrentProfile() {
        log.debug("REST request to get Profile");
        ProfileDTO profileDTO = profileService.findCurrentDTO();
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(profileDTO));
    }


    /**
     * GET  /profiles/:id : get the "id" profile.
     *
     * @param id the id of the profileDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the profileDTO, or with status 404 (Not Found)
     */
    @GetMapping("/profiles/{id}")
    @Timed
    public ResponseEntity<ProfileDTO> getProfile(@PathVariable Long id) {
        log.debug("REST request to get Profile : {}", id);
        ProfileDTO profileDTO = profileService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(profileDTO));
    }

    /**
     * DELETE  /profiles/:id : delete the "id" profile.
     *
     * @param id the id of the profileDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/profiles/{id}")
    @Timed
    public ResponseEntity<Void> deleteProfile(@PathVariable Long id) {
        log.debug("REST request to delete Profile : {}", id);
        profileService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
