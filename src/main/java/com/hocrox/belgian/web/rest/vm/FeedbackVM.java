package com.hocrox.belgian.web.rest.vm;

import lombok.*;
import org.hibernate.validator.constraints.NotBlank;

/**
 * View Model object for storing a user's credentials.
 */
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class FeedbackVM {

    @NotBlank(message = "Please enter a title for the feedback")
    private String title;

    private String description;

}
