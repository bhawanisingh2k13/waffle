package com.hocrox.belgian.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.hocrox.belgian.config.Constants;
import com.hocrox.belgian.service.UploadService;
import com.hocrox.belgian.service.dto.MediaDTO;
import com.hocrox.belgian.web.rest.errors.InternalServerErrorException;
import lombok.extern.slf4j.Slf4j;
import net.coobird.thumbnailator.Thumbnails;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.FilenameUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import java.awt.image.BufferedImage;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;

/**
 * Created by bhawanisingh on 24/08/16 7:18 PM.
 */
@Slf4j
@Controller
@RequestMapping("/api")
public class MediaController {

    private final UploadService uploadService;

    public MediaController(UploadService uploadService) {
        this.uploadService = uploadService;
    }

    @PostMapping("/upload")
    @Timed
    public ResponseEntity<?> uploadImage(@RequestParam("file") MultipartFile multipartFile) {
        HttpStatus statusCode = HttpStatus.OK;
        if (!multipartFile.isEmpty()) {
            try {
                byte[] bytes = multipartFile.getBytes();
                String originalFilename = System.nanoTime() + "_" + DigestUtils.md5Hex(multipartFile.getOriginalFilename()) + "." + FilenameUtils.getExtension(multipartFile.getOriginalFilename());
                File originalFile = new File(originalFilename);
                File thumbnail = new File(Constants.THUMBNAIL_PREFIX + originalFilename);

                BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(originalFile));
                stream.write(bytes);
                stream.close();
                String uploadedImageName = uploadService.uploadFileToAWS(originalFile);
                Thumbnails.of(originalFile)
                        .outputQuality(0.8)
                        .size(250, 250)
                        .imageType(BufferedImage.TYPE_INT_ARGB)
                        .toFile(thumbnail);
                uploadService.uploadFileToAWS(thumbnail, Constants.AWS_THUMBNAIL);
                MediaDTO mediaDTO = new MediaDTO(uploadService.getObjectPublicUrl(uploadedImageName), uploadedImageName);
                try {
                    originalFile.delete();
                    thumbnail.delete();
                } catch (Exception e){
                    log.debug("Error in deleting temp file : {}", originalFile.getAbsolutePath());
                }
                return new ResponseEntity<>(mediaDTO, statusCode);
            } catch (Exception e) {
                e.printStackTrace();
                throw new InternalServerErrorException("Error in uploading file");
            }
        }
        throw new InternalServerErrorException("Error in uploading file");
    }
}
