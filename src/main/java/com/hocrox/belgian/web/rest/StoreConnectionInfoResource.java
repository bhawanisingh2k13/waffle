package com.hocrox.belgian.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.hocrox.belgian.service.StoreConnectionInfoService;
import com.hocrox.belgian.web.rest.errors.BadRequestAlertException;
import com.hocrox.belgian.web.rest.util.HeaderUtil;
import com.hocrox.belgian.web.rest.util.PaginationUtil;
import com.hocrox.belgian.service.dto.StoreConnectionInfoDTO;
import com.hocrox.belgian.service.dto.StoreConnectionInfoCriteria;
import com.hocrox.belgian.service.StoreConnectionInfoQueryService;
import com.hocrox.belgian.web.rest.vm.StoreConnectionInfoVM;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing StoreConnectionInfo.
 */
@RestController
@RequestMapping("/api")
public class StoreConnectionInfoResource {

    private final Logger log = LoggerFactory.getLogger(StoreConnectionInfoResource.class);

    private static final String ENTITY_NAME = "storeConnectionInfo";

    private final StoreConnectionInfoService storeConnectionInfoService;

    private final StoreConnectionInfoQueryService storeConnectionInfoQueryService;

    public StoreConnectionInfoResource(StoreConnectionInfoService storeConnectionInfoService, StoreConnectionInfoQueryService storeConnectionInfoQueryService) {
        this.storeConnectionInfoService = storeConnectionInfoService;
        this.storeConnectionInfoQueryService = storeConnectionInfoQueryService;
    }

    /**
     * POST  /store-connection-infos : Create a new storeConnectionInfo.
     *
     * @param storeConnectionInfoVM the storeConnectionInfoVM to create
     * @return the ResponseEntity with status 201 (Created) and with body the new storeConnectionInfoVM, or with status 400 (Bad Request) if the storeConnectionInfo has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/store-connection-infos")
    @Timed
    public ResponseEntity<StoreConnectionInfoDTO> createStoreConnectionInfo(@Valid @RequestBody StoreConnectionInfoVM storeConnectionInfoVM) throws URISyntaxException {
        log.debug("REST request to save StoreConnectionInfo : {}", storeConnectionInfoVM);
        if (storeConnectionInfoVM.getId() != null) {
            throw new BadRequestAlertException("A new storeConnectionInfo cannot already have an ID", ENTITY_NAME, "idexists");
        }
        StoreConnectionInfoDTO result = storeConnectionInfoService.save(storeConnectionInfoVM);
        return ResponseEntity.created(new URI("/api/store-connection-infos/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /store-connection-infos : Updates an existing storeConnectionInfo.
     *
     * @param storeConnectionInfoVM the storeConnectionInfoVM to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated storeConnectionInfoVM,
     * or with status 400 (Bad Request) if the storeConnectionInfoVM is not valid,
     * or with status 500 (Internal Server Error) if the storeConnectionInfoVM couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/store-connection-infos")
    @Timed
    public ResponseEntity<StoreConnectionInfoDTO> updateStoreConnectionInfo(@Valid @RequestBody StoreConnectionInfoVM storeConnectionInfoVM) throws URISyntaxException {
        log.debug("REST request to update StoreConnectionInfo : {}", storeConnectionInfoVM);
        if (storeConnectionInfoVM.getId() == null) {
            return createStoreConnectionInfo(storeConnectionInfoVM);
        }
        StoreConnectionInfoDTO result = storeConnectionInfoService.save(storeConnectionInfoVM);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, storeConnectionInfoVM.getId().toString()))
            .body(result);
    }

    /**
     * GET  /store-connection-infos : get all the storeConnectionInfos.
     *
     * @param pageable the pagination information
     * @param criteria the criterias which the requested entities should match
     * @return the ResponseEntity with status 200 (OK) and the list of storeConnectionInfos in body
     */
    @GetMapping("/store-connection-infos")
    @Timed
    public ResponseEntity<List<StoreConnectionInfoDTO>> getAllStoreConnectionInfos(StoreConnectionInfoCriteria criteria, Pageable pageable) {
        log.debug("REST request to get StoreConnectionInfos by criteria: {}", criteria);
        Page<StoreConnectionInfoDTO> page = storeConnectionInfoQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/store-connection-infos");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /store-connection-infos/:id : get the "id" storeConnectionInfo.
     *
     * @param id the id of the storeConnectionInfoDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the storeConnectionInfoDTO, or with status 404 (Not Found)
     */
    @GetMapping("/store-connection-infos/{id}")
    @Timed
    public ResponseEntity<StoreConnectionInfoDTO> getStoreConnectionInfo(@PathVariable Long id) {
        log.debug("REST request to get StoreConnectionInfo : {}", id);
        StoreConnectionInfoDTO storeConnectionInfoDTO = storeConnectionInfoService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(storeConnectionInfoDTO));
    }

    /**
     * DELETE  /store-connection-infos/:id : delete the "id" storeConnectionInfo.
     *
     * @param id the id of the storeConnectionInfoDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/store-connection-infos/{id}")
    @Timed
    public ResponseEntity<Void> deleteStoreConnectionInfo(@PathVariable Long id) {
        log.debug("REST request to delete StoreConnectionInfo : {}", id);
        storeConnectionInfoService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
