package com.hocrox.belgian.web.rest.vm;


import com.hocrox.belgian.domain.enumeration.StoreRoleType;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.validator.constraints.Email;

import javax.persistence.Lob;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * A DTO for the StoreUserProfile entity.
 */
@Slf4j
@Getter
@Setter
@ToString
@NoArgsConstructor
public class StoreUserProfileUpdateVM implements Serializable {

    public static final String VM_NAME = "storeUserProfileVM";

    private Long id;

    @Lob
    private String license;

    private String login;

    @NotNull(message = "Please enter a valid phone number")
    @Pattern(regexp = "^[0-9+]*$", message = "Please enter a valid phone number")
    private String phoneNumber;

    @Size(max = 50, message = "First name must be less than 50 characters")
    private String firstName;

    @Size(max = 50, message = "Last name must be less than 50 characters")
    private String lastName;

    @Email(message = "Please enter a valid email")
    @Size(min = 5, max = 100, message = "Email must be between 5 and 100 characters")
    private String email;

    @Size(max = 256, message = "image name too long")
    private String imageUrl;

    private boolean activated = false;

    @Size(min = 2, max = 6)
    private String langKey = "en_US";

    private Long roleId;

    private StoreRoleType roleType;

}
