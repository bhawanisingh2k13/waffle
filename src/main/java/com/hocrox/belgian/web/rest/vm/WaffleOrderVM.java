package com.hocrox.belgian.web.rest.vm;


import com.hocrox.belgian.domain.enumeration.OrderType;
import com.hocrox.belgian.domain.enumeration.PaymentType;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.Instant;
import java.util.List;

/**
 * A DTO for the WaffleOrder entity.
 */
@Slf4j
@Getter
@Setter
@ToString
@NoArgsConstructor
public class WaffleOrderVM implements Serializable {

    @NotNull(message = "Invalid Cart")
    private String cartToken;

    @NotNull
    private OrderType orderType;

    @NotNull
    private PaymentType paymentType;

    private String couponCode;

    private Long userAddressId;

    private Long pickupLocationId;
}
