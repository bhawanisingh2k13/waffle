package com.hocrox.belgian.web.rest.vm;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

import javax.persistence.Lob;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A DTO for the MenuItem entity.
 */
@Slf4j
@Getter
@Setter
@ToString
@NoArgsConstructor
public class MenuItemVM implements Serializable {

    private Long id;

    @NotNull(message = "Name can't be empty")
    private String name;

    @NotNull(message = "Please enter a valid price")
    private Float price;

    @NotNull(message = "Please choose an image for the menu item")
    @Lob
    private String image;

    @NotNull(message = "Please enter a valid description")
    @Lob
    private String description;

    private Boolean panCake = false;

    private Long categoryId;

    private Set<AddonVM> addons = new HashSet<>();

    private boolean enabled = true;

    private boolean enablePackagingCost = true;

}
