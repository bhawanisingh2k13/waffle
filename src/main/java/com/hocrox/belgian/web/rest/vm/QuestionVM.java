package com.hocrox.belgian.web.rest.vm;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

import javax.persistence.Lob;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * A DTO for the AddonCategory entity.
 */
@Slf4j
@Getter
@Setter
@ToString
@NoArgsConstructor
public class QuestionVM implements Serializable {

    private Long id;

    @NotNull(message = "Please enter a valid question")
    private String title;

    @NotNull(message = "Please enter a valid answer")
    private String answer;

    private boolean enabled = true;
}
