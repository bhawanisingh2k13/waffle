package com.hocrox.belgian.web.rest.vm;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

/**
 * A DTO for the OrderTrack entity.
 */
@Slf4j
@Getter
@Setter
@ToString
@NoArgsConstructor
public class OrderTrackVM implements Serializable {

    private Long id;

    @NotNull(message = "Latitude can't be empty")
    private Double lat;

    @NotNull(message = "Longitude can't be empty")
    private Double lng;

//    private Instant trackTime;

    private boolean deliveryBoy = false;

    private List<Long> waffleOrderIds;
}
