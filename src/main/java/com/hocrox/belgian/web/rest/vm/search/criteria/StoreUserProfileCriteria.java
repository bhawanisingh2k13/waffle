package com.hocrox.belgian.web.rest.vm.search.criteria;

import com.hocrox.belgian.domain.enumeration.StoreRoleType;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

/**
 * Criteria class for the StoreUserProfile entity. This class is used in StoreUserProfileResource to
 * receive all the possible filtering options from the Http GET request parameters.
 * For example the following could be a valid requests:
 * <code> /store-user-profiles?id.greaterThan=5&amp;attr1.contains=something&amp;attr2.specified=false</code>
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
@Getter
@Setter
@ToString
@NoArgsConstructor
public class StoreUserProfileCriteria implements Serializable {
    /**
     * Class for filtering StoreRoleType
     */
    public static class StoreRoleTypeFilter extends Filter<StoreRoleType> {
    }

    private static final long serialVersionUID = 1L;


    private LongFilter id;

    private StringFilter phoneNumber;

    private StoreRoleTypeFilter roleType;

    private LongFilter userId;

    private LongFilter waffleStoreId;

}
