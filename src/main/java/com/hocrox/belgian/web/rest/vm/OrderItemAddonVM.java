package com.hocrox.belgian.web.rest.vm;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

import java.io.Serializable;

/**
 * A DTO for the OrderItemAddon entity.
 */
@Slf4j
@Getter
@Setter
@ToString
@NoArgsConstructor
public class OrderItemAddonVM implements Serializable {

    private Long id;

    private Double price;

    private Double offer;

    private Double finalPrice;

    private Long addonId;

//    private Long orderItemId;
}
