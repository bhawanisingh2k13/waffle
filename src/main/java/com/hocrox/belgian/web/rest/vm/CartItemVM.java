package com.hocrox.belgian.web.rest.vm;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * Created by hocrox on 28/06/16.
 */
@Getter
@Setter
@ToString
@NoArgsConstructor
public class CartItemVM {

    private Long id;

    private String cartToken;

    @NotNull(message = "Invalid Restaurant")
    private long restaurantProfileId;

    @NotNull(message = "Please choose the food item")
    private Long foodItemId;

    private List<CartItemAddonVM> addons;

    private Long itemQuantity = 1L;

//    private String description;
}
