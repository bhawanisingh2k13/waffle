package com.hocrox.belgian.web.rest.vm;

import com.hocrox.belgian.config.Constants;
import com.hocrox.belgian.domain.enumeration.AccountType;
import lombok.*;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

/**
 * View Model object for storing a user's credentials.
 */
@Getter
@Setter
@ToString(exclude = {"password"})
@NoArgsConstructor
@AllArgsConstructor
public class LoginVM {

    @Pattern(regexp = Constants.LOGIN_REGEX, message = "Please enter a valid username")
    @NotNull(message = "Username can't be empty")
    @Size(min = 6, max = 50, message = "Invalid username")
    private String username;

    @NotNull(message = "Please enter the password")
    @Size(min = ManagedUserVM.PASSWORD_MIN_LENGTH, message = "Invalid password")
    private String password;

    private Boolean rememberMe = false;

    private AccountType accountType;

}
