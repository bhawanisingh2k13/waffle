package com.hocrox.belgian.web.rest.vm;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

import javax.persistence.Lob;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * A DTO for the MenuItemCategory entity.
 */
@Slf4j
@Getter
@Setter
@ToString
@NoArgsConstructor
public class MenuItemCategoryVM implements Serializable {

    private Long id;

    @NotNull(message = "Name can't be empty")
    private String name;

    @Lob
    private String image;

    private boolean enabled = true;

    private boolean enablePackagingCost = true;

}
