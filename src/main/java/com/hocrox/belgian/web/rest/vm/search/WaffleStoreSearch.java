package com.hocrox.belgian.web.rest.vm.search;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;


/**
 * Criteria class for the WaffleStore entity. This class is used in WaffleStoreResource to
 * receive all the possible filtering options from the Http GET request parameters.
 * For example the following could be a valid requests:
 * <code> /waffle-stores?id.greaterThan=5&amp;attr1.contains=something&amp;attr2.specified=false</code>
 * As Spring is unable to properly convert the types, unless specific {@link } class are used, we need to use
 * fix type specific filters.
 */
@Getter
@Setter
@NoArgsConstructor
public class WaffleStoreSearch implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;

    private String shortCode;

    private String name;

    private Double lat;

    private Double lng;

    private String street;

    private String city;

    private String state;

    private String country;

    private Long ownerId;
}
