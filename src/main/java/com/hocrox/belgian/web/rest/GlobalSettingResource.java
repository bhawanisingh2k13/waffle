package com.hocrox.belgian.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.hocrox.belgian.service.GlobalSettingService;
import com.hocrox.belgian.service.dto.GlobalSettingDTO;
import com.hocrox.belgian.web.rest.vm.GlobalSettingVM;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URISyntaxException;
import java.util.Optional;

/**
 * REST controller for managing GlobalSetting.
 */
@RestController
@RequestMapping("/api")
public class GlobalSettingResource {

    private final Logger log = LoggerFactory.getLogger(GlobalSettingResource.class);

    private static final String ENTITY_NAME = "globalSetting";

    private final GlobalSettingService globalSettingService;

    public GlobalSettingResource(GlobalSettingService globalSettingService) {
        this.globalSettingService = globalSettingService;
    }

    /**
     * PUT  /global-settings : Updates an existing globalSetting.
     *
     * @param globalSettingVM the globalSettingVM to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated globalSettingVM,
     * or with status 400 (Bad Request) if the globalSettingVM is not valid,
     * or with status 500 (Internal Server Error) if the globalSettingVM couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/global-settings")
    @Timed
    public ResponseEntity<GlobalSettingDTO> updateGlobalSetting(@Valid @RequestBody GlobalSettingVM globalSettingVM) throws URISyntaxException {
        log.debug("REST request to update GlobalSetting : {}", globalSettingVM);
        GlobalSettingDTO result = globalSettingService.save(globalSettingVM);
        return ResponseEntity.ok()
            .body(result);
    }

    /**
     * GET  /global-settings/:id : get the "id" globalSetting.
     *
     * @return the ResponseEntity with status 200 (OK) and with body the globalSettingDTO, or with status 404 (Not Found)
     */
    @GetMapping("/global-settings")
    @Timed
    public ResponseEntity<GlobalSettingDTO> getGlobalSetting() {
        log.debug("REST request to get GlobalSetting");
        GlobalSettingDTO globalSettingDTO = globalSettingService.findOne();
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(globalSettingDTO));
    }
}
