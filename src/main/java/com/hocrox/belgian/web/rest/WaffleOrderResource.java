package com.hocrox.belgian.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.hocrox.belgian.domain.enumeration.OrderStatus;
import com.hocrox.belgian.service.WaffleOrderQueryService;
import com.hocrox.belgian.service.WaffleOrderService;
import com.hocrox.belgian.service.dto.DummyDTO;
import com.hocrox.belgian.service.dto.WaffleOrderDTO;
import com.hocrox.belgian.service.dto.graph.StoreStatsDTO;
import com.hocrox.belgian.web.rest.util.HeaderUtil;
import com.hocrox.belgian.web.rest.util.PaginationUtil;
import com.hocrox.belgian.web.rest.vm.OrderItemVM;
import com.hocrox.belgian.web.rest.vm.WaffleOrderVM;
import com.hocrox.belgian.web.rest.vm.search.criteria.WaffleOrderCriteria;
import io.github.jhipster.web.util.ResponseUtil;
import org.apache.commons.io.IOUtils;
import org.hibernate.validator.constraints.NotBlank;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.time.LocalDate;
import java.time.Period;
import java.time.ZoneId;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * REST controller for managing WaffleOrder.
 */
@RestController
@RequestMapping("/api/waffle-orders")
public class WaffleOrderResource {

    private final Logger log = LoggerFactory.getLogger(WaffleOrderResource.class);

    private static final String ENTITY_NAME = "waffleOrder";

    private final WaffleOrderService waffleOrderService;

    private final WaffleOrderQueryService waffleOrderQueryService;

    public WaffleOrderResource(WaffleOrderService waffleOrderService, WaffleOrderQueryService waffleOrderQueryService) {
        this.waffleOrderService = waffleOrderService;
        this.waffleOrderQueryService = waffleOrderQueryService;
    }

    @PostMapping()
    @Timed
    public ResponseEntity<WaffleOrderDTO> createWaffleOrder(@Valid @RequestBody WaffleOrderVM waffleOrderVM) throws URISyntaxException {
        log.debug("REST request to save WaffleOrder : {}", waffleOrderVM);
        WaffleOrderDTO result = waffleOrderService.save(waffleOrderVM);
        return ResponseEntity.created(new URI("/api/waffle-orders/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    @RequestMapping(value = "/add",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<WaffleOrderDTO> addItem(@Valid @RequestBody OrderItemVM orderItemVM) {
        WaffleOrderDTO result = waffleOrderService.addItem(orderItemVM);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @RequestMapping(value = "/update/{cartToken}/{itemId}/{qty}",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<WaffleOrderDTO> updateItem(@Valid @PathVariable @NotBlank(message = "Invalid cart") String cartToken,
                                                  @Valid @PathVariable @NotNull(message = "Invalid item") Long itemId,
                                                  @Valid @PathVariable @NotNull(message = "Please specify the quantity") Integer qty) {

        WaffleOrderDTO result = waffleOrderService.updateOrder(cartToken, itemId, qty);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @RequestMapping(value = "/remove/{cartToken}/{itemId}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<WaffleOrderDTO> remove(@Valid @PathVariable @NotNull(message = "Invalid cart") String cartToken,
                                              @Valid @PathVariable @NotNull(message = "Invalid item") Long itemId) {
        WaffleOrderDTO result = waffleOrderService.removeItem(cartToken, itemId);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @RequestMapping(value = "/get/cart/{cartToken}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<WaffleOrderDTO> getOrder(@Valid @PathVariable @NotNull(message = "Invalid cart") String cartToken) {
        WaffleOrderDTO orderResDTO = waffleOrderService.getOrder(cartToken);
        return new ResponseEntity<>(orderResDTO, HttpStatus.OK);
    }

    @RequestMapping(value = "/test",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<DummyDTO> getOrder() {
        waffleOrderService.testData();
        return new ResponseEntity<>(new DummyDTO("Shots Fired"), HttpStatus.OK);
    }

    @PutMapping("/update-store-order/{orderId}/{storeOrderNumber}")
    @Timed
    public ResponseEntity<WaffleOrderDTO> updateStoreOrder(@PathVariable Long orderId, @PathVariable String storeOrderNumber) throws URISyntaxException {
        log.debug("REST request to update store order number WaffleOrder : {} - {}", storeOrderNumber, orderId);
        WaffleOrderDTO result = waffleOrderService.updateStoreOrder(orderId, storeOrderNumber);
        return ResponseEntity.ok().body(result);
    }

    @PutMapping("/place-order/{orderId}/{razorpayId}")
    @Timed
    public ResponseEntity<WaffleOrderDTO> placeOrder(@PathVariable Long orderId, @PathVariable String razorpayId) throws URISyntaxException {
        log.debug("REST request to place WaffleOrder : {} - {}", razorpayId, orderId);
        WaffleOrderDTO result = waffleOrderService.placeOrder(orderId, razorpayId);
        return ResponseEntity.ok().body(result);
    }


    @PutMapping("/assign/{deliveryBoyId}/{orderId}")
    @Timed
    public ResponseEntity<WaffleOrderDTO> assignDeliveryBoy(@PathVariable Long deliveryBoyId, @PathVariable Long orderId) throws URISyntaxException {
        log.debug("REST request to update WaffleOrder : {} - {}", deliveryBoyId, orderId);
        WaffleOrderDTO result = waffleOrderService.assignDeliveryBoy(deliveryBoyId, orderId);
        return ResponseEntity.ok().body(result);
    }

    /**
     * GET  /waffle-orders : get all the waffleOrders.
     *
     * @param pageable the pagination information
     * @param criteria the criteria which the requested entities should match
     * @return the ResponseEntity with status 200 (OK) and the list of waffleOrders in body
     */
    @GetMapping()
    @Timed
    public ResponseEntity<List<WaffleOrderDTO>> getAllWaffleOrders(WaffleOrderCriteria criteria, Pageable pageable) {
        log.debug("REST request to get WaffleOrders by criteria: {}", criteria);
        Page<WaffleOrderDTO> page = waffleOrderQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/waffle-orders");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/download/excel")
    @Timed
    public void getExcelWaffleOrders(WaffleOrderCriteria criteria, HttpServletResponse response) throws IOException {
        log.debug("REST request to get WaffleOrders by criteria: {}", criteria);
        File file = waffleOrderService.mailExcel(criteria);
        response.setContentType(MediaType.APPLICATION_OCTET_STREAM_VALUE);
        response.setHeader("Content-Disposition", "filename=\"" + file.getName() + "\"");
        IOUtils.copy(new FileInputStream(file), response.getOutputStream());
        response.flushBuffer();
        file.delete();
    }

    @GetMapping("/today/driver")
    @Timed
    public ResponseEntity<List<WaffleOrderDTO>> getAllDriverTodayWaffleOrders(Pageable pageable) {
        log.debug("REST request to get WaffleOrders of driver");
        Page<WaffleOrderDTO> page = waffleOrderService.findAllByStoreDriver(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/waffle-orders/today/driver");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/driver")
    @Timed
    public ResponseEntity<List<WaffleOrderDTO>> getAllDriverWaffleOrders(Pageable pageable) {
        log.debug("REST request to get WaffleOrders of driver");
        Page<WaffleOrderDTO> page = waffleOrderService.findAllByStoreDriver(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/waffle-orders/driver");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/driver/filter")
    @Timed
    public ResponseEntity<List<WaffleOrderDTO>> getAllDriverWaffleOrdersFilter(@RequestParam(required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate startDate, @RequestParam(required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate endDate, Pageable pageable) {
        log.debug("What the Hellllll : {} - {}", startDate, endDate);
        Page<WaffleOrderDTO> page = waffleOrderQueryService.findByCriteriaDriver(startDate, endDate, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/waffle-orders/driver");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/user")
    @Timed
    public ResponseEntity<List<WaffleOrderDTO>> getAllUserWaffleOrders(Pageable pageable) {
        log.debug("REST request to get WaffleOrders of User");
        Page<WaffleOrderDTO> page = waffleOrderService.findAllByUser(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/waffle-orders/user");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/stats")
    @Timed
    public ResponseEntity<StoreStatsDTO> getWaffleOrdersStats(@RequestParam(value = "date", required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate date) {
        log.debug("REST request to get WaffleOrders stats");
        if(date == null) {
            date = LocalDate.now(ZoneId.of("Asia/Kolkata"));
        }
        StoreStatsDTO storeStats = waffleOrderQueryService.getStats(date);
        return new ResponseEntity<>(storeStats, HttpStatus.OK);
    }

    @GetMapping("/delivery/kms")
    @Timed
    public ResponseEntity<Map<String, String>> getWaffleOrdersDeliveryKms(@RequestParam(value = "date", required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate date) {
        log.debug("REST request to get WaffleOrders delivery kms");
        if(date == null) {
            date = LocalDate.now(ZoneId.of("Asia/Kolkata"));
            date = date.minus(Period.ofDays(6));
        }
        Map<String, String> storeStats = waffleOrderQueryService.getDeliveryKms(date);
        return new ResponseEntity<>(storeStats, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    @Timed
    public ResponseEntity<WaffleOrderDTO> getWaffleOrder(@PathVariable Long id) {
        log.debug("REST request to get WaffleOrder : {}", id);
        WaffleOrderDTO waffleOrderDTO = waffleOrderService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(waffleOrderDTO));
    }


    @DeleteMapping("/{id}")
    @Timed
    public ResponseEntity<Void> deleteWaffleOrder(@PathVariable Long id) {
        log.debug("REST request to delete WaffleOrder : {}", id);
        waffleOrderService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }


    @PostMapping("/status/update/{orderId}/{orderStatus}")
    @Timed
    public ResponseEntity<WaffleOrderDTO> createWaffleOrder(@PathVariable Long orderId, @PathVariable OrderStatus orderStatus) throws URISyntaxException {
        log.debug("REST request to save WaffleOrder : {} - {}", orderId, orderStatus);

        WaffleOrderDTO result = waffleOrderService.updateOrderStatus(orderId, orderStatus);
        return ResponseEntity.ok().body(result);
    }
}
