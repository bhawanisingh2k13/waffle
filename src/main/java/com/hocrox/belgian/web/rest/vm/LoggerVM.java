package com.hocrox.belgian.web.rest.vm;

import ch.qos.logback.classic.Logger;
import lombok.*;

/**
 * View Model object for storing a Logback logger.
 */
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class LoggerVM {

    private String name;

    private String level;

    public LoggerVM(Logger logger) {
        this.name = logger.getName();
        this.level = logger.getEffectiveLevel().toString();
    }
}
