package com.hocrox.belgian.web.rest.vm;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;

/**
 * A DTO for the Profile entity.
 */
@Slf4j
@Getter
@Setter
@ToString
@NoArgsConstructor
public class ProfileVM extends ManagedUserVM implements Serializable {

    private Long id;

    @NotNull(message = "Please enter a valid mobile number")
    @Size(min = 10, max = 10, message = "Please enter a valid 10 digit mobile number")
    private String phoneNumber;

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private Date dateOfBirth;

    private Long userAddressId;

    private UserAddressVM address;

}
