package com.hocrox.belgian.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.hocrox.belgian.security.jwt.JWTConfigurer;
import com.hocrox.belgian.security.jwt.JWTToken;
import com.hocrox.belgian.service.UserJWTService;
import com.hocrox.belgian.web.rest.vm.LoginVM;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

/**
 * Controller to authenticate users.
 */
@RestController
@RequestMapping("/api")
@Slf4j
public class UserJWTController {

    private final UserJWTService userJWTService;

    public UserJWTController(UserJWTService userJWTService) {
        this.userJWTService = userJWTService;
    }

    @PostMapping(value = "/authenticate")
    @Timed
    public ResponseEntity<JWTToken> authorize(@Valid @RequestBody LoginVM loginVM) {

        JWTToken jwtToken = userJWTService.getSecurityTokenAfterAuthentication(loginVM);
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add(JWTConfigurer.AUTHORIZATION_HEADER, "Bearer " + jwtToken.getIdToken());
        return new ResponseEntity<>(jwtToken, httpHeaders, HttpStatus.OK);

    }
}
