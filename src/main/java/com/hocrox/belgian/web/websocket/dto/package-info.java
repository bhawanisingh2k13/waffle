/**
 * Data Access Objects used by WebSocket services.
 */
package com.hocrox.belgian.web.websocket.dto;
