package com.hocrox.belgian.config;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Created by bhawanisingh on 16/12/17 7:55 PM.
 */
@Configuration
@AutoConfigureAfter
public class AwsConfig {

    private final ApplicationProperties applicationProperties;

    public AwsConfig(ApplicationProperties applicationProperties) {
        this.applicationProperties = applicationProperties;
    }

    @Bean
    public AmazonS3 amazonS3() {
        BasicAWSCredentials awsCredentials = new BasicAWSCredentials(applicationProperties.getAws().getAccessKey(), applicationProperties.getAws().getSecretKey());
        return AmazonS3ClientBuilder.standard().withPathStyleAccessEnabled(true)
                .withCredentials(new AWSStaticCredentialsProvider(awsCredentials))
                .withRegion(applicationProperties.getAws().getRegion()).build();

    }
}
