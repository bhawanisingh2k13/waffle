package com.hocrox.belgian.config;



import com.amazonaws.regions.Regions;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * Properties specific to Waffle.
 * <p>
 * Properties are configured in the application.yml file.
 * See {@link io.github.jhipster.config.JHipsterProperties} for a good example.
 */
@ConfigurationProperties(prefix = "application", ignoreUnknownFields = false)
public class ApplicationProperties {

    @Getter
    private Notification notification = new Notification();

    @Getter
    private Aws aws = new Aws();

    @Getter
    private RazorPay razorPay = new RazorPay();

    public static class Notification {
        @Getter
        private final Android android = new Android();

        @Getter
        private final Fcm fcm = new Fcm();

        @Getter
        private final Ios ios = new Ios();

        @Getter
        private final Web web = new Web();

        @Getter
        @Setter
        @ToString
        public static final class Android {
            private String gcmRegId;
            private int retries = 10;

        }

        @Getter
        @Setter
        @ToString
        public static final class Fcm {
            private String url;
            private String apiKey;

        }

        @Getter
        @Setter
        @ToString
        public static final class Ios {
            private String certificateFilename;
            private String certificatePassword;
            private boolean sandbox = true;

        }

        @ToString
        public static final class Web {

            @Getter
            private final Chrome chrome = new Chrome();

            @Getter
            @Setter
            @ToString
            public static final class Chrome {
                private String publicKey;
                private String privateKey;
                private String subject;

            }

        }
    }

    @Getter
    @Setter
    @ToString
    public static final class Aws {
        private String accessKey;
        private String secretKey;
        private Regions region;
        private String s3Bucket;
    }

    @Getter
    @Setter
    @ToString
    public static final class RazorPay {
        private String keyId;
        private String keySecret;
    }
}
