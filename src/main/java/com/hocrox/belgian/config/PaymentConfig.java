package com.hocrox.belgian.config;

import com.razorpay.RazorpayClient;
import com.razorpay.RazorpayException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Created by bhawanisingh on 06/01/18 11:45 AM.
 */
@Configuration
@Slf4j
public class PaymentConfig {

    private final ApplicationProperties applicationProperties;

    public PaymentConfig(ApplicationProperties applicationProperties) {
        this.applicationProperties = applicationProperties;
    }

    @Bean
    public RazorpayClient razorpayClient() throws RazorpayException {
        return new RazorpayClient(applicationProperties.getRazorPay().getKeyId(), applicationProperties.getRazorPay().getKeySecret());
    }
}
