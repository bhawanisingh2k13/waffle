package com.hocrox.belgian.config;

/**
 * Application constants.
 */
public final class Constants {

    // Server Constant
    private static final String BASE_URL = "/api";
    private static final int APP_VERSION = 1;
    public static final String SERVER_URL = BASE_URL + "/v" + APP_VERSION;

    // Regex for acceptable logins
    public static final String LOGIN_REGEX = "^[_'.@A-Za-z0-9-]*$";

    public static final String SYSTEM_ACCOUNT = "system";
    public static final String ANONYMOUS_USER = "anonymoususer";
    public static final String DEFAULT_LANGUAGE = "en";


    // Role Constants
    public static final Long USER_ROLE_ID = 1L;
    public static final Long ADMIN_ROLE_ID = 2L;

    public static final String AWS_THUMBNAIL = "thumb";

    public static final String THUMBNAIL_PREFIX = AWS_THUMBNAIL + "_";
    private Constants() {
    }
}
