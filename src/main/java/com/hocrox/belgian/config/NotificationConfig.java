package com.hocrox.belgian.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.hocrox.belgian.messaging.Notification;
import com.hocrox.belgian.messaging.WebNotification;
import com.hocrox.belgian.messaging.android.AndroidNotification;
import com.hocrox.belgian.messaging.android.FcmNotification;
import de.bytefish.fcmjava.client.FcmClient;
import de.bytefish.fcmjava.http.client.IFcmClient;
import de.bytefish.fcmjava.http.options.IFcmClientSettings;
import lombok.extern.slf4j.Slf4j;
import nl.martijndwars.webpush.PushService;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.Security;
import java.security.spec.InvalidKeySpecException;

/**
 * Created by bhawanisingh on 17/09/15.
 */
@Configuration
@Slf4j
public class NotificationConfig {

    private final ApplicationProperties applicationProperties;

    private final ObjectMapper objectMapper;

    public NotificationConfig(ApplicationProperties applicationProperties, ObjectMapper objectMapper) {
        this.applicationProperties = applicationProperties;
        this.objectMapper = objectMapper;
    }

    @Bean
    public IFcmClient fcmClient() {
        return new FcmClient(new IFcmClientSettings() {
            @Override
            public String getFcmUrl() {
                return applicationProperties.getNotification().getFcm().getUrl();
            }

            @Override
            public String getApiKey() {
                return applicationProperties.getNotification().getFcm().getApiKey();
            }
        });
    }

    @Bean
    public FcmNotification fcmNotification(){
        return new FcmNotification(applicationProperties.getNotification().getFcm().getUrl(), applicationProperties.getNotification().getFcm().getApiKey(), fcmClient(), objectMapper);
    }

    @Bean
    public AndroidNotification androidNotification() {
        return new AndroidNotification(applicationProperties.getNotification().getAndroid().getGcmRegId(), applicationProperties.getNotification().getAndroid().getRetries());
    }

    @Bean
    public PushService pushService() throws NoSuchAlgorithmException, NoSuchProviderException, InvalidKeySpecException {
        Security.addProvider(new BouncyCastleProvider());
        log.debug("Push Service");
        return new PushService()
                .setPublicKey(applicationProperties.getNotification().getWeb().getChrome().getPublicKey())
                .setPrivateKey(applicationProperties.getNotification().getWeb().getChrome().getPrivateKey())
                .setSubject(applicationProperties.getNotification().getWeb().getChrome().getSubject());
    }

    @Bean
    public WebNotification webNotification() throws NoSuchAlgorithmException, NoSuchProviderException, InvalidKeySpecException {
        return new WebNotification(pushService(), objectMapper);
    }
//    @Bean
//    public ApnsService apnsService() throws IOException {
//        ApnsService service = null;
//        InputStream certStream = new ClassPathResource(relaxedPropertyResolver.getProperty("apple.certificateFilename")).getInputStream();// this.getClass().getClassLoader().getResourceAsStream("your_certificate.p12");
////        service = APNS.newService().withCert(certStream, relaxedPropertyResolver.getProperty("apple.certificatePassword")).withSandboxDestination().build();
//        service = APNS.newService().withCert(certStream, relaxedPropertyResolver.getProperty("apple.certificatePassword")).withProductionDestination().build();
//        service.start();
//        return service;
//    }

//    @Bean
//    public AppleNotification appleNotification() throws IOException {
//        return new AppleNotification(apnsService());
//    }

    @Bean
    public Notification notification() {
        return new Notification();
    }
}
