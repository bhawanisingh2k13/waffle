package com.hocrox.belgian.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.simp.stomp.StompHeaderAccessor;
import org.springframework.messaging.support.ChannelInterceptorAdapter;

/**
 * Created by hocrox on 27/07/18.
 */

@Slf4j
public class PresenceChannelInterceptor extends ChannelInterceptorAdapter {

    @Override
    public void postSend(Message<?> message, MessageChannel channel, boolean sent) {

        StompHeaderAccessor sha = StompHeaderAccessor.wrap(message);

        // ignore non-STOMP messages like heartbeat messages
        if(sha.getCommand() == null) {
            return;
        }

        String sessionId = sha.getSessionId();
        String username = "cool"; //(String) sha.getSessionAttributes().get("username");

        sha.getSessionAttributes().forEach((key, value) -> log.debug(key + ":" + value));

        switch(sha.getCommand()) {
            case CONNECT:
                log.debug("STOMP Connect [sessionId: " + sessionId  + " : " + username + " : " + sha.getLogin() + "]");
                break;
            case CONNECTED:
                log.debug("STOMP Connected [sessionId: " + sessionId  + " : " + username + " : " + sha.getLogin() + "]");
                break;
            case DISCONNECT:
                log.debug("STOMP Disconnect [sessionId: " + sessionId  + " : " + username + " : " + sha.getLogin() + "]");
                break;
            default:
                break;

        }
    }
}
